
${AnsiColor.51}${AnsiStyle.BOLD}${AnsiStyle.ITALIC}
    /$$$$$$  /$$      /$$ /$$$$$$$  /$$       /$$$$$$$$
   /$$__  $$| $$$    /$$$| $$__  $$| $$      | $$_____/
  | $$  \ $$| $$$$  /$$$$| $$  \ $$| $$      | $$
  | $$$$$$$$| $$ $$/$$ $$| $$$$$$$/| $$      | $$$$$
  | $$__  $$| $$  $$$| $$| $$____/ | $$      | $$__/
  | $$  | $$| $$\  $ | $$| $$      | $$      | $$
  | $$  | $$| $$ \/  | $$| $$      | $$$$$$$$| $$$$$$$$
  |__/  |__/|__/     |__/|__/      |________/|________/
  ${AnsiColor.226}
  :: Ample Editor ::                  ${application.formatted-version}${AnsiColor.40}
  :: Spring Boot  ::                  ${spring-boot.formatted-version}${AnsiColor.DEFAULT}${AnsiStyle.NORMAL}