/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.persistence;

import eu.asclepios.ample.persistence.annotations.Id;
import eu.asclepios.ample.persistence.annotations.RdfPredicate;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.URLEncoder;
import java.util.Collection;
import java.util.Map;

public class PersistentField {
	Field field;
	Class type;
	String name;
	String fieldUri;
	boolean appendName;
	boolean persistable;
	boolean isId;
	boolean hasGetter;
	boolean hasSetter;
	boolean isPublic;
	Method getter;
	Method setter;
	boolean isArray;
	boolean isMap;
	boolean isCollection;
	boolean isSingleValue;
	int arrayDimensions;
	Class arrayComponentType;
	boolean cascadeRefresh;
	boolean cascadeUpdate;
	boolean cascadeDelete;
	boolean isLiteral;
	boolean isReference;
	boolean isUri;
	boolean dontSerialize;
	String lang;
	boolean omitIfNull;
	
	public PersistentField(Field f, String typeUri) throws UnsupportedEncodingException {
		// Check if field is persistable
		RdfPredicate ann = (RdfPredicate)f.getAnnotation(RdfPredicate.class);
		persistable = (ann!=null);
		if (!persistable) return;		// Don't throw exception. PersistentClass will omit it from further processing
		
		// Initialize field information
		this.field = f;
		this.type = f.getType();
		this.name = ann.name().trim();
		if (this.name.isEmpty()) this.name = f.getName();
		this.appendName = ann.appendName();
		this.fieldUri = ann.uri().trim();
		if (this.fieldUri.isEmpty()) {
			String ns = ann.namespace().trim();
			if (ns.isEmpty()) ns = typeUri.trim();
			if (appendName) this.fieldUri = ((ns.endsWith("/") || ns.endsWith("#")) ? ns : ns+"/" ) + URLEncoder.encode(this.name,"UTF-8");
			else this.fieldUri = ns;
		}
		String capName=this.name.length()>1 ? ( Character.toUpperCase(this.name.charAt(0)) + this.name.substring(1) ) : this.name.toUpperCase();
		
		// Check if field is array, Collection or Map
		isArray = type.isArray();
		isMap = Map.class.isAssignableFrom(type);
		isCollection = Collection.class.isAssignableFrom(type);
		isSingleValue = !(isArray || isMap || isCollection);
		
		if (isArray) {
			// get array dimension and component type
			Class typ = type;
			Class compType = null;
			int dim = 0;
			while ((typ=typ.getComponentType())!=null) { compType = typ; dim++; }
			arrayComponentType = compType;
			arrayDimensions = dim;
		} else
		if (isMap) {
			//throw new RdfPersistenceException("PersistentField: CURRENTLY NOT SUPPORTS MAP FIELDS");
		} else
		if (isCollection) {
			//throw new RdfPersistenceException("PersistentField: CURRENTLY NOT SUPPORTS COLLECTION FIELDS");
		} else
		{	// Not an array, Map or Collection
			
			// Check if field is 'Id'
			Id ann2 = (Id)f.getAnnotation(Id.class);
			isId = (ann2!=null);
		}
		
		// Find getter and setter methods, if available
		String getterName = ann.getter().trim();
		String setterName = ann.setter().trim();
		if (getterName.isEmpty()) getterName = "get"+capName;
		if (setterName.isEmpty()) setterName = "set"+capName;
		
		Class declClass = f.getDeclaringClass();
		try { getter = declClass.getMethod(getterName); } 
		catch (NoSuchMethodException e) {
			if (type.equals(Boolean.class) || type.equals(Boolean.TYPE)) {	// for boolean fields try again for an 'is___' getter
				getterName = "is"+capName;
				try { getter = declClass.getMethod(getterName); } catch (NoSuchMethodException e2) { }
			}
		}
		try { setter = declClass.getMethod(setterName, this.type); } catch (NoSuchMethodException e) { }
		hasGetter = (getter!=null);
		hasSetter = (setter!=null);
		
		// Check if public field
		isPublic = (f.getModifiers() & Modifier.PUBLIC)!=0;
		
		// Check that field can be accessed and modified
		if (!(hasGetter && hasSetter || isPublic)) {
			throw new RdfPersistenceException("Persistent field does not have getter/setter methods and is not public: Class="+declClass.getName()+", field: "+name);
		}
		
		// Get cascade flags
		cascadeRefresh = ann.refresh().trim().equalsIgnoreCase("cascade");
		cascadeUpdate  = ann.update().trim().equalsIgnoreCase("cascade");
		cascadeDelete  = ann.delete().trim().equalsIgnoreCase("cascade");
		
		// Check if it is a Literal value or a Reference field
		this.isLiteral = RdfPersistenceManagerImpl.isLiteralType(this.type);
		this.isReference = ! this.isLiteral;
		
		// Check if it is a URI
		this.isUri = ann.isUri();
		// Check dontSerialize flag
		this.dontSerialize = ann.dontSerialize();
		// Check omitIfNull flag
		this.omitIfNull = ann.omitIfNull();
		
		// Get lang
		this.lang = ann.lang();
	}
	
	public String toString() {
		return "PersistentField: type="+type+", name="+name+", type-uri="+fieldUri;
	}
}