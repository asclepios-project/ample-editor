/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.persistence;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

public class PersistentObject {
	Object object;
	String objectUri;
	Map<String,String> data;	// persistent state data
	boolean isPersisted;
	
	public PersistentObject(Object o, Map<String,String> data) throws IllegalAccessException, InvocationTargetException {
		this.object = o;
		this.objectUri = RdfPersistenceManagerImpl._getObjectUri(o);
		this.data = data;
		this.isPersisted = (data!=null && data.size()>0);
	}
	
	public PersistentObject(String oUri, Map<String,String> data) throws IllegalAccessException, InvocationTargetException {
		this.object = null;
		this.objectUri = oUri;
		this.data = data;
		this.isPersisted = (data!=null && data.size()>0);
	}
	
	public String getFieldXsdValue(PersistentField pf) {
		if (!isPersisted) return null;
		if (!data.containsKey(pf.fieldUri)) // and not 'ignorable'
			throw new RdfPersistenceException( String.format("Missing field from objects persistent state: field name=%s, field uri=%s, object uri=%s", pf.name, pf.fieldUri, objectUri) );
		return data.get( pf.fieldUri );
	}
	
	public String getFieldXsdValue(String fieldUri) {
		if (!isPersisted) return null;
		if (!data.containsKey(fieldUri)) // and not 'ignorable'
			throw new RdfPersistenceException( String.format("Missing field from objects persistent state: field uri=%s, object uri=%s", fieldUri, objectUri) );
		return data.get( fieldUri );
	}
}
