/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.persistence;

import eu.asclepios.ample.persistence.annotations.Id;
import eu.asclepios.ample.persistence.annotations.RdfSubject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

public class PersistentClass {
	private static final Logger logger = LoggerFactory.getLogger( (new Object() { }.getClass().getEnclosingClass()).getName() );
	
	Class type;
	String name;
	String typeUri;				// Used to build object instance URIs
	String rdfType;				// Used in generateObjectHeaders and findAll
	boolean appendName;
	boolean suppressRdfType;
	boolean suppressJavaType;
	String registerWithRdfType;
	PersistentField idField;
	String idFormatter;
	Pattern idPattern;
	HashSet<PersistentField> fields;
	
	public PersistentClass(Class c) throws UnsupportedEncodingException, ClassNotFoundException {
		// traverseObjectGraph should take care to avoid pass array, Map or Collection objects to this constructor
		if (c.isArray() || Map.class.isAssignableFrom(c) || Collection.class.isAssignableFrom(c)) {
			throw new RdfPersistenceException("Cannot analyze 'arrays' or objects of 'Map' or 'Collection' type");
		}
		
		// Check if class is persistable
		RdfSubject ann = (RdfSubject)c.getAnnotation(RdfSubject.class);
		if (ann==null) throw new RdfPersistenceException("Object type is not persistable: "+c);
		
		// Initialize field information
		this.type = c;
		this.name = ann.name().trim();
		if (this.name.isEmpty()) this.name = c.getName();
		this.appendName = ann.appendName();
		this.typeUri = ann.uri().trim();
		if (this.typeUri.isEmpty()) {
			String ns = ann.namespace().trim();
			if (ns.isEmpty()) ns = RdfPersistenceManagerImpl.defaultTypesPrefix;
			if (appendName) this.typeUri = ((ns.endsWith("/") || ns.endsWith("#")) ? ns : ns+"/" ) + URLEncoder.encode(this.name,"UTF-8");
			else this.typeUri = ns;
		}
		
		// Initialize rdf type (if specified)
		this.rdfType = null;
		if (ann.rdfType()!=null && !ann.rdfType().trim().isEmpty()) {
			this.rdfType = ann.rdfType().trim();
		}
		logger.trace("PersistentClass.<init>: rdf-type={}", rdfType);
		
		// Initialize suppress rdf & java type flags
		this.suppressRdfType = ann.suppressRdfType();
		this.suppressJavaType = ann.suppressJavaType();
		
		// Register this class with specified RDF type (if specified)
		this.registerWithRdfType = null;
		if (ann.registerWithRdfType()!=null && !ann.registerWithRdfType().trim().isEmpty()) {
			this.registerWithRdfType = ann.registerWithRdfType().trim();
			RdfPersistenceManagerImpl.registerTypeWithRdfClass(this, this.registerWithRdfType);
		}
		
		// Process class fields. Ignores non-persistable fields
		initializePersistentFields(false);		// don't suppress id-formatter and id-pattern preparation
	}
	
	protected void initializePersistentFields(boolean suppressIdFormatterAndPattern) throws UnsupportedEncodingException {
		// Process class fields. Ignores non-persistable fields
		this.idField = null;
		this.fields = new HashSet<PersistentField>();
		for (Field f : _getInheritedFields(this.type)) {
			// Process fields and create descriptor
			logger.trace("PersistentClass.<init>: {}: processing field: {}", this.type.getName(), f.getName());
			PersistentField pf = new PersistentField(f, this.typeUri);
			if (!pf.persistable) continue;		// ignore non-persistable fields
			
			// Check if 'id' field
			if (pf.isId && idField==null) {	// first occurence. Ok
				idField = pf;
				if (!suppressIdFormatterAndPattern) {
					// prepare id-formatter
					String sep1 = (typeUri.endsWith("#")) ? "" : "#";
					Id idAnn = pf.field.getAnnotation(Id.class);
					this.idFormatter = idAnn.formatter();
					if (idFormatter==null || idFormatter.trim().isEmpty()) {
						idFormatter = typeUri+sep1+"%s";
					} else {
						idFormatter = idFormatter.replaceAll("%URI%", typeUri).replaceAll("%HASH%", sep1);
					}
					// prepare id-pattern
					String sep2 = (typeUri.endsWith("#")) ? "" : "\\#";
					String patStr = idAnn.pattern();
					if (patStr==null || patStr.trim().isEmpty()) {
						patStr = "^"+typeUri.replace("\\","\\\\")+sep2+"([^\\#]+)$";
					} else {
						patStr = patStr.replaceAll("%URI%", typeUri).replaceAll("%HASH%", sep2);
					}
					this.idPattern = Pattern.compile(patStr);
				}
			} else
			if (pf.isId && idField!=null) {	// second occurence. Error!
				throw new RdfPersistenceException("Object type contains multiple fields annotated as '@Id'. Only one 'if' field is allowed: class="+type.getName()+", id-fields="+idField.field.getName()+","+pf.field.getName());
			}
			
			// store persistent field
			fields.add(pf);
		}
		if (idField==null) {	// check that one field is annotated as 'id'
			throw new RdfPersistenceException("Object type does not have 'id' field annotated with '@Id': class="+type.getName());
		}
	}
	
	public Set<Field> _getInheritedFields(Class startClass) {
		HashSet<Field> set = new HashSet<Field>();
		Class clss = startClass;
		while (clss!=null) {
			for (Field f : clss.getDeclaredFields()) {
				set.add(f);
			}
			clss = clss.getSuperclass();
		}
		return set;
	}
	
	public PersistentField getFieldFromUri(String fUri, String lang) {
		logger.trace("getFieldFromUri: BEGIN: Looking field descriptor for field uri={}", fUri);
		PersistentField result = null;
		try {
			for (PersistentField pf : fields) {
				result = pf;
				logger.trace("getFieldFromUri: ITERATION: checking with field descriptor: {}", pf.fieldUri);
				logger.trace("getFieldFromUri: \tis-array={}", pf.isArray);
				if (pf.isArray && fUri.startsWith(pf.fieldUri+RdfPersistenceManagerImpl.asep)) return pf;
				logger.trace("getFieldFromUri: \tis-collection={}", pf.isCollection);
				if (pf.isCollection && fUri.startsWith(pf.fieldUri+RdfPersistenceManagerImpl.csep)) return pf;
				logger.trace("getFieldFromUri: \tis-map={}", pf.isMap);
				//logger.trace("getFieldFromUri: \tIT IS MAP: fUri={}, pf.field-uri={}, msep={}", fUri, pf.fieldUri, RdfPersistenceManagerImpl.msep);
				if (pf.isMap && fUri.startsWith(pf.fieldUri+RdfPersistenceManagerImpl.msep)) return pf;
				
				boolean langMatch = (lang==null || (lang=lang.trim()).isEmpty()) ?
										( true ) : 
										( pf.lang.isEmpty() ? "en".equalsIgnoreCase(lang) : pf.lang.equalsIgnoreCase(lang) );
				logger.trace("getFieldFromUri: arg-lang={}, pf-lang={}, match={}", lang, pf.lang, langMatch);
				if (logger.isTraceEnabled()) logger.trace("getFieldFromUri: \tchecking for exact match: {}", fUri.equals(pf.fieldUri) && langMatch);
				if (fUri.equals(pf.fieldUri) && langMatch) return pf;
				logger.trace("getFieldFromUri: \tNo match");
				result = null;
			}
		} finally {
			if (result==null) logger.trace("getFieldFromUri: END: result={}", result);
		}
		logger.trace("getFieldFromUri: END: field value NOT FOUND: field-uri={}", fUri);
		return null;
	}
	
	public PersistentField getFieldByName(String fieldName) {
		logger.trace("getFieldByName: BEGIN: Looking field descriptor for field uri={}", fieldName);
		PersistentField result = null;
		try {
			for (PersistentField pf : fields) {
				result = pf;
				logger.trace("getFieldByName: ITERATION: checking with field descriptor: {}", pf.name);
				/*logger.trace("getFieldByName: \tis-array={}", pf.isArray);
				if (pf.isArray && fUri.startsWith(pf.name+RdfPersistenceManagerImpl.asep)) return pf;
				logger.trace("getFieldByName: \tis-collection={}", pf.isCollection);
				if (pf.isCollection && fUri.startsWith(pf.fieldUri+RdfPersistenceManagerImpl.csep)) return pf;
				logger.trace("getFieldByName: \tis-map={}", pf.isMap);
				//logger.trace("getFieldByName: \tIT IS MAP: fUri={}, pf.field-uri={}, msep={}", fUri, pf.fieldUri, RdfPersistenceManagerImpl.msep);
				if (pf.isMap && fUri.startsWith(pf.fieldUri+RdfPersistenceManagerImpl.msep)) return pf;*/
				if (logger.isTraceEnabled()) logger.trace("getFieldByName: \tchecking for exact match: {}", fieldName.equals(pf.name));
				if (fieldName.equals(pf.name)) return pf;
				logger.trace("getFieldByName: \tNo match");
				result = null;
			}
		} finally {
			if (result==null) logger.trace("getFieldByName: END: result={}", result);
		}
		logger.trace("getFieldByName: END: field value NOT FOUND: field-uri={}", fieldName);
		return null;
	}
	
	public String toString() {
		return "PersistentClass: type="+type+", name="+name+", type-uri="+typeUri+", rdf-type="+rdfType;
	}
}
