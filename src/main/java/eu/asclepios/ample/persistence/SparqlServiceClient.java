/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.persistence;

import org.apache.jena.query.QueryExecution;
import org.apache.jena.rdf.model.RDFNode;

import java.util.List;
import java.util.Map;

public interface SparqlServiceClient {
	String getSelectEndpoint();
	String getUpdateEndpoint();

	default void beginTrans(boolean readWrite) {}
	default void closeTrans() {}

	boolean ask(String askQuery);
	void execute(String sparqlUpdate);
	QueryExecution query(String selectQuery);
	
	Map<String,String> queryBySubject(String subjectUri);
	List<Map<String,RDFNode>> queryAndProcess(String selectQuery);
	List<String> queryForIds(String selectQuery, String idCol);
	Object queryValue(String selectQuery);
}