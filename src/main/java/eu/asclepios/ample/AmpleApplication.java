/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample;

import eu.asclepios.ample.util.SecretsUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

import java.util.Properties;

@SpringBootApplication(
        scanBasePackages = { "eu.asclepios.ample" },
        exclude = { SecurityAutoConfiguration.class, UserDetailsServiceAutoConfiguration.class } )
@EnableAsync
@Configuration
@Slf4j
public class AmpleApplication {
    private AmpleApplicationProperties properties;

    public static void main(String[] args) {
        Properties secretProperties = SecretsUtil.processSecretsArguments(args);
        SpringApplication springApplication = new SpringApplication(AmpleApplication.class);
        springApplication.setBannerMode(Banner.Mode.LOG);
        springApplication.addListeners(new ApplicationPidFileWriter("./logs/ample.pid"));
        if (secretProperties!=null) springApplication.setDefaultProperties(secretProperties);
        ConfigurableApplicationContext applicationContext = springApplication.run(args);
    }
}