/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.conf;

import eu.asclepios.ample.AmpleApplicationProperties;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.csrf.CsrfTokenRepository;

@Slf4j
@Configuration
@EnableWebSecurity
//@EnableGlobalMethodSecurity(prePostEnabled = false, securedEnabled = false, jsr250Enabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Autowired
    private AmpleApplicationProperties properties;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    };

    @SneakyThrows
    @Bean
    public UserDetailsService userDetailsService() {
        return new PropertiesUserDetailsService();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService()).passwordEncoder(passwordEncoder());
    }

    @Bean
    public CsrfTokenRepository csrfTokenRepository() {
        return CookieCsrfTokenRepository.withHttpOnlyFalse();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
            // endpoints always permitted
                .antMatchers( "/log*.jsp", "/forms/deps/**", "/forms/slick/**", "/images/**")
                .permitAll()
            // endpoints permitted only from localhost
                .antMatchers( "/opt/**")
                .hasIpAddress(properties.getBackend().getIpFilter())
            // endpoints requiring authentication
                //.antMatchers("/forms/admin/*.jsp").authenticated()
                .anyRequest().authenticated()
                .and()
            // permit login, logout and login error redirects
                .formLogin()
                    .loginPage("/login")
                    .failureUrl("/loginError")
                    .permitAll()
                .and()
                .logout()
                    .logoutSuccessUrl("/login")
                    .permitAll()

            // configure CORS and CSRF
                /*.and()
                    .cors()*/
                .and()
                    .csrf()
                    .csrfTokenRepository(csrfTokenRepository())
                .and()

            // require HTTPS
                .requiresChannel()
                    .anyRequest().requiresSecure()
                .and()

            // Security headers
                .headers()
                    .defaultsDisabled()
                    // HSTS header
                    .httpStrictTransportSecurity()
                        .includeSubDomains(true)
                        .maxAgeInSeconds(31536000)
                        .preload(true)
                .and()
                    // Content Type options header
                    .contentTypeOptions()
                .and()
                    // X-Frame-Options header
                    .frameOptions()
                        .sameOrigin()
                    // X-XSS-Protection header
                    .xssProtection()
                        //.block(false)

            // Cache control
                .and()
                    .cacheControl()
        ;
    }

    public class PropertiesUserDetailsService implements UserDetailsService {
        @Override
        public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
            AmpleApplicationProperties.User user = properties.getUsers().stream()
                    .filter(u -> u.getUsername().equals(username))
                    .findFirst()
                    .orElse(null);
            log.debug("PropertiesUserDetailsService.loadUserByUsername: {}", user);
            if (user==null || user.getPassword()==null)
                throw new UsernameNotFoundException("User ["+username+"] not exists or invalid password");
            String[] roles = user.getRoles().stream().map(Enum::name).toArray(String[]::new);
            return User
                    .withUsername(username)
                    .password(user.getPassword())
                    .authorities(roles)
                    .build();
        }
    }
}