/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.conf;

import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.Context;
import org.apache.catalina.startup.Tomcat;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.embedded.tomcat.TomcatWebServer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;

@Slf4j
@Configuration
public class ServerConfiguration {
    // See: https://www.vojtechruzicka.com/spring-boot-add-war-to-embedded-tomcat/
    @Bean
    @ConditionalOnProperty(name = "ample.server.external.war.file")
    public TomcatServletWebServerFactory servletContainerFactory(@Value("${ample.server.external.war.file}") String path,
                                                                 @Value("${ample.server.external.war.context}") String contextPath) {
        log.info("ServerConfiguration: Initializing Tomcat server...");
        return new TomcatServletWebServerFactory() {

            @Override
            protected TomcatWebServer getTomcatWebServer(Tomcat tomcat) {
                // create 'webapps' directory
                File webappsDir = new File(tomcat.getServer().getCatalinaBase(), "webapps");
                log.debug("ServerConfiguration: Tomcat 'webapps' directory: {}", webappsDir);
                boolean created = webappsDir.mkdirs();
                log.debug("ServerConfiguration: Creating 'webapps' directory... {}", created ? "Created" : "FAILED to create");

                // add WAR file
                log.info("ServerConfiguration: Adding external WAR: context-path={}, path-to-war={}", contextPath, path);
                Context context = tomcat.addWebapp(contextPath, path);
                //context.setParentClassLoader(getClass().getClassLoader());
                log.debug("ServerConfiguration: External WAR added");

                return super.getTomcatWebServer(tomcat);
            }

        };
    }
}