/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.gui;

import eu.asclepios.ample.AmpleApplicationProperties;
import eu.asclepios.ample.model.AbstractObject;
import eu.asclepios.ample.model.ITreeNodeSpec;
import eu.asclepios.ample.model.casm.CasmObject;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.function.Function;

@Slf4j
@RestController
public class AbstractGuiController implements InitializingBean {

    public final static MediaType TEXT_TURTLE = new MediaType("text","turtle");

    @Autowired
    protected AmpleApplicationProperties properties;
    @Autowired
    private CsrfTokenRepository csrfTokenRepository;
    @Autowired
    private RestTemplate restTemplate;

    private HttpHeaders headers;

    @Override
    public void afterPropertiesSet() {
        headers = createHeaders();
    }

    private HttpHeaders createHeaders() {
        HttpHeaders headers = new HttpHeaders();
        if (properties!=null &&
            properties.getFrontend()!=null &&
            properties.getFrontend().getConnection()!=null &&
            properties.getFrontend().getConnection().getKeepAlive()!=null)
        {
            AmpleApplicationProperties.Frontend.Connection.KeepAlive keepAlive = properties.getFrontend().getConnection().getKeepAlive();
            if (keepAlive.isEnable()) {
                headers.add("Connection", "Keep-Alive");
                headers.add("Keep-Alive", "timeout=" + keepAlive.getTimeout() + ", max=" + keepAlive.getMax());
            }
        }
        return headers;
    }

    protected ResponseEntity<String> createResponse(int statusCode, String mesg, Object...args) {
        mesg = String.format(mesg, args);
        return new ResponseEntity<String>(mesg, headers, HttpStatus.resolve(statusCode));
    }

    protected <T> ResponseEntity<T> createResponse(int statusCode, T entity) {
        return new ResponseEntity<T>(entity, headers, HttpStatus.resolve(statusCode));
    }

    protected <T> ResponseEntity<T> callService(String path, HttpMethod httpMethod, Object requestBody, MediaType mimeType, Class<T> responseType) {
        ResponseEntity<T> responseEntity =
                properties.getFrontend().isUseRestService()
                        ? callServiceRest(path, httpMethod, requestBody, mimeType, responseType)
                        : callServiceProgrammatic(path, requestBody, responseType);
        return responseEntity;
    }

    protected <T> ResponseEntity<T> callServiceRest(String path, HttpMethod httpMethod, Object requestBody, MediaType mimeType, Class<T> responseType) {
        // prepare request URL
        String url = StringUtils.startsWithIgnoreCase(path, "http")
                ? path
                : properties.getFrontend().getBackendServiceUrl() + path;
        log.debug("callServiceRest(): effective URL: {}", url);

        // prepare request headers
        HttpHeaders headers = new HttpHeaders();
        if (mimeType!=null)
            headers.setContentType(mimeType);

        // set CSRF token
        /*HttpServletRequest originalRequest = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
        log.trace("callServiceRest(): original-request: {} {}", originalRequest.getMethod(), originalRequest.getRequestURI());
        CsrfToken csrfToken = new HttpSessionCsrfTokenRepository().loadToken(originalRequest);
        log.trace("callServiceRest(): csrf-token: {} = {}", csrfToken.getHeaderName(), csrfToken.getToken());
        */
        CsrfToken csrfToken = csrfTokenRepository.generateToken(null);
        if (csrfToken!=null) {
            headers.add(csrfToken.getHeaderName(), csrfToken.getToken());
            headers.add("Cookie", "XSRF-TOKEN=" + csrfToken.getToken());
        }

        // prepare request entity
        HttpEntity<Object> entity = new HttpEntity<>(requestBody, headers);

        // send request
        log.debug("callServiceRest(): Sending request: {} {} --> response-type: {}", httpMethod, url, responseType);
        log.trace("callServiceRest(): Sending request: {} {} --> response-type: {}\nentity: {}", httpMethod, url, responseType, entity);
        ResponseEntity<T> response = restTemplate.exchange(url, httpMethod, entity, responseType);
        log.debug("callServiceRest(): Response: status={}, mime-type={}", response.getStatusCode(), response.getHeaders().getContentType());

        return response;
    }

    protected <T> ResponseEntity<T> callServiceProgrammatic(String path, Object requestBody, Class<T> responseType) {
        return null;
    }

    protected String createDownloadFileName(String baseName, String ext, String id) {
        // create download filename
        java.text.DateFormat df = new java.text.SimpleDateFormat("yyyy.MM.dd-HH.mm.ss.SSS");
        String nowAsISO = df.format(new Date());
        return String.format("%s-%s%s%s.%s", baseName, id, (StringUtils.isNotBlank(id)?"-":""), nowAsISO, ext);
        //return String.format("%s-%s.%s", baseName, nowAsISO, ext);
    }

    protected ResponseEntity<byte[]> _sendFileForDownload(MediaType contentType, String responseContents, String fileName) {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentLength(responseContents.length());
        responseHeaders.setContentType(contentType);
        responseHeaders.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        responseHeaders.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName);
        ResponseEntity<byte[]> fileResponse = new ResponseEntity<>(responseContents.getBytes(), responseHeaders, HttpStatus.OK);

        log.debug("-------------- _sendFileForDownload: content-length={}", responseContents.length());
        log.trace("-------------- _sendFileForDownload: content-length={}\n{}", responseContents.length(), responseContents);
        return fileResponse;
    }

    protected ResponseEntity<byte[]> _sendTtlFileForDownload(String contents, String baseName, String id, String extenstion) {
        // create download filename
        String fileName = createDownloadFileName(baseName, extenstion, id);

        // send file for download
        return _sendFileForDownload(TEXT_TURTLE, contents, fileName);
    }

    @Data
    @Builder
    protected static class JstreeJsonNode {
        private final String id;
        private final String text;
        private final String type;
        private final String icon;
        private final boolean children;
        private final boolean inAbac;
        private final boolean inAbe;
        private final boolean readonly;
    }

    protected <T extends AbstractObject>List<JstreeJsonNode> createResponseForJstree(
            T[] array,
            String filter,
            Function<String, ITreeNodeSpec> typeResolver)
    {
        return createResponseForJstree(array, filter, typeResolver, null);
    }

    protected <T extends AbstractObject>List<JstreeJsonNode> createResponseForJstree(
            T[] array,
            String filter,
            Function<String, ITreeNodeSpec> typeResolver,
            Function<T, String> iconResolver)
    {
        // Filter & Sort items
        List<T> list;
        if (array!=null) {
            list = Arrays.asList(array);
            list = filterList(list, filter, typeResolver);
            sortList(list);
        } else
            list = Collections.emptyList();

        // Prepare JSON to return to page
        List<JstreeJsonNode> response = new ArrayList<>();
        for (T item : list) {
            if (item == null) continue;
            ITreeNodeSpec itemType = typeResolver.apply(item.getType());
            String iconPath = iconResolver!=null ? iconResolver.apply(item) : null;
            if (StringUtils.isBlank(iconPath)) iconPath = itemType.getDefaultIcon();
            boolean inAbac = (item instanceof CasmObject) && ((CasmObject) item).isInAbac();
            boolean inAbe = (item instanceof CasmObject) && ((CasmObject) item).isInAbe();
            boolean readonly = false;
            try { readonly = Boolean.parseBoolean(item.getClass().getDeclaredMethod("isReadonly").invoke(item).toString()); } catch (Exception ignored) { }
            JstreeJsonNode node = JstreeJsonNode.builder()
                    .id(item.getId())
                    .text(item.getName())
                    .type(item.getType())
                    .icon(iconPath)
                    .children(itemType.canHaveChildren())
                    .inAbac(inAbac)
                    .inAbe(inAbe)
                    .readonly(readonly)
                    .build();
            response.add(node);
        }

        return response;
    }

    protected <T extends AbstractObject>List<T> filterList(List<T> inputList, String filter, Function<String, ITreeNodeSpec> typeChecker) {
        List<T> filteredList = inputList;
        if (StringUtils.isNotBlank(filter) && !filter.trim().equals("*")) {
            // parse filter
            Set<String> allowedTypes = new HashSet<>();
            String[] part = filter.trim().split(",");
            for (String s : part) {
                s = s.trim();
                if (!s.isEmpty()) {
                    try {
                        if (typeChecker.apply(s)!=null)
                            allowedTypes.add(s);
                    } catch (Exception ignored) { }
                }
            }

            // process inputList
            filteredList = new ArrayList<>();
            for (T item : inputList) {
                String type = item.getType();
                if (allowedTypes.contains(type)) {
                    filteredList.add(item);
                }
            }
        }
        return filteredList;
    }

    private static final Comparator<AbstractObject> comparator = (lhs, rhs) -> {
        // return 1 if rhs should be before lhs
        // return -1 if lhs should be before rhs
        // return 0 otherwise
        String lhsType = lhs.getType();
        String rhsType = rhs.getType();
        int lhsPos = lhs.getPosition();
        int rhsPos = rhs.getPosition();
        String lhsName = lhs.getName();
        String rhsName = rhs.getName();
        int r = lhsType.compareTo(rhsType);
        if (r != 0) return r;
        if (lhsPos != rhsPos) return lhsPos<rhsPos ? -1 : +1;
        return lhsName.compareTo(rhsName);
    };

    protected <T extends AbstractObject> void sortList(List<T> inputList) {
        inputList.sort(comparator);
    }
}