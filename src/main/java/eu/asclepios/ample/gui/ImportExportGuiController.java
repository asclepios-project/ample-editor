/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.gui;

import eu.asclepios.ample.persistence.SparqlServiceClientFactory;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.jena.rdf.model.RDFNode;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.security.RolesAllowed;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Scanner;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Slf4j
@RestController
@RequestMapping("/gui/admin")
@RolesAllowed({"ADMIN"})
public class ImportExportGuiController extends AbstractGuiController {

    public final static MediaType TEXT_TURTLE = new MediaType("text","turtle");

    @RequestMapping(value = "/clearRdf", method = GET)
    public ResponseEntity<String> clearRdf() {
        log.info("-------------- clearRdf: BEGIN: n/a");

        // if all parameters are ok upload file to RDF repository
        String rdfReposUrl = properties.getFrontend().getTriplestoreClearUrl();
        log.debug("clearRdf: Clearing RDF repository with URL: {}", rdfReposUrl);
        ResponseEntity<Object> response = callServiceRest(rdfReposUrl, HttpMethod.POST, "DELETE WHERE { ?s ?p ?o }", new MediaType("application", "sparql-update"), null);
        int status = response.getStatusCodeValue();
        log.debug("Response: {}", status);

        if (status>=200 && status<300) {
            log.info("-------------- clearRdf: END: RDF repository cleared: status={}", status);
            return createResponse(status, String.format("{ \"message\": \"RDF repository cleared: status=%d\" }", status));
        }

        log.info("-------------- clearRdf: ERROR: File upload failed: status={}", status);
        return createResponse(status, String.format("{ \"message\": \"RDF repository clear FAILED: See logs for details: status=%d\" }", status));
    }

    @RequestMapping(value = "/exportRdf", method = GET)
    public ResponseEntity<byte[]> exportRdf() {
        log.info("-------------- exportRdf: BEGIN: n/a");

        // Build triplestore query URL
        String url = properties.getFrontend().getTriplestoreExportUrl();
        log.info("exportRdf: Querying export service: {}", url);
        //ResponseEntity<String> response = callServiceRest(url, HttpMethod.GET, null, null, String.class);
        ResponseEntity<String> response = callServiceRest(url, HttpMethod.GET, null, null, String.class);
        int status = response.getStatusCodeValue();
        log.debug("exportRdf: Response: status: {} {}, mime-type: {}",
                status, response.getStatusCode().getReasonPhrase(), response.getHeaders().getContentType());

        String str = "";
        if (status>=200 && status<300) {
            str = response.getBody();
            if (str==null) str = "";
        }

        // create download filename timestamp
        java.text.DateFormat df = new java.text.SimpleDateFormat("yyyy.MM.dd-HH.mm.ss.SSS");
        String nowAsISO = df.format(new Date());

        String fileName = String.format("ample-export-%s.ttl", nowAsISO);
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentLength(str.length());
        responseHeaders.setContentType(TEXT_TURTLE);
        responseHeaders.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        responseHeaders.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName);
        ResponseEntity<byte[]> fileResponse = new ResponseEntity<byte[]>(str.getBytes(), responseHeaders, HttpStatus.OK);

        log.debug("-------------- exportRdf: END: content-length={}", str.length());
        log.trace("-------------- exportRdf: END: content-length={}\n{}", str.length(), str);
        return fileResponse;
    }

    @RequestMapping(value = "/importRdf", method = POST)
    public ResponseEntity<String> importRdf(@RequestParam("import_mode") String importMode, @RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) {
        log.info("-------------- importRdf: INPUT: mode={}, file={}, mime-type={}",
                importMode, file.getOriginalFilename(), file.getContentType());

        // check import mode
        HttpMethod method = null;
        if (StringUtils.isNotBlank(importMode)) {
            importMode = importMode.trim().toLowerCase();
            if ("append".equalsIgnoreCase(importMode)) method = HttpMethod.POST;
            else if ("replace".equalsIgnoreCase(importMode)) method = HttpMethod.PUT;
        }
        if (method == null) {
            return createErrorResponse(400, "importRdf: Invalid or missing import mode: " + importMode);
        }
        log.debug("importRdf: import mode={}", importMode);

        // check file contents
        if (file.isEmpty()) {
            return createErrorResponse(400, "importRdf: File is empty");
        }
        String contents = null;
        try {
            // Get the file contents
            byte[] bytes = file.getBytes();
            log.debug("importRdf: content length: {}", bytes.length);
            contents = new String(bytes, StandardCharsets.UTF_8);
            log.debug("importRdf: File successfully uploaded: {}", file.getOriginalFilename());
            log.trace("importRdf: file-contents:\n{}", contents);

        } catch (IOException e) {
            return createErrorResponse(400, "importRdf: Error while reading file: " + e.getMessage());
        }
        if (StringUtils.isBlank(contents)) {
            return createErrorResponse(400, "importRdf: File is empty: " + file.getOriginalFilename());
        }

        // if everything is ok upload file to RDF repository
        ResponseEntity<String> response = uploadToTriplestore(method, contents);
        log.info("-------------- importRdf: END: status={}", response.getStatusCodeValue());
        return response;
    }

    @RequestMapping(value = "/initializeModels", method = {GET, POST})
    public ResponseEntity<String> initializeModels() throws IOException {
        log.info("-------------- initializeModels: BEGIN: ");

        // Get model initialization file contents
        StringBuilder contents = new StringBuilder();
        for (String modelFile : properties.getFrontend().getModelInitializationFiles()) {
            // Get file contents
            log.debug("initializeModels: file={}", modelFile);
            contents.append( new Scanner(getClass().getResourceAsStream(modelFile)).useDelimiter("\\Z").next() );
            log.trace("initializeModels: file-contents:\n{}", contents);
            if (StringUtils.isBlank(contents)) {
                return createErrorResponse(400, "initializeModels: FAILED: Missing or empty Model initialization file: " + modelFile);
            }
        }

        // if all parameters are ok send file to RDF repository
        ResponseEntity<String> response = uploadToTriplestore(HttpMethod.PUT, contents.toString());
        int status = response.getStatusCodeValue();
        log.info("initializeModels: END: status={}", status);

        return response;
    }

    protected ResponseEntity<String> uploadToTriplestore(HttpMethod method, String contents) {
        String rdfReposUrl = properties.getFrontend().getTriplestoreImportUrl();
        log.debug("uploadToTriplestore: BEGIN: Upload to RDF repository: action={} {}", method, rdfReposUrl);
        log.trace("uploadToTriplestore: Upload to RDF repository: action={} {}, contents:\n{}", method, rdfReposUrl, contents);
        ResponseEntity<Object> response = callServiceRest(rdfReposUrl, method, contents, TEXT_TURTLE, null);

        int status = response.getStatusCodeValue();
        log.debug("uploadToTriplestore: Upload to RDF repository: SPARQL server response: status={}", status);

        if (status>=200 && status<300) {
            log.info("uploadToTriplestore: END: File upload completed: status={}", status);
            return createResponse(200, String.format("{ \"message\": \"File upload completed: status=%d\" }", status));
        }

        log.info("uploadToTriplestore: ERROR: File upload failed: status={}", status);
        return createResponse(status, String.format("{ \"message\": \"File upload failed: See logs for details: status=%d\" }", status) );
    }

    private ResponseEntity<String> createErrorResponse(int status, String message) {
        log.error(message);
        return createResponse(status, String.format("{ \"message\": \"%s\" }", message));
    }

    @EventListener(ApplicationReadyEvent.class)
    public void checkInitializeModelsOnStartup() throws Exception {
        if (! properties.getFrontend().isModelInitializationOnStartup()) return;

        log.info("checkInitializeModelsOnStartup: Checking if RDF repository is empty");
        RDFNode result = (RDFNode) SparqlServiceClientFactory.getClientInstance().queryValue("SELECT (COUNT(*) AS ?count) WHERE { ?s ?p ?o }");
        int count = result.asLiteral().getInt();
        log.debug("checkInitializeModelsOnStartup: Checking if RDF repository is empty: count={}", count);

        if (count==0) {
            log.info("checkInitializeModelsOnStartup: RDF repository is empty. Will initialize it");
            initializeModels();
        } else {
            log.debug("checkInitializeModelsOnStartup: RDF repository is NOT empty. Will not initialize it");
        }
    }
}