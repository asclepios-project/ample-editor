/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.gui;

import eu.asclepios.ample.conf.ROLE;
import eu.asclepios.ample.model.validation.POLICY_VALIDATION_TYPE;
import eu.asclepios.ample.model.validation.PolicyValidationObject;
import eu.asclepios.ample.validation.PolicyValidationResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

import static eu.asclepios.ample.model.validation.POLICY_VALIDATION_TYPE.*;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Slf4j
@RestController
@RequestMapping("/gui/validation")
@RolesAllowed({ROLE.ROLE_ADMIN, ROLE.ROLE_ABAC})
public class PolicyValidationGuiController extends AbstractGuiController {

    private final static Map<String,Map<String,String>> GROUP_ICON_MAP = new HashMap<String,Map<String,String>>() {{
        put("POLICY-INSPECTION", new HashMap<String,String>() {{
            put(POLICY_VALIDATION_GROUP_NAME, "/images/validation/validation-group-inspection.png");
            put(POLICY_VALIDATION_SET_NAME, "/images/validation/validation-set-inspection.png");
            put(POLICY_VALIDATION_RULE_NAME, "/images/validation/validation-rule-inspection.png");
        }});
        put("SECURITY-AWARENESS", new HashMap<String,String>() {{
            put(POLICY_VALIDATION_GROUP_NAME, "/images/validation/validation-group-awareness.png");
            put(POLICY_VALIDATION_SET_NAME, "/images/validation/validation-set-awareness.png");
            put(POLICY_VALIDATION_RULE_NAME, "/images/validation/validation-rule-awareness.png");
        }});
    }};

    // ------------------------------------------------------------------------
    // Policy Validation Object methods
    // ------------------------------------------------------------------------

    @RequestMapping(value = "/", method = GET, produces = APPLICATION_JSON_VALUE)
    public List<JstreeJsonNode> getTopLevelObjects() {
        log.debug("-------------- getTopLevelObjects: INPUT: n/a");

        // Call REST service in order to get attribute list
        PolicyValidationObject[] objectArr = callService("/opt/validation/", HttpMethod.GET, null, null, PolicyValidationObject[].class).getBody();

        // Prepare response to return to page (filter & sort items, and build response)
        List<JstreeJsonNode> response = createResponseForJstree(
                objectArr, null/*"POLICY-VALIDATION-GROUP"*/, POLICY_VALIDATION_TYPE::valueByName,
                group -> GROUP_ICON_MAP.get(group.getId()).get(POLICY_VALIDATION_GROUP_NAME));

        log.debug("-------------- getTopLevelObjects: OUTPUT: {}", response);
        return response;
    }

    @RequestMapping(value = "/{id}", method = GET, produces = APPLICATION_JSON_VALUE)
    public PolicyValidationObject getPolicyValidationObject(@PathVariable("id") String id) {
        log.info("-------------- getPolicyValidationObject: INPUT: id={}", id);

        // Call REST service in order to get object's data
        PolicyValidationObject objectOut = callService("/opt/validation/" + id, HttpMethod.GET, null, null, PolicyValidationObject.class).getBody();

        log.info("-------------- getPolicyValidationObject: OUTPUT: {}", objectOut);
        return objectOut;
    }

    @RequestMapping(value = "/get-children/{id}", method = GET, produces = APPLICATION_JSON_VALUE)
    public List<JstreeJsonNode> getChildren(@PathVariable("id") String id, @RequestParam(name = "filter", defaultValue = "*", required = false) String filter) {
        log.info("-------------- getChildren: INPUT: id={}, filter={}", id, filter);

        // Call REST service in order to get attribute list
        PolicyValidationObject[] objectArr = callService("/opt/validation/" + id + "/children", HttpMethod.GET, null, null, PolicyValidationObject[].class).getBody();

        // Prepare response to return to page (filter & sort items, and build response)
        List<JstreeJsonNode> response = createResponseForJstree(
                objectArr, filter, POLICY_VALIDATION_TYPE::valueByName,
                object -> {
                    PolicyValidationObject o = object;
                    while (o!=null && o.getParent()!=null) o = o.getParent();
                    return GROUP_ICON_MAP.get(o.getId()).get(object.getType());
                });

        log.info("-------------- getChildren: OUTPUT: {}", response);
        return response;
    }

    @RequestMapping(value = "/save-object", method = POST, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public PolicyValidationObject savePolicyValidationObject(@RequestBody PolicyValidationObject setIn) {
        log.info("-------------- savePolicyValidationObject: INPUT: {}", setIn);
        setIn.setLastUpdateTimestamp(new Date());

        // Call REST service for update
        String id = setIn.getId();
        String responseMessage = callService("/opt/validation/" + id, HttpMethod.POST, setIn, APPLICATION_JSON, String.class).getBody();
        log.debug("-------------- savePolicyValidationObject: Response message: {}", responseMessage);

        PolicyValidationObject setOut = setIn;
        log.info("-------------- savePolicyValidationObject: OUTPUT: {}", setOut);
        return setOut;
    }

    @RequestMapping(value = { "/delete-object/{id}", "/delete-object/{id}/cascade" }, method = GET)
    public ResponseEntity<String> deletePolicyValidationObject(HttpServletRequest request, @PathVariable("id") String id) {
        log.info("-------------- deletePolicyValidationObject: INPUT: id={}, request={}", id, request);

        // Check if cascade
        boolean cascade = true;     // delete is always cascading
        //boolean cascade = StringUtils.endsWithIgnoreCase(request.getRequestURI(), "/cascade");
        String cascadeSufix = cascade ? "/all" : "";

        // Call REST service to delete attribute
        String responseMessage = callService("/opt/validation/"+id+cascadeSufix, HttpMethod.DELETE, null, null, String.class).getBody();
        log.debug("-------------- deletePolicyValidationObject: Response message: {}", responseMessage);

        return createResponse(200, "Policy Validation Object deleted "+id);
    }

    @RequestMapping(value = "/create-object", method = POST, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<String> createPolicyValidationObject(@RequestBody PolicyValidationObject setIn) {
        log.info("-------------- createPolicyValidationObject: INPUT: {}", setIn);

        setIn.setLastUpdateTimestamp(new Date());
        String id = setIn.getId();
        if (StringUtils.isBlank(id)) {
            // generate a new Id
            id = "policy-validation-object-"+UUID.randomUUID();
            setIn.setId(id);
            log.debug("-------------- creacreatePolicyValidationObject: New policy validation object Id:  {}", id);
        }

        // Set creation date
        setIn.setCreateTimestamp(new Date());

        // Call REST service for update
        ResponseEntity<String> response = callService("/opt/validation/", HttpMethod.PUT, setIn, APPLICATION_JSON, String.class);
        log.debug("-------------- createPolicyValidationObject: Response message: {}", response.getBody());

        log.info("-------------- createPolicyValidationObject: OUTPUT: n/a");
        return createResponse(200, String.format("{ \"response\":  \"%d\", \"entity\": \"%s\" }", response.getStatusCodeValue(), response.getBody()));
    }

    @RequestMapping(value = "/{id}/reorder-children", method = POST, consumes = APPLICATION_JSON_VALUE)
    public String reorderPolicyValidationObjectChildren(@PathVariable("id") String id, @RequestBody String childrenIds) {
        log.info("-------------- reorderPolicyValidationObjectChildren: INPUT: id={}, children-ids={}", id, childrenIds);

        // Call REST service in order to store policy rule positions
        ResponseEntity<String> response = callService("/opt/validation/" + id + "/reorder-children", HttpMethod.POST, childrenIds, APPLICATION_JSON, String.class);
        log.debug("-------------- reorderPolicyValidationObjectChildren: Response: {}", response);

        log.info("-------------- reorderPolicyValidationObjectChildren: OUTPUT: {}", response.getBody());
        return response.getBody();
    }

    // ------------------------------------------------------------------------
    // Policy Validation export methods
    // ------------------------------------------------------------------------

    @RequestMapping(value = "/export-validation-set-as-rdf/{id}", method = GET)
    public ResponseEntity<byte[]> exportPolicyValidationObjectAsRdf(@PathVariable("id") String id) {
        log.info("-------------- exportPolicyValidationObjectAsRdf: BEGIN: id={}", id);

        // check if 'id' is valid (prevent sparql injection attacks)
        if (! id.matches("^[0-9A-Za-z-]+$")) {
            throw new IllegalArgumentException("Invalid 'id': "+id);
        }

        // Call REST service for update
        String url = "/opt/validation/export-as-rdf/"+id;
        ResponseEntity<String> response = callService(url, HttpMethod.GET, null, null, String.class);
        log.trace("-------------- exportPolicyValidationObjectAsRdf: Exported RDF: {}", response.getBody());

        // get response content
        String responseContents = "";
        if (response.getStatusCode().is2xxSuccessful()) {
            responseContents = response.getBody();
            if (responseContents == null) responseContents = "";
        }

        // send TTL file for download
        return _sendTtlFileForDownload(responseContents, "policy-validation", id, "ttl");
    }

    // ------------------------------------------------------------------------
    // Policy Validation methods
    // ------------------------------------------------------------------------

    @RequestMapping(value = "/validate-policies-with/{id}", method = GET, produces = APPLICATION_JSON_VALUE)
    public List<PolicyValidationResult> validatePoliciesWith(@PathVariable("id") String id) {
        log.info("-------------- validatePoliciesWith: INPUT: id={}", id);

        // Call REST service in order to get validation results
        List<PolicyValidationResult> results = (List<PolicyValidationResult>) callService("/opt/validation/validate-policies-with/" + id, HttpMethod.GET, null, null, ArrayList.class).getBody();

        log.info("-------------- validatePoliciesWith: OUTPUT: {}", results);
        return results;
    }

    @RequestMapping(value = "/validate-policy/{id}", method = GET, produces = APPLICATION_JSON_VALUE)
    public List<PolicyValidationResult> validateAbacOrAbePolicy(@PathVariable("id") String id) {
        log.info("-------------- validateAbacOrAbePolicy: INPUT: id={}", id);

        // Call REST service in order to get validation results
        List<PolicyValidationResult> results = (List<PolicyValidationResult>) callService("/opt/validation/validate/policy/" + id, HttpMethod.GET, null, null, ArrayList.class).getBody();

        log.info("-------------- validateAbacOrAbePolicy: OUTPUT: {}", results);
        return results;
    }
}
