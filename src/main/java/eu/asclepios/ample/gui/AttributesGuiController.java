/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.gui;

import eu.asclepios.ample.conf.ROLE;
import eu.asclepios.ample.model.casm.CASM_TYPE;
import eu.asclepios.ample.model.casm.CasmObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Slf4j
@RestController
@RequestMapping("/gui/attributes")
@RolesAllowed({ROLE.ROLE_ADMIN, ROLE.ROLE_CASM})
public class AttributesGuiController extends AbstractGuiController {

    @RequestMapping(value = "/get-toplevel-attributes", method = {GET, POST}, produces = APPLICATION_JSON_VALUE)
    public List<JstreeJsonNode> getTopLevelAttributes(@RequestParam(name = "filter", defaultValue = "*", required = false) String filter) {
        log.debug("-------------- getTopLevelAttributes: INPUT: filter={}", filter);

        // Call REST service in order to get attribute list
        CasmObject[] atArr = callService("/opt/attributes/", HttpMethod.GET, null, null, CasmObject[].class).getBody();

        // Prepare response to return to page (filter & sort items, and build response)
        List<JstreeJsonNode> response = createResponseForJstree(atArr, filter, CASM_TYPE::valueByName);

        log.debug("-------------- getTopLevelAttributes: OUTPUT: {}", response);
        return response;
    }

    @RequestMapping(value = "/get-attribute-children/{attr_id}", method = {GET, POST}, produces = APPLICATION_JSON_VALUE)
    public List<JstreeJsonNode> getAttributeChildren(@PathVariable("attr_id") String id, @RequestParam(name = "filter", defaultValue = "*", required = false) String filter) {
        log.info("-------------- getAttributeChildren: INPUT: attr-id={}, filter={}", id, filter);

        // Call REST service in order to get attribute list
        CasmObject[] atArr = callService("/opt/attributes/" + id + "/subattributes", HttpMethod.GET, null, null, CasmObject[].class).getBody();

        // Prepare response to return to page (filter & sort items, and build response)
        List<JstreeJsonNode> response = createResponseForJstree(atArr, filter, CASM_TYPE::valueByName);

        log.info("-------------- getAttributeChildren: OUTPUT: {}", response);
        return response;
    }

    @RequestMapping(value = "/get-attribute/{attr_id}", method = {GET, POST}, produces = APPLICATION_JSON_VALUE)
    public CasmObject getAttribute(@PathVariable("attr_id") String id) {
        log.info("-------------- getAttribute: INPUT: attr-id={}", id);

        // Call REST service in order to get attribute's data'
        CasmObject atOut = callService("/opt/attributes/" + id, HttpMethod.GET, null, null, CasmObject.class).getBody();

        // If an OBJECT PROPERTY, retrieve range concept's name
        if (atOut != null && atOut.getType() != null && atOut.getType().equals("PROPERTY") && atOut.getPropertyType() != null && atOut.getPropertyType().equals("ObjectProperty")) {
            String range = atOut.getRange();
            if (range != null) {
                CasmObject oaRange = callService("/opt/attributes/" + range, HttpMethod.GET, null, null, CasmObject.class).getBody();
                if (oaRange != null && oaRange.getName() != null)
                    atOut.setRange_display(oaRange.getName());
            } else {
                log.error("-------------- getAttribute: NON-FATAL ERROR: Missing 'range' value for ObjectProperty: id={}", id);
            }
        }

        // If a PROPERTY-INSTANCE, retrieve referenced property's name
        if (atOut != null && atOut.getType() != null && atOut.getType().equals("PROPERTY-INSTANCE")) {
            String propIsA = atOut.getPropertyIsA();
            if (propIsA != null) {
                CasmObject oaPropIsA = callService("/opt/attributes/" + propIsA, HttpMethod.GET, null, null, CasmObject.class).getBody();
                if (oaPropIsA != null && oaPropIsA.getName() != null)
                    atOut.setPropertyIsA_display(oaPropIsA.getName());
            } else {
                log.error("-------------- getAttribute: NON-FATAL ERROR: Missing 'propertyIsA' value for PropertyInstance: id={}", id);
            }
        }

        log.info("-------------- getAttribute: OUTPUT: {}", atOut);
        return atOut;
    }

    @RequestMapping(value = "/save-attribute", method = POST, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public CasmObject saveAttribute(@RequestBody CasmObject atIn) {
        log.info("-------------- saveAttribute: INPUT: {}", atIn);
        atIn.setLastUpdateTimestamp(new Date());

        // Call REST service for update
        String id = atIn.getId();
        String responseMessage = callService("/opt/attributes/" + id, HttpMethod.POST, atIn, APPLICATION_JSON, String.class).getBody();
        log.debug("-------------- saveAttribute: Response message: {}", responseMessage);

        CasmObject atOut = atIn;
        log.info("-------------- saveAttribute: OUTPUT: {}", atOut);
        return atOut;
    }

    @RequestMapping(value = { "/delete-attribute/{attr_id}", "/delete-attribute/{attr_id}/cascade" }, method = GET)
    public ResponseEntity<String> deleteAttribute(HttpServletRequest request, @PathVariable("attr_id") String id) {
        log.info("-------------- deleteAttribute: INPUT: id={}, request={}", id, request);

        // Check if cascade
        boolean cascade = true;     // delete is always cascading
        //boolean cascade = StringUtils.endsWithIgnoreCase(request.getRequestURI(), "/cascade");
        String cascadeSufix = cascade ? "/all" : "";

        // Call REST service to delete attribute
        String responseMessage = callService("/opt/attributes/"+id+cascadeSufix, HttpMethod.DELETE, null, null, String.class).getBody();
        log.debug("-------------- deleteAttribute: Response message: {}", responseMessage);

        return createResponse(200, "Attribute deleted "+id);
    }

    @RequestMapping(value = "/create-attribute", method = POST, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<String> createAttribute(@RequestBody CasmObject atIn) {
        log.info("-------------- createAttribute: INPUT: {}", atIn);

        atIn.setLastUpdateTimestamp(new Date());
        String id = atIn.getId();
        if (StringUtils.isBlank(id)) {
            // generate a new Id
            id = "attr-"+UUID.randomUUID();
            atIn.setId(id);
            log.debug("New attribute Id:  {}", id);
        }

        // Set parent
        CasmObject parent = atIn.getParent();
        if (parent!=null) {
            atIn.setParent(parent);
            log.debug("Parent set to:  {}", parent.getId());
        }

        // Set creation date
        atIn.setCreateTimestamp(new Date());

        // Call REST service for update
        ResponseEntity<String> response = callService("/opt/attributes/", HttpMethod.PUT, atIn, APPLICATION_JSON, String.class);
        log.debug("-------------- createAttribute: Response message: {}", response.getBody());

        log.info("-------------- createAttribute: OUTPUT: n/a");
        return createResponse(200, String.format("{ \"response\":  \"%d\", \"entity\": \"%s\" }", response.getStatusCodeValue(), response.getBody()));
    }

    @RequestMapping(value = "/{attr_id}/reorder-children", method = POST, consumes = APPLICATION_JSON_VALUE)
    public String reorderChildren(@PathVariable("attr_id") String id, @RequestBody String childrenIds) {
        log.info("-------------- reorderChildren: INPUT: attr-id={}, children-ids={}", id, childrenIds);

        // Call REST service in order to store children positions
        ResponseEntity<String> response = callService("/opt/attributes/" + id + "/reorder-children", HttpMethod.POST, childrenIds, APPLICATION_JSON, String.class);
        log.debug("-------------- reorderChildren: Response: {}", response);

        log.info("-------------- reorderChildren: OUTPUT: {}", response.getBody());
        return response.getBody();
    }

    // ---------------------------------------------------------------------------
    // Methods for setting/clearing the in-ABAC and in-ABE flags
    // ---------------------------------------------------------------------------

    @RequestMapping(value = "/{attr_id}/{operation}/use-in/{policy_type}", method = GET, produces = APPLICATION_JSON_VALUE)
    public String setUseInPolicies(
            @PathVariable("attr_id") String id,
            @PathVariable("operation") CasmObject.POLICY_USE operation,
            @PathVariable("policy_type") CasmObject.POLICY_TYPE policyType)
    {
        log.info("-------------- setUseInPolicies: INPUT: attr-id={}, operation={}, policy-type={}", id, operation, policyType);

        // Call REST service for update
        ResponseEntity<String> response = callService(
                String.format("/opt/attributes/%s/%s/use-in/%s", id, operation, policyType),
                HttpMethod.GET, null, null, String.class);
        log.debug("-------------- setUseInPolicies: Response status: {}", response.getStatusCodeValue());

        log.info("-------------- setUseInPolicies: OUTPUT: {}", response.getBody());
        return String.format("{ \"status\": \"%d\", \"message\": \"%s\" }", response.getStatusCodeValue(), response.getBody());
    }

    // ---------------------------------------------------------------------------
    // Methods providing the endpoints used by expression builder (select2) lists
    // ---------------------------------------------------------------------------

    @RequestMapping(value = "/search{in_scope}/attributes", method = GET, produces = APPLICATION_JSON_VALUE)
    public String searchForAttributes(@PathVariable("in_scope") String inScope, @RequestParam String term, @RequestParam String q, @RequestParam String _type, @RequestParam(required = false) Integer page) throws UnsupportedEncodingException {
        log.info("-------------- searchForAttributes: INPUT: scope={}, term={}, q={}, type={}, page={}", inScope, term, q, _type, page);

        // Call REST service for update
        ResponseEntity<CasmObject[]> response = callService(
                "/opt/attributes/search"+inScope+"/by-name/" + URLEncoder.encode(term, StandardCharsets.UTF_8.name()),
                HttpMethod.GET, null, null, CasmObject[].class);
        log.debug("-------------- searchForAttributes: Response status: {}", response.getStatusCodeValue());

        // Cast results list to List<CasmObject>
        CasmObject[] arr = response.getBody();
        List<CasmObject> results = arr!=null ? Arrays.asList(arr) : new ArrayList<>();
        log.debug("-------------- searchForAttributes: Results count: {}", results.size());

        // Sort results by name
        results.sort((o1, o2) -> o1.getName().compareToIgnoreCase(o2.getName()));

        // Prepare the searchForAttributes response message
        StringBuilder sb = new StringBuilder();
        sb.append("{ \"results\": [ ");
        boolean first = true;
        for (CasmObject attr : results) {
            if (first) first=false; else sb.append(", ");
            sb.append(String.format("{ \"id\": \"%s\", \"text\": \"%s\" }", attr.getId(), attr.getName()));
        }
        sb.append(" ] }");
        String jsonStr = sb.toString();

        log.info("-------------- searchForAttributes: OUTPUT: {}", jsonStr);
        return jsonStr;
    }

    @RequestMapping(value = "/search{in_scope}/properties", method = GET, produces = APPLICATION_JSON_VALUE)
    public Map<String,Object> searchForProperties(@PathVariable("in_scope") String inScope, @RequestParam(name = "attributeId") String attrId) throws UnsupportedEncodingException {
        log.info("-------------- searchForProperties: INPUT: in-scope={}, attr-id={}", inScope, attrId);

        // Call REST service for update
        ResponseEntity<CasmObject[]> response = callService(
                "/opt/attributes/search"+inScope+"/properties/by-attribute/" + URLEncoder.encode(attrId, StandardCharsets.UTF_8.name()),
                HttpMethod.GET, null, null, CasmObject[].class);
        log.debug("-------------- searchForProperties: Response status: {}", response.getStatusCodeValue());

        // Cast results list to List<CasmObject>
        CasmObject[] arr = response.getBody();
        List<CasmObject> results = arr!=null ? Arrays.asList(arr) : new ArrayList<>();
        log.debug("-------------- searchForProperties: Results count: {}", results.size());

        // Prepare the select2 response message
        Map<String,Object> resultsMap = new LinkedHashMap<>();
        List<Map<String,Object>> resultsArray = new ArrayList<>();
        resultsMap.put("results", resultsArray);
        for (CasmObject prop : results) {
            Map<String,Object> propertyMap = new LinkedHashMap<>();
            resultsArray.add(propertyMap);

            boolean isDataProperty = "DataProperty".equalsIgnoreCase(prop.getPropertyType());
            String rangeType = prop.getRangeUri();
            String rangeId = prop.getRange();

            propertyMap.put("id", prop.getId());
            propertyMap.put("text", prop.getName());
            propertyMap.put("parentAttribute", prop.getParent().getName());
            propertyMap.put("dataProperty", isDataProperty);
            if (StringUtils.isNotBlank(rangeType))
                propertyMap.put("rangeType", rangeType);
            if (StringUtils.isNotBlank(rangeId))
                propertyMap.put("rangeId", rangeId);

            List<CasmObject.MinimalCasmObject> instances = prop.getInstances();
            if (instances!=null && instances.size()>0) {
                List<Map<String,String>> instancesList = new ArrayList<>();
                propertyMap.put("instances", instancesList);

                for (CasmObject.MinimalCasmObject i : instances) {
                    Map<String,String> instanceMap = new LinkedHashMap<>();
                    instancesList.add(instanceMap);

                    instanceMap.put("id", i.getId());
                    instanceMap.put("name", i.getName());
                    instanceMap.put("description", i.getDescription());
                }
            }
        }

        log.info("-------------- searchForProperties: OUTPUT: {}", resultsMap);
        return resultsMap;
    }

    // ------------------------------------------------------------------------
    // CASM export method
    // ------------------------------------------------------------------------

    @RequestMapping(value = "/export-casm-as-rdf", method = GET)
    public ResponseEntity<byte[]> exportCasmAsRdf() throws UnsupportedEncodingException {
        log.info("-------------- exportCasmAsRdf: BEGIN: n/a");

        // Call REST service for update
        String url = "/opt/attributes/export-casm-as-rdf";
        ResponseEntity<String> response = callService(url, HttpMethod.GET, null, null, String.class);
        log.debug("-------------- exportCasmAsRdf: Exported RDF: {}", response.getBody());

        // get response content
        String responseContents = "";
        if (response.getStatusCode().is2xxSuccessful()) {
            responseContents = response.getBody();
            if (responseContents == null) responseContents = "";
        }

        // send TTL file for download
        return _sendTtlFileForDownload(responseContents, "ample-casm", "", "ttl");
    }
}