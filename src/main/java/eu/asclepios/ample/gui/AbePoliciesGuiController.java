/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.gui;

import eu.asclepios.ample.conf.ROLE;
import eu.asclepios.ample.model.abe.ABE_TYPE;
import eu.asclepios.ample.model.abe.AbePolicy;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.springframework.http.MediaType.*;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Slf4j
@RestController
@RequestMapping("/gui/abe")
@RolesAllowed({ROLE.ROLE_ADMIN, ROLE.ROLE_ABE})
public class AbePoliciesGuiController extends AbstractGuiController {

    // ------------------------------------------------------------------------
    // ABE Policy methods
    // ------------------------------------------------------------------------

    @RequestMapping(value = "/get-toplevel-policies", method = {GET, POST}, produces = APPLICATION_JSON_VALUE)
    public List<JstreeJsonNode> getTopLevelPolicies(@RequestParam(name = "filter", defaultValue = "*", required = false) String filter) {
        log.debug("-------------- getTopLevelPolicies: INPUT: filter={}", filter);

        // Call REST service in order to get attribute list
        AbePolicy[] policiesArr = callService("/opt/abe-policies/", HttpMethod.GET, null, null, AbePolicy[].class).getBody();

        // Prepare response to return to page (filter & sort items, and build response)
        List<JstreeJsonNode> response = createResponseForJstree(policiesArr, filter, ABE_TYPE::valueByName);

        log.debug("-------------- getTopLevelPolicies: OUTPUT: {}", response);
        return response;
    }

    @RequestMapping(value = "/get-policy/{policy_id}", method = {GET, POST}, produces = APPLICATION_JSON_VALUE)
    public AbePolicy getPolicy(@PathVariable("policy_id") String id) {
        log.info("-------------- getPolicy: INPUT: policy-id={}", id);

        // Call REST service in order to get attribute's data'
        AbePolicy policyOut = callService("/opt/abe-policies/" + id, HttpMethod.GET, null, null, AbePolicy.class).getBody();

        log.info("-------------- getPolicy: OUTPUT: {}", policyOut);
        return policyOut;
    }

    @RequestMapping(value = "/save-policy", method = POST, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public AbePolicy savePolicy(@RequestBody AbePolicy policyIn) {
        log.info("-------------- savePolicy: INPUT: {}", policyIn);
        policyIn.setLastUpdateTimestamp(new Date());

        // Call REST service for update
        String id = policyIn.getId();
        String responseMessage = callService("/opt/abe-policies/" + id, HttpMethod.POST, policyIn, APPLICATION_JSON, String.class).getBody();
        log.debug("-------------- savePolicy: Response message: {}", responseMessage);

        AbePolicy policyOut = policyIn;
        log.info("-------------- savePolicy: OUTPUT: {}", policyOut);
        return policyOut;
    }

    @RequestMapping(value = { "/delete-policy/{policy_id}", "/delete-policy/{policy_id}/cascade" }, method = GET)
    public ResponseEntity<String> deletePolicy(HttpServletRequest request, @PathVariable("policy_id") String id) {
        log.info("-------------- deletePolicy: INPUT: id={}, request={}", id, request);

        // Call REST service to delete attribute
        String responseMessage = callService("/opt/abe-policies/"+id, HttpMethod.DELETE, null, null, String.class).getBody();
        log.debug("-------------- deletePolicy: Response message: {}", responseMessage);

        return createResponse(200, "ABE policy deleted "+id);
    }

    @RequestMapping(value = "/create-policy", method = POST, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<String> createPolicy(@RequestBody AbePolicy policyIn) {
        log.info("-------------- createPolicy: INPUT: {}", policyIn);

        policyIn.setLastUpdateTimestamp(new Date());
        String id = policyIn.getId();
        if (StringUtils.isBlank(id)) {
            // generate a new Id
            id = "abe-policy-"+UUID.randomUUID();
            policyIn.setId(id);
            log.debug("-------------- createPolicy: New ABE policy Id:  {}", id);
        }

        // Set creation date
        policyIn.setCreateTimestamp(new Date());

        // Call REST service for update
        ResponseEntity<String> response = callService("/opt/abe-policies/", HttpMethod.PUT, policyIn, APPLICATION_JSON, String.class);
        log.debug("-------------- createPolicy: Response message: {}", response.getBody());

        log.info("-------------- createPolicy: OUTPUT: n/a");
        return createResponse(200, String.format("{ \"response\":  \"%d\", \"entity\": \"%s\" }", response.getStatusCodeValue(), response.getBody()));
    }

    // ------------------------------------------------------------------------
    // ABE Policy export and interpretation methods
    // ------------------------------------------------------------------------

    @RequestMapping(value = "/export-policy-as-rdf/{policy_id}", method = GET)
    public ResponseEntity<byte[]> exportPolicyAsRdf(@PathVariable("policy_id") String id) throws UnsupportedEncodingException {
        log.info("-------------- exportPolicyAsRdf: BEGIN: policy-id={}", id);

        // Call REST service for update
        String url = "/opt/abe-policies/export-as-rdf/"+id;
        ResponseEntity<String> response = callService(url, HttpMethod.GET, null, null, String.class);
        log.debug("-------------- exportPolicyAsRdf: Exported RDF: {}", response.getBody());

        // get response content
        String responseContents = "";
        if (response.getStatusCode().is2xxSuccessful()) {
            responseContents = response.getBody();
            if (responseContents == null) responseContents = "";
        }

        // send TTL file for download
        return _sendTtlFileForDownload(responseContents, "abe-policy", id, "ttl");
    }

    @RequestMapping(value = "/export-policy-as-text/{policy_id}", method = GET)
    public ResponseEntity<byte[]> exportPolicyAsText(@PathVariable("policy_id") String id) {
        log.info("-------------- exportPolicyAsText: BEGIN: policy-id={}", id);

        // Call REST interpretation service to convert policy to XACML
        String url = "/opt/interpreter/abe-policy-to-text/"+id;
        String responseMessage =
                callService(url, HttpMethod.GET, null, null, String.class).getBody();
        if (responseMessage==null) responseMessage = "";
        log.debug("-------------- exportPolicyAsText: Response message: {}", responseMessage);

        // create download filename
        String fileName = createDownloadFileName("abe-policy", "txt", id);

        // send file for download
        return _sendFileForDownload(TEXT_PLAIN, responseMessage, fileName);
    }

    @RequestMapping(value = "/apply-policy/{policy_id}", method = GET)
    public ResponseEntity<String> applyPolicy(@PathVariable("policy_id") String id) {
        log.info("-------------- applyPolicy: BEGIN: policy-id={}", id);

        // Interpret policy to Text
        String policyContents = callService("/opt/interpreter/abe-policy-to-text/"+id, HttpMethod.GET, null, null, String.class).getBody();
        log.debug("-------------- applyPolicy: ABE Policy Text: {}", policyContents);
        if (StringUtils.isBlank(policyContents))
            throw new IllegalArgumentException("ABE Policy is empty: id="+id);

        // send Text policy to ABE Service
        String url = String.format(properties.getBackend().getAbeServiceUrl(), id);
        log.debug("-------------- applyPolicy: Contacting ABE service: {}", url);
        ResponseEntity<String> response = callServiceRest(url, HttpMethod.PUT, policyContents, TEXT_PLAIN, String.class);
        log.debug("-------------- applyPolicy: ABE service response: {}", response);

        log.info("-------------- applyPolicy: END: {}", response.getBody());
        return createResponse(200, String.format("{ \"response\":  \"%d\", \"entity\": \"%s\" }", response.getStatusCodeValue(), response.getBody()));
    }

    @RequestMapping(value = "/revoke-policy/{policy_id}", method = GET)
    public ResponseEntity<String> revokePolicy(@PathVariable("policy_id") String id) {
        log.info("-------------- revokePolicy: BEGIN: policy-id={}", id);

        // send Text policy to ABE Service
        String url = String.format(properties.getBackend().getAbeServiceUrl(), id);
        log.debug("-------------- revokePolicy: Contacting ABE service: {}", url);
        ResponseEntity<String> response = callServiceRest(url, HttpMethod.DELETE, null, TEXT_PLAIN, String.class);
        log.debug("-------------- revokePolicy: ABE service response: {}", response);

        log.info("-------------- revokePolicy: END: {}", response.getBody());
        return createResponse(200, String.format("{ \"response\":  \"%d\", \"entity\": \"%s\" }", response.getStatusCodeValue(), response.getBody()));
    }
}