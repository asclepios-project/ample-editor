/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.gui;

import eu.asclepios.ample.conf.ROLE;
import eu.asclepios.ample.model.abac.ABAC_TYPE;
import eu.asclepios.ample.model.abac.AbacPolicy;
import eu.asclepios.ample.model.abac.AbacRule;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.springframework.http.MediaType.*;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Slf4j
@RestController
@RequestMapping("/gui/abac")
@RolesAllowed({ROLE.ROLE_ADMIN, ROLE.ROLE_ABAC})
public class AbacPoliciesGuiController extends AbstractGuiController {

    public final static MediaType APP_XACML_XML = new MediaType("application", "xacml+xml");

    // ------------------------------------------------------------------------
    // ABAC Policy methods
    // ------------------------------------------------------------------------

    @RequestMapping(value = "/get-toplevel-policies", method = {GET, POST}, produces = APPLICATION_JSON_VALUE)
    public List<JstreeJsonNode> getTopLevelPolicies(@RequestParam(name = "filter", defaultValue = "*", required = false) String filter) {
        log.debug("-------------- getTopLevelPolicies: INPUT: filter={}", filter);

        // Call REST service in order to get attribute list
        AbacPolicy[] policiesArr = callService("/opt/abac-policies/", HttpMethod.GET, null, null, AbacPolicy[].class).getBody();

        // Prepare response to return to page (filter & sort items, and build response)
        List<JstreeJsonNode> response = createResponseForJstree(policiesArr, filter, ABAC_TYPE::valueByName);

        log.debug("-------------- getTopLevelPolicies: OUTPUT: {}", response);
        return response;
    }

    @RequestMapping(value = "/get-policy-rules/{policy_id}", method = {GET, POST}, produces = APPLICATION_JSON_VALUE)
    public List<JstreeJsonNode> getPolicyRules(@PathVariable("policy_id") String id, @RequestParam(name = "filter", defaultValue = "*", required = false) String filter) {
        log.info("-------------- getPolicyRules: INPUT: policy-id={}, filter={}", id, filter);

        // Call REST service in order to get attribute list
        AbacRule[] rulesArr = callService("/opt/abac-policies/" + id + "/rules", HttpMethod.GET, null, null, AbacRule[].class).getBody();

        // Prepare response to return to page (filter & sort items, and build response)
        List<JstreeJsonNode> response = createResponseForJstree(rulesArr, filter, ABAC_TYPE::valueByName);

        log.info("-------------- getPolicyRules: OUTPUT: {}", response);
        return response;
    }

    @RequestMapping(value = "/get-policy/{policy_id}", method = {GET, POST}, produces = APPLICATION_JSON_VALUE)
    public AbacPolicy getPolicy(@PathVariable("policy_id") String id) {
        log.info("-------------- getPolicy: INPUT: policy-id={}", id);

        // Call REST service in order to get attribute's data'
        AbacPolicy policyOut = callService("/opt/abac-policies/" + id, HttpMethod.GET, null, null, AbacPolicy.class).getBody();

        log.info("-------------- getPolicy: OUTPUT: {}", policyOut);
        return policyOut;
    }

    @RequestMapping(value = "/save-policy", method = POST, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public AbacPolicy savePolicy(@RequestBody AbacPolicy policyIn) {
        log.info("-------------- savePolicy: INPUT: {}", policyIn);
        policyIn.setLastUpdateTimestamp(new Date());

        // Call REST service for update
        String id = policyIn.getId();
        String responseMessage = callService("/opt/abac-policies/" + id, HttpMethod.POST, policyIn, APPLICATION_JSON, String.class).getBody();
        log.debug("-------------- savePolicy: Response message: {}", responseMessage);

        AbacPolicy policyOut = policyIn;
        log.info("-------------- savePolicy: OUTPUT: {}", policyOut);
        return policyOut;
    }

    @RequestMapping(value = { "/delete-policy/{policy_id}", "/delete-policy/{policy_id}/cascade" }, method = GET)
    public ResponseEntity<String> deletePolicy(HttpServletRequest request, @PathVariable("policy_id") String id) {
        log.info("-------------- deletePolicy: INPUT: id={}, request={}", id, request);

        // Check if cascade
        boolean cascade = true;     // delete is always cascading
        //boolean cascade = StringUtils.endsWithIgnoreCase(request.getRequestURI(), "/cascade");
        String cascadeSufix = cascade ? "/all" : "";

        // Call REST service to delete attribute
        String responseMessage = callService("/opt/abac-policies/"+id+cascadeSufix, HttpMethod.DELETE, null, null, String.class).getBody();
        log.debug("-------------- deletePolicy: Response message: {}", responseMessage);

        return createResponse(200, "ABAC policy deleted "+id);
    }

    @RequestMapping(value = "/create-policy", method = POST, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<String> createPolicy(@RequestBody AbacPolicy policyIn) {
        log.info("-------------- createPolicy: INPUT: {}", policyIn);

        policyIn.setLastUpdateTimestamp(new Date());
        String id = policyIn.getId();
        if (StringUtils.isBlank(id)) {
            // generate a new Id
            id = "abac-policy-"+UUID.randomUUID();
            policyIn.setId(id);
            log.debug("-------------- createPolicy: New ABAC policy Id:  {}", id);
        }

        // Set creation date
        policyIn.setCreateTimestamp(new Date());

        // Call REST service for update
        ResponseEntity<String> response = callService("/opt/abac-policies/", HttpMethod.PUT, policyIn, APPLICATION_JSON, String.class);
        log.debug("-------------- createPolicy: Response message: {}", response.getBody());

        log.info("-------------- createPolicy: OUTPUT: n/a");
        return createResponse(200, String.format("{ \"response\":  \"%d\", \"entity\": \"%s\" }", response.getStatusCodeValue(), response.getBody()));
    }

    @RequestMapping(value = "/{policy_id}/reorder-rules", method = POST, consumes = APPLICATION_JSON_VALUE)
    public String reorderPolicyRule(@PathVariable("policy_id") String id, @RequestBody String ruleIds) {
        log.info("-------------- reorderPolicyRule: INPUT: policy-id={}, rule-ids={}", id, ruleIds);

        // Call REST service in order to store policy rule positions
        ResponseEntity<String> response = callService("/opt/abac-policies/" + id + "/reorder-rules", HttpMethod.POST, ruleIds, APPLICATION_JSON, String.class);
        log.debug("-------------- reorderPolicyRule: Response: {}", response);

        log.info("-------------- reorderPolicyRule: OUTPUT: {}", response.getBody());
        return response.getBody();
    }

    // ------------------------------------------------------------------------
    // ABAC Policy Rule methods
    // ------------------------------------------------------------------------

    @RequestMapping(value = "/get-rule/{rule_id}", method = {GET, POST}, produces = APPLICATION_JSON_VALUE)
    public AbacRule getRule(@PathVariable("rule_id") String id) {
        log.info("-------------- getRule: INPUT: rule-id={}", id);

        // Call REST service in order to get attribute's data'
        AbacRule ruleOut = callService("/opt/abac-policies/rule/" + id, HttpMethod.GET, null, null, AbacRule.class).getBody();

        log.info("-------------- getRule: OUTPUT: {}", ruleOut);
        return ruleOut;
    }

    @RequestMapping(value = "/save-rule", method = POST, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public AbacRule saveRule(@RequestBody AbacRule ruleIn) {
        log.info("-------------- saveRule: INPUT: {}", ruleIn);
        ruleIn.setLastUpdateTimestamp(new Date());

        // Call REST service for update
        String id = ruleIn.getId();
        String responseMessage = callService("/opt/abac-policies/rule/" + id, HttpMethod.POST, ruleIn, APPLICATION_JSON, String.class).getBody();
        log.debug("-------------- saveRule: Response message: {}", responseMessage);

        AbacRule ruleOut = ruleIn;
        log.info("-------------- saveRule: OUTPUT: {}", ruleIn);
        return ruleOut;
    }

    @RequestMapping(value = "/delete-rule/{rule_id}", method = GET)
    public ResponseEntity<String> deleteRule(HttpServletRequest request, @PathVariable("rule_id") String id) {
        log.info("-------------- deleteRule: INPUT: id={}, request={}", id, request);

        // Call REST service to delete attribute
        String responseMessage = callService("/opt/abac-policies/rule/"+id, HttpMethod.DELETE, null, null, String.class).getBody();
        log.debug("-------------- deleteRule: Response message: {}", responseMessage);

        return createResponse(200, "ABAC rule deleted "+id);
    }

    @RequestMapping(value = "/create-rule", method = POST, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<String> createRule(@RequestBody AbacRule ruleIn) {
        log.info("-------------- createRule: INPUT: {}", ruleIn);

        ruleIn.setLastUpdateTimestamp(new Date());
        String id = ruleIn.getId();
        if (StringUtils.isBlank(id)) {
            // generate a new Id
            id = "abac-rule-"+UUID.randomUUID();
            ruleIn.setId(id);
            log.debug("-------------- createRule: New ABAC rule Id:  {}", id);
        }

        // Set creation date
        ruleIn.setCreateTimestamp(new Date());

        // Call REST service for update
        ResponseEntity<String> response = callService("/opt/abac-policies/rule/", HttpMethod.PUT, ruleIn, APPLICATION_JSON, String.class);
        log.debug("-------------- createRule: Response message: {}", response.getBody());

        log.info("-------------- createRule: OUTPUT: n/a");
        return createResponse(200, String.format("{ \"response\":  \"%d\", \"entity\": \"%s\" }", response.getStatusCodeValue(), response.getBody()));
    }

    // ------------------------------------------------------------------------
    // ABAC Policy export and interpretation methods
    // ------------------------------------------------------------------------

    @RequestMapping(value = "/export-policy-as-rdf/{policy_id}", method = GET)
    public ResponseEntity<byte[]> exportPolicyAsRdf(@PathVariable("policy_id") String id) throws UnsupportedEncodingException {
        log.info("-------------- exportPolicyAsRdf: BEGIN: policy-id={}", id);

        // check if 'id' is valid (prevent sparql injection attacks)
        if (! id.matches("^[0-9A-Za-z-]+$")) {
            throw new IllegalArgumentException("Invalid 'id': "+id);
        }

        // Call REST service for update
        String url = "/opt/abac-policies/export-as-rdf/"+id;
        ResponseEntity<String> response = callService(url, HttpMethod.GET, null, null, String.class);
        log.trace("-------------- exportPolicyAsRdf: Exported RDF: {}", response.getBody());

        // get response content
        String responseContents = "";
        if (response.getStatusCode().is2xxSuccessful()) {
            responseContents = response.getBody();
            if (responseContents == null) responseContents = "";
        }

        // send TTL file for download
        return _sendTtlFileForDownload(responseContents, "abac-policy", id, "ttl");
    }

    @RequestMapping(value = "/export-policy-as-xacml/{policy_id}", method = GET)
    public ResponseEntity<byte[]> exportPolicyAsXacml(@PathVariable("policy_id") String id) {
        log.info("-------------- exportPolicyAsXacml: BEGIN: policy-id={}", id);

        // Call REST interpretation service to convert policy to XACML
        String responseContents = callService("/opt/interpreter/abac-policy-to-xacml/"+id, HttpMethod.GET, null, null, String.class).getBody();
        log.debug("-------------- exportPolicyAsXacml: Response XACML: {}", responseContents);

        // create download filename
        String fileName = createDownloadFileName("abac-policy", "xacml", id);

        // send file for download
        return _sendFileForDownload(APP_XACML_XML, responseContents, fileName);
    }

    @RequestMapping(value = "/apply-policy/{policy_id}", method = GET)
    public ResponseEntity<String> applyPolicy(@PathVariable("policy_id") String id) {
        log.info("-------------- applyPolicy: BEGIN: policy-id={}", id);

        // Interpret policy to XACML
        String policyContents = callService("/opt/interpreter/abac-policy-to-xacml/"+id, HttpMethod.GET, null, null, String.class).getBody();
        log.debug("-------------- applyPolicy: XACML: {}", policyContents);
        if (StringUtils.isBlank(policyContents))
            throw new IllegalArgumentException("ABAC Policy is empty: id="+id);

        // send XACML policy to ABAC policy enforcement mechanism
        String url = String.format(properties.getBackend().getPapUrl(), id);
        log.debug("-------------- applyPolicy: Contacting PAP server: {}", url);
        ResponseEntity<String> response = callServiceRest(url, HttpMethod.PUT, policyContents, TEXT_XML, String.class);
        log.debug("-------------- applyPolicy: PAP response: {}", response);

        log.info("-------------- applyPolicy: END: {}", response.getBody());
        return createResponse(200, String.format("{ \"response\":  \"%d\", \"entity\": \"%s\" }", response.getStatusCodeValue(), response.getBody()));
    }

    @RequestMapping(value = "/revoke-policy/{policy_id}", method = GET)
    public ResponseEntity<String> revokePolicy(@PathVariable("policy_id") String id) {
        log.info("-------------- revokePolicy: BEGIN: policy-id={}", id);

        // delete XACML policy from ABAC policy enforcement mechanism
        String url = String.format(properties.getBackend().getPapUrl(), id);
        log.debug("-------------- revokePolicy: Contacting PAP server: {}", url);
        ResponseEntity<String> response = callServiceRest(url, HttpMethod.DELETE, null, TEXT_XML, String.class);
        log.debug("-------------- revokePolicy: PAP response: {}", response);

        log.info("-------------- revokePolicy: END: {}", response.getBody());
        return createResponse(200, String.format("{ \"response\":  \"%d\", \"entity\": \"%s\" }", response.getStatusCodeValue(), response.getBody()));
    }
}
