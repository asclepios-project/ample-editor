/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.interpreter;

import eu.asclepios.ample.model.casm.CasmObject;
import eu.asclepios.ample.model.abac.AbacPolicy;
import eu.asclepios.ample.model.abac.AbacRule;
import eu.asclepios.ample.model.expr.CompositeExpression;
import eu.asclepios.ample.model.expr.Expression;
import eu.asclepios.ample.model.expr.SimpleExpression;
import eu.asclepios.ample.rest.AbacPolicyRestController;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Slf4j
@Service
public class InterpreterAbacPolicyToXacml implements Interpreter<AbacPolicy, String> {
	public final static String CASM_URN = "urn:eu:asclepios-project:casm:1.0";
	public final static String CASM_PREFIX = CASM_URN+":";
	public final static String TRACKING_OBLIGATION_PREFIX = "TrackingObligation:";

	@Autowired
	private AbacPolicyRestController abacPolicyRestController;

	@Override
	public String interpret(AbacPolicy abacPolicy) {
		// write XACML policy head
		StringBuilder sb = new StringBuilder();
		writePolicyHead(sb, abacPolicy.getId(), abacPolicy.getName(), abacPolicy.getDescription(), abacPolicy.getPolicyCombiningAlgorithm());

		// write XACML policy rules
		List<AbacRule> abacRulesList;
		try {
			abacRulesList = abacPolicyRestController._retrievePolicyRulesInternal(abacPolicy.getId());
			// sort rule by Type, Position, Name
			abacRulesList.sort((a, b) -> {
				int result = a.getType().compareTo(b.getType());
				if (result!=0) return result;
				result = (int) Math.signum(a.getPosition() - b.getPosition());
				if (result!=0) return result;
				return a.getName().compareTo(b.getName());
			});
		} catch (Exception e) {
			//abacRulesList = Collections.emptyList();
			throw new RuntimeException("interpret: EXCEPTION: while retrieving policy rules: "+abacPolicy.getId(), e);
		}
		AbacRule[] abacRules = abacRulesList.toArray(new AbacRule[0]);
		for (AbacRule rule : abacRules) {
			writeRuleHead(sb, rule.getId(), rule.getName(), rule.getDescription(), rule.getRuleOutcome());
			writeCondition(sb, rule.getRuleCondition());
			writeRuleObligations(sb, abacPolicy, rule);
			writeRuleTail(sb);
		}

		// write XACML policy obligations and tail
		writePolicyObligations(sb, abacPolicy);
		writePolicyTail(sb);
		return sb.toString();
	}

	private StringBuilder ident(StringBuilder sb, int num) {
		for (int i=0; i<num; i++)
			sb.append("    ");
		return sb;
	}

	private void writePolicyHead(StringBuilder sb, String id, String name, String description, String policyCombiningAlgorithm) {
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");

		sb.append("<xacml3:Policy \n");
		ident(sb, 1).append("xmlns:xacml3=\"urn:oasis:names:tc:xacml:3.0:core:schema:wd-17\" \n");
		ident(sb, 1).append("PolicyId=\"").append(id).append("\" \n");
		ident(sb, 1).append("RuleCombiningAlgId=\"").append(policyCombiningAlgorithm).append("\" \n");
		ident(sb, 1).append("Version=\"1.0\">\n\n");

		ident(sb, 1).append("<xacml3:Description><![CDATA[\n");
		ident(sb, 1).append("        Policy Id:   "); _cdata(sb, id); sb.append("\n");
		ident(sb, 1).append("        Policy Name: "); _cdata(sb, name); sb.append("\n");
		if (StringUtils.isNotBlank(description))
			ident(sb, 1).append("        Description: "); _cdata(sb, description); sb.append("\n");
		ident(sb, 1).append("]]></xacml3:Description>\n");
		sb.append("\n");
		ident(sb, 1).append("<xacml3:Target />\n");
		sb.append("\n");
	}

	private void writePolicyObligations(StringBuilder sb, AbacPolicy policy) {
		String policyId = policy.getId();
		ident(sb, 1).append("<!-- Policy Obligations -->\n");
		ident(sb, 1).append("<xacml3:ObligationExpressions>\n");
		ident(sb, 2).append("<ObligationExpression ObligationId=\"")
				.append(TRACKING_OBLIGATION_PREFIX).append(policyId)
				.append(":permit\" FulfillOn=\"Permit\">\n");
		writeObligationAssignments(sb, "PERMIT", "Policy", policy.getId(), policy.getName(), null);
		ident(sb, 2).append("</ObligationExpression>\n");

		ident(sb, 2).append("<ObligationExpression ObligationId=\"")
				.append(TRACKING_OBLIGATION_PREFIX).append(policyId)
				.append(":deny\" FulfillOn=\"Deny\">\n");
		writeObligationAssignments(sb, "DENY", "Policy", policy.getId(), policy.getName(), null);
		ident(sb, 2).append("</ObligationExpression>\n");
		ident(sb, 1).append("</xacml3:ObligationExpressions>\n");
	}

	private void writePolicyTail(StringBuilder sb) {
		sb.append("</xacml3:Policy>\n");
	}

	private void writeRuleHead(StringBuilder sb, String id, String name, String description, String ruleOutcome) {
		ident(sb, 1).append("<!-- Rule Name: ").append(name.trim());
		if (description!=null && !description.isEmpty())
			ident(sb, 1).append("\n     Description: ").append(description).append("\n");
		sb.append(" -->\n");
		ident(sb, 1).append("<xacml3:Rule Effect=\"")
				.append( getRuleEffect(ruleOutcome) )
				.append("\" RuleId=\"")
				.append(id)
				.append("\">\n");
	}

	private String getRuleEffect(String ruleOutcome) {
		if ("Permit".equalsIgnoreCase(ruleOutcome)) return "Permit";
		if ("Deny".equalsIgnoreCase(ruleOutcome)) return "Deny";
		throw new IllegalArgumentException("Invalid Rule Effect: "+ruleOutcome);
	}

	private void writeRuleObligations(StringBuilder sb, AbacPolicy policy, AbacRule rule) {
		String policyId = policy.getId();
		String ruleId = rule.getId();
		ident(sb, 2).append("<!-- Rule Obligations -->\n");
		ident(sb, 2).append("<xacml3:ObligationExpressions>\n");
		ident(sb, 3).append("<ObligationExpression ObligationId=\"")
				.append(TRACKING_OBLIGATION_PREFIX).append(policyId).append(":").append(ruleId);
		if ("PERMIT".equalsIgnoreCase(rule.getRuleOutcome()))
				sb.append(":permit\" FulfillOn=\"Permit\">\n");
		else
				sb.append(":deny\" FulfillOn=\"Deny\">\n");
		writeObligationAssignments(sb, rule.getRuleOutcome().toUpperCase(), "Rule", rule.getId(), rule.getName(), policy.getName());
		ident(sb, 3).append("</ObligationExpression>\n");
		ident(sb, 2).append("</xacml3:ObligationExpressions>\n");
	}

	private void writeObligationAssignments(StringBuilder sb, String decision, String type, String id, String name, String parent) {
		writeObligationAssignment(sb, "asclepios:obligation:decision", decision, XSD_STRING);
		writeObligationAssignment(sb, "asclepios:obligation:type", type, XSD_STRING);
		writeObligationAssignment(sb, "asclepios:obligation:id", id, XSD_STRING);
		writeObligationAssignment(sb, "asclepios:obligation:name", name, XSD_STRING);
		if (parent!=null) writeObligationAssignment(sb, "asclepios:obligation:parent", parent, XSD_STRING);
	}

	private void writeObligationAssignment(StringBuilder sb, String attributeId, String attributeValue, String attributeType) {
		ident(sb, 4).append("<AttributeAssignmentExpression ")
				.append("AttributeId=\"").append(attributeId).append("\">\n");
		ident(sb, 5).append("<xacml3:AttributeValue ")
				.append("DataType=\"").append(attributeType).append("\">")
				.append(attributeValue)
				.append("</xacml3:AttributeValue>\n");
		ident(sb, 4).append("</AttributeAssignmentExpression>\n");
	}

	private void writeRuleTail(StringBuilder sb) {
		ident(sb, 1).append("</xacml3:Rule>\n\n");
	}

	private void writeCondition(StringBuilder sb, Expression ruleCondition) {
		if (ruleCondition==null)
			return;

		ident(sb, 2).append("<xacml3:Condition>\n");
		writeExpression(sb, ruleCondition, 3);
		ident(sb, 2).append("</xacml3:Condition>\n");
	}

	private void writeExpression(StringBuilder sb, Expression expression, int ident) {
		if (expression instanceof SimpleExpression)
			writeSimpleExpression(sb, (SimpleExpression)expression, ident);
		if (expression instanceof CompositeExpression)
			writeCompositeExpression(sb, (CompositeExpression)expression, ident);
	}

	private void writeSimpleExpression(StringBuilder sb, SimpleExpression expression, int ident) {
		// get XACML functions for operator
		OperatorConfig operatorConfig = getXacmlOperatorConfig(expression.getOperator(), expression.getPropertyRef());
		String operator = operatorConfig.getXacmlUri();
		String[] operatorPart = operator.split(" ");
		operator = operatorPart[operatorPart.length-1];
		operatorPart[operatorPart.length-1] = null;
		boolean reverseOperants = operatorConfig.isReverseOperantsRequired();

		// add expression opening comment
		ident(sb, ident)
				.append("<!-- SIMPLE EXPRESSION - START:  attribute=")
				.append(expression.getAttributeRef().getName())
				.append(", property=").append(expression.getPropertyRef().getName())
				.append(", operator=").append(expression.getOperator())
				.append(", value=").append(expression.getValue())
				.append(" -->\n");

		// open NOT internal operator first
		boolean usesEmpty = false;
		for (String term : operatorPart) {
			if ("NOT".equals(term)) {
				ident(sb, ident).append("<xacml3:Apply FunctionId=\"urn:oasis:names:tc:xacml:1.0:function:not\">\n");
				ident++;
			} else
			if ("EMPTY".equals(term)) {
				usesEmpty = true;
			}
		}

		// gather attribute designator and attribute value information

		// attribute id (for attribute designator)
		String[] categoryAndId = expression.getPropertyRef().getUri().split("@", 2);
		String attributeUrn =
				//CASM_PREFIX + getObjectCasmPath(expression.getPropertyRef());
				categoryAndId.length>1 ? categoryAndId[1].trim() : categoryAndId[0].trim();
		//String attributeUrn = expression.getPropertyRef().getUri();
		// attribute category (for attribute designator)
		String attributeCategory = //CASM_URN;
				categoryAndId.length>1 ? categoryAndId[0].trim() : CASM_URN;
		// data type (for both attribute designator and value)
		String attributeDataType = getXsdDataType(expression.getPropertyRef());
		String attributeDataTypeName = getXsdStringLastPart( attributeDataType );
		// attribute value (content)
		String value = expression.getValue();

		// open the main operator
		ident(sb, ident).append("<xacml3:Apply FunctionId=\"").append(operator).append("\">\n");

		if (!reverseOperants) {
			// add attribute designator along with any internal operators (e.g. CI)
			ident = writeAttributeDesignator(sb, ident, operatorPart, attributeUrn, attributeCategory, attributeDataType, attributeDataTypeName);

			// add attribute value along with any internal operators (e.g. CI)
			ident = writeAttributeValue(sb, ident, operatorPart, usesEmpty, attributeDataType, value);
		} else {
			// add attribute value along with any internal operators (e.g. CI)
			ident = writeAttributeValue(sb, ident, operatorPart, usesEmpty, attributeDataType, value);

			// add attribute designator along with any internal operators (e.g. CI)
			ident = writeAttributeDesignator(sb, ident, operatorPart, attributeUrn, attributeCategory, attributeDataType, attributeDataTypeName);
		}

		// close the main operator
		ident(sb, ident).append("</xacml3:Apply>\n");

		// close NOT internal operator
		for (String term : operatorPart) {
			if ("NOT".equals(term)) {
				ident--;
				ident(sb, ident).append("</xacml3:Apply>\n");
			}
		}

		// add expression closing comment
		ident(sb, ident)
				.append("<!-- SIMPLE EXPRESSION - END -->\n");
	}

	private int writeAttributeValue(StringBuilder sb, int ident, String[] operatorPart, boolean usesEmpty, String attributeDataType, String value) {
		ident = writeInternalOperatorStarts(sb, ident, operatorPart);
		ident(sb, ident +1).append("<xacml3:AttributeValue\n");
		ident(sb, ident +1)
				.append("\tDataType=\"").append(attributeDataType).append("\">")
				.append( usesEmpty ? "" : value)
				.append("</xacml3:AttributeValue>\n");
		ident = writeInternalOperatorEnds(sb, ident, operatorPart);
		return ident;
	}

	private int writeAttributeDesignator(StringBuilder sb, int ident, String[] operatorPart, String attributeUrn, String attributeCategory, String attributeDataType, String attributeDataTypeName) {
		ident = writeInternalOperatorStarts(sb, ident, operatorPart);

		ident(sb, ident +1)
				.append("<xacml3:Apply FunctionId=\"")
				.append("urn:oasis:names:tc:xacml:1.0:function:").append(attributeDataTypeName).append("-one-and-only")
				.append("\">\n");

		ident(sb, ident +2).append("<xacml3:AttributeDesignator\n");
		ident(sb, ident +2).append("\tAttributeId=\"").append(attributeUrn).append("\"\n");
		ident(sb, ident +2).append("\tCategory=\"").append(attributeCategory).append("\"\n");
		ident(sb, ident +2).append("\tDataType=\"").append(attributeDataType).append("\"\n");
		ident(sb, ident +2).append("\tMustBePresent=\"true\"\n");
		ident(sb, ident +2).append("/>\n");

		ident(sb, ident +1)
				.append("</xacml3:Apply>\n");

		ident = writeInternalOperatorEnds(sb, ident, operatorPart);
		return ident;
	}

	private int writeInternalOperatorStarts(StringBuilder sb, int ident, String[] operatorPart) {
		for (String term : operatorPart) {
			if ("CI".equals(term)) {
				ident(sb, ident+1).append("<xacml3:Apply FunctionId=\"urn:oasis:names:tc:xacml:1.0:function:string-normalize-to-lower-case\">\n");
				ident++;
			} else
			if ("TRIM".equals(term)) {
				ident(sb, ident+1).append("<xacml3:Apply FunctionId=\"urn:oasis:names:tc:xacml:1.0:function:string-normalize-space\">\n");
				ident++;
			}
		}
		return ident;
	}
	private int writeInternalOperatorEnds(StringBuilder sb, int ident, String[] operatorPart) {
		for (String term : operatorPart) {
			if ("CI".equals(term)) {
				ident(sb, ident).append("</xacml3:Apply>\n");
				ident--;
			} else
			if ("TRIM".equals(term)) {
				ident(sb, ident).append("</xacml3:Apply>\n");
				ident--;
			}
		}
		return ident;
	}

	private void writeCompositeExpression(StringBuilder sb, CompositeExpression expression, int ident) {
		// check operator
		String booleanOperator = null;
		if ("AND".equals(expression.getOperator())) booleanOperator = "urn:oasis:names:tc:xacml:1.0:function:and";
		if ("OR".equals(expression.getOperator())) booleanOperator = "urn:oasis:names:tc:xacml:1.0:function:or";
		if (booleanOperator==null)
			throw new IllegalArgumentException("CompositeExpression with an invalid operator: "+expression.getOperator());

		String notStr = expression.isNot() ? "NOT " : "";
		ident(sb, ident).append("<!-- COMPOSITE EXPRESSION - START:  operator: ").append(notStr).append(expression.getOperator()).append(" -->\n");

		// open 'NOT' expression, if 'not' flag is set
		if (expression.isNot()) {
			ident(sb, ident).append("<xacml3:Apply FunctionId=\"").append("urn:oasis:names:tc:xacml:1.0:function:not").append("\">\n");
			ident++;
		}

		// write composite expression
		ident(sb, ident).append("<xacml3:Apply FunctionId=\"").append(booleanOperator).append("\">\n");
		for (Expression expr : expression.getExpressions()) {
			writeExpression(sb, expr, ident+1);
		}
		ident(sb, ident).append("</xacml3:Apply>\n");

		// close 'NOT' expression, if 'not' flag is set
		if (expression.isNot()) {
			ident--;
			ident(sb, ident).append("</xacml3:Apply>\n");
		}
		ident(sb, ident).append("<!-- COMPOSITE EXPRESSION - END:  operator: ").append(notStr).append(expression.getOperator()).append(" -->\n");
	}

	private void _cdata(StringBuilder sb, String s) {
		if (s==null || s.isEmpty())
			return;
		String[] part = s.split("]]>");
		for (int i=0; i<part.length-1; i++) {
			sb.append(part[i]);
			sb.append("]]]]>");
			sb.append("<![CDATA[>");
		}
		sb.append(part[part.length-1]);
	}

	// ===========================================================================
	// Operator and Data type conversion to equivalent XACML functions/data types
	// ===========================================================================

	private final static Map<String,String> XSD_DATA_TYPE_MAP = Collections.unmodifiableMap(
			new HashMap<String, String>() {{
				put("string", "http://www.w3.org/2001/XMLSchema#string");
				put("integer", "http://www.w3.org/2001/XMLSchema#integer");
				put("double", "http://www.w3.org/2001/XMLSchema#double");
				put("boolean", "http://www.w3.org/2001/XMLSchema#boolean");
				put("date", "http://www.w3.org/2001/XMLSchema#date");
				put("time", "http://www.w3.org/2001/XMLSchema#time");
				put("datetime", "http://www.w3.org/2001/XMLSchema#dateTime");
			}}
	);

	private final static String XSD_STRING = XSD_DATA_TYPE_MAP.get("string");

	private final static Map<String,String> XSD_DATA_TYPE_REVERSE_MAP = Collections.unmodifiableMap(
			new HashMap<String, String>() {{
				put("http://www.w3.org/2001/XMLSchema#string", "string");
				put("http://www.w3.org/2001/XMLSchema#integer", "integer");
				put("http://www.w3.org/2001/XMLSchema#double", "double");
				put("http://www.w3.org/2001/XMLSchema#boolean", "boolean");
				put("http://www.w3.org/2001/XMLSchema#date", "date");
				put("http://www.w3.org/2001/XMLSchema#time", "time");
				put("http://www.w3.org/2001/XMLSchema#dateTime", "datetime");
			}}
	);

	private final static  Map<String,String> XSD_BASE_TYPE_MAP = Collections.unmodifiableMap(
			new HashMap<String, String>() {{
				put("anyuri", "string");
				put("x500Name", "string");
				put("rfc822Name", "string");
				put("dnsName", "string");

				put("byte", "integer");
				put("short", "integer");
				put("int", "integer");
				put("long", "integer");

				put("positiveinteger", "integer");
				put("nonnegativeinteger", "integer");
				put("negativeinteger", "integer");
				put("nonpositiveinteger", "integer");

				put("unsignedbyte", "integer");
				put("unsignedshort", "integer");
				put("unsignedint", "integer");
				put("unsignedlong", "integer");

				put("decimal", "double");
				put("float", "double");
			}}
	);

	@Getter
	private final static class OperatorConfig {
		private final String xacmlUri;
		private final boolean reverseOperantsRequired;
		OperatorConfig(String uri) { xacmlUri = uri; reverseOperantsRequired = false; }
		OperatorConfig(String uri, boolean reverse) { xacmlUri = uri; reverseOperantsRequired = reverse; }
	}

	private final static Map<String, Map<String,OperatorConfig>> OPERATOR_MAP = Collections.unmodifiableMap(
			new HashMap<String, Map<String, OperatorConfig>>() {{
				put("string",
						Collections.unmodifiableMap(new HashMap<String, OperatorConfig>() {{
							put("EQUALS", new OperatorConfig("urn:oasis:names:tc:xacml:1.0:function:string-equal"));
							put("NOT_EQUALS", new OperatorConfig("NOT urn:oasis:names:tc:xacml:1.0:function:string-equal"));
							put("EQUALS_CI", new OperatorConfig("urn:oasis:names:tc:xacml:3.0:function:string-equal-ignore-case"));
							put("NOT_EQUALS_CI", new OperatorConfig("NOT urn:oasis:names:tc:xacml:3.0:function:string-equal-ignore-case"));
							put("LESS_THAN", new OperatorConfig("urn:oasis:names:tc:xacml:1.0:function:string-less-than"));
							put("LESS_OR_EQUAL", new OperatorConfig("urn:oasis:names:tc:xacml:1.0:function:string-less-than-or-equal"));
							put("GREATER_THAN", new OperatorConfig("urn:oasis:names:tc:xacml:1.0:function:string-greater-than"));
							put("GREATER_OR_EQUAL", new OperatorConfig("urn:oasis:names:tc:xacml:1.0:function:string-greater-than-or-equal"));
							put("STARTS_WITH", new OperatorConfig("urn:oasis:names:tc:xacml:3.0:function:string-starts-with", true));
							put("ENDS_WITH", new OperatorConfig("urn:oasis:names:tc:xacml:3.0:function:string-ends-with", true));
							put("STARTS_WITH_CI", new OperatorConfig("CI urn:oasis:names:tc:xacml:3.0:function:string-starts-with", true));
							put("ENDS_WITH_CI", new OperatorConfig("CI urn:oasis:names:tc:xacml:3.0:function:string-ends-with", true));
							put("IS_EMPTY", new OperatorConfig("EMPTY urn:oasis:names:tc:xacml:1.0:function:string-equal"));
							put("IS_NOT_EMPTY", new OperatorConfig("NOT EMPTY urn:oasis:names:tc:xacml:1.0:function:string-equal"));
							put("IS_BLANK", new OperatorConfig("TRIM EMPTY urn:oasis:names:tc:xacml:1.0:function:string-equal"));
							put("IS_NOT_BLANK", new OperatorConfig("NOT TRIM EMPTY urn:oasis:names:tc:xacml:1.0:function:string-equal"));
							put("CONTAINS", new OperatorConfig("urn:oasis:names:tc:xacml:3.0:function:string-contains", true));
							put("NOT_CONTAINS", new OperatorConfig("NOT urn:oasis:names:tc:xacml:3.0:function:string-contains", true));
							put("CONTAINS_CI", new OperatorConfig("CI urn:oasis:names:tc:xacml:3.0:function:string-contains", true));
							put("NOT_CONTAINS_CI", new OperatorConfig("NOT CI urn:oasis:names:tc:xacml:3.0:function:string-contains", true));
						}}));

				put("integer",
						Collections.unmodifiableMap(new HashMap<String, OperatorConfig>() {{
							put("EQUALS", new OperatorConfig("urn:oasis:names:tc:xacml:1.0:function:integer-equal"));
							put("NOT_EQUALS", new OperatorConfig("NOT urn:oasis:names:tc:xacml:1.0:function:integer-equal"));
							put("LESS_THAN", new OperatorConfig("urn:oasis:names:tc:xacml:1.0:function:integer-less-than"));
							put("LESS_OR_EQUAL", new OperatorConfig("urn:oasis:names:tc:xacml:1.0:function:integer-less-than-or-equal"));
							put("GREATER_THAN", new OperatorConfig("urn:oasis:names:tc:xacml:1.0:function:integer-greater-than"));
							put("GREATER_OR_EQUAL", new OperatorConfig("urn:oasis:names:tc:xacml:1.0:function:integer-greater-than-or-equal"));
						}}));

				put("double",
						Collections.unmodifiableMap(new HashMap<String, OperatorConfig>() {{
							put("EQUALS", new OperatorConfig("urn:oasis:names:tc:xacml:1.0:function:double-equal"));
							put("NOT_EQUALS", new OperatorConfig("NOT urn:oasis:names:tc:xacml:1.0:function:double-equal"));
							put("LESS_THAN", new OperatorConfig("urn:oasis:names:tc:xacml:1.0:function:double-less-than"));
							put("LESS_OR_EQUAL", new OperatorConfig("urn:oasis:names:tc:xacml:1.0:function:double-less-than-or-equal"));
							put("GREATER_THAN", new OperatorConfig("urn:oasis:names:tc:xacml:1.0:function:double-greater-than"));
							put("GREATER_OR_EQUAL", new OperatorConfig("urn:oasis:names:tc:xacml:1.0:function:double-greater-than-or-equal"));
						}}));

				put("boolean",
						Collections.unmodifiableMap(new HashMap<String, OperatorConfig>() {{
							put("EQUALS", new OperatorConfig("urn:oasis:names:tc:xacml:1.0:function:boolean-equal"));
							put("NOT_EQUALS", new OperatorConfig("NOT urn:oasis:names:tc:xacml:1.0:function:boolean-equal"));
						}}));

				put("date",
						Collections.unmodifiableMap(new HashMap<String, OperatorConfig>() {{
							put("EQUALS", new OperatorConfig("urn:oasis:names:tc:xacml:1.0:function:date-equal"));
							put("NOT_EQUALS", new OperatorConfig("NOT urn:oasis:names:tc:xacml:1.0:function:date-equal"));
							put("LESS_THAN", new OperatorConfig("urn:oasis:names:tc:xacml:1.0:function:date-less-than"));
							put("LESS_OR_EQUAL", new OperatorConfig("urn:oasis:names:tc:xacml:1.0:function:date-less-than-or-equal"));
							put("GREATER_THAN", new OperatorConfig("urn:oasis:names:tc:xacml:1.0:function:date-greater-than"));
							put("GREATER_OR_EQUAL", new OperatorConfig("urn:oasis:names:tc:xacml:1.0:function:date-greater-than-or-equal"));
						}}));

				put("time",
						Collections.unmodifiableMap(new HashMap<String, OperatorConfig>() {{
							put("EQUALS", new OperatorConfig("urn:oasis:names:tc:xacml:1.0:function:time-equal"));
							put("NOT_EQUALS", new OperatorConfig("NOT urn:oasis:names:tc:xacml:1.0:function:time-equal"));
							put("LESS_THAN", new OperatorConfig("urn:oasis:names:tc:xacml:1.0:function:time-less-than"));
							put("LESS_OR_EQUAL", new OperatorConfig("urn:oasis:names:tc:xacml:1.0:function:time-less-than-or-equal"));
							put("GREATER_THAN", new OperatorConfig("urn:oasis:names:tc:xacml:1.0:function:time-greater-than"));
							put("GREATER_OR_EQUAL", new OperatorConfig("urn:oasis:names:tc:xacml:1.0:function:time-greater-than-or-equal"));
						}}));

				put("datetime",
						Collections.unmodifiableMap(new HashMap<String, OperatorConfig>() {{
							put("EQUALS", new OperatorConfig("urn:oasis:names:tc:xacml:1.0:function:dateTime-equal"));
							put("NOT_EQUALS", new OperatorConfig("NOT urn:oasis:names:tc:xacml:1.0:function:dateTime-equal"));
							put("LESS_THAN", new OperatorConfig("urn:oasis:names:tc:xacml:1.0:function:dateTime-less-than"));
							put("LESS_OR_EQUAL", new OperatorConfig("urn:oasis:names:tc:xacml:1.0:function:dateTime-less-than-or-equal"));
							put("GREATER_THAN", new OperatorConfig("urn:oasis:names:tc:xacml:1.0:function:dateTime-greater-than"));
							put("GREATER_OR_EQUAL", new OperatorConfig("urn:oasis:names:tc:xacml:1.0:function:dateTime-greater-than-or-equal"));
						}}));
			}}
	);

	private String getXsdDataType(CasmObject property) {
		if (!"PROPERTY".equals(property.getType()))
			throw new IllegalArgumentException("Argument #0 must be a property: "+property);

		if ("DataProperty".equals(property.getPropertyType())) {
			// It is 'DataProperty'
			String dataType = getDataTypeFromXsdString(property.getRangeUri());
			dataType = getBaseType(dataType);
			return XSD_DATA_TYPE_MAP.get(dataType);
		} else {
			// It is 'ObjectProperty'
			return XSD_DATA_TYPE_MAP.get("string");
		}
	}

	private boolean isOfType(String operator, String...types) {
		if (types==null || types.length==0)
			return false;
		for (String type : types) {
			if (type!=null && type.equalsIgnoreCase(operator))
				return true;
		}
		return false;
	}

	private String getXacmlOperator(String operator, CasmObject property) {
		return getXacmlOperatorConfig(operator, property).getXacmlUri();
	}

	private boolean isOperatorOperantsReverseRequired(String operator, CasmObject property) {
		return getXacmlOperatorConfig(operator, property).isReverseOperantsRequired();
	}

	private OperatorConfig getXacmlOperatorConfig(String operator, CasmObject property) {
		if (!"PROPERTY".equals(property.getType()))
			throw new IllegalArgumentException("Argument #1 must be a property: "+property);

		if ("DataProperty".equals(property.getPropertyType())) {
			// Get data type from XSD data type string
			String dataType = getDataTypeFromXsdString(property.getRangeUri());

			// get XACML operator based on data type
			dataType = getBaseType(dataType);
			return getXacmlOperatorConfigForType(operator, dataType);
		} else {
			// It is 'ObjectProperty'

			List<CasmObject.MinimalCasmObject> instances = property.getInstances();
			if (instances!=null && instances.size()>0) {
				if ("EQUALS".equalsIgnoreCase(operator))
					return getXacmlOperatorConfigForType(operator, "string");
				else
					throw new IllegalArgumentException("Unsupported operator for ObjectProperty with instances: operator" + operator + ", property=" + property.getName());
			} else {
				return getXacmlOperatorConfigForType(operator, "string");
			}
		}
	}

	private String getBaseType(String xsdDataType) {
		return XSD_BASE_TYPE_MAP.getOrDefault(xsdDataType, xsdDataType);
	}

	private OperatorConfig getXacmlOperatorConfigForType(String operator, String type) {
		Objects.requireNonNull(operator, "Null operator");
		Objects.requireNonNull(operator, "Null type");
		Map<String, OperatorConfig> operatorsOfType = OPERATOR_MAP.get(type);
		if (operatorsOfType==null)
			throw new IllegalArgumentException("Unknown type: "+type);
		OperatorConfig xacmlFunctionConfig = operatorsOfType.get(operator);
		if (xacmlFunctionConfig==null)
			throw new IllegalArgumentException("Unknown operator: "+operator);
		return xacmlFunctionConfig;
	}

	private String getXacmlOperatorForType(String operator, String type) {
		String xacmlFunction = getXacmlOperatorConfigForType(operator, type).getXacmlUri();
		if (xacmlFunction==null)
			throw new IllegalArgumentException("Unknown operator: "+operator);
		return xacmlFunction;
	}

	private String getXsdStringLastPart(String xsdDataType) {
		String dataType = xsdDataType;
		int p = Math.max( Math.max( xsdDataType.lastIndexOf(":"), xsdDataType.lastIndexOf("#")), xsdDataType.lastIndexOf("/") );
		if (p>0) dataType = dataType.substring(p+1);
		return dataType;
	}

	private String getDataTypeFromXsdString(String xsdDataType) {
		return getXsdStringLastPart(xsdDataType).toLowerCase();
	}

	private String getObjectCasmPath(CasmObject o) {
		if (o == null) return null;
		StringBuilder sb = new StringBuilder();
		return getObjectCasmPath(sb, o, ":").toString();
	}

	private StringBuilder getObjectCasmPath(StringBuilder sb, CasmObject o, String delimiter) {
		String objectName = prepareName(o.getName());
		CasmObject parent = o.getParent();
		if (parent!=null)
			getObjectCasmPath(sb, parent, delimiter)
					.append(delimiter)
					.append(objectName);
		else
			sb.append(objectName);
		return sb;
	}

	private String prepareName(String str) {
		return str.replaceAll("[^A-Za-z0-9_-]+", "_");
	}
}
