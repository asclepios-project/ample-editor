/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.interpreter;

import eu.asclepios.ample.model.abe.AbePolicy;
import eu.asclepios.ample.model.expr.CompositeExpression;
import eu.asclepios.ample.model.expr.Expression;
import eu.asclepios.ample.model.expr.SimpleExpression;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Slf4j
@Service
public class InterpreterAbePolicyToText implements Interpreter<AbePolicy, String> {
	@Override
	public String interpret(AbePolicy abePolicy) {
		StringBuilder sb = new StringBuilder();
		writeExpression(sb, abePolicy.getPolicyExpression(), 0);

		return sb.toString();
	}

	private StringBuilder ident(StringBuilder sb, int num) {
		for (int i=0; i<num; i++)
			sb.append("    ");
		return sb;
	}

	private void writeExpression(StringBuilder sb, Expression expression, int ident) {
		if (expression instanceof SimpleExpression)
			writeSimpleExpression(sb, (SimpleExpression)expression, ident);
		if (expression instanceof CompositeExpression)
			writeCompositeExpression(sb, (CompositeExpression)expression, ident);
	}

	private void writeSimpleExpression(StringBuilder sb, SimpleExpression expression, int ident) {
		ident(sb, ident).append("(")
				.append(expression.getAttributeRef().getName())
				.append(".")
				.append(expression.getPropertyRef().getName())
				.append(" ")
				.append( getTextOperator(expression.getOperator()) )
				.append(" ")
				.append(expression.getValue())
				.append(")\n");
	}

	private void writeCompositeExpression(StringBuilder sb, CompositeExpression expression, int ident) {
		boolean first = true;
		ident(sb, ident).append("(\n");
		for (Expression expr : expression.getExpressions()) {
			if (first)
				first = false;
			else
				ident(sb, ident+1).append(expression.getOperator()).append("\n");
			writeExpression(sb, expr, ident+1);
		}
		ident(sb, ident).append(")\n");
	}

	private String getTextOperator(String operator) {
		Objects.requireNonNull(operator, "Null operator");
		if ("EQUALS".equalsIgnoreCase(operator)) return "=";
		if ("NOT_EQUALS".equalsIgnoreCase(operator)) return "<>";
		if ("GREATER_THAN".equalsIgnoreCase(operator)) return ">";
		if ("GREATER_OR_EQUAL".equalsIgnoreCase(operator)) return ">=";
		if ("LESS_THAN".equalsIgnoreCase(operator)) return "<";
		if ("LESS_OR_EQUAL".equalsIgnoreCase(operator)) return "<=";
		throw new IllegalArgumentException("Unknown operator: "+operator);
	}
}