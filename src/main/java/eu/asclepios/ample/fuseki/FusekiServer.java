/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.fuseki;

import eu.asclepios.ample.AmpleApplicationProperties;
import eu.asclepios.ample.datastore.Datastore;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.jena.fuseki.server.Operation;
import org.apache.jena.query.Dataset;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.event.ContextStoppedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
@ConditionalOnProperty(
        value = "ample.server.enable",
        havingValue = "true")
public class FusekiServer implements InitializingBean {
    private final AmpleApplicationProperties properties;
    private final Datastore datastore;

    private org.apache.jena.fuseki.main.FusekiServer server;
    private Dataset dataset;
    private String queryEndpoint;
    private String updateEndpoint;
    private String uploadEndpoint;

    @Override
    public void afterPropertiesSet() {
        startServer();
        queryEndpoint = getEndpointFor(Operation.Query);
        updateEndpoint = getEndpointFor(Operation.Update);
        uploadEndpoint = getEndpointFor(Operation.Upload);
    }

    @EventListener
    public void onApplicationEvent(ContextStoppedEvent event) {
        stopServer();
    }

    String getQueryEndpoint() { return queryEndpoint; }
    String getUpdateEndpoint() { return updateEndpoint; }
    String getUploadEndpoint() { return uploadEndpoint; }

    private String getEndpointFor(Operation operation) {
        String service = properties.getServer().getServiceName();
        log.debug("getEndpointFor(): operation={}, service={}", operation, service);
        if (!service.startsWith("/")) service = "/"+service;
        String url = server.getJettyServer().getURI()+service+"/"+
            server.getDataAccessPointRegistry()
                    .get(service)
                    .getDataService()
                    .getEndpoints(operation)
                    .stream()
                    .filter(e -> operation.getName().toLowerCase().equalsIgnoreCase(e.getName()))
                    .findFirst()
                    .get()
                    .getName();
        log.debug("getEndpointFor(): url={}", url);
        return url;
    }

    public boolean isRunning() { return server!=null; }

    private synchronized void startServer() {
        // check if fuseki server is already running
        if (server!=null) {
            log.warn("Fuseki server is already running.");
            return;
        }

        // start fuseki server
        log.info("Fuseki server configuration: {}", properties);
        log.info("Starting Fuseki server...");
        dataset = datastore.getDataset();
        String serviceName = properties.getServer().getServiceName();
        server = org.apache.jena.fuseki.main.FusekiServer.create()
                .port(properties.getServer().getPort())
                .loopback(properties.getServer().isLoopbackAccess())
                .verbose(properties.getServer().isVerbose())
                .add(serviceName, dataset, properties.getServer().isAllowUpdates())
                .addOperation(serviceName, Operation.Query)
                .addOperation(serviceName, Operation.Update)
                .build();
        server.start();
        log.info("Fuseki server started");
    }

    private void stopServer() {
        // check if fuseki server is down
        if (server==null) {
            log.warn("Fuseki server is not running.");
            return;
        }

        // stop fuseki server
        log.info("Stopping Fuseki server...");
        server.stop();
        server = null;
        dataset = null;
        log.info("Fuseki server stopped");
    }
}