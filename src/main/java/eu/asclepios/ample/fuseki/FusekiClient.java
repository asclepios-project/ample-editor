/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.fuseki;

import eu.asclepios.ample.AmpleApplicationProperties;
import eu.asclepios.ample.persistence.SparqlServiceClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.update.UpdateExecutionFactory;
import org.apache.jena.update.UpdateProcessor;
import org.apache.jena.update.UpdateRequest;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import java.util.*;

@Slf4j
@Service
@RequiredArgsConstructor
@ConditionalOnProperty(
		value = "ample.server.enable",
		havingValue = "true")
public class FusekiClient implements SparqlServiceClient {
	@Nullable
	private final FusekiServer server;
	private final AmpleApplicationProperties properties;

	private AmpleApplicationProperties.FusekiEndpoints getEndpoints() { return properties.getClient().getEndpoints(); }

	public String getSelectEndpoint() {
		if (StringUtils.isNotBlank(getEndpoints().getQuery())) return getEndpoints().getQuery();
		if (server!=null) return server.getQueryEndpoint();
		return null;
	}
	public String getUpdateEndpoint() {
		if (StringUtils.isNotBlank(getEndpoints().getUpdate())) return getEndpoints().getUpdate();
		if (server!=null) return server.getUpdateEndpoint();
		return null;
	}
	public String getUploadEndpoint() {
		if (StringUtils.isNotBlank(getEndpoints().getUpload())) return getEndpoints().getUpload();
		if (server!=null) return server.getUploadEndpoint();
		return null;
	}

	public void execute(String sparqlUpdate) {
		long startTm = System.nanoTime();
		log.debug("execute(): Query=\n"+sparqlUpdate);
		UpdateRequest updReq = new UpdateRequest();
		updReq.add(sparqlUpdate);
		UpdateProcessor upInsertData = UpdateExecutionFactory.createRemote(updReq, getUpdateEndpoint());
		
		// Perform the SPARQL Update operation
		upInsertData.execute();
		sumElapsedTime( System.nanoTime()-startTm );
	}
	
	public Map<String,String> queryBySubject(String subjectUri) {
		long startTm = System.nanoTime();
		String qry = String.format("SELECT ?p ?o WHERE { <%s> ?p ?o }", subjectUri);
		log.debug("queryBySubject(): Query=\n{}", qry);
		QueryExecution qeSelect = query(qry);
		try {
			ResultSet resultSet = qeSelect.execSelect();
			
			// Iterating over the SPARQL Query results
			Map<String,String> results = new HashMap<String,String>();
			while (resultSet.hasNext()) {
				QuerySolution soln = resultSet.nextSolution();
				String p = soln.get("?p").asResource().getURI();
				RDFNode oNd = soln.get("?o");
				String o;
				if (oNd.isLiteral()) {
					org.apache.jena.rdf.model.Literal lit = oNd.asLiteral();
					if (lit.getDatatypeURI()!=null && !lit.getDatatypeURI().isEmpty()) {
						o = String.format("\"%s\"^^<%s>", lit.getLexicalForm(), lit.getDatatypeURI());
					} else {
						o = String.format("\"%s\"", lit.getLexicalForm());
					}
					// append language suffix if specified
					if (!lit.getLanguage().isEmpty()) {
						String langSuffix = "@"+lit.getLanguage();
						o += langSuffix;
						p += langSuffix;
					}
				} else o = "<"+oNd.asResource().getURI()+">";
				
				if (results.containsKey(p)) o = results.get(p)+", "+o;
				results.put(p, o);
			}
			log.debug("queryBySubject(): Results=\n{}", results);
			return results;
		} finally {
			qeSelect.close();
			sumElapsedTime( System.nanoTime()-startTm );
		}
	} // end method
	
	public List<Map<String,RDFNode>> queryAndProcess(String selectQuery) {
		long startTm = System.nanoTime();
		log.debug("queryAndProcess(): Query=\n{}", selectQuery);
		QueryExecution qeSelect = query(selectQuery);
		try {
			ResultSet results = qeSelect.execSelect();
			
			// Iterating over the SPARQL Query results
			List<Map<String,RDFNode>> resultsList = new LinkedList<Map<String,RDFNode>>();
			while (results.hasNext()) {
				QuerySolution soln = results.nextSolution();
				HashMap<String,RDFNode> row = new HashMap<String,RDFNode>();
				Iterator<String> it = soln.varNames();
				while (it.hasNext()) {
					String varName = it.next();
					RDFNode value = soln.get(varName);
					row.put(varName, value);
				}
				resultsList.add(row);
			}
			log.debug("queryAndProcess(): Results=\n{}", resultsList);
			return resultsList;
		} finally {
			qeSelect.close();
			sumElapsedTime( System.nanoTime()-startTm );
		}
	} // end method
	
	public List<String> queryForIds(String selectQuery, String idCol) {
		long startTm = System.nanoTime();
		log.debug("queryForIds(): Query=\n{}", selectQuery);
		QueryExecution qeSelect = query(selectQuery);
		try {
			ResultSet results = qeSelect.execSelect();
			
			// Iterating over the SPARQL Query results
			List<String> resultsList = new LinkedList<String>();
			while (results.hasNext()) {
				QuerySolution soln = results.nextSolution();
				if (idCol==null) {
					Iterator<String> it = soln.varNames();
					while (idCol==null && it.hasNext()) idCol = it.next();
				}
				RDFNode node = soln.get(idCol);
				String id;
				if (node.isLiteral()) id = node.asLiteral().getLexicalForm();
				else id = "<"+node.asResource().getURI()+">";
				resultsList.add(id);
			}
			log.debug("queryForIds(): Results=\n{}", resultsList);
			return resultsList;
		} finally {
			qeSelect.close();
			sumElapsedTime( System.nanoTime()-startTm );
		}
	} // end method
	
	public QueryExecution query(String selectQuery) {
		log.debug("query(): Query=\n{}", selectQuery);
		return _query(selectQuery, true);
	}
	
	protected QueryExecution _query(String selectQuery, boolean updateStats) {
		return QueryExecutionFactory.sparqlService(getSelectEndpoint(), selectQuery);
	} // end method
	
	public Object queryValue(String selectQuery) {
		long startTm = System.nanoTime();
		log.debug("queryValue(): Query=\n{}", selectQuery);
		QueryExecution qeSelect = _query(selectQuery, false);
		try {
			ResultSet rs = qeSelect.execSelect();
			if (rs.hasNext()) {
				QuerySolution soln = rs.next();
				Iterator<String> it = soln.varNames();
				if (it.hasNext()) {
					String key = it.next();
					RDFNode val = soln.get(key);
					log.debug("queryValue(): Result={}", val);
					return val;
				}
			}
			log.debug("queryValue(): Result=null");
			return null;
		} finally {
			qeSelect.close();
			sumElapsedTime( System.nanoTime()-startTm );
		}
	} // end method

	public boolean ask(String askQuery) {
		long startTm = System.nanoTime();
		log.debug("ask(): Query=\n{}", askQuery);
		QueryExecution qeAsk = query(askQuery);
		try {
			return qeAsk.execAsk();
		} finally {
			qeAsk.close();
			sumElapsedTime( System.nanoTime()-startTm );
		}
	} // end method
	
	// Time measurement methods and variables
	protected static long cntSplits = 0;
	protected static long sumElapsedTime = 0;
	protected static long minElapsedTime = Long.MAX_VALUE;
	protected static long maxElapsedTime = 0;
	private static Object statsLock = new Object();
	
	protected static void sumElapsedTime( long elapsedTime ) {
		synchronized (statsLock) {
			cntSplits++;
			sumElapsedTime += elapsedTime;
			if (minElapsedTime>elapsedTime) minElapsedTime = elapsedTime;
			if (maxElapsedTime<=elapsedTime) maxElapsedTime = elapsedTime;
		}
	}
	
	public static long getSplitCount() { return cntSplits; }
	public static long getTotalElapsedTime() { return sumElapsedTime; }
	public static long getMinElapsedTime() { return minElapsedTime; }
	public static long getMaxElapsedTime() { return maxElapsedTime; }
	public static void resetTimers() {
		synchronized(statsLock) {
			cntSplits = 0; sumElapsedTime = 0; minElapsedTime = Long.MAX_VALUE; maxElapsedTime = 0;
		}
	}
}