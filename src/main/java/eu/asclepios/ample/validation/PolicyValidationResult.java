/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.validation;

import eu.asclepios.ample.model.AbstractObject;
import eu.asclepios.ample.model.expr.Expression;
import eu.asclepios.ample.model.validation.PolicyValidationObject;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.UUID;

@Slf4j
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PolicyValidationResult {
    //private final String id = UUID.randomUUID().toString();
    private AbstractObject objectChecked;
    private PolicyValidationObject validationObject;
    @Builder.Default private MESSAGE_TYPE messageType = MESSAGE_TYPE.ERROR;
    private String message;

    public enum MESSAGE_TYPE { ERROR, WARNING, INFO, DEBUG }
}
