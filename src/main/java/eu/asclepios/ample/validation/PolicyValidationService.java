/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.validation;

import eu.asclepios.ample.model.AbstractObject;
import eu.asclepios.ample.model.abac.AbacPolicy;
import eu.asclepios.ample.model.abac.AbacRule;
import eu.asclepios.ample.model.abe.AbePolicy;
import eu.asclepios.ample.model.casm.CasmObject;
import eu.asclepios.ample.model.expr.CompositeExpression;
import eu.asclepios.ample.model.expr.Expression;
import eu.asclepios.ample.model.expr.SimpleExpression;
import eu.asclepios.ample.model.validation.POLICY_VALIDATION_TYPE;
import eu.asclepios.ample.model.validation.PolicyValidationObject;
import eu.asclepios.ample.persistence.RdfPersistenceManager;
import eu.asclepios.ample.persistence.RdfPersistenceManagerFactory;
import eu.asclepios.ample.rest.AbacPolicyRestController;
import eu.asclepios.ample.rest.AbePolicyRestController;
import eu.asclepios.ample.rest.PolicyValidationRestController;
import eu.asclepios.ample.validation.naive.NaiveValidationCheckImplementation;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class PolicyValidationService implements ApplicationContextAware {
    @Autowired
    private AbacPolicyRestController abacPolicyRestController;
    @Autowired
    private AbePolicyRestController abePolicyRestController;
    //private PolicyValidationRestController policyValidationRestController;
    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public List<PolicyValidationResult> validateAbacPolicy(@NotNull AbacPolicy policy) throws Exception {
        log.debug("validateAbacPolicy: Validating ABAC policy using all active policy validation objects: policy={}", policy.getId());

        // Retrieve all active policy validation objects
        List<PolicyValidationObject> objects = getActivePolicyValidationObjects();
        log.debug("validateAbacPolicy: Retrieved {} active policy validation objects: {}", objects.size(), objects);

        // Validate ABAC policy using the active policy validation objects
        List<PolicyValidationResult> results = new ArrayList<>();
        for (PolicyValidationObject object : objects) {
            results.addAll( validatePolicyWithPolicyValidationObject(policy, object) );
        }

        // Return policy validation results
        log.debug("validateAbacPolicy: Policy validation results: {}", results);
        return results;
    }

    public List<PolicyValidationResult> validateAbePolicy(@NotNull AbePolicy policy) throws Exception {
        log.debug("validateAbePolicy: Validating ABE policy using all active policy validation objects: policy={}", policy.getId());

        // Retrieve all active policy validation objects
        List<PolicyValidationObject> objects = getActivePolicyValidationObjects();
        log.debug("validateAbePolicy: Retrieved {} active policy validation objects: {}", objects.size(), objects);

        // Validate ABE policy using the active policy validation objects
        List<PolicyValidationResult> results = new ArrayList<>();
        for (PolicyValidationObject object : objects) {
            results.addAll( validatePolicyWithPolicyValidationObject(policy, object) );
        }

        // Return policy validation results
        log.debug("validateAbePolicy: Policy validation results: {}", results);
        return results;
    }

    private List<PolicyValidationObject> getActivePolicyValidationObjects() throws Exception {
        PolicyValidationRestController policyValidationRestController = applicationContext.getBean(PolicyValidationRestController.class);
        return Arrays.stream(policyValidationRestController.getAllPolicyValidationObjects())
                .filter(PolicyValidationObject::isEnabled)
                .filter(PolicyValidationObject::isValidationRule)
                .collect(Collectors.toList());
    }

    public List<PolicyValidationResult> validatePoliciesWithPolicyValidationObject(@NotNull PolicyValidationObject object) throws Exception {
        log.debug("validatePoliciesWithPolicyValidationObject: Validating policies with: {}", object);

        // Retrieve policies
        AbacPolicy[] abacPolicies = abacPolicyRestController.getAllPolicies();
        AbePolicy[] abePolicies = abePolicyRestController.getAllPolicies();
        log.debug("validatePoliciesWithPolicyValidationObject: Retrieved ABAC policies: {}", Arrays.asList(abacPolicies));
        log.debug("validatePoliciesWithPolicyValidationObject: Retrieved ABE policies: {}", Arrays.asList(abePolicies));

        // Validate each policy using the given policy validation object
        List<PolicyValidationResult> results = new ArrayList<>();
        for (AbacPolicy abacPolicy : abacPolicies) {
            results.addAll( validatePolicyWithPolicyValidationObject(abacPolicy, object) );
        }
        for (AbePolicy abePolicy : abePolicies) {
            results.addAll( validatePolicyWithPolicyValidationObject(abePolicy, object) );
        }

        // Return policy validation results
        log.debug("validatePoliciesWithPolicyValidationObject: RESULTS: {}", results);
        return results;
    }

    public List<PolicyValidationObject> getValidationRules(@NotNull PolicyValidationObject object) throws Exception {
        RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();
        return getValidationRules(object, pm);
    }

    public List<PolicyValidationObject> getValidationRules(@NotNull PolicyValidationObject object, @NotNull RdfPersistenceManager pm) throws Exception {
        // Check if object is enabled
        if (! object.isEnabled())
            return Collections.emptyList();

        // If object is a rule, return it
        if (POLICY_VALIDATION_TYPE.POLICY_VALIDATION_RULE_NAME.equals(object.getType()))
            return Collections.singletonList(object);

        // If object is NOT a rule then process its children
        List<PolicyValidationObject> rules = new ArrayList<>();
        PolicyValidationRestController policyValidationRestController = applicationContext.getBean(PolicyValidationRestController.class);
        List<PolicyValidationObject> children = policyValidationRestController.getChildren(object, pm);
        for (PolicyValidationObject child : children) {
            rules.addAll( getValidationRules(child, pm) );
        }
        return rules;
    }

    public List<PolicyValidationResult> validatePolicyWithPolicyValidationObject(AbacPolicy policy, PolicyValidationObject object) throws Exception {
        List<PolicyValidationObject> rules = getValidationRules(object);
        List<PolicyValidationResult> results = rules.stream()
                .flatMap(rule -> validatePolicyWithRule(policy, rule).stream())
                .collect(Collectors.toList());
        return results;
    }

    public List<PolicyValidationResult> validatePolicyWithPolicyValidationObject(AbePolicy policy, PolicyValidationObject object) throws Exception {
        List<PolicyValidationObject> rules = getValidationRules(object);
        List<PolicyValidationResult> results = rules.stream()
                .flatMap(rule -> validatePolicyWithRule(policy, rule).stream())
                .collect(Collectors.toList());
        return results;
    }

    private List<PolicyValidationResult> validatePolicyWithRule(AbacPolicy policy, PolicyValidationObject validationRule) {
        AbacRule[] policyRules = abacPolicyRestController.getPolicyRules(policy.getId());
        List<Expression> ruleConditions = Arrays.stream(policyRules).map(AbacRule::getRuleCondition).collect(Collectors.toList());
        List<PolicyValidationResult> results;

        PolicyValidationChecker checker = getPolicyValidationChecker(validationRule);
        if ((results=checker.checkExistenceOf(ruleConditions, policy))!=null) {
            return results;
        }

        results = new ArrayList<>();
        for (AbacRule abacRule : policyRules) {
            if (abacRule.getRuleCondition()!=null) {
                results.addAll( validateExpressionWithRule(abacRule.getRuleCondition(), abacRule, validationRule) );
            }
        }
        return results;
    }

    private List<PolicyValidationResult> validatePolicyWithRule(AbePolicy policy, PolicyValidationObject validationRule) {
        if (policy.getPolicyExpression()!=null) {
            return validateExpressionWithRule(policy.getPolicyExpression(), policy, validationRule);
        }
        return Collections.emptyList();
    }

    private List<PolicyValidationResult> validateExpressionWithRule(Expression expression, AbstractObject objectChecked, PolicyValidationObject validationRule) {
        PolicyValidationChecker checker = getPolicyValidationChecker(validationRule);
        List<PolicyValidationResult> results = checker.validateExpression(expression, objectChecked);
        if (! results.isEmpty()) return results;

        return Collections.emptyList();
    }

    private PolicyValidationChecker getPolicyValidationChecker(PolicyValidationObject validationRule) {
        return new NaiveValidationCheckImplementation(validationRule);
    }
}