/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.validation.naive;

import eu.asclepios.ample.model.AbstractObject;
import eu.asclepios.ample.model.casm.CasmObject;
import eu.asclepios.ample.model.expr.CompositeExpression;
import eu.asclepios.ample.model.expr.Expression;
import eu.asclepios.ample.model.expr.SimpleExpression;
import eu.asclepios.ample.model.validation.PolicyValidationObject;
import eu.asclepios.ample.validation.PolicyValidationChecker;
import eu.asclepios.ample.validation.PolicyValidationException;
import eu.asclepios.ample.validation.PolicyValidationResult;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Getter
public class NaiveValidationCheckImplementation implements PolicyValidationChecker {
    private final PolicyValidationObject validationRule;
    private boolean not = false;
    private boolean checkAbac = false;
    private boolean checkAbe = false;
    private boolean checkExists = false;
    private boolean searchAttr = false;
    private boolean searchProp = false;
    private boolean searchOper = false;
    private boolean searchExpr = false;
    private String lookFor = null;
    private String lookFor2 = null;
    private PolicyValidationResult.MESSAGE_TYPE messageType;

    public NaiveValidationCheckImplementation(PolicyValidationObject validationRule) {
        this.validationRule = validationRule;
        parseValidationRule(validationRule);
        //messageType = getMessageTypeBasedOnGroupId(validationRule);
    }

    private void parseValidationRule(PolicyValidationObject validationRule) {
        //String vExprStr = validationRule.getDescription();
        String vExprStr = validationRule.getPolicyValidationRuleDefinition();
        if (StringUtils.isNotBlank(vExprStr)) {
            log.warn(">>>>>>  VALIDATION-EXPR STRING: {}", vExprStr);
            String[] term = vExprStr.split("[ \t\r\n]+");
            log.warn(">>>>>>  TERMS: {}", Arrays.asList(term));

            int pos = 0;

            if (term.length>2 && "WHEN".equals(term[1].trim().toUpperCase())) {
                try {
                    messageType = PolicyValidationResult.MESSAGE_TYPE
                            .valueOf(term[0].trim().toUpperCase());
                    pos += 2;
                } catch (IllegalArgumentException ex) {
                    throw new PolicyValidationException("Invalid OUTCOME in RULE EXPRESSION: "+vExprStr, ex);
                }
            } else {
                throw new PolicyValidationException("Missing OUTCOME in RULE EXPRESSION: "+vExprStr);
            }

            if ("NO".equalsIgnoreCase(term[pos])) {
                log.warn(">>>>>>  TERM 'NO' ENCOUNTERED");
                not = true;
                pos++;
            }

            if ("ABAC-RULE".equalsIgnoreCase(term[pos])) {
                checkAbac = true;
                pos++;
            } else if ("ABE-POLICY".equalsIgnoreCase(term[pos])) {
                checkAbe = true;
                pos++;
            } else if ("ANY".equalsIgnoreCase(term[pos])) {
                checkAbac = checkAbe = true;
                pos++;
            } else {
                throw new PolicyValidationException("Invalid validation TARGET in RULE EXPRESSION: "+vExprStr);
            }

            if ("EXISTS".equalsIgnoreCase(term[pos])) {
                checkExists = true;
                pos++;
            } else
            if ("NOT-EXISTS".equalsIgnoreCase(term[pos])) {
                checkExists = false;
                pos++;
            } else {
                throw new PolicyValidationException("Invalid validation OPERATION in RULE EXPRESSION: "+vExprStr);
            }

            if ("WITH".equalsIgnoreCase(term[pos]))
                pos++;

            if ("ATTRIBUTE".equalsIgnoreCase(term[pos])) {
                searchAttr = true;
                pos++;
            } else if ("PROPERTY".equalsIgnoreCase(term[pos])) {
                searchProp = true;
                pos++;
            } else if ("OPERATOR".equalsIgnoreCase(term[pos])) {
                searchOper = true;
                pos++;
            } else if ("EXPRESSION".equalsIgnoreCase(term[pos])) {
                searchExpr = true;
                pos++;
            } else {
                throw new PolicyValidationException("Invalid validation SOURCE TYPE in RULE EXPRESSION: "+vExprStr);
            }

            //lookFor = term[pos].trim();
            lookFor = expressionToNormalizedString(validationRule.getPolicyValidationExpression()).trim();
            lookFor2 = expressionToNormalizedString(validationRule.getPolicyValidationExpression(), false).trim();
            if (lookFor==null || lookFor.trim().isEmpty())
                throw new PolicyValidationException("Missing SOURCE EXPRESSION in RULE EXPRESSION: "+vExprStr);
            if (searchAttr)
                lookFor = lookFor.trim().split("[ \\.]")[0];
            if (searchProp)
                lookFor = lookFor.trim().split("[ ]")[0];
            if (searchOper) {
                String t[] = lookFor.trim().split("[ \\.]");
                lookFor = t[0]+"."+t[1]+" "+t[2];
            }

            log.warn(">>>>>>  Search settings: is-no={}, look-in-abac={}, look-in-abe={}, check-exists={}, look-for-attr={}, look-for-prop={}, look-for-oper={}, look-for-expr={}, look-for={} / {}",
                    not, checkAbac, checkAbe, checkExists, searchAttr, searchProp, searchOper, searchExpr, lookFor2, lookFor);
        }
    }

    /*private PolicyValidationResult.MESSAGE_TYPE getMessageTypeBasedOnGroupId(PolicyValidationObject validationRule) {
        // Get group id and message type
        PolicyValidationObject n = validationRule;
        while (n.getParent() != null) n = n.getParent();
        String groupId = n.getId();
        return "SECURITY-AWARENESS".equals(groupId)
                ? PolicyValidationResult.MESSAGE_TYPE.WARNING
                : PolicyValidationResult.MESSAGE_TYPE.ERROR;
    }*/

    public List<PolicyValidationResult> checkExistenceOf(List<Expression> expressions, AbstractObject objectChecked) {
        boolean found = false;
        for (Expression expr : expressions) {
            if (expr == null) continue;
            if (searchAttr || searchProp || searchOper)
                found = attributeExists(expr, lookFor, lookFor2, searchOper?3:(searchProp?2:1));
            if (searchExpr)
                found = expressionExists(expr, lookFor, lookFor2);

            if (found) break;
        }

        String searchType = searchAttr ? "ATTRIBUTE" : (searchProp ? "PROPERTY" : (searchOper ? "OPERATOR" : "EXPRESSION"));
        String messageFmt;
        if (!found && !checkExists) messageFmt = "NOT FOUND EXPECTED %s: %s";
        else if (found && !checkExists) messageFmt = "";
        else if (!found && checkExists) messageFmt = "";
        else messageFmt = "FOUND %s WHILE NOT EXPECTED: %s";
        log.debug(">>>>>>  Selected message-format: {}", messageFmt);

        String message = String.format(messageFmt, searchType, lookFor2);
        log.debug(">>>>>>  Resulting message: {}", message);
        return message.isEmpty()
                ? Collections.emptyList()
                : Collections.singletonList(
                        new PolicyValidationResult(objectChecked, validationRule, messageType, message));
    }

    public List<PolicyValidationResult> validateExpression(Expression expression, AbstractObject objectChecked) {
        boolean result = false;
        if (searchAttr || searchProp || searchOper)
            result = attributeExists(expression, lookFor, lookFor2, searchOper?3:(searchProp?2:1));
        if (searchExpr)
            result = expressionExists(expression, lookFor, lookFor2);

        if (not) result = !result;

        log.warn(">>>>>>>  Search result: {}", result);
        if (!result) {
            String searchType = searchAttr ? "ATTRIBUTE" : (searchProp ? "PROPERTY" : (searchOper ? "OPERATOR" : "EXPRESSION"));
            if (!not) {
                log.warn(">>>>>>>  Search result: NOT FOUND EXPECTED {}: {}", searchType, lookFor2);
                return Collections.singletonList(new PolicyValidationResult(objectChecked, validationRule,
                        messageType, "NOT FOUND EXPECTED "+searchType+": " + lookFor2));
            } else {
                log.warn(">>>>>>>  Search result: FOUND {} WHILE NOT EXPECTED: {}", searchType, lookFor2);
                return Collections.singletonList(new PolicyValidationResult(objectChecked, validationRule,
                        messageType, "FOUND "+searchType+" WHILE NOT EXPECTED: " + lookFor2));
            }
        }
        return Collections.emptyList();
    }

    private boolean attributeExists(Expression expr, String lookFor, String lookFor2, int numTerms) {
        log.warn(">>>>>>  attributeExists: expr={}, look-for={} / {}", expr.getName(), lookFor2, lookFor);
        if (expr instanceof SimpleExpression) {
            //log.warn(">>>>>>  attributeExists: SIMPLE ATTR");
            CasmObject attr = ((SimpleExpression) expr).getAttributeRef();
            CasmObject prop = ((SimpleExpression) expr).getPropertyRef();
            String operator = ((SimpleExpression) expr).getOperator();
            String value = ((SimpleExpression) expr).getValue();

            //String[] term = lookFor.split("[ \\.]+");
            String[] term = lookFor2.split("[ \\.]+");
            log.warn(">>>>>>  attributeExists: attr={}, prop={}, operator={}, value={}, terms={}",
                    attr.getName(), prop.getName(), operator, value, term);

            if (term.length<numTerms) return false;
            if (numTerms>0 && !term[0].equalsIgnoreCase(attr.getName())) return false;
            if (numTerms>1 && !term[1].equalsIgnoreCase(prop.getName())) return false;
            if (numTerms>2 && !term[2].equalsIgnoreCase(operator)) return false;
            if (numTerms>3 && !term[3].equalsIgnoreCase(value)) return false;
            return true;
        } else if (expr instanceof CompositeExpression) {
            //log.warn(">>>>>>  attributeExists: COMPOSITE ATTR");
            for (Expression e : ((CompositeExpression) expr).getExpressions())
                if (attributeExists(e, lookFor, lookFor2, numTerms))
                    return true;
        }
        return false;
    }

    private boolean expressionExists(Expression expression, String lookFor, String lookFor2) {
        log.warn(">>>>>>  expressionExists: Normalized expression string: {}", expressionToNormalizedString(expression, false));
        log.warn(">>>>>>  expressionExists: Normalized lookup string: {}", lookFor2);
        lookFor = lookFor.replaceAll("[ \t\r\n]+", " ");
        lookFor = " "+lookFor.trim()+" ";
        String exprStr = expressionToNormalizedString(expression);
        exprStr = " "+exprStr.trim()+" ";
        boolean result = (StringUtils.indexOfIgnoreCase(exprStr, lookFor) >= 0);
        log.warn(">>>>>>  expressionExists: Lookup result: {}", result);
        return result;
    }

    private String expressionToNormalizedString(Expression expression) {
        return expressionToNormalizedString(expression, true);
    }

    private String expressionToNormalizedString(Expression expression, boolean encodeTerms) {
        if (expression instanceof CompositeExpression) {
            // it is CompositeExpression
            CompositeExpression ce = (CompositeExpression) expression;
            String operator = " " + ce.getOperator().trim() + " ";
            return ce.getExpressions().stream()
                    .map(e -> expressionToNormalizedString(e, encodeTerms))
                    .collect(Collectors.joining(operator));
        } else {
            // it is SimpleExpression
            SimpleExpression se = (SimpleExpression) expression;
            String attr = encodeString(se.getAttributeText().trim(), encodeTerms);
            String prop = encodeString(se.getPropertyText(), encodeTerms);
            String oper = encodeString(se.getOperator(), encodeTerms);
            String val = encodeString(se.getValue(), encodeTerms);

            if (StringUtils.isNotBlank(prop))
                attr = String.join(".", attr, prop.trim());

            if (StringUtils.isNotBlank(oper) && StringUtils.isNotBlank(val))
                return attr + " " + oper.trim() + " " + val.trim();
            else if (StringUtils.isNotBlank(oper) && StringUtils.isBlank(val))
                return attr + " " + oper.trim();

            return attr;
        }
    }

    private String encodeString(String s, boolean encodeTerms) {
        if (s==null) return null;
        if (s.isEmpty()) return s;
        if (encodeTerms) s = Base64.getEncoder().encodeToString(s.getBytes(StandardCharsets.UTF_8));
        return s;
    }

    private String decodeString(String s) {
        if (s==null) return null;
        if (s.isEmpty()) return s;
        return new String(Base64.getDecoder().decode(s.getBytes(StandardCharsets.UTF_8)), StandardCharsets.UTF_8);
    }
}