/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.util;

import eu.asclepios.ample.model.AbstractObject;
import eu.asclepios.ample.model.RootObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.util.HtmlUtils;

import java.beans.Introspector;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Objects;

@Slf4j
public class EscapeDataUtil {
	public static void escapeData(AbstractObject object) {
		try {
			Arrays.stream(Introspector.getBeanInfo(object.getClass(), RootObject.class).getPropertyDescriptors())
					.filter(pd -> Objects.nonNull(pd.getReadMethod()))
					.filter(pd -> Objects.nonNull(pd.getWriteMethod()))
					.forEach(pd -> {
						try {
							Method getter = pd.getReadMethod();
							Method setter = pd.getWriteMethod();
							if (String.class.isAssignableFrom(getter.getReturnType())) {
								String value = (String) getter.invoke(object);
								if (value!=null) {
									String newValue = escapeValue(value);
									try {
										setter.invoke(object, newValue);
										log.debug("escapeData: Escaped value if: {} : {} => {}", pd.getName(), value, newValue);
									} catch (IllegalAccessException | InvocationTargetException e) {
										log.warn("escapeData: EXCEPTION: while invoking setter for: {}, ", pd.getName(), e);
									}
								}
							}
						} catch (IllegalAccessException | InvocationTargetException e) {
							log.warn("escapeData: EXCEPTION: while invoking getter for: {}, ", pd.getName(), e);
						}
					});
		} catch (Exception e) {
			log.warn("escapeData: EXCEPTION: while introspecting: {}, {}", object, e);
		}
	}

	private static String escapeValue(String value) {
		return HtmlUtils.htmlEscape(HtmlUtils.htmlUnescape(value));
	}
}