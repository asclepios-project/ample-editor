/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.util;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.jasypt.util.text.BasicTextEncryptor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import sun.security.x509.*;

import java.io.*;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.*;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.Properties;
import java.util.regex.Pattern;

@Slf4j
public class SecretsUtil {
	private final static char[] possibleCharacters = (new String("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$^&*()-_=+[{]}|;:\'\",<.>/?")).toCharArray();
	private final static int MIN_RANDOM_PASSWORD_SIZE = 8;
	private final static int MAX_RANDOM_PASSWORD_SIZE = 16;
	private final static Pattern usernamePattern = Pattern.compile("^[a-zA-Z0-9_]+$");
	public final static String AMPLE_BASE_NAME = "ample-editor";
	public final static String DEFAULT_CONFIG_DIR = "config";

	public static void main(String[] args) throws IOException {
		// Check if jasypt-decode flag is set
		if (args.length>0 && ("--jasypt-decode".equalsIgnoreCase(args[0]) || "-jd".equalsIgnoreCase(args[0]))) {
			log_info(jasyptDecrypt(askUserInput("Encrypted message", "", false), null));
			return;
		}
		// Check if auto-init flag is set
		String outputDir = checkForAutoInitFlag(args);
		if (outputDir!=null) {
			initializeSecretsAndKeystores(AMPLE_BASE_NAME, outputDir, true);
			return;
		}
		outputDir = args.length>0 ? args[0].trim() : DEFAULT_CONFIG_DIR;
		initializeSecretsAndKeystores(AMPLE_BASE_NAME, outputDir, false);
	}

	static void log_info(String message) {
		System.out.println(message);
	}

	static String getOutputDir(String dir) {
		if (StringUtils.isBlank(dir)) return "";
		dir = dir.trim().replace("\\", "/");
		return (dir.endsWith("/")) ? dir : dir+"/";
	}

	@SneakyThrows
	public static String initializeSecretsAndKeystores(String baseName, String outputDir, boolean noAsk) {
		outputDir = getOutputDir(outputDir);

		// Ask user for secrets
		String keystorePassword = askUserInput("Keystore password", SecretsUtil.randomPassword(), noAsk);
		String truststorePassword = askUserInput("Truststore password", keystorePassword, noAsk);
		String alias = askUserInput("Entry alias", baseName, noAsk);
		String filePrefix = askUserInput("Keystore files prefix", baseName, noAsk);
		String papApiKey = askUserInput("PAP endpoint API Key", "", noAsk);
		String abeApiKey = askUserInput("ABE endpoint API Key", "", noAsk);
		String adminPassword = askUserInput("Admin user password", SecretsUtil.randomPassword(), noAsk);

		// BCrypt encode admin password
		String adminPasswordHash = SecretsUtil.bcrypt(adminPassword);

		// Generate keystores and key pair for AMPLE
		String keystoreFile = filePrefix + "-ks.p12";
		String truststoreFile = filePrefix + "-ts.p12";
		String certificateFile = filePrefix + ".cer";
		String pemFile = filePrefix + ".pem";
		try {
			SecretsUtil.generateKeystore(
					alias, "PKCS12", keystoreFile, keystorePassword,
					truststoreFile, truststorePassword, certificateFile,
					pemFile, outputDir);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		// Ask user for Jasypt password
		if (!noAsk) {
			log_info("\nSet a Jasypt password to enable encryption of secrets. Leave password empty to skip encryption");
			char[] jasyptPassword = null;
			while (jasyptPassword == null) {
				jasyptPassword = askPassword("Jasypt password");
				if (jasyptPassword.length > 0) {
					char[] retype = askPassword("Retype password");
					if (!Arrays.equals(jasyptPassword, retype)) {
						log_info("Passwords do not match. Try again");
						jasyptPassword = null;
					}
				}
			}
			if (jasyptPassword.length > 0) {
				keystorePassword = jasyptEncrypt(keystorePassword, jasyptPassword);
				truststorePassword = jasyptEncrypt(truststorePassword, jasyptPassword);
				papApiKey = jasyptEncrypt(papApiKey, jasyptPassword);
				abeApiKey = jasyptEncrypt(abeApiKey, jasyptPassword);
				log_info("Jasypt encryption enabled");
			} else {
				log_info("Jasypt encryption disabled");
			}
		}

		// Generate secrets file contents
		StringBuilder sb = new StringBuilder("# Generated on " + new Date() + "\n\n");
		sb.append("AMPLE_KEYSTORE_FILE=").append(outputDir).append(keystoreFile).append("\n");
		sb.append("AMPLE_KEYSTORE_PASSWORD=").append(keystorePassword).append("\n");
		sb.append("AMPLE_TRUSTSTORE_FILE=").append(outputDir).append(truststoreFile).append("\n");
		sb.append("AMPLE_TRUSTSTORE_PASSWORD=").append(truststorePassword).append("\n");
		sb.append("AMPLE_KEY_ALIAS=").append(alias).append("\n");
		sb.append("PAP_SERVER_API_KEY=").append(papApiKey).append("\n");
		sb.append("ABE_SERVER_API_KEY=").append(abeApiKey).append("\n");
		sb.append("\n");
		sb.append("# User: admin\n");
		sb.append("ample.users[0].username=admin\n");
		sb.append("ample.users[0].password=").append(adminPasswordHash).append("\n");
		sb.append("ample.users[0].roles=ADMIN\n");

		// Ask if additional users are needed
		if ("y".equalsIgnoreCase(askUserInput("Add more users? (y/n)", "n", noAsk))) {
			// Ask for additional users
			String newUsername = null;
			log_info("Add extra users. Enter dot '.' in username to stop");
			int cnt = 1;
			// Ask new username
			while (StringUtils.isNotBlank(
					(newUsername = askUserInput(
							"Add new user (Enter '.' to stop):\nUsername",
							"user" + cnt, noAsk)))
					&& !".".equals(newUsername.trim()))
			{
				newUsername = newUsername.trim();
				if (usernamePattern.matcher(newUsername).matches()) {
					// Ask new user password
					String password = askUserInput("Password", SecretsUtil.randomPassword(), noAsk);
					if (!password.isEmpty()) {
						// BCrypt new user password
						password = SecretsUtil.bcrypt(password);
						// Append new user settings to secrets
						sb.append(String.format("# User: %s\n", newUsername));
						sb.append(String.format("ample.users[%d].username=%s\n", cnt, newUsername));
						sb.append(String.format("ample.users[%d].password=%s\n", cnt, password));
						sb.append(String.format("ample.users[%d].roles=USER\n", cnt));
						cnt++;
					} else {
						log_info("Password cannot be empty");
					}
				} else {
					log_info("Username is not valid. Legal chars: A-Za-z0-9_");
				}
			}
		}

		// Write secrets file
		String secretsFile = outputDir + filePrefix + "-secret.properties";
		Path secretsPath = Paths.get(secretsFile).toAbsolutePath();
		Files.write(secretsPath, sb.toString().getBytes());
		log_info("Secrets properties saved at: " + secretsPath);
		if (noAsk) log_info("Admin password: " + adminPassword);
		return secretsPath.toString();
	}

	public static String askUserInput(String message, String defValue, boolean noAsk) {
		if (noAsk) return defValue;
		String s = System.console().readLine(String.format("%s [%s]: ", message, defValue));
		return StringUtils.isNotBlank(s) ? s : defValue;
	}

	public static char[] askPassword(String message) {
		return System.console().readPassword("%s: ", message);
	}

	public static String randomPassword() {
		int randomSize = MIN_RANDOM_PASSWORD_SIZE + (int) (Math.random() * (MAX_RANDOM_PASSWORD_SIZE - MIN_RANDOM_PASSWORD_SIZE));
		return randomPassword(randomSize);
	}

	public static String randomPassword(int length) {
		return RandomStringUtils.random(length, 0, possibleCharacters.length - 1, false, false, possibleCharacters, new SecureRandom());
	}

	public static String bcrypt(String input) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder.encode(input);
	}

	public static void generateKeystore(String alias, String type, String keystoreFileName, String keystorePassword, String truststoreFileName, String truststorePassword, String certificateFileName, String pemFileName, String outputDir) throws GeneralSecurityException, IOException {
		outputDir = getOutputDir(outputDir);

		// Create keystore
		KeyStore keyStore = KeyStore.getInstance(
				(StringUtils.isNotBlank(type)) ? type : KeyStore.getDefaultType());
		char[] password = keystorePassword.toCharArray();
		keyStore.load(null, password);

		// Add an entry
		KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
		keyPairGenerator.initialize(4096);
		KeyPair keyPair = keyPairGenerator.generateKeyPair();
		X509Certificate certificate = generateCertificate("cn=" + alias, keyPair, 365, "SHA256withRSA");
		X509Certificate[] chain = {certificate};
		keyStore.setKeyEntry(alias, keyPair.getPrivate(), password, chain);

		// Store keystore to file
		FileOutputStream fos = new FileOutputStream(outputDir + keystoreFileName);
		keyStore.store(fos, password);
		fos.close();
		log_info("Keystore created");

		// Store certificate to file
		FileOutputStream cos = new FileOutputStream(outputDir + certificateFileName);
		cos.write(certificate.getEncoded());
		cos.close();
		log_info("Certificate created (DER)");

		// Convert certificate to PEM format
		if (StringUtils.isNotBlank(pemFileName)) {
			String b64 = Base64.getEncoder().encodeToString(certificate.getEncoded());
			String[] lines = b64.split("(?<=\\G.{64})");
			StringBuilder sb = new StringBuilder();
			sb.append("-----BEGIN CERTIFICATE-----\n");
			Arrays.stream(lines).forEach(line -> sb.append(line).append("\n"));
			sb.append("-----END CERTIFICATE-----");
			String certificatePem = sb.toString();
			cos = new FileOutputStream(outputDir + pemFileName);
			cos.write(certificatePem.getBytes());
			cos.close();
			log_info("Certificate created (PEM)");
		}

		// Create truststore
		KeyStore trustStore = KeyStore.getInstance(
				(StringUtils.isNotBlank(type)) ? type : KeyStore.getDefaultType());
		if (truststorePassword == null)
			truststorePassword = keystorePassword;
		password = truststorePassword.toCharArray();
		trustStore.load(null, password);

		// Add certificate to truststore
		trustStore.setCertificateEntry(alias, certificate);

		// Store truststore to file
		fos = new FileOutputStream(outputDir + truststoreFileName);
		trustStore.store(fos, password);
		fos.close();
		log_info("Truststore created");
	}

	private static X509Certificate generateCertificate(String dn, KeyPair keyPair, int validity, String sigAlgName) throws GeneralSecurityException, IOException {
		PrivateKey privateKey = keyPair.getPrivate();
		X509CertInfo info = new X509CertInfo();

		Date from = new Date();
		Date to = new Date(from.getTime() + validity * 1000L * 24L * 60L * 60L);

		CertificateValidity interval = new CertificateValidity(from, to);
		BigInteger serialNumber = new BigInteger(64, new SecureRandom());
		X500Name owner = new X500Name(dn);
		AlgorithmId sigAlgId = new AlgorithmId(AlgorithmId.md5WithRSAEncryption_oid);

		info.set(X509CertInfo.VALIDITY, interval);
		info.set(X509CertInfo.SERIAL_NUMBER, new CertificateSerialNumber(serialNumber));
		info.set(X509CertInfo.SUBJECT, owner);
		info.set(X509CertInfo.ISSUER, owner);
		info.set(X509CertInfo.KEY, new CertificateX509Key(keyPair.getPublic()));
		info.set(X509CertInfo.VERSION, new CertificateVersion(CertificateVersion.V3));
		info.set(X509CertInfo.ALGORITHM_ID, new CertificateAlgorithmId(sigAlgId));

		// Process extensions
		final CertificateExtensions extensions = new CertificateExtensions();

		// Add subject alternative names
		final GeneralNames generalNames = new GeneralNames();
		generalNames.add(new GeneralName(new IPAddressName("127.0.0.1")));
		generalNames.add(new GeneralName(new DNSName("localhost")));
		final SubjectAlternativeNameExtension san = new SubjectAlternativeNameExtension(false, generalNames);
		extensions.set(SubjectAlternativeNameExtension.NAME, san);

		info.set(X509CertInfo.EXTENSIONS, extensions);

		// Sign the cert to identify the algorithm that's used.
		X509CertImpl certificate = new X509CertImpl(info);
		certificate.sign(privateKey, sigAlgName);

		// Update the algorith, and resign.
		sigAlgId = (AlgorithmId) certificate.get(X509CertImpl.SIG_ALG);
		info.set(CertificateAlgorithmId.NAME + "." + CertificateAlgorithmId.ALGORITHM, sigAlgId);
		certificate = new X509CertImpl(info);
		certificate.sign(privateKey, sigAlgName);

		return certificate;
	}

	public static String jasyptEncrypt(String message, char[] password) {
		BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
		textEncryptor.setPasswordCharArray(password);
		return "ENC("+textEncryptor.encrypt(message)+")";
	}

	public static String jasyptDecrypt(String message, char[] password) {
		BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
		if (password==null)
			password = askPassword("Jasypt password");
		textEncryptor.setPasswordCharArray(password);
		if (StringUtils.startsWithIgnoreCase(message, "ENC(") && StringUtils.endsWith(message,")")) {
			message = message.substring(4, message.length()-1);
		}
		return textEncryptor.decrypt(message);
	}

	public static String getJasyptPassword() {
		// Read Jasypt password from System properties
		String password = System.getProperties().getProperty("jasypt.encryptor.password");
		if (StringUtils.isNotEmpty(password))
			return password;
		// Read Jasypt password from file specified in environment variable
		String file = System.getenv("JASYPT_ENCRYPTOR_PASSWORD_FILE");
		if (StringUtils.isNotBlank(file)) {
			try (BufferedReader br = new BufferedReader(new FileReader(file))) {
				password = br.readLine();
				if (StringUtils.isNotEmpty(password))
					return password;
			} catch (Exception e) {
				log.warn("Error while reading JASYPT_ENCRYPTOR_PASSWORD_FILE file: file={}, error=", file, e);
			}
		}
		// Read Jasypt password from environment variable
		return System.getenv("JASYPT_ENCRYPTOR_PASSWORD");
	}

	@SneakyThrows
	public static Properties processSecretsArguments(String[] args) {
		// Check if auto-init flag is set
		String outputDir = checkForAutoInitFlag(args);
		if (outputDir!=null) {
			String secretsFile = initializeSecretsAndKeystores(AMPLE_BASE_NAME, outputDir, true);
			try (FileInputStream in = new FileInputStream(secretsFile)) {
				Properties defaultProperties = new Properties();
				defaultProperties.load(in);
				return defaultProperties;
			}
		}

		// Check if '-nj' no-jasypt flag is set. If it is skip getting Jasypt settings
		// from environment or command line. It does not prevent further Jasypt processing
		boolean noJasypt = Arrays.stream(args).anyMatch("-nj"::equalsIgnoreCase);
		if (noJasypt) return null;
		String skip = System.getenv("JASYPT_SKIP");
		if (StringUtils.isNotBlank(skip) && "true".equalsIgnoreCase(skip.trim())) return null;

		String jasyptPassword = getJasyptPassword();
		// Check if '-njp' no-jasypt-prompt flag is set. If it is don't ask for Jasypt password
		boolean noJasyptPrompt = Arrays.stream(args).anyMatch("-njp"::equalsIgnoreCase);
		if (!noJasyptPrompt && StringUtils.isEmpty(jasyptPassword))
			jasyptPassword = new String(System.console().readPassword("Jasypt password: "));
		// Set Jasypt encryptor password System property
		if (StringUtils.isNotEmpty(jasyptPassword))
			System.getProperties().setProperty("jasypt.encryptor.password", jasyptPassword);
		return null;
	}

	private static String checkForAutoInitFlag(String[] args) throws IOException {
		boolean autoInit = false;
		String outputDir = DEFAULT_CONFIG_DIR;
		for (int i=0; i<args.length; i++) {
			if ("--auto-init".equalsIgnoreCase(args[i]) || "-ai".equalsIgnoreCase(args[i])) {
				autoInit = true;
				if (i + 1 < args.length && StringUtils.isNotBlank(args[i]))
					outputDir = args[i + 1];
				return outputDir;
			}
		}
		return null;
	}
}