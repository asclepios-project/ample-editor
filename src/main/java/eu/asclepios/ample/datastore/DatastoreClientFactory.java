/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.datastore;

import eu.asclepios.ample.AmpleApplicationProperties;
import eu.asclepios.ample.fuseki.FusekiClient;
import eu.asclepios.ample.fuseki.FusekiServer;
import eu.asclepios.ample.persistence.SparqlServiceClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import static eu.asclepios.ample.AmpleApplicationProperties.ClientType.LOCAL;

@Slf4j
@Service
@RequiredArgsConstructor
public class DatastoreClientFactory {
	private final AmpleApplicationProperties properties;
	@Nullable
	private final Datastore datastore;
	@Nullable
	private final FusekiServer server;

	public SparqlServiceClient getInstance() {
		log.debug("DatastoreClientFactory: client-type={}", properties.getClient().getType());
		if (properties.getClient().getType()!=null) {
			if (properties.getClient().getType().equals(LOCAL))
				return new DatastoreClient(datastore);
		}
		return new FusekiClient(server, properties);
	}
}