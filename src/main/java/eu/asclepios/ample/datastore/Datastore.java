/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.datastore;

import eu.asclepios.ample.AmpleApplicationProperties;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.jena.query.Dataset;
import org.apache.jena.query.DatasetFactory;
import org.apache.jena.tdb2.TDB2Factory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class Datastore implements InitializingBean {
    private final AmpleApplicationProperties properties;

    @Getter
    private Dataset dataset;

    @Override
    public void afterPropertiesSet() {
        STORE_TYPE datastoreType = properties.getDatastore().getType();
        log.info("Datastore: initializing with type: {}", datastoreType);
        switch (datastoreType) {
            case TDB2:
                String location = properties.getDatastore().getLocation();
                dataset = TDB2Factory.connectDataset(location);
                break;
            default:
                log.warn("Datastore: No datastore type specified. Using MEMORY");
            case MEMORY:
                dataset = DatasetFactory.createTxnMem();
        }
        log.info("Datastore: initialized");
    }

    public enum STORE_TYPE { MEMORY, TDB2 }
}