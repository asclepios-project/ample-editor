/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.model.expr;

import eu.asclepios.ample.persistence.RdfPersistenceManager;
import eu.asclepios.ample.persistence.annotations.RdfPredicate;
import eu.asclepios.ample.persistence.annotations.RdfSubject;
import eu.asclepios.ample.rest.AttributeRestController;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@RdfSubject(
        uri="http://www.asclepios.eu/expression/COMPOSITE-EXPRESSION",
        rdfType="http://www.asclepios.eu/expression#COMPOSITE-EXPRESSION"
)
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class CompositeExpression extends Expression {
    @XmlAttribute
    @RdfPredicate(uri = "http://www.asclepios.eu/expression/BOOLEAN-OPERATOR")
    private String operator;

    @XmlAttribute
    @RdfPredicate(uri = "http://www.asclepios.eu/expression/NOT")
    private boolean not;

    @XmlAttribute
    @RdfPredicate(uri = "http://www.asclepios.eu/expression/EXPRESSION", update = "cascade", delete = "cascade")
    private List<Expression> expressions = new ArrayList<>();

    public void setOperator(String operator) {
        BOOLEAN_OPERATOR.valueOf(operator); // will throw exception if operator value is not valid
        this.operator = operator;
    }

    public void prepareForSerialization() {
        expressions.forEach(Expression::prepareForSerialization);
    }

    public void prepareForPersistence(RdfPersistenceManager pm) throws Exception {
        for (Expression e : expressions)
            e.prepareForPersistence(pm);
    }

    @Override
    public void populatePropertyInstances(AttributeRestController pm) {
        for (Expression e : expressions)
            e.populatePropertyInstances(pm);
    }
}