/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.model.expr;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import eu.asclepios.ample.model.AbstractObject;
import eu.asclepios.ample.persistence.RdfPersistenceManager;
import eu.asclepios.ample.persistence.annotations.RdfSubject;
import eu.asclepios.ample.rest.AttributeRestController;
import lombok.Data;
import lombok.EqualsAndHashCode;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY)
@JsonSubTypes({
        @JsonSubTypes.Type(value = SimpleExpression.class, name = "SimpleExpression"),
        @JsonSubTypes.Type(value = CompositeExpression.class, name = "CompositeExpression") }
)
@Data
@RdfSubject
@EqualsAndHashCode(callSuper = false)
public abstract class Expression extends AbstractObject {
    protected Expression() {
        setId(createId());
    }

    public abstract void prepareForSerialization();

    public abstract void prepareForPersistence(RdfPersistenceManager pm) throws Exception;

    public abstract void populatePropertyInstances(AttributeRestController pm);
}
