/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.model.expr;

import eu.asclepios.ample.model.casm.CasmObject;
import eu.asclepios.ample.persistence.RdfPersistenceManager;
import eu.asclepios.ample.persistence.annotations.RdfPredicate;
import eu.asclepios.ample.persistence.annotations.RdfSubject;
import eu.asclepios.ample.rest.AttributeRestController;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.Objects;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@RdfSubject(
        uri="http://www.asclepios.eu/expression/SIMPLE-EXPRESSION",
        rdfType="http://www.asclepios.eu/expression#SIMPLE-EXPRESSION"
)
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class SimpleExpression extends Expression {
    @RdfPredicate(uri="http://www.asclepios.eu/expression/LEFT-SIDE", update="no-cascade", delete="no-cascade")
	private CasmObject attributeRef;
    private String attributeText;
    private String attribute;

    @RdfPredicate(uri="http://www.asclepios.eu/expression/PROPERTY", update="no-cascade", delete="no-cascade")
	private CasmObject propertyRef;
    private String propertyText;
    private String property;

    private String propertyType;
    private String propertyDataType;
    private boolean dataProperty;
    private String rangeType;
    private String rangeId;
    private List<CasmObject.MinimalCasmObject> instances;

    @RdfPredicate(uri="http://www.asclepios.eu/expression/OPERATOR", update="no-cascade", delete="no-cascade")
    private String operator;

    @RdfPredicate(uri="http://www.asclepios.eu/expression/RIGHT-SIDE", update="no-cascade", delete="no-cascade")
    //private CasmObject valueRef;
    //private String valueText;
    private String value;

    public void prepareForSerialization() {
        // check referenced objects are present
        Objects.requireNonNull(attributeRef, "Attribute object not set");
        Objects.requireNonNull(propertyRef, "Property object not set");
        //Objects.requireNonNull(valueRef, "Value object not set");

        // initialize attribute fields
        this.attribute = attributeRef.getId();
        this.attributeText = attributeRef.getName();

        // initialize property fields
        this.property = propertyRef.getId();
        this.propertyText = propertyRef.getName();
        this.propertyType = propertyRef.getPropertyType();
        this.propertyDataType = "DataProperty".equals(propertyRef.getPropertyType())
                ? propertyRef.getRangeUri()
                : propertyRef.getRange();

        // ...additional property fields used for GUI initialization
        this.dataProperty = "DataProperty".equals(propertyRef.getPropertyType());
        this.rangeType = propertyRef.getRangeUri();
        this.rangeId = propertyRef.getRange();
        this.instances = propertyRef.getInstances();

        // initialize value fields
        //this.value = valueRef.getId();
        //this.valueText = valueRef.getName();
    }

    public void prepareForPersistence(RdfPersistenceManager pm) throws Exception {
        // check id fields are set
        if (StringUtils.isBlank(attribute)) throw new IllegalStateException("SimpleExpression attribute is not set or is empty");
        if (StringUtils.isBlank(property)) throw new IllegalStateException("SimpleExpression property is not set or is empty");
        //if (StringUtils.isBlank(value)) throw new IllegalStateException("SimpleExpression value is not set or is empty");

        // retrieve referenced objects
        CasmObject attrObj = (CasmObject)pm.find(attribute, CasmObject.class);
        CasmObject propObj = (CasmObject)pm.find(property, CasmObject.class);
        //CasmObject valueObj = (CasmObject)pm.find(value, CasmObject.class);

        // check values
        Objects.requireNonNull(attrObj, "Attribute with id not found: "+attribute);
        Objects.requireNonNull(propObj, "Property with id not found: "+property);
        //Objects.requireNonNull(valueObj, "Value with id not found: "+value);

        // initialize reference fields
        this.attributeRef = attrObj;
        this.propertyRef = propObj;
        //this.valueRef = valueObj;
    }

    @Override
    public void populatePropertyInstances(AttributeRestController controller) {
        controller.populatePropertyInstances(propertyRef);
    }
}
