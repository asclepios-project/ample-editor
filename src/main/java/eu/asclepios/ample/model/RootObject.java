/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.model;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
public abstract class RootObject {
	/*@XmlAttribute
	@RdfPredicate(lang="en", uri="http://www.w3.org/2000/01/rdf-schema#label", omitIfNull=true)
	protected String labelEn;
	@XmlAttribute
	@RdfPredicate(lang="de", uri="http://www.w3.org/2000/01/rdf-schema#label", omitIfNull=true)
	protected String labelDe;
	@XmlAttribute
	@RdfPredicate(uri="http://www.w3.org/2000/01/rdf-schema#comment", omitIfNull=true)
	protected String comment;
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if (labelEn!=null) sb.append("label@en=").append(labelEn);
		if (labelDe!=null) {
			if (sb.length()>0) sb.append(", ");
			sb.append("label@de=").append(labelDe);
		}
		if (comment!=null) {
			if (sb.length()>0) sb.append(", ");
			sb.append("comment=").append(comment);
		}
		if (sb.length()>0) sb.insert(0, "RootObject: ");
		return sb.toString();
	}*/
}
