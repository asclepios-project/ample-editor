package eu.asclepios.ample.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.Arrays;

public interface ITreeNodeSpec {
    String getName();
    String getDefaultIcon();
    boolean canHaveChildren();

    static <E>E valueByName(@NotBlank String name, @NotEmpty E[] values) {
        return Arrays.stream(values)
                .filter(v -> v instanceof ITreeNodeSpec)
                .filter(v -> ((ITreeNodeSpec)v).getName().equals(name))
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException("No "+values[0].getClass().getSimpleName()+" with name: "+name));
    }
}
