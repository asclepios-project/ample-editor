/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.model.abac;

import eu.asclepios.ample.model.expr.Expression;
import eu.asclepios.ample.persistence.annotations.RdfPredicate;
import eu.asclepios.ample.persistence.annotations.RdfSubject;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@RdfSubject(
	uri="http://www.asclepios.eu/abac/ASCLEPIOS-ABAC-RULE",
	rdfType="http://www.asclepios.eu/abac#ASCLEPIOS-ABAC-RULE"
)
@Data
@EqualsAndHashCode(callSuper = true)
public class AbacRule extends AbacObject {
	@XmlAttribute
	@RdfPredicate(uri="http://www.asclepios.eu/abac/ABAC-RULE#policy", update="no-cascade", delete="no-cascade", omitIfNull=true)
	protected AbacPolicy rulePolicy;

	@XmlAttribute
	@RdfPredicate(uri="http://www.asclepios.eu/abac/ABAC-RULE#outcome", update="no-cascade", delete="no-cascade", omitIfNull=true)
	protected String ruleOutcome;

	@XmlAttribute
	@RdfPredicate(uri="http://www.asclepios.eu/abac/ABAC-RULE#condition", update="cascade", delete="cascade", omitIfNull=true)
	protected Expression ruleCondition;

	public void setRuleOutcome(String val) {
		ruleOutcome = Outcome.valueOf(val).name();
	}

	public String toString() {
		return 	"AbacRule: {\n"+super.toString()+
				"\n\tpolicy = "+(rulePolicy !=null ? rulePolicy.getName()+" ("+ rulePolicy.getId()+")" : "")+
				"\n\toutcome = "+ruleOutcome+
				"\n\tcondition = "+ruleCondition+
				"\n}\n";
	}

	public enum Outcome {
		PERMIT, DENY, NOT_APPLICABLE, INDETERMINATE, ERROR
	}
}
