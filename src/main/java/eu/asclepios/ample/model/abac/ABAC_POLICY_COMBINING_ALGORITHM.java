/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.model.abac;

import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
public enum ABAC_POLICY_COMBINING_ALGORITHM {
	FIRST_APPLICABLE("First Applicable", "urn:oasis:names:tc:xacml:1.0:rule-combining-algorithm:first-applicable"),
	DENY_OVERRIDES("Deny Overrides", "urn:oasis:names:tc:xacml:3.0:rule-combining-algorithm:deny-overrides"),
	PERMIT_OVERRIDES("Permit Overrides", "urn:oasis:names:tc:xacml:3.0:rule-combining-algorithm:permit-overrides"),
	ORDERED_DENY_OVERRIDES("Ordered Deny Overrides", "urn:oasis:names:tc:xacml:3.0:rule-combining-algorithm:ordered-deny-overrides"),
	ORDERED_PERMIT_OVERRIDES("Ordered Permit Overrides", "urn:oasis:names:tc:xacml:3.0:rule-combining-algorithm:ordered-permit-overrides"),
	DENY_UNLESS_PERMIT("Deny unless Permit", "urn:oasis:names:tc:xacml:3.0:rule-combining-algorithm:deny-unless-permit"),
	PERMIT_UNLESS_DENY("Permit unless Deny", "urn:oasis:names:tc:xacml:3.0:rule-combining-algorithm:permit-unless-deny");

	public final static String FIRST_APPLICABLE_NAME = FIRST_APPLICABLE.getName();
	public final static String DENY_OVERRIDES_NAME = DENY_OVERRIDES.getName();
	public final static String PERMIT_OVERRIDES_NAME = PERMIT_OVERRIDES.getName();
	public final static String ORDERED_DENY_OVERRIDES_NAME = ORDERED_DENY_OVERRIDES.getName();
	public final static String ORDERED_PERMIT_OVERRIDES_NAME = ORDERED_PERMIT_OVERRIDES.getName();
	public final static String DENY_UNLESS_PERMIT_NAME = DENY_UNLESS_PERMIT.getName();
	public final static String PERMIT_UNLESS_DENY_NAME = PERMIT_UNLESS_DENY.getName();

	public final static String FIRST_APPLICABLE_URN = FIRST_APPLICABLE.getURN();
	public final static String DENY_OVERRIDES_URN = DENY_OVERRIDES.getURN();
	public final static String PERMIT_OVERRIDES_URN = PERMIT_OVERRIDES.getURN();
	public final static String ORDERED_DENY_OVERRIDES_URN = ORDERED_DENY_OVERRIDES.getURN();
	public final static String ORDERED_PERMIT_OVERRIDES_URN = ORDERED_PERMIT_OVERRIDES.getURN();
	public final static String DENY_UNLESS_PERMIT_URN = DENY_UNLESS_PERMIT.getURN();
	public final static String PERMIT_UNLESS_DENY_URN = PERMIT_UNLESS_DENY.getURN();

	private String name;
	private String urn;

	ABAC_POLICY_COMBINING_ALGORITHM(String name, String urn) {
		this.name = name;
		this.urn = urn;
	}

	public String getURN() { return urn; }

	public static ABAC_POLICY_COMBINING_ALGORITHM valueOfName(@NotNull String name) {
		for (ABAC_POLICY_COMBINING_ALGORITHM alg : values()) {
			if (alg.getName().equals(name))
				return alg;
		}
		throw new IllegalArgumentException(
				"No enum constant in " + ABAC_POLICY_COMBINING_ALGORITHM.class.getCanonicalName() + " for Name: " + name);
	}
	public static ABAC_POLICY_COMBINING_ALGORITHM valueOfUrn(@NotNull String urn) {
		for (ABAC_POLICY_COMBINING_ALGORITHM alg : values()) {
			if (alg.getURN().equals(urn))
				return alg;
		}
		throw new IllegalArgumentException(
				"No enum constant in " + ABAC_POLICY_COMBINING_ALGORITHM.class.getCanonicalName() + " for URN: " + urn);
	}
	public static ABAC_POLICY_COMBINING_ALGORITHM valueOfAny(@NotNull String s) {
		for (ABAC_POLICY_COMBINING_ALGORITHM alg : values()) {
			if (alg.name().equals(s) || alg.getName().equals(s) || alg.getURN().equals(s))
				return alg;
		}
		throw new IllegalArgumentException(
				"No enum constant " + ABAC_POLICY_COMBINING_ALGORITHM.class.getCanonicalName() + "." + s);
	}
}
