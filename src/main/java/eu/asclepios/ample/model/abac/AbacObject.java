/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.model.abac;

import eu.asclepios.ample.model.AbstractObject;
import eu.asclepios.ample.persistence.annotations.RdfPredicate;
import eu.asclepios.ample.persistence.annotations.RdfSubject;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@RdfSubject(
	uri="http://www.asclepios.eu/abac/ASCLEPIOS-ABAC-OBJECT",
	rdfType="http://www.asclepios.eu/abac#ASCLEPIOS-ABAC-OBJECT"
)
@Data
@EqualsAndHashCode(callSuper = true)
public class AbacObject extends AbstractObject {
	/*@XmlAttribute
	@RdfPredicate(uri="http://www.w3.org/2004/02/skos/core#broader", update="no-cascade", delete="no-cascade", omitIfNull=true)
	protected AbacObject parent;*/

	/*public List<String> getParentIdList(boolean includeThis) {
		// Walk the path up to the root and store id's in 'list'
		Vector<String> list = new Vector<>();
		if (includeThis) { list.add(this.getId()); }
		AbacObject parent = getParent();
		while (parent!=null) { list.add(parent.getId()); parent = parent.getParent(); }
		
		// reverse list so that id's of root appear first and this attribute's id appears last
		Collections.reverse(list);
		return list;
	}*/
	
	public String toString() {
		return 	"AbacObject: {\n"+super.toString()+
				//"\tparent = "+(parent!=null ? parent.getId() : "")+
				"\n}\n";
	}
}
