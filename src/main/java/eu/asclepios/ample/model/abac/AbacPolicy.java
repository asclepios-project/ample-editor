/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.model.abac;

import eu.asclepios.ample.persistence.annotations.RdfPredicate;
import eu.asclepios.ample.persistence.annotations.RdfSubject;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@RdfSubject(
	uri="http://www.asclepios.eu/abac/ASCLEPIOS-ABAC-POLICY",
	rdfType="http://www.asclepios.eu/abac#ASCLEPIOS-ABAC-POLICY"
)
@Data
@EqualsAndHashCode(callSuper = true)
public class AbacPolicy extends AbacObject {
	@XmlAttribute
	@RdfPredicate(uri="http://www.asclepios.eu/abac/ABAC-POLICY#combining-algorithm", update="no-cascade", delete="no-cascade", omitIfNull=true)
	protected String policyCombiningAlgorithm;

	public String toString() {
		return 	"AbacPolicy: {\n"+super.toString()+
				"\tcombining-algorithm = "+policyCombiningAlgorithm+
				"\n}\n";
	}
}
