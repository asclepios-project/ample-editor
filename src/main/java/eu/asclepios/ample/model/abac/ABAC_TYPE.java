/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.model.abac;

import eu.asclepios.ample.model.ITreeNodeSpec;
import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotBlank;

@Getter
@AllArgsConstructor
public enum ABAC_TYPE implements ITreeNodeSpec {
	ABAC_POLICY_SET("ABAC-POLICY-SET", true, "/images/abac/abac-policy-set-16x16.png"),
	ABAC_POLICY("ABAC-POLICY", true, "/images/abac/abac-policy-16x16-4.png"),
	ABAC_RULE("ABAC-RULE", false, "/images/abac/abac-rule-16x16.png");

	public final static String ABAC_POLICY_SET_NAME = ABAC_POLICY_SET.getName();
	public final static String ABAC_POLICY_NAME = ABAC_POLICY.getName();
	public final static String ABAC_RULE_NAME = ABAC_RULE.getName();

	private final String name;
	private final boolean canHaveChildren;
	private final String defaultIcon;

	public boolean canHaveChildren() { return canHaveChildren; }
	public static ABAC_TYPE valueByName(@NotBlank String name) { return ITreeNodeSpec.valueByName(name, values()); }
}
