/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.model;

import eu.asclepios.ample.persistence.annotations.Id;
import eu.asclepios.ample.persistence.annotations.RdfPredicate;
import eu.asclepios.ample.persistence.annotations.RdfSubject;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Date;

@Data
@RdfSubject
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public abstract class AbstractObject extends RootObject {
	@Id
	@RdfPredicate(uri="http://purl.org/dc/terms/identifier")
	private String id;
	@RdfPredicate(uri="http://purl.org/dc/terms/title", omitIfNull=true)
	private String name;
	@RdfPredicate(uri="http://purl.org/dc/terms/description", omitIfNull=true)
	private String description;
	@RdfPredicate(uri="http://purl.org/dc/terms/created", omitIfNull=true)
	private Date createTimestamp;
	@RdfPredicate(uri="http://purl.org/dc/terms/modified", omitIfNull=true)
	private Date lastUpdateTimestamp;
	@RdfPredicate(uri="http://purl.org/dc/terms/creator", omitIfNull=true)
	private String owner;
	@RdfPredicate(uri="http://purl.org/dc/terms/URI", omitIfNull=true)
	protected String uri;
	@RdfPredicate(uri="http://purl.org/dc/elements/1.1/type", omitIfNull=true)
	protected String type;
	@RdfPredicate(uri="https://schema.org/position", omitIfNull=true)
	private int position;

	protected String createId() {
		String clss = getClass().getName();
		int p = clss.lastIndexOf('.');
		clss = (p!=-1) ? clss.substring(p+1) : clss;
		clss = clss.replaceAll("[^A-Za-z0-9_]","");
		clss = clss.replaceAll("([A-Z_])", "-$1");
		clss = clss.replaceAll("^-*", "").replaceAll("-*$","");
		clss = clss.toUpperCase();
		return createId(clss);
	}
	
	protected static String createId(String prefix) {
		return String.format("%s-%s", prefix, java.util.UUID.randomUUID().toString());
	}
}
