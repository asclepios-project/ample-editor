/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.model.abe;

import eu.asclepios.ample.model.ITreeNodeSpec;
import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotBlank;

@Getter
@AllArgsConstructor
public enum ABE_TYPE implements ITreeNodeSpec {
	ABE_POLICY("ABE-POLICY", false, "/images/abe/abe-policy-16x16.png");

	public final static String ABE_POLICY_NAME = ABE_POLICY.getName();

	private final String name;
	private final boolean canHaveChildren;
	private final String defaultIcon;

	public boolean canHaveChildren() { return canHaveChildren; }
	public static ABE_TYPE valueByName(@NotBlank String name) { return ITreeNodeSpec.valueByName(name, values()); }
}
