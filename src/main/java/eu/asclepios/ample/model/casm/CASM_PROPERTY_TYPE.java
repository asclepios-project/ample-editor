/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.model.casm;

import lombok.Getter;

@Getter
public enum CASM_PROPERTY_TYPE {
	OBJECT_PROPERTY("ObjectProperty"),
	DATA_PROPERTY("DataProperty");

	public final static String OBJECT_PROPERTY_NAME = OBJECT_PROPERTY.getName();
	public final static String DATA_PROPERTY_NAME = DATA_PROPERTY.getName();

	private String name;

	CASM_PROPERTY_TYPE(String name) {
		this.name = name;
	}
}
