/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.model.casm;

import eu.asclepios.ample.model.ITreeNodeSpec;
import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotBlank;

@Getter
@AllArgsConstructor
public enum CASM_TYPE implements ITreeNodeSpec {
	CASM_CONCEPT("CONCEPT", true, "/images/concept.png"),
	CASM_PROPERTY("PROPERTY", false, "/images/property.png"),
	CASM_CONCEPT_INSTANCE("CONCEPT-INSTANCE", false, "/images/concept-instance.png");
	//CASM_PROPERTY_INSTANCE("PROPERTY-INSTANCE", false, "/images/property-instance.png");

	public final static String CASM_CONCEPT_NAME = CASM_CONCEPT.getName();
	public final static String CASM_PROPERTY_NAME = CASM_PROPERTY.getName();
	public final static String CASM_CONCEPT_INSTANCE_NAME = CASM_CONCEPT_INSTANCE.getName();
	//public final static String CASM_PROPERTY_INSTANCE_NAME = CASM_PROPERTY_INSTANCE.getName();

	private final String name;
	private final boolean canHaveChildren;
	private final String defaultIcon;

	public boolean canHaveChildren() { return canHaveChildren; }
	public static CASM_TYPE valueByName(@NotBlank String name) { return ITreeNodeSpec.valueByName(name, values()); }
}
