/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.model.casm;

import java.util.Collections;
import java.util.List;
import java.util.Vector;

import eu.asclepios.ample.model.AbstractObject;
import eu.asclepios.ample.persistence.annotations.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAttribute;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@RdfSubject(
	uri="http://www.asclepios.eu/casm/ASCLEPIOS-OBJECT",
	rdfType="http://www.asclepios.eu/casm#ASCLEPIOS-OBJECT"
)
@Data
@EqualsAndHashCode(callSuper = true)
public class CasmObject extends AbstractObject {
	@XmlAttribute
	@RdfPredicate(uri="http://www.w3.org/2004/02/skos/core#broader", update="no-cascade", delete="no-cascade", omitIfNull=true)
	protected CasmObject parent;

	@XmlAttribute
	@RdfPredicate(uri="http://www.asclepios.eu/casm#propertyType", update="no-cascade", delete="no-cascade", omitIfNull=true)
	protected String propertyType;
	@XmlAttribute
	@RdfPredicate(uri="http://www.asclepios.eu/casm#range", update="no-cascade", delete="no-cascade", omitIfNull=true)
	protected String range;
	@XmlAttribute
	protected String range_display;
	@XmlAttribute
	@RdfPredicate(uri="http://www.asclepios.eu/casm#rangeUri", update="no-cascade", delete="no-cascade", omitIfNull=true)
	protected String rangeUri;

	@XmlAttribute
	protected List<MinimalCasmObject> instances;
	
	@XmlAttribute
	@RdfPredicate(uri="http://www.asclepios.eu/casm#propertyValue", update="no-cascade", delete="no-cascade", omitIfNull=true)
	protected String propertyValue;
	@XmlAttribute
	@RdfPredicate(uri="http://www.asclepios.eu/casm#propertyIsA", update="no-cascade", delete="no-cascade", omitIfNull=true)
	protected String propertyIsA;
	@XmlAttribute
	protected String propertyIsA_display;

	@XmlAttribute
	@RdfPredicate(uri="http://www.asclepios.eu/casm#inAbac", update="no-cascade", delete="no-cascade", omitIfNull=true)
	protected boolean inAbac = true;
	@XmlAttribute
	@RdfPredicate(uri="http://www.asclepios.eu/casm#inAbe", update="no-cascade", delete="no-cascade", omitIfNull=true)
	protected boolean inAbe = true;

	public List<String> getParentIdList(boolean includeThis) {
		// Walk the path up to the root and store id's in 'list'
		Vector<String> list = new Vector<String>();
		if (includeThis) { list.add(this.getId()); }
		CasmObject parent = getParent();
		while (parent!=null) { list.add(parent.getId()); parent = parent.getParent(); }
		
		// reverse list so that id's of root appear first and this attribute's id appears last
		Collections.reverse(list);
		return list;
	}
	
	public String toString() {
		return 	"CasmObject: {\n"+super.toString()+
				"\tparent = "+(parent!=null ? parent.getId() : "")+
				"\n\tproperty-type = "+propertyType+
				"\n\trange = "+range+
				"\n\trange_display = "+range_display+
				"\n\trange-uri = "+rangeUri+
				"\n\tproperty-Is_A = "+propertyIsA+
				"\n\tproperty-Is_A_display = "+propertyIsA_display+
				"\n\tproperty-value = "+propertyValue+
				"\n\tinstances = "+instances+
				"\n\tin-Abac = "+inAbac+",  in-Abe = "+inAbe+
				"\n}\n";
	}

	@XmlRootElement
	@XmlAccessorType(XmlAccessType.FIELD)
	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	@EqualsAndHashCode(callSuper = false)
	public static class MinimalCasmObject {
		private String id;
		private String name;
		private String type;
		private String uri;
		private String description;

		public MinimalCasmObject(CasmObject so) {
			this.id = so.getId();
			this.name = so.getName();
			this.type = so.getType();
			this.uri = so.getUri();
			this.description = so.getDescription();
		}
	}

	public enum POLICY_USE { set, clear }
	public enum POLICY_TYPE { abac, abe }
}
