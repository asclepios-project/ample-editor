/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.model.validation;

import eu.asclepios.ample.model.AbstractObject;
import eu.asclepios.ample.model.expr.Expression;
import eu.asclepios.ample.persistence.annotations.RdfPredicate;
import eu.asclepios.ample.persistence.annotations.RdfSubject;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@RdfSubject(
	uri="http://www.asclepios.eu/validation/ASCLEPIOS-POLICY-VALIDATION-OBJECT",
	rdfType="http://www.asclepios.eu/validation#ASCLEPIOS-POLICY-VALIDATION-OBJECT"
)
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class PolicyValidationObject extends AbstractObject {
	@RdfPredicate(uri="http://www.w3.org/2004/02/skos/core#broader", update="no-cascade", delete="no-cascade", omitIfNull=true)
	protected PolicyValidationObject parent;

	@RdfPredicate(uri="http://www.asclepios.eu/validation/ASCLEPIOS-POLICY-VALIDATION-OBJECT#enabled", update="no-cascade", delete="no-cascade", omitIfNull=true)
	protected boolean enabled = true;
	@RdfPredicate(uri="http://www.asclepios.eu/validation/ASCLEPIOS-POLICY-VALIDATION-OBJECT#readonly", update="no-cascade", delete="no-cascade", omitIfNull=true)
	protected boolean readonly;

	@RdfPredicate(uri="http://www.asclepios.eu/validation/ASCLEPIOS-POLICY-VALIDATION-OBJECT#rule-definition", update="no-cascade", delete="no-cascade", omitIfNull=true)
	protected String policyValidationRuleDefinition;

	@RdfPredicate(uri="http://www.asclepios.eu/validation/ASCLEPIOS-POLICY-VALIDATION-OBJECT#expression", update="cascade", delete="cascade", omitIfNull=true)
	protected Expression policyValidationExpression;

	public boolean isEnabled() {
		PolicyValidationObject object = this;
		while (object!=null) {
			if (!object.enabled) return false;
			object = object.getParent();
		}
		return true;
	}

	public boolean isValidationGroup() { return "POLICY-VALIDATION-GROUP".equals(type); }
	public boolean isValidationSet() { return "POLICY-VALIDATION-SET".equals(type); }
	public boolean isValidationRule() { return "POLICY-VALIDATION-RULE".equals(type); }

	public enum POLICY_VALIDATION_OUTCOME {
		VALID, INVALID
	}
}
