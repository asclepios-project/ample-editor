/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.model.validation;

import eu.asclepios.ample.model.ITreeNodeSpec;
import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotBlank;

@Getter
@AllArgsConstructor
public enum POLICY_VALIDATION_TYPE implements ITreeNodeSpec {
	POLICY_VALIDATION_GROUP("POLICY-VALIDATION-GROUP", true, null),
	POLICY_VALIDATION_SET("POLICY-VALIDATION-SET", true, null),
	POLICY_VALIDATION_RULE("POLICY-VALIDATION-RULE", false, null);

	public final static String POLICY_VALIDATION_GROUP_NAME = POLICY_VALIDATION_GROUP.getName();
	public final static String POLICY_VALIDATION_SET_NAME = POLICY_VALIDATION_SET.getName();
	public final static String POLICY_VALIDATION_RULE_NAME = POLICY_VALIDATION_RULE.getName();

	private final String name;
	private final boolean canHaveChildren;
	private final String defaultIcon;

	public boolean canHaveChildren() { return canHaveChildren; }
	public static POLICY_VALIDATION_TYPE valueByName(@NotBlank String name) { return ITreeNodeSpec.valueByName(name, values()); }
}
