/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.rest;

import eu.asclepios.ample.datastore.DatastoreClientFactory;
import eu.asclepios.ample.model.AbstractObject;
import eu.asclepios.ample.model.abac.AbacPolicy;
import eu.asclepios.ample.model.abe.AbePolicy;
import eu.asclepios.ample.model.validation.POLICY_VALIDATION_TYPE;
import eu.asclepios.ample.model.validation.PolicyValidationObject;
import eu.asclepios.ample.persistence.RdfPersistenceManager;
import eu.asclepios.ample.persistence.RdfPersistenceManagerFactory;
import eu.asclepios.ample.validation.PolicyValidationResult;
import eu.asclepios.ample.validation.PolicyValidationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@Slf4j
@Validated
@Service
@RestController
@RequiredArgsConstructor
@RequestMapping("/opt/validation")
public class PolicyValidationRestController extends AbstractRestController {
	private final DatastoreClientFactory clientFactory;
	private final AttributeRestController attributeRestController;
	private final PolicyValidationService policyValidationService;

	// ------------------------------------------------------------------------
	// Policy Validation Object methods
	// ------------------------------------------------------------------------

	// GET /opt/validation/
	// Description: Get a list of top-level policy validation nodes (i.e. groups)
	@RequestMapping(value = "/", method = GET, produces = APPLICATION_JSON_VALUE)
	public PolicyValidationObject[] getTopLevelPolicyValidationObjects() throws Exception {
		RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();

		String rdfType = pm.getClassRdfType(PolicyValidationObject.class);
		String parentUri = pm.getFieldUri(PolicyValidationObject.class, "parent");
		String queryStr = "SELECT ?s WHERE { ?s  a  <"+rdfType+"> .  ?s <http://purl.org/dc/terms/title> ?title .  FILTER NOT EXISTS { ?s  <"+parentUri+">  ?s1 } .  FILTER ( ?s != <http://www.asclepios.eu/casm/types#null> ) } ORDER BY ?title";

		log.debug("getTopLevelPolicyValidationObjects: Retrieving top-level policy validation groups");
		List<Object> list = pm.findByQuery(queryStr);
		log.debug("getTopLevelPolicyValidationObjects: {} top-level policy validation groups found", list.size());

		// prepare objects' expressions for GUI
		return preparePolicyValidationObjectArray(list);
	}

	// GET /opt/validation/{id}/children
	// Description: Get a list of policy validation group/set children (if any)
	@RequestMapping(value = "/{id}/children", method = GET, produces = APPLICATION_JSON_VALUE)
	public PolicyValidationObject[] getChildren(@PathVariable("id") String id) throws Exception {
		log.debug("getChildren: Retrieving children of policy validation group/set with id: {}", id);
		List<PolicyValidationObject> list = _retrieveChildrenInternal(id);
		log.debug("getChildren: {} children found for policy validation group/set with id: {}", list.size(), id);

		// prepare objects' expressions for GUI
		return preparePolicyValidationObjectArray((List<Object>)(List<?>) list);
	}

	// GET /opt/validation/all
	// Description: Get a list of ALL policy validation sets
	@RequestMapping(value = "/all", method = GET, produces = APPLICATION_JSON_VALUE)
	public PolicyValidationObject[] getAllPolicyValidationObjects() throws Exception {
		RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();

		String rdfType = pm.getClassRdfType(PolicyValidationObject.class);
		String queryStr = "SELECT ?s WHERE { ?s  a  <"+rdfType+"> .  ?s <http://purl.org/dc/terms/title> ?title .  FILTER ( ?s != <http://www.asclepios.eu/casm/types#null> ) } ORDER BY ?title";

		log.debug("getAllPolicyValidationObjects: Retrieving ALL policy validation sets");
		List<Object> list = pm.findByQuery(queryStr);
		log.debug("getAllPolicyValidationObjects: {} policies validation sets found", list.size());

		// prepare objects' expressions for GUI
		return preparePolicyValidationObjectArray(list);
	}

	// GET /opt/validation/{id}
	// Description: Get policy validation object description
	@RequestMapping(value = "/{id}", method = GET, produces = APPLICATION_JSON_VALUE)
	public PolicyValidationObject getPolicyValidationObject(@PathVariable("id") String id) throws Exception {
		RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();
		log.debug("getPolicyValidationObject: Retrieving policy validation object with id: {}", id);
		PolicyValidationObject object = (PolicyValidationObject) pm.find(id, PolicyValidationObject.class);
		log.debug("getPolicyValidationObject: Policy validation object {} :\n{}", id, object);

		// prepare objects expression for GUI
		if (object.getPolicyValidationExpression()!=null) {
			preparePolicyValidationObjectForCommunication(object);
		}
		return object;
	}

	// ------------------------------------------------------------------------
	// Not a REST API method
	// ------------------------------------------------------------------------

	public List<PolicyValidationObject> getChildren(@NotNull PolicyValidationObject object, @NotNull RdfPersistenceManager pm) throws Exception {
		return _retrieveChildrenInternal(pm, object.getId());
	}

	public List<PolicyValidationObject> _retrieveChildrenInternal(String id) throws Exception {
		RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();
		return _retrieveChildrenInternal(pm, id);
	}

	public List<PolicyValidationObject> _retrieveChildrenInternal(RdfPersistenceManager pm, String id) throws Exception {
		String rdfType = pm.getClassRdfType(PolicyValidationObject.class);
		String parentFieldUri = pm.getFieldUri(PolicyValidationObject.class, "parent");
		String parentUri = pm.getObjectUri(id, PolicyValidationObject.class);
		String queryStr = "SELECT ?s WHERE { ?s  a  <"+rdfType+"> ; <"+parentFieldUri+">  <"+parentUri+"> ; <http://purl.org/dc/terms/title> ?title } ORDER BY ?title";

		List<PolicyValidationObject> list = (List<PolicyValidationObject>)(List<?>) pm.findByQuery( String.format(queryStr, id) );
		return list;
	}

	private PolicyValidationObject[] preparePolicyValidationObjectArray(List<Object> list) {
		// prepare objects' expressions for GUI
		PolicyValidationObject[] objectArray = list.toArray(new PolicyValidationObject[0]);
		for (PolicyValidationObject object : objectArray)
			preparePolicyValidationObjectForCommunication(object);
		return objectArray;
	}

	// ------------------------------------------------------------------------

	// PUT /opt/validation/
	// Description: Create a new policy validation object
	@RequestMapping(value = "/", method = PUT, consumes = APPLICATION_JSON_VALUE)
	public ResponseEntity<String> createPolicyValidationObject(@RequestBody PolicyValidationObject object) throws Exception {
		RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();
		preparePolicyValidationObjectForPersistence(object, pm);
		object.setCreateTimestamp(object.getLastUpdateTimestamp());
		checkPolicyValidationObject(object);
		try {
			log.debug("createPolicyValidationObject: Creating a new policy validation object with id = {}", object.getId());
			log.debug("createPolicyValidationObject: New policy validation object values:\n{}", object);
			pm.persist(object);
			log.debug("createPolicyValidationObject: Object added to RDF persistent store");
			return createResponse(HTTP_STATUS_CREATED, "Result=Created");
		} catch (Exception e) {
			log.error("createPolicyValidationObject: EXCEPTION THROWN: ", e);
			log.debug("createPolicyValidationObject: Returning Status {}", HTTP_STATUS_ERROR);
			return createResponse(HTTP_STATUS_ERROR, "Result=Exception: "+e);
		}
	}

	// POST /opt/validation/{id}
	// Description: Update a policy validation object's data
	@RequestMapping(value = "/{id}", method = POST, consumes = APPLICATION_JSON_VALUE)
	public ResponseEntity<String> updatePolicyValidationObject(@PathVariable("id") String id, @RequestBody PolicyValidationObject object) throws Exception {
		RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();
		preparePolicyValidationObjectForPersistence(object, pm);
		try {
			// retrieve the stored version of object
			log.debug("updatePolicyValidationObject: Retrieving stored object with id = {}", id);
			PolicyValidationObject storedObject = (PolicyValidationObject) pm.find(id, PolicyValidationObject.class);
			log.debug("updatePolicyValidationObject: Stored object:\n{}", storedObject);

			// update create timestamp and last update timestamp
			object.setCreateTimestamp(storedObject.getCreateTimestamp());
			object.setLastUpdateTimestamp(new Date());

			// check if object is valid and is not readonly
			checkReadonly(storedObject);
			checkPolicyValidationObject(object);

			// store new version of object
			log.debug("updatePolicyValidationObject: Updating policy validation object with id = {}", id);
			log.debug("updatePolicyValidationObject: New policy validation object values:\n{}", object);
			pm.attach(object);
			log.debug("updatePolicyValidationObject: Object attached to RDF persistent store manager");
			pm.merge(object);
			log.debug("updatePolicyValidationObject: Persistent store state updated");
			return createResponse(HTTP_STATUS_OK, "Result=Updated");
		} catch (Exception e) {
			log.error("updatePolicyValidationObject: EXCEPTION THROWN: ", e);
			log.debug("updatePolicyValidationObject: Returning Status {}", HTTP_STATUS_ERROR);
			return createResponse(HTTP_STATUS_ERROR, "Result=Exception: "+e);
		}
	}

	// DELETE /opt/validation/{id}
	// Description: Delete a policy validation object
	@RequestMapping(value = "/{id}", method = DELETE)
	public ResponseEntity<String> deletePolicyValidationObject(@PathVariable("id") String id) {
		try {
			log.debug("deletePolicyValidationObject: Deleting policy validation object with id = {}", id);

			RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();

			// retrieve the stored version of object
			log.debug("deletePolicyValidationObject: Retrieving stored object with id = {}", id);
			PolicyValidationObject storedObject = (PolicyValidationObject) pm.find(id, PolicyValidationObject.class);
			log.debug("deletePolicyValidationObject: Stored object:\n{}", storedObject);

			// check if object is readonly
			checkReadonly(storedObject);

			// Remove object from RDF persistent store
			log.debug("deletePolicyValidationObject: Deleting policy validation object with id = {}", id);
			pm.remove(storedObject);
			log.debug("deletePolicyValidationObject: Object deleted from RDF persistent store");
			return createResponse(HTTP_STATUS_OK, "Result=Deleted");
		} catch (Exception e) {
			log.error("deletePolicyValidationObject: EXCEPTION THROWN: ", e);
			log.debug("deletePolicyValidationObject: Returning Status {}", HTTP_STATUS_ERROR);
			return createResponse(HTTP_STATUS_ERROR, "Result=Exception: "+e);
		}
	}

	// DELETE /opt/validation/{id}/all
	// Description: Delete a policy validation object and its children
	@RequestMapping(value = "/{id}/all", method = DELETE)
	public ResponseEntity<String> deletePolicyValidationObjectAndChildren(@PathVariable("id") String id) {
		try {
			log.debug("deletePolicyValidationObjectAndChildren: Deleting policy validation object with id = {} and its children", id);

			RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();

			// retrieve the stored version of object
			log.debug("deletePolicyValidationObjectAndChildren: Retrieving stored object with id = {}", id);
			PolicyValidationObject storedObject = (PolicyValidationObject) pm.find(id, PolicyValidationObject.class);
			log.debug("deletePolicyValidationObjectAndChildren: Stored object:\n{}", storedObject);

			// check if object is readonly
			checkReadonly(storedObject);

			// Remove object and children from RDF persistent store
			String deleteStmt = String.format("DELETE {?x ?y ?z} WHERE { ?s <http://purl.org/dc/terms/identifier>  \"%s\" . ?x <http://www.w3.org/2004/02/skos/core#broader>* ?s . ?x ?y ?z . } ", id);
			clientFactory.getInstance().execute( deleteStmt );
			
			log.debug("deletePolicyValidationObjectAndChildren: Object deleted from RDF persistent store");
			return createResponse(HTTP_STATUS_OK, "Result=Deleted");
		} catch (Exception e) {
			log.error("deletePolicyValidationObjectAndChildren: EXCEPTION THROWN: ", e);
			log.debug("deletePolicyValidationObjectAndChildren: Returning Status {}", HTTP_STATUS_ERROR);
			return createResponse(HTTP_STATUS_ERROR, "Result=Exception: "+e);
		}
	}

	// POST /opt/validation/{id}/reorder-children
	// Description: Stores the order (positions) of policy validation objects inside a policy validation group/set
	@RequestMapping(value = "/{id}/reorder-children", method = POST, consumes = APPLICATION_JSON_VALUE)
	public String reorderPolicyValidationObjects(@PathVariable("id") String id, @RequestBody List<String> childrenId) throws Exception {
		log.info("reorderPolicyValidationObjects: id={}, children-ids={}", id, childrenId);

		// check if duplicates exist in given chidren id's
		Set<String> childrenIdSet = new HashSet<>(childrenId);
		if (childrenIdSet.size() != childrenId.size())
			throw new IllegalArgumentException("Provided children id set contains duplicate values: "+childrenId);

		// get stored policy validation object children and check if they match to childrenId's
		RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();
		List<PolicyValidationObject> children = _retrieveChildrenInternal(pm, id);
		Set<String> childrenIdSetStored = children.stream().map(AbstractObject::getId).collect(Collectors.toSet());
		log.info("reorderPolicyValidationObjects: Stored policy validation set rules: id={}, children-ids={}", id, childrenIdSetStored);
		if (! childrenIdSetStored.equals(childrenIdSet))
			throw new IllegalArgumentException("Provided children id set does not match the stored children set for policy validation object: "+id);

		// set new children positions
		Map<String, PolicyValidationObject> childrenMap = children.stream().collect(Collectors.toMap(AbstractObject::getId, x -> x));
		log.info("reorderPolicyValidationObjects: policy validation object children map: id={}, children-map={}", id, childrenMap);
		for (int i=0, n=childrenId.size(); i<n; i++) {
			String rId = childrenId.get(i);
			PolicyValidationObject object = childrenMap.get(rId);
			object.setPosition(i);
		}

		// save changes
		log.info("reorderPolicyValidationObjects: Storing updated children for policy validation object: {}", id);
		for (PolicyValidationObject object : children)
			pm.merge(object);

		log.info("reorderPolicyValidationObjects: Children stored");
		return "SAVED NEW OBJECT POSITIONS";
	}

	// ------------------------------------------------------------------------
	// Helper methods (Non-API)
	// ------------------------------------------------------------------------

	private void preparePolicyValidationObjectForCommunication(@NotNull PolicyValidationObject object) {
		if (object==null) return;

		// prepare object expression for serialization to JSON
		if (object.getPolicyValidationExpression()!=null) {
			object.getPolicyValidationExpression().populatePropertyInstances(attributeRestController);
			object.getPolicyValidationExpression().prepareForSerialization();
		}
	}

	private void preparePolicyValidationObjectForPersistence(@NotNull PolicyValidationObject object, RdfPersistenceManager pm) throws Exception {
		if (object==null) return;

		// update timestamps
		Date now = new Date();
		//object.setCreateTimestamp(now);
		object.setLastUpdateTimestamp(now);

		// preapre expression for persistence
		if (object.getPolicyValidationExpression()!=null) {
			object.getPolicyValidationExpression().prepareForPersistence(pm);
		}
	}

	private void checkReadonly(@NotNull PolicyValidationObject storedObject) throws IllegalAccessException {
		if (storedObject.isReadonly())
			throw new IllegalAccessException(storedObject.getClass().getSimpleName()+" is readonly: "+storedObject.getId());
	}

	private void checkPolicyValidationObject(@NotNull PolicyValidationObject object) {
		if (object.getParent()==null || POLICY_VALIDATION_TYPE.POLICY_VALIDATION_GROUP_NAME.equals(object.getType()))
			throw new IllegalArgumentException("Policy validation groups and Top-level objects cannot be created or modified or deleted");
		if (POLICY_VALIDATION_TYPE.POLICY_VALIDATION_SET_NAME.equals(object.getType()))
			checkPolicyValidationSet(object);
		if (POLICY_VALIDATION_TYPE.POLICY_VALIDATION_RULE_NAME.equals(object.getType()))
			checkPolicyValidationRule(object);
	}

	private void checkPolicyValidationSet(PolicyValidationObject set) {
		// Check mandatory values
		boolean hasMandatoryValueMissing =
				StringUtils.isBlank(set.getId()) ||
				StringUtils.isBlank(set.getName()) ||
				StringUtils.isBlank(set.getType()) ||
				StringUtils.isBlank(set.getUri()) ||
				set.getCreateTimestamp()==null ||
				set.getLastUpdateTimestamp()==null ||
				set.getParent()==null;
		if (hasMandatoryValueMissing) {
			log.error("checkPolicyValidationSet: Policy validation set is missing a mandatory value:\n{}", set);
			throw new IllegalArgumentException("Policy validation set is missing a mandatory value. Please check logs. Set Id: " + set.getId());
		}

		// Check type
		if (!POLICY_VALIDATION_TYPE.POLICY_VALIDATION_SET_NAME.contentEquals(set.getType().trim())) {
			log.error("checkPolicyValidationSet: Policy validation set has wrong type:\n{}", set);
			throw new IllegalArgumentException("Policy validation set has wrong type. Please check logs. Set Id=" + set.getId() + ", Type=" + set.getType());
		}
	}

	private void checkPolicyValidationRule(PolicyValidationObject rule) {
		// Check mandatory values
		boolean hasMandatoryValueMissing =
				StringUtils.isBlank(rule.getId()) ||
						StringUtils.isBlank(rule.getName()) ||
						StringUtils.isBlank(rule.getType()) ||
						StringUtils.isBlank(rule.getUri()) ||
						//StringUtils.isBlank(rule.getPolicyValidationOutcome()) ||
						rule.getCreateTimestamp()==null ||
						rule.getLastUpdateTimestamp()==null ||
						rule.getParent()==null;
		if (hasMandatoryValueMissing) {
			log.error("checkPolicyValidationRule: Policy validation rule is missing a mandatory value:\n{}", rule);
			throw new IllegalArgumentException("Policy validation rule is missing a mandatory value. Please check logs. Rule Id: " + rule.getId());
		}

		// Check type
		if (!POLICY_VALIDATION_TYPE.POLICY_VALIDATION_RULE_NAME.contentEquals(rule.getType().trim())) {
			log.error("checkPolicyValidationRule: Policy validation rule has wrong type:\n{}", rule);
			throw new IllegalArgumentException("Policy validation rule has wrong type. Please check logs. Rule Id=" + rule.getId() + ", Type=" + rule.getType());
		}
	}

	// ------------------------------------------------------------------------
	// Policy validation export as RDF
	// ------------------------------------------------------------------------

	protected final static String EXPORT_POLICY_VALIDATION_OBJECT_AND_CHILDREN_SPARQL_QUERY_TEMPLATE =
			"CONSTRUCT { ?s ?p ?o } \n" +
			"WHERE { \n" +
			"  { \n" +
			"    SELECT (?s1 AS ?s) (?p1 AS ?p) (?o1 AS ?o) \n" +
			"    WHERE { \n" +
			"      ?s0 a <http://www.asclepios.eu/validation#ASCLEPIOS-POLICY-VALIDATION-OBJECT> . \n" +
			"      ?s0 <http://purl.org/dc/terms/identifier> \"%s\" . \n" +
			"      ?s1 ?p1 ?o1 . \n" +
			"      ?s1 a <http://www.asclepios.eu/validation#ASCLEPIOS-POLICY-VALIDATION-OBJECT> . \n" +
			"      ?s1 <http://www.w3.org/2004/02/skos/core#broader>* ?s0 . \n" +
			"    } \n" +
			"  } \n" +
			"} ";
	protected final static String FIND_POLICY_VALIDATION_RULE_IDS_SPARQL_QUERY_TEMPLATE =
			"SELECT DISTINCT ?s \n" +
			"WHERE { \n" +
			"      ?s0 a <http://www.asclepios.eu/validation#ASCLEPIOS-POLICY-VALIDATION-OBJECT> . \n" +
			"      ?s0 <http://purl.org/dc/terms/identifier> \"%s\" . \n" +
			"      ?s a <http://www.asclepios.eu/validation#ASCLEPIOS-POLICY-VALIDATION-OBJECT> . \n" +
			"      ?s <http://www.w3.org/2004/02/skos/core#broader>* ?s0 . \n" +
			"} \n" +
			"ORDER BY ?s ";

	// GET /opt/validation/export-as-rdf/{id}
	// Description: Export policy validation object description, its children and its expression
	@RequestMapping(value = "/export-as-rdf/{id}", method = GET)
	public String exportPolicyValidationSetAsRdf(@PathVariable("id") String id) throws Exception {
		log.info("-------------- exportPolicyValidationSetAsRdf: BEGIN: set-id={}", id);

		// check if 'id' is valid (prevent sparql injection attacks)
		if (! id.matches("^[0-9A-Za-z-]+$")) {
			throw new IllegalArgumentException("Invalid 'id': "+id);
		}

		// Build triple store query and post data
		String queryStr = String.format(EXPORT_POLICY_VALIDATION_OBJECT_AND_CHILDREN_SPARQL_QUERY_TEMPLATE, id);
		String postData = "output=ttl&query="+queryStr;

		// Query SPARQL server for policy validation set RDF
		String url = properties.getFrontend().getTriplestoreQueryUrl();
		log.info("-------------- exportPolicyValidationSetAsRdf: Querying export service: {}", url);
		String responseContents = callService(url, HttpMethod.POST,
				postData, MediaType.APPLICATION_FORM_URLENCODED, String.class);
		log.trace("-------------- exportPolicyValidationSetAsRdf: Response: body: {}", responseContents);
		if (responseContents == null)
			return "";

		// Export RDF of validation rule expressions
		StringBuilder sb = new StringBuilder(responseContents).append("\n");
		RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();

		String queryStr2 = String.format(FIND_POLICY_VALIDATION_RULE_IDS_SPARQL_QUERY_TEMPLATE, id);
		List<PolicyValidationObject> rulesList = pm.findByQuery(PolicyValidationObject.class, queryStr2).stream()
				.map(o -> (PolicyValidationObject) o)
				.collect(Collectors.toList());
		for (PolicyValidationObject rule : rulesList) {
			if (rule.getPolicyValidationExpression()!=null) {
				String exprStr = attributeRestController
						.exportExpressionGraphAsRdf(rule.getPolicyValidationExpression().getId());
				sb.append(exprStr);
				sb.append("\n");
			}
		}
		String rdfExport = sb.toString();

		log.trace("-------------- exportPolicyValidationSetAsRdf: END: \n{}", rdfExport);
		return rdfExport;
	}

	// ------------------------------------------------------------------------
	// Policy Validation methods
	// ------------------------------------------------------------------------

	// GET /opt/validation/validate/{id}
	// Description: Validates all policies using the policy validation object specified by 'id' and its children
	@RequestMapping(value = "/validate-policies-with/{id}", method = GET, produces = APPLICATION_JSON_VALUE)
	public List<PolicyValidationResult> validatePoliciesWithObjectAndChildren(@PathVariable("id") String id) throws Exception {
		log.debug("validatePoliciesWithObjectAndChildren: Validate all policies with policy validation object with id = {} and its children", id);

		RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();

		// retrieve the stored version of object
		log.debug("validatePoliciesWithObjectAndChildren: Retrieving stored object with id = {}", id);
		PolicyValidationObject storedObject = (PolicyValidationObject) pm.find(id, PolicyValidationObject.class);
		log.debug("validatePoliciesWithObjectAndChildren: Stored object:\n{}", storedObject);

		// validate policies with retrieved object and its children
		List<PolicyValidationResult> results = policyValidationService.validatePoliciesWithPolicyValidationObject(storedObject);

		log.debug("validatePoliciesWithObjectAndChildren: Policy validation results: {}", results);
		return results;
	}

	// GET /opt/validation/validate/policy/{id}
	// Description: Validates ABAC or ABE policy using all active policy validation objects
	@RequestMapping(value = "/validate/policy/{id}", method = GET, produces = APPLICATION_JSON_VALUE)
	public List<PolicyValidationResult> validateAbacOrAbePolicy(@PathVariable("id") String id) throws Exception {
		log.debug("validateAbacOrAbePolicy: Validate ABAC or ABE policy using all active policy validation objects: policy-id = {}", id);

		// check if given 'id' is valid
		if (! id.matches("^[A-Za-z0-9_-]+$"))
			throw new IllegalArgumentException("Invalid policy id passed: "+id);

		RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();

		// retrieve the stored version of object
		log.debug("validateAbacOrAbePolicy: Retrieving stored object with id = {}", id);
		String query = String.format("SELECT DISTINCT ?s WHERE { ?s <http://purl.org/dc/terms/identifier> \"%s\" }", id);
		List<Object> list = pm.findByQuery(query);
		if (list.size()!=1)
			throw new IllegalArgumentException("Given id returns "+list.size()+" results. It must return exactly 1 object: "+id);
		Object policy = list.get(0);
		log.debug("validateAbacOrAbePolicy: Stored object:\n{}", policy);

		// validate policy by calling Policy Validation Service
		List<PolicyValidationResult> results;
		if (policy instanceof AbacPolicy) {
			// validate ABAC policy
			log.debug("validateAbacOrAbePolicy: It is an ABAC policy");
			results = policyValidationService.validateAbacPolicy((AbacPolicy) policy);
		} else
		if (policy instanceof AbePolicy) {
			// validate ABE policy
			log.debug("validateAbacOrAbePolicy: It is an ABE policy");
			results = policyValidationService.validateAbePolicy((AbePolicy) policy);
		} else
			throw new IllegalArgumentException("Given id does not identify an ABAC or ABE policy: "+id);

		log.debug("validateAbacOrAbePolicy: Policy validation results: {}", results);
		return results;
	}
}
