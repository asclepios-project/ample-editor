/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.rest;

import eu.asclepios.ample.datastore.DatastoreClientFactory;
import eu.asclepios.ample.model.AbstractObject;
import eu.asclepios.ample.model.casm.CASM_PROPERTY_TYPE;
import eu.asclepios.ample.model.casm.CASM_TYPE;
import eu.asclepios.ample.model.casm.CasmObject;
import eu.asclepios.ample.model.expr.CompositeExpression;
import eu.asclepios.ample.model.expr.Expression;
import eu.asclepios.ample.persistence.RdfPersistenceManager;
import eu.asclepios.ample.persistence.RdfPersistenceManagerFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@Slf4j
@Service
@RestController
@RequiredArgsConstructor
@RequestMapping("/opt/attributes")
public class AttributeRestController extends AbstractRestController {
	private final DatastoreClientFactory clientFactory;

	// GET /opt/attributes/
	// Description: Get a list of top-level schema elements
	@RequestMapping(value = "/", method = GET, produces = APPLICATION_JSON_VALUE)
	public CasmObject[] getTopLevelAttributes() {
		try {
			RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();
			
			String rdfType = pm.getClassRdfType(CasmObject.class);
			String parentUri = pm.getFieldUri(CasmObject.class, "parent");
			String queryStr = "SELECT ?s WHERE { ?s  a  <"+rdfType+"> .  ?s <http://purl.org/dc/terms/title> ?title .  FILTER NOT EXISTS { ?s  <"+parentUri+">  ?s1 } .  FILTER ( ?s != <http://www.melodic.eu/metadata-schema/types#null> ) } ORDER BY ?title";
			
			log.debug("getTopLevelAttributes: Retrieving top-level schema elements");
			List<Object> list = pm.findByQuery(queryStr);
			log.debug("{} top-level schema elements found", list.size());
			return list.toArray(new CasmObject[0]);
		} catch (Exception e) {
			log.error("getTopLevelAttributes: EXCEPTION THROWN: ", e);
			log.debug("getTopLevelAttributes: Returning an empty array of {}", CasmObject.class);
			return new CasmObject[0];
		}
	}
	
	// GET /opt/attributes/all
	// Description: Get a list of ALL schema elements
	@RequestMapping(value = "/all", method = GET, produces = APPLICATION_JSON_VALUE)
	public CasmObject[] getAllAttributes() {
		try {
			RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();

			String rdfType = pm.getClassRdfType(CasmObject.class);
			String parentUri = pm.getFieldUri(CasmObject.class, "parent");
			String queryStr = "SELECT ?s WHERE { ?s  a  <"+rdfType+"> .  ?s <http://purl.org/dc/terms/title> ?title .  FILTER ( ?s != <http://www.melodic.eu/metadata-schema/types#null> ) } ORDER BY ?title";
			
			log.debug("getAllAttributes: Retrieving ALL schema elements");
			List<Object> list = pm.findByQuery(queryStr);
			log.debug("{} schema elements found", list.size());
			return list.toArray(new CasmObject[0]);
		} catch (Exception e) {
			log.error("getAllAttributes: EXCEPTION THROWN: ", e);
			log.debug("getAllAttributes: Returning an empty array of {}", CasmObject.class);
			return new CasmObject[0];
		}
	}
	
	// GET /opt/attributes/{attr_id}
	// Description: Get attribute description
	@RequestMapping(value = "/{attr_id}", method = GET, produces = APPLICATION_JSON_VALUE)
	public CasmObject getAttribute(@PathVariable("attr_id") String id) {
		try {
			RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();
			log.debug("getAttribute: Retrieving schema element with id = {}", id);
			CasmObject attr = (CasmObject)pm.find(id, CasmObject.class);
			log.debug("getAttribute: Schema element {} :\n{}", id, attr);
			return attr;
		} catch (Exception e) {
			log.error("getAttribute: EXCEPTION THROWN: ", e);
			log.debug("getAttribute: Returning an empty instance of {}", CasmObject.class);
			return new CasmObject();
		}
	}
	
	// GET /opt/attributes/{attr_id}/subattributes
	// Description: Get a list of attribute subattributes (if any)
	@RequestMapping(value = "/{attr_id}/subattributes", method = GET, produces = APPLICATION_JSON_VALUE)
	public CasmObject[] getAttributeSubattributes(@PathVariable("attr_id") String id) {
		try {
			RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();
			
			String attrRdfType = pm.getClassRdfType(CasmObject.class);
			String parentFieldUri = pm.getFieldUri(CasmObject.class, "parent");
			String parentId = pm.getObjectUri(id, CasmObject.class);
			String queryStr = "SELECT ?s WHERE { ?s  a  <"+attrRdfType+"> ; <"+parentFieldUri+">  <"+parentId+"> ; <http://purl.org/dc/terms/title> ?title } ORDER BY ?title";
			
			log.debug("getAttributeSubattributes: Retrieving sub-elements of schema element with id = {}", id);
			List<Object> list = pm.findByQuery( String.format(queryStr, id) );
			log.debug("{} sub-elements found for schema element with id = {}", list.size(), id);
			return list.toArray(new CasmObject[0]);
		} catch (Exception e) {
			log.error("getAttributeSubattributes: EXCEPTION THROWN: ", e);
			log.debug("getAttributeSubattributes: Returning an empty array of {}", CasmObject.class);
			return new CasmObject[0];
		}
	}
	
	// PUT /opt/attributes/
	// Description: Create a new schema element
	@RequestMapping(value = "/", method = PUT, consumes = APPLICATION_JSON_VALUE)
	public ResponseEntity<String> createAttribute(@RequestBody CasmObject attr) {
		checkAttribute(attr);
		try {
			RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();
			log.debug("createAttribute: Creating a new schema element with id = {}", attr.getId());
			log.debug("New attribute values:\n{}", attr);
			pm.persist(attr);
			log.debug("Object added to RDF persistent store");
			return createResponse(HTTP_STATUS_CREATED, "Result=Created");
		} catch (Exception e) {
			log.error("createAttribute: EXCEPTION THROWN: ", e);
			log.debug("createAttribute: Returning Status {}", HTTP_STATUS_ERROR);
			return createResponse(HTTP_STATUS_ERROR, "Result=Exception: "+e);
		}
	}
	
	// POST /opt/attributes/{attr_id}
	// Description: Update an schema element's description
	@RequestMapping(value = "/{attr_id}", method = POST, consumes = APPLICATION_JSON_VALUE)
	public ResponseEntity<String> updateAttribute(@PathVariable("attr_id") String id, @RequestBody CasmObject attr) {
		try {
			RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();
			log.debug("updateAttribute: Retrieving stored schema element with id = {}", id);
			CasmObject storedAttr = (CasmObject) pm.find(id, CasmObject.class);
			log.debug("updateAttribute: Stored schema element:\n{}", storedAttr);

			log.debug("updateAttribute: Updating schema element with id = {}", id);
			Date createTm = storedAttr.getCreateTimestamp();
			if (createTm==null) createTm = new Date(0L);
			attr.setCreateTimestamp(createTm);
			attr.setLastUpdateTimestamp(new Date());
			log.debug("updateAttribute: New attribute values:\n{}", attr);
			checkAttribute(attr);
			pm.attach(attr);

			log.debug("updateAttribute: Object attached to RDF persistent store manager");
			pm.merge(attr);
			log.debug("updateAttribute: Persistent store state updated");
			return createResponse(HTTP_STATUS_OK, "Result=Updated");
		} catch (Exception e) {
			log.error("updateAttribute: EXCEPTION THROWN: ", e);
			log.debug("updateAttribute: Returning Status {}", HTTP_STATUS_ERROR);
			return createResponse(HTTP_STATUS_ERROR, "Result=Exception: "+e);
		}
	}
	
	// DELETE /opt/attributes/{attr_id}
	// Description: Delete an schema element
	@RequestMapping(value = "/{attr_id}", method = DELETE)
	public ResponseEntity<String> deleteAttribute(@PathVariable("attr_id") String id) {
		try {
			RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();
			log.debug("deleteAttribute: Deleting schema element with id = {}", id);
			Object o = pm.find(id, CasmObject.class);
			log.debug("Object retrieved from RDF persistent store");
			pm.remove(o);
			log.debug("Object deleted from RDF persistent store");
			return createResponse(HTTP_STATUS_OK, "Result=Deleted");
		} catch (Exception e) {
			log.error("deleteAttribute: EXCEPTION THROWN: ", e);
			log.debug("deleteAttribute: Returning Status {}", HTTP_STATUS_ERROR);
			return createResponse(HTTP_STATUS_ERROR, "Result=Exception: "+e);
		}
	}
	
	// DELETE /opt/attributes/{attr_id}/all
	// Description: Delete an schema element and its sub-elements
	@RequestMapping(value = "/{attr_id}/all", method = DELETE)
	public ResponseEntity<String> deleteAttributeAndSubattributes(@PathVariable("attr_id") String id) {
		try {
			log.debug("deleteAttributeAndSubattributes: Deleting schema element with id = {} and its sub-elements", id);
			
			// Remove attribute and sub-attributes from RDF persistent store
			RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();
			String deleteStmt = String.format("DELETE {?x ?y ?z} WHERE { ?s <http://purl.org/dc/terms/identifier>  \"%s\" . ?x <http://www.w3.org/2004/02/skos/core#broader>* ?s . ?x ?y ?z . } ", id);
			clientFactory.getInstance().execute( deleteStmt );
			
			log.debug("Object deleted from RDF persistent store");
			return createResponse(HTTP_STATUS_OK, "Result=Deleted");
		} catch (Exception e) {
			log.error("deleteAttributeAndSubattributes: EXCEPTION THROWN: ", e);
			log.debug("deleteAttributeAndSubattributes: Returning Status {}", HTTP_STATUS_ERROR);
			return createResponse(HTTP_STATUS_ERROR, "Result=Exception: "+e);
		}
	}

	@RequestMapping(value = "/{attr_id}/reorder-children", method = POST, consumes = APPLICATION_JSON_VALUE)
	public String reorderChildren(@PathVariable("attr_id") String id, @RequestBody List<String> childrenIds) throws Exception {
		log.info("reorderChildren: attr-id={}, children-ids={}", id, childrenIds);

		// check if duplicates exist in given children id's
		Set<String> childrenIdSet = new HashSet<>(childrenIds);
		if (childrenIdSet.size() != childrenIds.size())
			throw new IllegalArgumentException("Provided children id set contains duplicate values: "+childrenIds);

		// get stored children and check if they match to children id's
		CasmObject[] storedChildren = getAttributeSubattributes(id);
		Set<String> storedChildrenIdSet = Arrays.stream(storedChildren).map(AbstractObject::getId).collect(Collectors.toSet());
		log.info("reorderChildren: Stored attribute: attr-id={}, children-ids={}", id, childrenIdSet);
		if (! storedChildrenIdSet.equals(childrenIdSet))
			throw new IllegalArgumentException("Provided children id set does not match the stored children set for attribute: "+id);

		// set new children positions
		Map<String, CasmObject> childrenMap = Arrays.stream(storedChildren).collect(Collectors.toMap(AbstractObject::getId, x -> x));
		log.info("reorderChildren: Attribute children map: attr-id={}, children-map={}", id, childrenMap);
		for (int i=0, n=childrenIds.size(); i<n; i++) {
			String rId = childrenIds.get(i);
			CasmObject child = childrenMap.get(rId);
			child.setPosition(i);
		}

		// save changes
		log.info("reorderChildren: Storing updated children nodes: parent attr-id={}", id);
		RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();
		for (CasmObject child : storedChildren)
			pm.merge(child);

		log.info("reorderChildren: Children nodes stored");
		return "SAVED NEW CHILDREN POSITIONS";
	}

	private void checkAttribute(@NotNull CasmObject casmObject) {
		// Check mandatory values
		boolean hasMandatoryValueMissing =
				StringUtils.isBlank(casmObject.getId()) ||
						StringUtils.isBlank(casmObject.getName()) ||
						StringUtils.isBlank(casmObject.getType()) ||
						StringUtils.isBlank(casmObject.getUri()) ||
						casmObject.getCreateTimestamp()==null ||
						casmObject.getLastUpdateTimestamp()==null;
		if (hasMandatoryValueMissing) {
			log.error("checkAttribute: CASM object is missing a mandatory value:\n{}", casmObject);
			throw new IllegalArgumentException("CASM object is missing a mandatory value. Please check logs. CASM object Id: " + casmObject.getId());
		}

		// Check type
		if (! CASM_TYPE.CASM_CONCEPT_NAME.contentEquals(casmObject.getType().trim()) &&
				! CASM_TYPE.CASM_PROPERTY_NAME.contentEquals(casmObject.getType().trim()) &&
				! CASM_TYPE.CASM_CONCEPT_INSTANCE_NAME.contentEquals(casmObject.getType().trim()))
		{
			log.error("checkAttribute: CASM object has wrong type:\n{}", casmObject);
			throw new IllegalArgumentException("CASM object has wrong type. Please check logs. CASM object Id=" + casmObject.getId() + ", Type=" + casmObject.getType());
		}

		// Extra checks, if it is a PROPERTY
		if (CASM_TYPE.CASM_PROPERTY_NAME.contentEquals(casmObject.getType().trim())) {
			checkProperty(casmObject);
		}
	}

	private void checkProperty(@NotNull CasmObject casmObject) {
		// Check mandatory values
		boolean hasMandatoryValueMissing =
				StringUtils.isBlank(casmObject.getPropertyType()) ||
						StringUtils.isBlank(casmObject.getRange()) && StringUtils.isBlank(casmObject.getRangeUri());
		if (hasMandatoryValueMissing) {
			log.error("checkProperty: CASM property is missing a mandatory value:\n{}", casmObject);
			throw new IllegalArgumentException("CASM property is missing a mandatory value. Please check logs. CASM property Id: " + casmObject.getId());
		}

		// Check type
		if (! CASM_PROPERTY_TYPE.OBJECT_PROPERTY_NAME.contentEquals(casmObject.getPropertyType().trim()) &&
				! CASM_PROPERTY_TYPE.DATA_PROPERTY_NAME.contentEquals(casmObject.getPropertyType().trim()))
		{
			log.error("checkProperty: CASM property has wrong type:\n{}", casmObject);
			throw new IllegalArgumentException("CASM property has wrong type. Please check logs. CASM property Id=" + casmObject.getId() + ", Type=" + casmObject.getType());
		}
	}

	// ---------------------------------------------------------------------------
	// Methods for setting/clearing the in-ABAC and in-ABE flags
	// ---------------------------------------------------------------------------

	// SPARQL query returning all sub-node id's of a given node
	private final static String GET_IDS_OF_SUBNODES_OF_ATTRIBUTE_SPARQL_QUERY =
			"SELECT DISTINCT ?s WHERE { \n" +
			"  {\n" +
			"    SELECT ?childNode (COUNT(?midNode) AS ?cnt)\n" +
			"    WHERE {\n" +
			"      ?childNode <http://www.w3.org/2004/02/skos/core#broader>* ?midNode .\n" +
			"      ?midNode <http://www.w3.org/2004/02/skos/core#broader>* <http://www.asclepios.eu/casm/ASCLEPIOS-OBJECT#%s> .\n" +
			"    }\n" +
			"    GROUP BY ?childNode\n" +
			"    ORDER BY count(?midNode)\n" +
			"  }\n" +
			"  ?s ?p ?o .\n" +
			"  FILTER ( ?s = ?childNode ) .\n" +
			"}";

	@RequestMapping(value = "/{attr_id}/{operation}/use-in/{policy_type}", method = GET)
	public String setUseInPolicies(
			@PathVariable("attr_id") String id,
			@PathVariable("operation") CasmObject.POLICY_USE operation,
			@PathVariable("policy_type") CasmObject.POLICY_TYPE policyType)
	{
		log.info("setUseInPolicies: INPUT: attr-id={}, operation={}, policy-type={}", id, operation, policyType);

		try {
			RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();

			// retrieve all sub-attributes of given attribute (including itself)
			log.debug("setUseInPolicies: Retrieving sub-attributes of attribute: id={}", id);
			String queryStr = String.format(GET_IDS_OF_SUBNODES_OF_ATTRIBUTE_SPARQL_QUERY, id);
			List<CasmObject> list = pm.findByQuery(queryStr).stream().map(o -> (CasmObject)o).collect(Collectors.toList());
			log.debug("{} schema elements found", list.size());
			log.debug("setUseInPolicies: Attibutes retrieved:\n{}", list.stream().map(AbstractObject::getName).collect(Collectors.toList()));

			// setting inABAC and inABE flags
			boolean setUseFlag = operation== CasmObject.POLICY_USE.set;
			if (policyType== CasmObject.POLICY_TYPE.abac)
				list.forEach(so -> so.setInAbac(setUseFlag));
			if (policyType== CasmObject.POLICY_TYPE.abe)
				list.forEach(so -> so.setInAbe(setUseFlag));

			// saving changes
			for (CasmObject so : list)
				pm.merge(so);

			String message = operation.toString().toUpperCase()+"-USE-IN-"+policyType.toString().toUpperCase()+" for "+list.size()+" attributes";
			log.info("setUseInPolicies: OUTPUT: {}", message);
			return message;
		} catch (Exception e) {
			log.error("setUseInPolicies: EXCEPTION THROWN: ", e);
			return "EXCEPTION: "+e.getMessage();
		}
	}

	// ------------------------------------------------------------------------
	// Search methods used to support dynamic select lists (select2)
	// ------------------------------------------------------------------------

	// GET /opt/attributes/search{in_scope}/by-name/{term}
	// Description: Search for attributes with names containing the given term
	@RequestMapping(value = "/search{in_scope}/by-name/{term}", method = GET, produces = APPLICATION_JSON_VALUE)
	public List<CasmObject> findAttributesByName(@PathVariable("in_scope") String inScope, @PathVariable("term") String term) {
		try {
			log.debug("findAttributesByName: Looking for attributes with names containing: term={}, in-scope={}", term, inScope);

			// Search for attributes with names containing the given term
			RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();
			String rdfType = pm.getClassRdfType(CasmObject.class);
			String policyFilter = "";
			if (StringUtils.isNotBlank(inScope)) {
				if (inScope.contains("-in-abac")) policyFilter += "  ?s <http://www.asclepios.eu/casm#inAbac> true . \n";
				if (inScope.contains("-in-abe")) policyFilter += "  ?s <http://www.asclepios.eu/casm#inAbe> true . \n";
			}
			int resultsLimit = 50;
			String queryStr =
					"SELECT ?s WHERE { "+
					"  ?s  a  <"+rdfType+"> . "+
					"  ?s <http://purl.org/dc/terms/title> ?title . "+
					policyFilter +
					"  ?s <http://purl.org/dc/elements/1.1/type> \"CONCEPT\" . " +
					"  FILTER ( CONTAINS(UCASE(?title), \""+term.toUpperCase()+"\") ) "+
					"} ORDER BY ?title LIMIT "+resultsLimit;

			log.debug("findAttributesByName: Retrieving schema elements with names containing: {}", term);
			List<Object> list = pm.findByQuery(queryStr);
			log.debug("findAttributesByName: {} schema elements found", list.size());

			List<CasmObject> results = list.stream().map(o -> (CasmObject)o).collect(Collectors.toList());
			log.debug("findAttributesByName: List to return: {}", results);
			return results;
		} catch (Exception e) {
			log.error("findAttributesByName: EXCEPTION THROWN: ", e);
			log.debug("findAttributesByName: Returning empty list");
			return Collections.emptyList();
		}
	}

	// SPARQL query returning parent concepts' properties too
	private final static String FIND_PROPERTIES_BY_ATTRIBUTE_SPARQL_QUERY =
			"SELECT DISTINCT ?sProperty " +
			"WHERE { " +
			"  { " +
			"    SELECT ?sConcept ?titleConcept (COUNT(?midConcept) AS ?cnt) " +
			"    WHERE { " +
			"      ?sConcept  a  <http://www.asclepios.eu/casm#ASCLEPIOS-OBJECT> . " +
			"  	   ?sConcept <http://purl.org/dc/elements/1.1/type> \"CONCEPT\" . " +
			"  	   ?sConcept <http://purl.org/dc/terms/title> ?titleConcept . " +
			"  	   <http://www.asclepios.eu/casm/ASCLEPIOS-OBJECT#%s>  " +
			"  	     <http://www.w3.org/2004/02/skos/core#broader>* ?midConcept . " +
			"  	   ?midConcept <http://www.w3.org/2004/02/skos/core#broader>* ?sConcept . " +
			"    } " +
			"    GROUP BY ?sConcept ?titleConcept " +
			"    ORDER BY count(?midConcept) " +
			"  } " +
			"  ?sProperty <http://www.w3.org/2004/02/skos/core#broader> ?sConcept . " +
			"%s" +
			"  ?sProperty <http://purl.org/dc/elements/1.1/type> \"PROPERTY\" . " +
			"  ?sProperty <http://purl.org/dc/terms/title> ?titleProperty . " +
			"  ?sProperty ?p ?o . " +
			"} " +
			"ORDER BY ?cnt ?titleConcept ?titleProperty ?p " +
			"LIMIT %d"
			;

	// GET /opt/attributes/search{in_scope}/properties/by-attribute/{attr_id}
	// Description: Looking for properties belonging to attribute with the given id
	@RequestMapping(value = "/search{in_scope}/properties/by-attribute/{attr_id}", method = GET, produces = APPLICATION_JSON_VALUE)
	public List<CasmObject> findPropertiesByAttribute(@PathVariable("in_scope") String inScope, @PathVariable("attr_id") String attrId) {
		try {
			log.debug("findPropertiesByAttribute: Looking for properties belonging to attribute: id={}, in-scope={}", attrId, inScope);

			// Search for properties belonging to attribute with given id
			RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();
			//String rdfType = pm.getClassRdfType(CasmObject.class);
			String policyFilter = "";
			if (StringUtils.isNotBlank(inScope)) {
				if (inScope.contains("-in-abac")) policyFilter += "  ?s <http://www.asclepios.eu/casm#inAbac> true . \n";
				if (inScope.contains("-in-abe")) policyFilter += "  ?s <http://www.asclepios.eu/casm#inAbe> true . \n";
			}
			int resultsLimit = Integer.MAX_VALUE;
			String queryStr =
					String.format(FIND_PROPERTIES_BY_ATTRIBUTE_SPARQL_QUERY, attrId, policyFilter, resultsLimit);

			log.debug("findPropertiesByAttribute: Retrieving properties belonging to attribute: {}", attrId);
			List<Object> list = pm.findByQuery(queryStr);
			List<CasmObject> results = list.stream().map(o -> (CasmObject)o).collect(Collectors.toList());
			log.debug("findPropertiesByAttribute: {} properties found", results.size());

			// For Object properties, search for range (concept) instances, if any
			results.stream()
					.filter(p -> "ObjectProperty".equals(p.getPropertyType()))
					.forEach(p -> {
						populatePropertyInstances(p);
						log.debug("findPropertiesByAttribute: Range instances for property {}: {}", p.getId(), p.getInstances().size());
					});

			log.debug("findPropertiesByAttribute: List to return: {}", results);
			return results;
		} catch (Exception e) {
			log.error("findPropertiesByAttribute: EXCEPTION THROWN: ", e);
			log.debug("findPropertiesByAttribute: Returning empty list");
			return Collections.emptyList();
		}
	}

	public void populatePropertyInstances(CasmObject property) {
		List<CasmObject> instances = findPropertiesRangeInstances(property);
		List<CasmObject.MinimalCasmObject> minimalInstances = instances.stream()
				.map(CasmObject.MinimalCasmObject::new)
				.collect(Collectors.toList());
		property.setInstances(minimalInstances);
	}

	public List<CasmObject> findPropertiesRangeInstances(CasmObject property) {
		try {
			log.debug("findPropertiesRangeInstances: Looking for range instances belonging to property: {}", property.getId());

			// check if it is an ObjectProperty
			if (! "ObjectProperty".equals(property.getPropertyType())) {
				log.debug("findPropertiesRangeInstances: Not an object property. Returning empty list: property-id={}", property.getId());
				return Collections.emptyList();
			}

			// Search for range instances belonging to the given property
			RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();
			String rdfType = pm.getClassRdfType(CasmObject.class);
			String rangeConceptId = property.getRange();
			String queryStr =
					"SELECT ?s WHERE {"+
							"  ?s  a  <"+rdfType+"> . "+
							"  ?s <http://purl.org/dc/elements/1.1/type> \"CONCEPT-INSTANCE\" . " +
							"  ?s <http://www.w3.org/2004/02/skos/core#broader> <http://www.asclepios.eu/casm/ASCLEPIOS-OBJECT#"+rangeConceptId+"> . " +
							"} ORDER BY ?title ";

			log.debug("findPropertiesRangeInstances: Retrieving range instances for property: {}", property.getId());
			List<Object> list = pm.findByQuery(queryStr);
			List<CasmObject> results = list.stream().map(o -> (CasmObject)o).collect(Collectors.toList());
			log.debug("findPropertiesRangeInstances: {} properties found", results.size());

			log.debug("findPropertiesRangeInstances: List to return: {}", results);
			return results;
		} catch (Exception e) {
			log.error("findPropertiesRangeInstances: EXCEPTION THROWN: ", e);
			log.debug("findPropertiesRangeInstances: Returning empty list");
			return Collections.emptyList();
		}
	}

	// ------------------------------------------------------------------------
	// Expression export as RDF
	// ------------------------------------------------------------------------

	private static final String EXPORT_BY_ID_SPARQL_QUERY_TEMPLATE =
			"output=text&query=CONSTRUCT { ?s ?p ?o } " +
					"WHERE { ?s ?p ?o . ?s <http://purl.org/dc/terms/identifier> \"%s\" . }";

	@RequestMapping(value = "/exportExpressionGraphAsRdf/{expr_id}", method = GET)
	public String exportExpressionGraphAsRdf(@PathVariable("expr_id") String id) throws Exception {
		log.info("-------------- exportExpressionGraphAsRdf: BEGIN: expr-id={}", id);

		// Add root expression and its subexpressions in the expressions lists
		List<Expression> expressions = new LinkedList<>();
		RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();
		Expression rootExpr = (Expression) pm.find(id, CompositeExpression.class);
		enlistSubexpressions(rootExpr, expressions);

		// Export RDF for each enlisted expression
		StringBuilder sb = new StringBuilder();
		final String url = properties.getFrontend().getTriplestoreQueryUrl();
		log.info("exportExpressionGraphAsRdf: Export service url: {}", url);

		expressions.stream()
				.map(AbstractObject::getId)
				.forEach(exprId -> {
					// prepare request content (containing SPARQL query)
					String requestStr = String.format(EXPORT_BY_ID_SPARQL_QUERY_TEMPLATE, exprId);
					log.debug("exportExpressionGraphAsRdf: Exporting RDF of expression: id={}, url={}", exprId, url);

					// contact SPARQL server
					String responseStr = callService(url, HttpMethod.POST,
							requestStr, MediaType.APPLICATION_FORM_URLENCODED, String.class);
					log.trace("exportExpressionGraphAsRdf: Response: body: {}", responseStr);

					// process SPARQL server response (containing query results)
					if (responseStr==null) responseStr = "";
					sb.append(responseStr);
					sb.append("\n");
				});
		String exportedRdf = sb.toString();

		log.debug("-------------- exportExpressionGraphAsRdf: END: content-length={}", exportedRdf.length());
		log.trace("-------------- exportExpressionGraphAsRdf: END: content-length={}\n{}", exportedRdf.length(), exportedRdf);
		return exportedRdf;
	}

	protected void enlistSubexpressions(@NotNull Expression expr, List<Expression> expressions) {
		Objects.requireNonNull(expr, "Argument #0 (expression) cannot be null");
		Objects.requireNonNull(expressions, "Argument #1 (expressions list) cannot be null");

		// enlist expression
		expressions.add(expr);

		// enlist sub-expressions
		if (expr instanceof CompositeExpression) {
			CompositeExpression cExpr = (CompositeExpression)expr;
			for (Expression subExpr : cExpr.getExpressions()) {
				enlistSubexpressions(subExpr, expressions);
			}
		}
	}

	// ------------------------------------------------------------------------
	// CASM export as RDF
	// ------------------------------------------------------------------------

	protected final static String EXPORT_CASM_SPARQL_QUERY =
			"CONSTRUCT { ?s ?p ?o } \n" +
			"WHERE { \n" +
			"  ?s a <http://www.asclepios.eu/casm#ASCLEPIOS-OBJECT> . \n" +
			"  ?s ?p ?o . \n" +
			"} \n";

	// GET /opt/attributes/export-casm-as-rdf
	// Description: Export CASM items and their descriptions
	@RequestMapping(value = "/export-casm-as-rdf", method = GET)
	public String exportCasmAsRdf() throws Exception {
		log.info("-------------- exportCasmAsRdf: BEGIN: n/a");

		// Build triple store query and post data
		String postData = "output=ttl&query="+EXPORT_CASM_SPARQL_QUERY;

		// Query SPARQL server for CASM RDF
		String url = properties.getFrontend().getTriplestoreQueryUrl();
		log.info("-------------- exportCasmAsRdf: Querying export service: {}", url);
		String rdfExport = callService(url, HttpMethod.POST,
				postData, MediaType.APPLICATION_FORM_URLENCODED, String.class);
		log.trace("-------------- exportCasmAsRdf: Response: body: {}", rdfExport);
		if (rdfExport == null)
			return "";

		log.trace("-------------- exportCasmAsRdf: END: \n{}", rdfExport);
		return rdfExport;
	}
}
