/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.rest;

import eu.asclepios.ample.model.AbstractObject;
import eu.asclepios.ample.util.EscapeDataUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.RequestBodyAdvice;

import java.lang.reflect.Type;
import java.util.Collection;

@Slf4j
@ControllerAdvice
public class EscapeRequestDataAdvice implements RequestBodyAdvice {
	@Override
	public boolean supports(MethodParameter methodParameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
		boolean supportsController = AbstractRestController.class.isAssignableFrom(methodParameter.getContainingClass());
		Class<?> targetClass = null;
		try {
			targetClass = Class.forName(targetType.getTypeName());
		} catch (ClassNotFoundException e) {
			log.debug("EscapeRequestDataAdvice.supports(): controller={}, target-type={} => Exception: {}",
					methodParameter.getContainingClass().getSimpleName(),
					targetType.getTypeName(), e.getMessage());
			return false;
		}
		boolean supportsReturnType = AbstractObject.class.isAssignableFrom(
				targetClass.isArray()
						? targetClass.getComponentType()
						: targetClass
		);
		boolean supports = supportsController && supportsReturnType;

		log.debug("EscapeRequestDataAdvice.supports(): controller={}, target-type={} => supported={}",
				methodParameter.getContainingClass().getSimpleName(),
				targetClass.getSimpleName(), supports);
		return supports;
	}

	@Override
	public HttpInputMessage beforeBodyRead(HttpInputMessage httpInputMessage, MethodParameter methodParameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
		return httpInputMessage;
	}

	@Override
	public Object afterBodyRead(Object requestBody, HttpInputMessage httpInputMessage, MethodParameter methodParameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
		log.debug("EscapeRequestDataAdvice.afterBodyRead(): target-type={}\nREQUEST BODY BEFORE ESCAPE:\n{}",
				targetType.getTypeName(), requestBody);

		if (requestBody.getClass().isArray()) {
			Object[] arr = (Object[]) requestBody;
			for (Object obj : arr) {
				if (obj instanceof  AbstractObject)
					escapeData((AbstractObject) obj);
			}
		} else if (requestBody instanceof Collection) {
			for (Object obj : (Collection<?>)requestBody) {
				if (obj instanceof  AbstractObject)
					escapeData((AbstractObject) obj);
			}
		} else {
			if (requestBody instanceof  AbstractObject)
				escapeData((AbstractObject) requestBody);
		}

		log.debug("EscapeRequestDataAdvice.afterBodyRead(): target-type={}\nREQUEST BODY AFTER ESCAPE:\n{}",
				targetType.getTypeName(), requestBody);
		return requestBody;
	}

	@Override
	public Object handleEmptyBody(Object body, HttpInputMessage httpInputMessage, MethodParameter methodParameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
		return body;
	}

	private void escapeData(AbstractObject object) { EscapeDataUtil.escapeData(object); }
}