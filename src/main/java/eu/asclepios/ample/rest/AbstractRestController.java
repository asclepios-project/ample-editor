/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.rest;

import eu.asclepios.ample.AmpleApplicationProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

@Slf4j
public class AbstractRestController {
	protected static final int HTTP_STATUS_OK = HttpStatus.OK.value();
	protected static final int HTTP_STATUS_CREATED = HttpStatus.CREATED.value();
	protected static final int HTTP_STATUS_ERROR = HttpStatus.INTERNAL_SERVER_ERROR.value();

	@Autowired
	protected AmpleApplicationProperties properties;
	@Autowired
	protected RestTemplate restTemplate;

	private final HttpHeaders headers = createHeaders();

	protected ResponseEntity<String> createResponse(int statusCode, String mesg) {
		return new ResponseEntity<String>(mesg, headers, HttpStatus.resolve(statusCode));
	}

	private HttpHeaders createHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Connection", "Keep-Alive");
		headers.add("Keep-Alive", "timeout=600, max=99");
		return headers;
	}

	protected <T> T callService(String url, HttpMethod method, String requestStr, MediaType contentType, Class<T> type) {
		// prepare request object
		HttpEntity<String> request;
		if (contentType!=null) {
			HttpHeaders requestHeaders = new HttpHeaders();
			requestHeaders.setContentType(contentType);
			request = new HttpEntity<String>(requestStr, requestHeaders);
		} else {
			request = new HttpEntity<String>(requestStr);
		}

		// contact SPARQL server
		log.debug("callService: Contacting service: url: {} {}", method, url);
		ResponseEntity<T> response =
				restTemplate.exchange(url, method, request, type);
		log.debug("callService: Response: status: {} {}, mime-type: {}",
				response.getStatusCodeValue(), response.getStatusCode().getReasonPhrase(),
				response.getHeaders().getContentType());

		// get server response
		if (response.getStatusCode().is2xxSuccessful()) {
			T responseObject = response.getBody();
			log.trace("callService: Response: body: {}", responseObject);
			return responseObject;
		} else
			throw new RuntimeException("Service request failed. Service response status: "+response.getStatusCode());
	}
}