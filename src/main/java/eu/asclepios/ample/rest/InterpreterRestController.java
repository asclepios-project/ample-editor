/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.rest;

import eu.asclepios.ample.interpreter.InterpreterAbacPolicyToXacml;
import eu.asclepios.ample.interpreter.InterpreterAbePolicyToText;
import eu.asclepios.ample.model.abac.AbacPolicy;
import eu.asclepios.ample.model.abe.AbePolicy;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.TEXT_XML_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Slf4j
@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping("/opt/interpreter")
public class InterpreterRestController extends AbstractRestController {

	private final AbacPolicyRestController abacPolicyRestController;
	private final AbePolicyRestController abePolicyRestController;
	private final InterpreterAbacPolicyToXacml interpreterAbacPolicyToXacml;
	private final InterpreterAbePolicyToText interpreterAbePolicyToText;

	// GET /opt/interpreter/abac-policy-to-xacml/{policy_id}
	// Description: Get ABAC policy description and convert it to XACML
	@RequestMapping(value = "/abac-policy-to-xacml/{policy_id}", method = GET, produces = TEXT_XML_VALUE)
	public String abacPolicyToXacml(@PathVariable("policy_id") String id) {
		AbacPolicy abacPolicy = abacPolicyRestController.getPolicy(id);
		String xacml = interpreterAbacPolicyToXacml.interpret(abacPolicy);
		return xacml;
	}

	// GET /opt/interpreter/abe-policy-to-text/{policy_id}
	// Description: Get ABE policy description and convert it to text format
	@RequestMapping(value = "/abe-policy-to-text/{policy_id}", method = GET, produces = TEXT_XML_VALUE)
	public String abePolicyToText(@PathVariable("policy_id") String id) {
		AbePolicy abePolicy = abePolicyRestController.getPolicy(id);
		String text = interpreterAbePolicyToText.interpret(abePolicy);
		return text;
	}
}
