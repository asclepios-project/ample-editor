/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.rest;

import eu.asclepios.ample.model.abe.AbePolicy;
import eu.asclepios.ample.persistence.RdfPersistenceManager;
import eu.asclepios.ample.persistence.RdfPersistenceManagerFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@Slf4j
@Validated
@Service
@RestController
@RequiredArgsConstructor
@RequestMapping("/opt/abe-policies")
public class AbePolicyRestController extends AbstractRestController {
	private final AttributeRestController attributeRestController;

	// ------------------------------------------------------------------------
	// ABE Policy methods
	// ------------------------------------------------------------------------

	// GET /opt/abe-policies/
	// Description: Get a list of top-level ABE policies
	@RequestMapping(value = "/", method = GET, produces = APPLICATION_JSON_VALUE)
	public AbePolicy[] getTopLevelPolicies() {
		try {
			RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();
			
			String rdfType = pm.getClassRdfType(AbePolicy.class);
			String parentUri = pm.getFieldUri(AbePolicy.class, "parent");
			String queryStr = "SELECT ?s WHERE { ?s  a  <"+rdfType+"> .  ?s <http://purl.org/dc/terms/title> ?title .  FILTER NOT EXISTS { ?s  <"+parentUri+">  ?s1 } .  FILTER ( ?s != <http://www.asclepios.eu/casm/types#null> ) } ORDER BY ?title";
			
			log.debug("getTopLevelPolicies: Retrieving top-level ABE policies");
			List<Object> list = pm.findByQuery(queryStr);
			log.debug("getTopLevelPolicies: {} top-level ABE policies found", list.size());

			// prepare policy expression for GUI
			AbePolicy[] policyArray = list.toArray(new AbePolicy[0]);
			for (AbePolicy abePolicy : policyArray)
				preparePolicyForCommunication(abePolicy);

			return policyArray;
		} catch (Exception e) {
			log.error("getTopLevelPolicies: EXCEPTION THROWN: ", e);
			log.debug("getTopLevelPolicies: Returning an empty array of {}", AbePolicy.class);
			return new AbePolicy[0];
		}
	}
	
	// GET /opt/abe-policies/all
	// Description: Get a list of ALL ABE policies
	@RequestMapping(value = "/all", method = GET, produces = APPLICATION_JSON_VALUE)
	public AbePolicy[] getAllPolicies() {
		try {
			RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();

			String rdfType = pm.getClassRdfType(AbePolicy.class);
			String parentUri = pm.getFieldUri(AbePolicy.class, "parent");
			String queryStr = "SELECT ?s WHERE { ?s  a  <"+rdfType+"> .  ?s <http://purl.org/dc/terms/title> ?title .  FILTER ( ?s != <http://www.asclepios.eu/casm/types#null> ) } ORDER BY ?title";
			
			log.debug("getAllPolicies: Retrieving ALL ABE policies");
			List<Object> list = pm.findByQuery(queryStr);
			log.debug("getAllPolicies: {} ABE policies found", list.size());

			// prepare policy expression for GUI
			AbePolicy[] policyArray = list.toArray(new AbePolicy[0]);
			for (AbePolicy abePolicy : policyArray)
				preparePolicyForCommunication(abePolicy);

			return policyArray;
		} catch (Exception e) {
			log.error("getAllPolicies: EXCEPTION THROWN: ", e);
			log.debug("getAllPolicies: Returning an empty array of {}", AbePolicy.class);
			return new AbePolicy[0];
		}
	}
	
	// GET /opt/abe-policies/{policy_id}
	// Description: Get ABE policy description
	@RequestMapping(value = "/{policy_id}", method = GET, produces = APPLICATION_JSON_VALUE)
	public AbePolicy getPolicy(@PathVariable("policy_id") String id) {
		try {
			RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();
			log.debug("getPolicy: Retrieving ABE policy with id = {}", id);
			AbePolicy pol = (AbePolicy)pm.find(id, AbePolicy.class);
			log.debug("getPolicy: ABE policy {} :\n{}", id, pol);

			// prepare policy expression for GUI
			if (pol.getPolicyExpression()!=null) {
				pol.getPolicyExpression().populatePropertyInstances(attributeRestController);
				preparePolicyForCommunication(pol);
			}

			return pol;
		} catch (Exception e) {
			log.error("getPolicy: EXCEPTION THROWN: ", e);
			log.debug("getPolicy: Returning an empty instance of {}", AbePolicy.class);
			return new AbePolicy();
		}
	}
	
	// PUT /opt/abe-policies/
	// Description: Create a new ABE policy
	@RequestMapping(value = "/", method = PUT, consumes = APPLICATION_JSON_VALUE)
	public ResponseEntity<String> createPolicy(@RequestBody AbePolicy policy) throws Exception {
		preparePolicyForPersistence(policy);
		checkPolicy(policy);
		try {
			RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();
			log.debug("createPolicy: Creating a new ABE policy with id = {}", policy.getId());
			log.debug("createPolicy: New ABE policy values:\n{}", policy);
			pm.persist(policy);
			log.debug("createPolicy: Object added to RDF persistent store");
			return createResponse(HTTP_STATUS_CREATED, "Result=Created");
		} catch (Exception e) {
			log.error("createPolicy: EXCEPTION THROWN: ", e);
			log.debug("createPolicy: Returning Status {}", HTTP_STATUS_ERROR);
			return createResponse(HTTP_STATUS_ERROR, "Result=Exception: "+e);
		}
	}
	
	// POST /opt/abe-policies/{policy_id}
	// Description: Update an ABE policy's data
	@RequestMapping(value = "/{policy_id}", method = POST, consumes = APPLICATION_JSON_VALUE)
	public ResponseEntity<String> updatePolicy(@PathVariable("policy_id") String id, @RequestBody AbePolicy policy) throws Exception {
		preparePolicyForPersistence(policy);
		checkPolicy(policy);
		try {
			RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();
			log.debug("updatePolicy: Updating ABE policy with id = {}", id);
			log.debug("updatePolicy: New ABE policy values:\n{}", policy);
			pm.attach(policy);
			log.debug("updatePolicy: Object attached to RDF persistent store manager");
			pm.merge(policy);
			log.debug("updatePolicy: Persistent store state updated");
			return createResponse(HTTP_STATUS_OK, "Result=Updated");
		} catch (Exception e) {
			log.error("updatePolicy: EXCEPTION THROWN: ", e);
			log.debug("updatePolicy: Returning Status {}", HTTP_STATUS_ERROR);
			return createResponse(HTTP_STATUS_ERROR, "Result=Exception: "+e);
		}
	}

	// DELETE /opt/abe-policies/{policy_id}
	// Description: Delete an ABE policy
	@RequestMapping(value = "/{policy_id}", method = DELETE)
	public ResponseEntity<String> deletePolicy(@PathVariable("policy_id") String id) {
		try {
			RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();
			log.debug("deletePolicy: Deleting ABE policy with id = {}", id);
			Object o = pm.find(id, AbePolicy.class);
			log.debug("deletePolicy: Object retrieved from RDF persistent store");
			pm.remove(o);
			log.debug("deletePolicy: Object deleted from RDF persistent store");
			return createResponse(HTTP_STATUS_OK, "Result=Deleted");
		} catch (Exception e) {
			log.error("deletePolicy: EXCEPTION THROWN: ", e);
			log.debug("deletePolicy: Returning Status {}", HTTP_STATUS_ERROR);
			return createResponse(HTTP_STATUS_ERROR, "Result=Exception: "+e);
		}
	}

	private void preparePolicyForCommunication(@NotNull AbePolicy policy) {
		// prepare rule condition for serialization to JSON
		if (policy.getPolicyExpression()!=null)
			policy.getPolicyExpression().prepareForSerialization();
	}

	private void preparePolicyForPersistence(@NotNull AbePolicy policy) throws Exception {
		// update timestamps
		Date now = new Date();
		policy.setCreateTimestamp(now);
		policy.setLastUpdateTimestamp(now);

		// set persistent expression
		if (policy.getPolicyExpression()!=null) {
			RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();
			policy.getPolicyExpression().prepareForPersistence(pm);
		}
	}

	private void checkPolicy(@NotNull AbePolicy policy) {
		// Check mandatory values
		boolean hasMandatoryValueMissing =
				StringUtils.isBlank(policy.getId()) ||
				StringUtils.isBlank(policy.getName()) ||
				StringUtils.isBlank(policy.getType()) ||
				StringUtils.isBlank(policy.getUri()) ||
				policy.getCreateTimestamp()==null ||
				policy.getLastUpdateTimestamp()==null;
		if (hasMandatoryValueMissing) {
			log.error("checkPolicy: ABE Policy is missing a mandatory value:\n{}", policy);
			throw new IllegalArgumentException("ABE Policy is missing a mandatory value. Please check logs. Policy Id: " + policy.getId());
		}

		// Check type
		if (! "ABE-POLICY".contentEquals(policy.getType().trim())) {
			log.error("checkPolicy: ABE Policy has wrong type:\n{}", policy);
			throw new IllegalArgumentException("ABE Policy has wrong type. Please check logs. Policy Id=" + policy.getId() + ", Type=" + policy.getType());
		}

		// Check for empty expression
		/*if (policy.getExpression()==null) {
			log.error("checkPolicy: ABE Policy does not have an expression:\n{}", policy);
			throw new IllegalArgumentException("ABE Policy does not have an expression. Please check logs. Policy Id=" + policy.getId() + ", Type=" + policy.getType());
		}*/
	}

	// ------------------------------------------------------------------------
	// ABE Policy export as RDF
	// ------------------------------------------------------------------------

	protected final static String EXPORT_POLICY_SPARQL_QUERY_TEMPLATE =
			"CONSTRUCT { ?s ?p ?o }\n" +
					"WHERE {\n" +
					"    ?s a <http://www.asclepios.eu/abe#ASCLEPIOS-ABE-POLICY> .\n" +
					"    ?s <http://purl.org/dc/terms/identifier> \"%s\" .\n" +
					"    ?s ?p ?o .\n" +
					"}\n";

	// GET /opt/abe-policies/export-as-rdf/{policy_id}
	// Description: Export ABE policy description and its policy expression
	@RequestMapping(value = "/export-as-rdf/{policy_id}", method = GET)
	public String exportPolicyAsRdf(@PathVariable("policy_id") String id) throws Exception {
		log.info("-------------- exportPolicyAsRdf: BEGIN: policy-id={}", id);

		// check if 'id' is valid (prevent sparql injection attacks)
		if (! id.matches("^[0-9A-Za-z-]+$")) {
			throw new IllegalArgumentException("Invalid 'id': "+id);
		}

		// Build triple store query and post data
		String queryStr = String.format(EXPORT_POLICY_SPARQL_QUERY_TEMPLATE, id);
		String postData = "output=ttl&query="+queryStr;

		// Query SPARQL server for ABE policy RDF
		String url = properties.getFrontend().getTriplestoreQueryUrl();
		log.info("-------------- exportPolicyAsRdf: Querying export service: {}", url);
		String responseContents = callService(url, HttpMethod.POST,
				postData, MediaType.APPLICATION_FORM_URLENCODED, String.class);
		log.trace("-------------- exportPolicyAsRdf: Response: body: {}", responseContents);
		if (responseContents == null)
			return "";

		// Export RDF of ABE policy expression
		RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();
		AbePolicy pol = (AbePolicy) pm.find(id, AbePolicy.class);
		StringBuilder sb = new StringBuilder(responseContents).append("\n");
		if (pol.getPolicyExpression()!=null) {
			String exprStr = attributeRestController
					.exportExpressionGraphAsRdf(pol.getPolicyExpression().getId());
			sb.append(exprStr);
			sb.append("\n");
		}
		String rdfExport = sb.toString();

		log.trace("-------------- exportPolicyAsRdf: END: \n{}", rdfExport);
		return rdfExport;
	}
}
