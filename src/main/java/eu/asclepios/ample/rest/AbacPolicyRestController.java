/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.rest;

import eu.asclepios.ample.datastore.DatastoreClientFactory;
import eu.asclepios.ample.model.AbstractObject;
import eu.asclepios.ample.model.abac.ABAC_POLICY_COMBINING_ALGORITHM;
import eu.asclepios.ample.model.abac.ABAC_TYPE;
import eu.asclepios.ample.model.abac.AbacPolicy;
import eu.asclepios.ample.model.abac.AbacRule;
import eu.asclepios.ample.model.expr.Expression;
import eu.asclepios.ample.persistence.RdfPersistenceManager;
import eu.asclepios.ample.persistence.RdfPersistenceManagerFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@Slf4j
@Validated
@Service
@RestController
@RequiredArgsConstructor
@RequestMapping("/opt/abac-policies")
public class AbacPolicyRestController extends AbstractRestController {
	private final DatastoreClientFactory clientFactory;
	private final AttributeRestController attributeRestController;

	// ------------------------------------------------------------------------
	// ABAC Policy methods
	// ------------------------------------------------------------------------

	// GET /opt/abac-policies/
	// Description: Get a list of top-level ABAC policies
	@RequestMapping(value = "/", method = GET, produces = APPLICATION_JSON_VALUE)
	public AbacPolicy[] getTopLevelPolicies() {
		try {
			RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();
			
			String rdfType = pm.getClassRdfType(AbacPolicy.class);
			String parentUri = pm.getFieldUri(AbacPolicy.class, "parent");
			String queryStr = "SELECT ?s WHERE { ?s  a  <"+rdfType+"> .  ?s <http://purl.org/dc/terms/title> ?title .  FILTER NOT EXISTS { ?s  <"+parentUri+">  ?s1 } .  FILTER ( ?s != <http://www.asclepios.eu/casm/types#null> ) } ORDER BY ?title";
			
			log.debug("getTopLevelPolicies: Retrieving top-level ABAC policies");
			List<Object> list = pm.findByQuery(queryStr);
			log.debug("getTopLevelPolicies: {} top-level ABAC policies found", list.size());
			return list.toArray(new AbacPolicy[0]);
		} catch (Exception e) {
			log.error("getTopLevelPolicies: EXCEPTION THROWN: ", e);
			log.debug("getTopLevelPolicies: Returning an empty array of {}", AbacPolicy.class);
			return new AbacPolicy[0];
		}
	}
	
	// GET /opt/abac-policies/all
	// Description: Get a list of ALL ABAC policies
	@RequestMapping(value = "/all", method = GET, produces = APPLICATION_JSON_VALUE)
	public AbacPolicy[] getAllPolicies() {
		try {
			RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();

			String rdfType = pm.getClassRdfType(AbacPolicy.class);
			String parentUri = pm.getFieldUri(AbacPolicy.class, "parent");
			String queryStr = "SELECT ?s WHERE { ?s  a  <"+rdfType+"> .  ?s <http://purl.org/dc/terms/title> ?title .  FILTER ( ?s != <http://www.asclepios.eu/casm/types#null> ) } ORDER BY ?title";
			
			log.debug("getAllPolicies: Retrieving ALL ABAC policies");
			List<Object> list = pm.findByQuery(queryStr);
			log.debug("getAllPolicies: {} ABAC policies found", list.size());
			return list.toArray(new AbacPolicy[0]);
		} catch (Exception e) {
			log.error("getAllPolicies: EXCEPTION THROWN: ", e);
			log.debug("getAllPolicies: Returning an empty array of {}", AbacPolicy.class);
			return new AbacPolicy[0];
		}
	}
	
	// GET /opt/abac-policies/{policy_id}
	// Description: Get ABAC policy description
	@RequestMapping(value = "/{policy_id}", method = GET, produces = APPLICATION_JSON_VALUE)
	public AbacPolicy getPolicy(@PathVariable("policy_id") String id) {
		try {
			RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();
			log.debug("getPolicy: Retrieving ABAC policy with id = {}", id);
			AbacPolicy pol = (AbacPolicy)pm.find(id, AbacPolicy.class);
			log.debug("getPolicy: ABAC policy {} :\n{}", id, pol);
			return pol;
		} catch (Exception e) {
			log.error("getPolicy: EXCEPTION THROWN: ", e);
			log.debug("getPolicy: Returning an empty instance of {}", AbacPolicy.class);
			return new AbacPolicy();
		}
	}
	
	// GET /opt/abac-policies/{policy_id}/rules
	// Description: Get a list of ABAC policy rules (if any)
	@RequestMapping(value = "/{policy_id}/rules", method = GET, produces = APPLICATION_JSON_VALUE)
	public AbacRule[] getPolicyRules(@PathVariable("policy_id") String id) {
		try {
			log.debug("getPolicyRules: Retrieving rules of ABAC policy with id = {}", id);
			List<AbacRule> list = _retrievePolicyRulesInternal(id);
			list.stream()
					.map(AbacRule::getRuleCondition)
					.filter(Objects::nonNull)
					.forEach(Expression::prepareForSerialization);

			log.debug("getPolicyRules: {} rules found for ABAC policy with id = {}", list.size(), id);
			return list.toArray(new AbacRule[0]);
		} catch (Exception e) {
			log.error("getPolicyRules: EXCEPTION THROWN: ", e);
			log.debug("getPolicyRules: Returning an empty array of {}", AbacRule.class);
			return new AbacRule[0];
		}
	}

	// Not a REST API method
	public List<AbacRule> _retrievePolicyRulesInternal(String id) throws Exception {
		RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();
		return _retrievePolicyRulesInternal(pm, id);
	}

	public List<AbacRule> _retrievePolicyRulesInternal(RdfPersistenceManager pm, String id) throws Exception {
		String attrRdfType = pm.getClassRdfType(AbacRule.class);
		String parentFieldUri = pm.getFieldUri(AbacRule.class, "rulePolicy");
		String parentId = pm.getObjectUri(id, AbacPolicy.class);
		String queryStr = "SELECT ?s WHERE { ?s  a  <"+attrRdfType+"> ; <"+parentFieldUri+">  <"+parentId+"> ; <http://purl.org/dc/terms/title> ?title } ORDER BY ?title";

		List<AbacRule> list = (List<AbacRule>)(List<?>) pm.findByQuery( String.format(queryStr, id) );
		return list;
	}
	
	// PUT /opt/abac-policies/
	// Description: Create a new ABAC policy
	@RequestMapping(value = "/", method = PUT, consumes = APPLICATION_JSON_VALUE)
	public ResponseEntity<String> createPolicy(@RequestBody AbacPolicy policy) {
		Date now = new Date();
		policy.setCreateTimestamp(now);
		policy.setLastUpdateTimestamp(now);
		checkPolicy(policy);
		try {
			RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();
			log.debug("createPolicy: Creating a new ABAC policy with id = {}", policy.getId());
			log.debug("createPolicy: New ABAC policy values:\n{}", policy);
			pm.persist(policy);
			log.debug("createPolicy: Object added to RDF persistent store");
			return createResponse(HTTP_STATUS_CREATED, "Result=Created");
		} catch (Exception e) {
			log.error("createPolicy: EXCEPTION THROWN: ", e);
			log.debug("createPolicy: Returning Status {}", HTTP_STATUS_ERROR);
			return createResponse(HTTP_STATUS_ERROR, "Result=Exception: "+e);
		}
	}
	
	// POST /opt/abac-policies/{policy_id}
	// Description: Update an ABAC policy's data
	@RequestMapping(value = "/{policy_id}", method = POST, consumes = APPLICATION_JSON_VALUE)
	public ResponseEntity<String> updatePolicy(@PathVariable("policy_id") String id, @RequestBody AbacPolicy policy) {
		Date now = new Date();
		policy.setCreateTimestamp(now);
		policy.setLastUpdateTimestamp(now);
		checkPolicy(policy);
		try {
			RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();
			log.debug("updatePolicy: Updating ABAC policy with id = {}", id);
			log.debug("updatePolicy: New ABAC policy values:\n{}", policy);
			pm.attach(policy);
			log.debug("updatePolicy: Object attached to RDF persistent store manager");
			pm.merge(policy);
			log.debug("updatePolicy: Persistent store state updated");
			return createResponse(HTTP_STATUS_OK, "Result=Updated");
		} catch (Exception e) {
			log.error("updatePolicy: EXCEPTION THROWN: ", e);
			log.debug("updatePolicy: Returning Status {}", HTTP_STATUS_ERROR);
			return createResponse(HTTP_STATUS_ERROR, "Result=Exception: "+e);
		}
	}

	// DELETE /opt/abac-policies/{policy_id}
	// Description: Delete an ABAC policy
	@RequestMapping(value = "/{policy_id}", method = DELETE)
	public ResponseEntity<String> deletePolicy(@PathVariable("policy_id") String id) {
		try {
			RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();
			log.debug("deletePolicy: Deleting ABAC policy with id = {}", id);
			Object o = pm.find(id, AbacPolicy.class);
			log.debug("deletePolicy: Object retrieved from RDF persistent store");
			pm.remove(o);
			log.debug("deletePolicy: Object deleted from RDF persistent store");
			return createResponse(HTTP_STATUS_OK, "Result=Deleted");
		} catch (Exception e) {
			log.error("deletePolicy: EXCEPTION THROWN: ", e);
			log.debug("deletePolicy: Returning Status {}", HTTP_STATUS_ERROR);
			return createResponse(HTTP_STATUS_ERROR, "Result=Exception: "+e);
		}
	}
	
	// DELETE /opt/abac-policies/{policy_id}/all
	// Description: Delete an ABAC policy and its sub-elements
	@RequestMapping(value = "/{policy_id}/all", method = DELETE)
	public ResponseEntity<String> deletePolicyAndRules(@PathVariable("policy_id") String id) {
		try {
			log.debug("deletePolicyAndRules: Deleting ABAC policy with id = {} and its rules", id);
			
			// Remove attribute and sub-attributes from RDF persistent store
			RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();
			String deleteStmt = String.format("DELETE {?x ?y ?z} WHERE { ?s <http://purl.org/dc/terms/identifier>  \"%s\" . ?x <http://www.w3.org/2004/02/skos/core#broader>* ?s . ?x ?y ?z . } ", id);
			clientFactory.getInstance().execute( deleteStmt );
			
			log.debug("deletePolicyAndRules: Object deleted from RDF persistent store");
			return createResponse(HTTP_STATUS_OK, "Result=Deleted");
		} catch (Exception e) {
			log.error("deletePolicyAndRules: EXCEPTION THROWN: ", e);
			log.debug("deletePolicyAndRules: Returning Status {}", HTTP_STATUS_ERROR);
			return createResponse(HTTP_STATUS_ERROR, "Result=Exception: "+e);
		}
	}

	@RequestMapping(value = "/{policy_id}/reorder-rules", method = POST, consumes = APPLICATION_JSON_VALUE)
	public String reorderPolicyRule(@PathVariable("policy_id") String id, @RequestBody List<String> ruleId) throws Exception {
		log.info("reorderPolicyRule: policy-id={}, rule-ids={}", id, ruleId);

		// check if duplicates exist in given rule id's
		Set<String> ruleIdSet = new HashSet<>(ruleId);
		if (ruleIdSet.size() != ruleId.size())
			throw new IllegalArgumentException("Provided rule id set contains duplicate values: "+ruleId);

		// get stored policy rules and check if they match to ruleId's
		RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();
		List<AbacRule> children = _retrievePolicyRulesInternal(pm, id);
		Set<String> childrenIdSet = children.stream().map(AbstractObject::getId).collect(Collectors.toSet());
		log.info("reorderPolicyRule: Stored ABAC policy rules: policy-id={}, rule-ids={}", id, childrenIdSet);
		if (! ruleIdSet.equals(childrenIdSet))
			throw new IllegalArgumentException("Provided rule id set does not match the store rule set for ABAC policy: "+id);

		// set new rule positions
		Map<String, AbacRule> rulesMap = children.stream().collect(Collectors.toMap(AbstractObject::getId, x -> x));
		log.info("reorderPolicyRule: ABAC policy rules map: policy-id={}, rules-map={}", id, rulesMap);
		for (int i=0, n=ruleId.size(); i<n; i++) {
			String rId = ruleId.get(i);
			AbacRule rule = rulesMap.get(rId);
			rule.setPosition(i);
		}

		// save changes
		log.info("reorderPolicyRule: Storing updated rules: policy-id={}", id);
		for (AbacRule rule : children)
			pm.merge(rule);

		log.info("reorderPolicyRule: Rules stored");
		return "SAVED NEW RULE POSITIONS";
	}

	private void checkPolicy(@NotNull AbacPolicy policy) {
		// Check mandatory values
		boolean hasMandatoryValueMissing =
				StringUtils.isBlank(policy.getId()) ||
				StringUtils.isBlank(policy.getName()) ||
				StringUtils.isBlank(policy.getType()) ||
				StringUtils.isBlank(policy.getUri()) ||
				policy.getCreateTimestamp()==null ||
				policy.getLastUpdateTimestamp()==null ||
				StringUtils.isBlank(policy.getPolicyCombiningAlgorithm());
		if (hasMandatoryValueMissing) {
			log.error("checkPolicy: ABAC Policy is missing a mandatory value:\n{}", policy);
			throw new IllegalArgumentException("ABAC Policy is missing a mandatory value. Please check logs. Policy Id: " + policy.getId());
		}

		// Check type
		if (! ABAC_TYPE.ABAC_POLICY_NAME.contentEquals(policy.getType().trim())) {
			log.error("checkPolicy: ABAC Policy has wrong type:\n{}", policy);
			throw new IllegalArgumentException("ABAC Policy has wrong type. Please check logs. Policy Id=" + policy.getId() + ", Type=" + policy.getType());
		}

		// Check policy combining algorithm
		if (StringUtils.isBlank(policy.getPolicyCombiningAlgorithm())) {
			log.error("checkPolicy: ABAC Policy has no Policy Combining Algorithm:\n{}", policy);
			throw new IllegalArgumentException("ABAC Policy has no Policy Combining Algorithm. Please check logs. Policy Id=" + policy.getId() + ", Type=" + policy.getType());
		} else {
			ABAC_POLICY_COMBINING_ALGORITHM.valueOfUrn(policy.getPolicyCombiningAlgorithm());
			// if policy combining algorithm URN does not exist an exception will be thrown
		}
	}

	// ------------------------------------------------------------------------
	// ABAC Policy Rule methods
	// ------------------------------------------------------------------------

	// GET /opt/abac-policies/rule/{rule_id}
	// Description: Get ABAC policy rule description
	@RequestMapping(value = "/rule/{rule_id}", method = GET, produces = APPLICATION_JSON_VALUE)
	public AbacRule getRule(@PathVariable("rule_id") String id) {
		try {
			// retrieve rule from data store
			RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();

			log.debug("getRule: Retrieving ABAC rule with id = {}", id);
			AbacRule rule = (AbacRule)pm.find(id, AbacRule.class);
			log.debug("getRule: Retrieved ABAC rule with id = {}\n{}", id, rule);

			// prepare rule condition for serialization to JSON
			if (rule.getRuleCondition()!=null) {
				rule.getRuleCondition().populatePropertyInstances(attributeRestController);
				rule.getRuleCondition().prepareForSerialization();
			}

			log.debug("getRule: ABAC rule: id={}\n{}", id, rule);
			return rule;
		} catch (Exception e) {
			log.error("getRule: EXCEPTION THROWN: ", e);
			log.debug("getRule: Returning an empty instance of {}", AbacRule.class);
			return new AbacRule();
		}
	}

	// PUT /opt/abac-policies/rule/
	// Description: Create a new ABAC policy rule
	@RequestMapping(value = "/rule/", method = PUT, consumes = APPLICATION_JSON_VALUE)
	public ResponseEntity<String> createRule(@RequestBody AbacRule rule) throws Exception {
		prepareRuleForPersistence(rule);
		checkRule(rule);
		try {
			RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();
			log.debug("createRule: Creating a new ABAC rule with id = {}", rule.getId());
			log.debug("createRule: New ABAC rule values:\n{}", rule);
			pm.persist(rule);
			log.debug("createRule: Object added to RDF persistent store");
			return createResponse(HTTP_STATUS_CREATED, "Result=Created");
		} catch (Exception e) {
			log.error("createRule: EXCEPTION THROWN: ", e);
			log.debug("createRule: Returning Status {}", HTTP_STATUS_ERROR);
			return createResponse(HTTP_STATUS_ERROR, "Result=Exception: "+e);
		}
	}

	// POST /opt/abac-policies/rule/{rule_id}
	// Description: Update an ABAC policy rule's data
	@RequestMapping(value = "/rule/{rule_id}", method = POST, consumes = APPLICATION_JSON_VALUE)
	public ResponseEntity<String> updateRule(@PathVariable("rule_id") String id, @RequestBody AbacRule rule) throws Exception {
		prepareRuleForPersistence(rule);
		checkRule(rule);
		try {
			RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();
			log.debug("updateRule: Updating ABAC rule with id = {}", id);
			log.debug("updateRule: New ABAC rule values:\n{}", rule);
			pm.attach(rule);
			log.debug("updateRule: Object attached to RDF persistent store manager");
			pm.merge(rule);
			log.debug("updateRule: Persistent store state updated");
			return createResponse(HTTP_STATUS_OK, "Result=Updated");
		} catch (Exception e) {
			log.error("updateRule: EXCEPTION THROWN: ", e);
			log.debug("updateRule: Returning Status {}", HTTP_STATUS_ERROR);
			return createResponse(HTTP_STATUS_ERROR, "Result=Exception: "+e);
		}
	}

	// DELETE /opt/abac-policies/rule/{rule_id}
	// Description: Delete an ABAC rule
	@RequestMapping(value = "/rule/{rule_id}", method = DELETE)
	public ResponseEntity<String> deleteRule(@PathVariable("rule_id") String id) {
		try {
			RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();
			log.debug("deleteRule: Deleting ABAC rule with id = {}", id);
			Object o = pm.find(id, AbacRule.class);
			log.debug("deleteRule: Object retrieved from RDF persistent store");
			pm.remove(o);
			log.debug("deleteRule: Object deleted from RDF persistent store");
			return createResponse(HTTP_STATUS_OK, "Result=Deleted");
		} catch (Exception e) {
			log.error("deleteRule: EXCEPTION THROWN: ", e);
			log.debug("deleteRule: Returning Status {}", HTTP_STATUS_ERROR);
			return createResponse(HTTP_STATUS_ERROR, "Result=Exception: "+e);
		}
	}

	private void prepareRuleForPersistence(@NotNull AbacRule rule) throws Exception {
		// update timestamps
		Date now = new Date();
		rule.setCreateTimestamp(now);
		rule.setLastUpdateTimestamp(now);

		// set persistent expression
		RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();
		if (rule.getRuleCondition()!=null)
			rule.getRuleCondition().prepareForPersistence(pm);
	}

	private void checkRule(@NotNull AbacRule rule) {
		// Check mandatory values
		boolean hasMandatoryValueMissing =
				StringUtils.isBlank(rule.getId()) ||
				StringUtils.isBlank(rule.getName()) ||
				StringUtils.isBlank(rule.getType()) ||
				StringUtils.isBlank(rule.getUri()) ||
				StringUtils.isBlank(rule.getRuleOutcome()) ||
				rule.getCreateTimestamp()==null ||
				rule.getLastUpdateTimestamp()==null ||
				rule.getRulePolicy()==null;
		if (hasMandatoryValueMissing) {
			log.error("checkRule: ABAC Rule is missing a mandatory value:\n{}", rule);
			throw new IllegalArgumentException("ABAC Rule is missing a mandatory value. Please check logs. Rule Id: " + rule.getId());
		}

		// Check type
		if (!ABAC_TYPE.ABAC_RULE_NAME.contentEquals(rule.getType().trim())) {
			log.error("checkRule: ABAC Rule has wrong type:\n{}", rule);
			throw new IllegalArgumentException("ABAC Rule has wrong type. Please check logs. Rule Id=" + rule.getId() + ", Type=" + rule.getType());
		}
	}

	// ------------------------------------------------------------------------
	// ABAC Policy export as RDF
	// ------------------------------------------------------------------------

	protected final static String EXPORT_POLICY_AND_RULES_SPARQL_QUERY_TEMPLATE =
			"CONSTRUCT { ?s ?p ?o } " +
			"WHERE { \n" +
			"  { \n" +
			"    SELECT (?s1 AS ?s) (?p1 AS ?p) (?o1 AS ?o) \n" +
			"    WHERE { \n" +
			"      ?s1 a <http://www.asclepios.eu/abac#ASCLEPIOS-ABAC-POLICY> . \n" +
			"      ?s1 <http://purl.org/dc/terms/identifier> \"%s\" . \n" +
			"      ?s1 ?p1 ?o1 . \n" +
			"    } \n" +
			"  } UNION { \n" +
			"    SELECT (?s2 AS ?s) (?p2 AS ?p) (?o2 AS ?o) \n" +
			"    WHERE { \n" +
			"      ?s2 ?p2 ?o2 . \n" +
			"      ?s2 <http://www.asclepios.eu/abac/ABAC-RULE#policy> ?s1 . \n" +
			"    } \n" +
			"  } \n" +
			"} ";
	protected final static String FIND_POLICY_RULES_IDS_SPARQL_QUERY_TEMPLATE =
			"SELECT DISTINCT ?s \n" +
			"WHERE { \n" +
			"  ?s ?p ?o . \n" +
			"  ?s a <http://www.asclepios.eu/abac#ASCLEPIOS-ABAC-RULE> . \n" +
			"  ?s <http://www.asclepios.eu/abac/ABAC-RULE#policy> ?pol . \n" +
			"  ?pol a <http://www.asclepios.eu/abac#ASCLEPIOS-ABAC-POLICY> . \n" +
			"  ?pol <http://purl.org/dc/terms/identifier> \"%s\" . \n" +
			"} \n" +
			"ORDER BY ?s ?p ";

	// GET /opt/abac-policies/export-as-rdf/{policy_id}
	// Description: Export ABAC policy description, its rules and the rule expressions
	@RequestMapping(value = "/export-as-rdf/{policy_id}", method = GET)
	public String exportPolicyAsRdf(@PathVariable("policy_id") String id) throws Exception {
		log.info("-------------- exportPolicyAsRdf: BEGIN: policy-id={}", id);

		// check if 'id' is valid (prevent sparql injection attacks)
		if (! id.matches("^[0-9A-Za-z-]+$")) {
			throw new IllegalArgumentException("Invalid 'id': "+id);
		}

		// Build triple store query and post data
		String queryStr = String.format(EXPORT_POLICY_AND_RULES_SPARQL_QUERY_TEMPLATE, id);
		String postData = "output=ttl&query="+queryStr;

		// Query SPARQL server for ABAC policy RDF
		String url = properties.getFrontend().getTriplestoreQueryUrl();
		log.info("-------------- exportPolicyAsRdf: Querying export service: {}", url);
		String responseContents = callService(url, HttpMethod.POST,
				postData, MediaType.APPLICATION_FORM_URLENCODED, String.class);
		log.trace("-------------- exportPolicyAsRdf: Response: body: {}", responseContents);
		if (responseContents == null)
			return "";

		// Export RDF of ABAC rule expression
		StringBuilder sb = new StringBuilder(responseContents).append("\n");
		RdfPersistenceManager pm = RdfPersistenceManagerFactory.createRdfPersistenceManager();

		String queryStr2 = String.format(FIND_POLICY_RULES_IDS_SPARQL_QUERY_TEMPLATE, id);
		List<AbacRule> rulesList = pm.findByQuery(AbacRule.class, queryStr2).stream()
				.map(o -> (AbacRule) o)
				.collect(Collectors.toList());
		for (AbacRule rule : rulesList) {
			if (rule.getRuleCondition()!=null) {
				String exprStr = attributeRestController
						.exportExpressionGraphAsRdf(rule.getRuleCondition().getId());
				sb.append(exprStr);
				sb.append("\n");
			}
		}
		String rdfExport = sb.toString();

		log.trace("-------------- exportPolicyAsRdf: END: \n{}", rdfExport);
		return rdfExport;
	}
}
