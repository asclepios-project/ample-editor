/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample.rest;

import eu.asclepios.ample.model.AbstractObject;
import eu.asclepios.ample.util.EscapeDataUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.util.Collection;

@Slf4j
@ControllerAdvice
public class EscapeResponseDataAdvice implements ResponseBodyAdvice<Object> {
	@Override
	public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
		boolean supportsController = AbstractRestController.class.isAssignableFrom(returnType.getContainingClass());
		boolean supportsReturnType = AbstractObject.class.isAssignableFrom(
				returnType.getParameterType().isArray()
						? returnType.getParameterType().getComponentType()
						: returnType.getParameterType()
		);
		boolean supports = supportsController && supportsReturnType;

		log.debug("EscapeResponseDataAdvice.supports(): controller={}, return-type={} => supported={}",
				returnType.getContainingClass().getSimpleName(),
				returnType.getParameterType().getSimpleName(),
				supports);
		return supports;
	}

	@Override
	public Object beforeBodyWrite(Object responseBody, MethodParameter returnType, MediaType contentType,
								  Class<? extends HttpMessageConverter<?>> converterType,
								  ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse)
	{
		log.debug("EscapeResponseDataAdvice.beforeBodyWrite(): return-type={}, content-type={}, converter={}\nRESPONSE BODY BEFORE ESCAPE:\n{}",
				returnType.getParameterType().getSimpleName(), contentType, contentType.getType(), responseBody);

		if (returnType.getParameterType().isArray()) {
			Object[] arr = (Object[]) responseBody;
			for (Object obj : arr) {
				if (obj instanceof  AbstractObject)
					escapeData((AbstractObject) obj);
			}
		} else if (responseBody instanceof Collection) {
			for (Object obj : (Collection<?>)responseBody) {
				if (obj instanceof  AbstractObject)
					escapeData((AbstractObject) obj);
			}
		} else {
			if (responseBody instanceof  AbstractObject)
				escapeData((AbstractObject) responseBody);
		}

		log.debug("EscapeResponseDataAdvice.beforeBodyWrite(): return-type={}, content-type={}, converter={}\nRESPONSE BODY AFTER ESCAPE:\n{}",
				returnType.getParameterType().getSimpleName(), contentType, contentType.getType(), responseBody);
		return responseBody;
	}

	private void escapeData(AbstractObject object) { EscapeDataUtil.escapeData(object); }
}