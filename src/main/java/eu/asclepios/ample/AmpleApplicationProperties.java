/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample;

import eu.asclepios.ample.util.ROLE;
import lombok.Data;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.lang.Nullable;
import org.springframework.validation.annotation.Validated;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import static eu.asclepios.ample.datastore.Datastore.STORE_TYPE;

@Slf4j
@Data
@Validated
@Configuration
@ConfigurationProperties(prefix = "ample")
public class AmpleApplicationProperties {
    private Datastore datastore = new Datastore();
    private FusekiServer server = new FusekiServer();
    private Client client = new Client();
    private Frontend frontend = new Frontend();
    private Backend backend = new Backend();
    private Collection<User> users = Collections.emptyList();

    @Data
    public static class Datastore {
        private STORE_TYPE type = STORE_TYPE.TDB2;
        private String location = "DB";
    }

    @Data
    public static class FusekiServer {
        private boolean enable = true;
        private int port = 3030;
        private boolean verbose = false;

        private String serviceName = "ample";
        private boolean allowUpdates = true;

        //XXX:AMPLE:OPTIONAL: Add authentication and Authorization
        private boolean loopbackAccess = true;
    }

    @Data
    public static class Client {
        private ClientType type = ClientType.FUSEKI;
        private FusekiEndpoints endpoints = new FusekiEndpoints();
    }

    public enum ClientType { LOCAL, FUSEKI }

    @Data
    public static class FusekiEndpoints {
        @Nullable private String query;
        @Nullable private String update;
        @Nullable private String upload;
    }

    @Data
    public static class Frontend {
        private boolean useRestService = true;
        private String backendServiceUrl = "http://localhost:9090/";    //XXX:AMPLE: Improve to use http/https and port defined before
        private String triplestoreUrl = "http://localhost:3030/ample";  //XXX:AMPLE: Improve to use http/https and port defined before
        private String trustStoreType;
        private String trustStore;
        @ToString.Exclude
        private String trustStorePassword;

        private String triplestoreClearUrl = triplestoreUrl+"/update";
        private String triplestoreExportUrl = triplestoreUrl+"/data";
        private String triplestoreImportUrl = triplestoreUrl+"/data?default";
        private String triplestoreQueryUrl = triplestoreUrl+"/query";

        private boolean modelInitializationOnStartup;
        private String[] modelInitializationFiles;

        private Connection connection;

        @Data
        public static class Connection {
            private KeepAlive keepAlive;

            @Data
            public static class KeepAlive {
                private boolean enable = true;
                private long timeout = 600;
                private int max = 100;
            }
        }
    }

    @Data
    public static class Backend {
        private String ipFilter = "127.0.0.1";
        private String papUrl;  // '%s': policy id
        private String abeServiceUrl;  // '%s': policy id
    }

    @Data
    public static class User {
        private String username;
        @ToString.Exclude
        private String password;
        private Set<ROLE> roles;
    }
}
