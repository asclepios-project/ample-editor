/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.asclepios.ample;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import java.io.File;

@Slf4j
@Configuration
@AllArgsConstructor(onConstructor = @__({@Autowired}))
public class AmpleApplicationContext {
    private AmpleApplicationProperties properties;

    @Bean
    public RestTemplate getRestTemplate() throws Exception {
        String trustStore = properties.getFrontend().getTrustStore();
        String trustStoreType = properties.getFrontend().getTrustStoreType();
        String trustStorePassword = properties.getFrontend().getTrustStorePassword();
        log.info("getRestTemplate(): keystore-file: {}", trustStore);
        if (StringUtils.isBlank(trustStore)) {
            log.warn("getRestTemplate(): SSL not configured");
            return new RestTemplate();
        } else {
            log.info("getRestTemplate(): Configuring SSL");
            SSLContext sslContext = new SSLContextBuilder()
                    .loadTrustMaterial(new File(trustStore), trustStorePassword.toCharArray())
                    .build();
            SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(sslContext);
            HttpClient httpClient = HttpClients.custom()
                    .setSSLSocketFactory(socketFactory)
                    .build();
            HttpComponentsClientHttpRequestFactory factory =
                    new HttpComponentsClientHttpRequestFactory(httpClient);
            return new RestTemplate(factory);
        }
    }
}
