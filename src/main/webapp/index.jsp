<!DOCTYPE html>
<!--
  ~
  ~ Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~      http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  ~
  -->
<%@ include file="includes/prelude.html" %>
<html>
  <head>
<% pageTitle = "ASCLEPIOS Models and Policies Editor (AMPLE)"; %>
<%@ include file="includes/head.html" %>
	<link rel="stylesheet" href="/forms/slick/common-grid-styles.css" type="text/css"/>
	
	<style>
		.dashboard-block {
			background: lightgrey;
			border : black bevel 2px;
			padding: 5px;
			margin : 5px;
			font-size: 1.1em;
			line-height: 1.3em;
			width: 75%;
			text-align: justify;
		}
		.success-mesg {
			color: darkgreen;
			background: lightgreen;
			font-size: medium;
			padding: 3px;
			text-align: center;
		}
		.error-mesg {
			color: red;
			background: pink;
			font-size: medium;
			font-weight: bold;
			font-style: italic;
			padding: 3px;
			text-align: center;
		}
		.actions-line {
			font-size: 12pt;
			margin-top: 5px;
			margin-bottom: 0px;
		}
		.menu-btn {
			width: 128px;
			height: 80px;
			cursor: pointer;
			vertical-align: middle;
			border-radius: 12px;
			font-size: smaller;
			font-style: italic;
			line-height: 10pt;
		}
		.menu-btn > img {
			width: 32px;
			height: 32px;
			padding-bottom: 5px;
		}
		.menu-btn-table {
			margin-top: 20px;
			margin-bottom: 20px;
		}
	</style>
	
  </head>
  <body>
	
<%@ include file="includes/header.html" %>
	
	<h1 align="center">Welcome to <i>ASCLEPIOS Models and Policies Editor (AMPLE)</i></h1>
	<p><br/></p>
	
	<center>
		<!-- Menu buttons -->
		<table class="menu-btn-table" align="center" cellpadding="5">
			<tr>
				<td><div class="col"><a class="btn menu-btn span1" href="/forms/casm/casm-mgnt.jsp"><br/><img class="drawer-btn" src="/images/hierarchy2-color-32x32.png" title="ASCLEPIOS Context-Aware Security Model" /><br/>Context-Aware Security<br/>Model management</a></div></td>
				<td><div class="col"><a class="btn menu-btn span1" href="/forms/abac/abac-policy-mgnt.jsp"><br/><img class="drawer-btn" src="/images/abac/abac-policy-editor-32x32-9.png" title="ABAC Policy Management" /><br/>ABAC Policy<br/>management</a></div></td>
				<td><div class="col"><a class="btn menu-btn span1" href="/forms/abe/abe-policy-mgnt.jsp"><br/><img class="drawer-btn" src="/images/abe/abe-policy-editor-32x32.png" title="ABE Policy Management" /><br/>ABE Policy<br/>management</a></div></td>
				<td><div class="col"><a class="btn menu-btn span1" href="/forms/validation/validation-mgnt.jsp"><br/><img class="drawer-btn" src="/images/validation/validation-32x32.png" title="Policy Validation Management" /><br/>Policy Validation<br/>management</a></div></td>
			</tr>
			<tr>
				<td><div class="col"><a class="btn menu-btn span1" href="/forms/admin/import.jsp"><br/><img class="drawer-btn" src="/images/upload-blue-32x32.png" title="Import Models" /><br>&nbsp;<br/>Import Models</a></div></td>
				<td><div class="col"><a class="btn menu-btn span1" href="/gui/admin/exportRdf"><br/><img class="drawer-btn" src="/images/download-red-32x32.png" title="Export Models" /><br>&nbsp;<br/>Export Models</a></div></td>
				<td><div class="col"><a class="btn menu-btn span1" onClick="contactServer('POST', '/gui/admin/initializeModels', 'Initialize with default ASCLEPIOS Context-Aware Security Model?\n\nThis action will overwrite any existing contents.', 'Initializing models...', function() { location.reload(); });"><br/><img class="drawer-btn" src="/images/reset-32x32.png" title="Initialize with default ASCLEPIOS Context-Aware Security Model" /><br><span style="color:red; font-weight:bold;">Initialize</span><br/>Models</a></div></td>
				<td><div class="col"><a class="btn menu-btn span1" onClick="contactServer('GET', '/gui/admin/clearRdf', 'Clear repository?\n\nThis action will delete all existing repository contents.', 'Clearing repository...', function() { location.reload(); });"><br/><img class="drawer-btn" src="/images/delete-pink-32x32.png" title="Clear repository" /><br/>&nbsp;<br/>Clear repository</a></div></td>
			</tr>
		</table>
	</center>
	
<%@ include file="includes/footer.html" %>
  </body>
</html>
<%@ include file="includes/debug.html" %>
<%@ include file="includes/trail.html" %>