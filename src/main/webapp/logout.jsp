<!DOCTYPE html>
<!--
  ~
  ~ Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~      http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  ~
  -->
<%@ page session="true"%>
<%@ include file="includes/prelude.html" %>
<%
    String userName = request.getRemoteUser();
    session.invalidate();
%>
<html>
<head>
	<title><%= editorFullName %></title>
	<meta charset="utf-8" />
	<link rel="stylesheet" style="text/css" href="/forms/deps/opt/bootstrap.css" />
	<link rel="stylesheet" href="/forms/slick/common-grid-styles.css" type="text/css"/>
	<meta http-equiv="refresh" content="3;url=/login" />
</head>
<body style="padding:10px;">
	<h1 align="center"><%= editorFullName %></h1>
	<p><br/></p>
	<p align="center">
		User <em>'<%= userName %>'</em> has been logged out.
		<br/><br/>
		<a href="/login">Click here to go to login page</a>
	</p>

	<%@ include file="includes/footer.html" %>
</body>
</html>
