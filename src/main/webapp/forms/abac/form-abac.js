/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *	Utility Function common to all forms
 */
// User-provided variables
var baseUrl = '';
var formFields = [];
var emptyData = {};
var formSelector = "";
var attrParent;
var attrNode;

// Timer variables
var startTm = null;
var callStartTm = null;

/*
 * Common form functions
 */
// Initialization and auxiliary functions
function makeEmpty(fields) {
	if (!fields) fields = formFields;
	emptyData = {};
	for (f in fields) emptyData[f] = '';
}
function uuidv4() {
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
		var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
		return v.toString(16);
	});
}
function createNewId() {
	return uuidv4();
}
function generateId() {
	var idf = $('input[name=id]');
	if (idf.prop('readonly')===false) {
		var guid = uuidv4();
		idf.val( guid );
		return guid;
	}
	return false;
}
function createUriById(id) {
	return 'asclepios:'+id;
}
function generateUri() {
	var idf = $('input[name=id]');
	var urif = $('input[name=uri]');
	if (urif.prop('readonly')===false && idf.val().trim()!=='') {
		var uri = 'asclepios:'+idf.val().trim();
		urif.val( uri );
		return uri;
	}
	return false;
}
function setEnabledIdAndUriButtons(enable) {
	var idf = $('input[name=id]');
	var urif = $('input[name=uri]');
	if (idf.prop('readonly')===false) {
		$('#btn-gen-id').prop( "disabled", false ).removeClass( 'grayscale' );
	} else {
		$('#btn-gen-id').prop( "disabled", true ).addClass( 'grayscale' );
	}
	if (urif.prop('readonly')===false) {
		$('#btn-gen-uri').prop( "disabled", false ).removeClass( 'grayscale' );
	} else {
		$('#btn-gen-uri').prop( "disabled", true ).addClass( 'grayscale' );
	}
}
function getBaseEditFormOptions(entityName) {
	var entity;
	if (!entityName) entity = 'node'; else entity = entityName;
	return {
		schema: {
		  id: {
			type: 'string',
			title: 'Id',
			readonly: true,
			required: true
		  },
/*		  parent: {
			type: 'string',
			title: 'Parent',
			readonly: true
		  },*/
/*		  owner: {
			type: 'string',
			title: 'Owner',
			readonly: true,
			required: false
		  },*/
		  uri: {
			type: 'string',
			title: 'URI',
			readonly: false,
			required: true
		  },
		  type: {
			type: 'string',
			title: 'Type',
			//enum: ['ABAC-POLICY', 'ABAC-RULE', 'ABAC-POLICY-SET'],
			readonly: true,
			required: true
		  },
		  name: {
			type: 'string',
			title: 'Name',
			required: true
		  },
		  description: {
			type: 'string',
			title: 'Description',
		  },
/*		  createTimestamp: {
			//type: 'date',
			type: 'string',
			title: 'Creation',
			readonly: true,
		  },
		  lastUpdateTimestamp: {
			//type: 'date',
			type: 'string',
			title: 'Last Update',
			readonly: true,
		  },*/
		  policyCombiningAlgorithm: {
			type: 'string',
			title: 'Policy Combining Algorithm',
			readonly: true,
		  },
		  rulePolicy: {
			type: 'string',
			title: 'Policy',
			readonly: true
		  },
		  ruleOutcome: {
			type: 'string',
			title: 'Rule Outcome',
			readonly: true,
		  },
		  /*ruleCondition: {
			type: 'string',
			title: 'Rule Condition',
			readonly: false,
		  },*/
		},
		"form": [
			{	"key": "id",
				"notitle": true,
				"prepend": "<span class=\"jsonform-title\">Id:</span>",
				"append": "<font color=red><i>required</i></font>",
				"description": "A string uniquely identifying this "+entity+". If left empty a GUID will be generated for Id. <button id=\"btn-gen-id\" onclick=\"generateId(); return false;\" class=\"btn btn-warning\">Generate new Id as GUID</button>",
				"fieldHtmlClass": "input-xxlarge",
			},
			{	"key": "rulePolicy",
				"notitle": true,
				"prepend": "<span class=\"jsonform-title\">Policy:</span>",
				"append": '<span id="parent_Selector"><a style="cursor: pointer;" onclick="startSelectTreeNode(\'parent_Selector\', \'rulePolicy\', \'\', \'PARENT\', \'changeRulePolicy\'); return false;">Change</a></span>',
				"description": "The Id of the parent policy",
				"fieldHtmlClass": "input-xxlarge",
				onClick: function (e) {
					e.preventDefault();
					startSelectTreeNode('parent_Selector', 'rulePolicy', '', 'PARENT', 'changeRulePolicy');
				}
			},
/*			{	"key": "parent",
				"notitle": true,
				"prepend": "<span class=\"jsonform-title\">Parent:</span>",
				"append": '<span id="parent_Selector"><a style="cursor: pointer;" onclick="startSelectTreeNode(\'parent_Selector\', \'parent\', \'\', \'PARENT\', \'changeParent\'); return false;">Change</a></span>',
				"description": "A URI specifying the parent policy",
				"fieldHtmlClass": "input-xxlarge",
				onClick: function (e) {
					e.preventDefault();
					startSelectTreeNode('parent_Selector', 'parent', '', 'PARENT', 'changeParent');
				}
			},*/
/*			{	"key": "owner",
				"notitle": true,
				"prepend": "class=\"jsonform-title\"Owner:</span>",
				"append": "<font color=red><i>required</i></font>",
				"description": "The owner of the "+entity+" (normally you)",
				"fieldHtmlClass": "input-xxlarge",
			},*/
			{	"key": "uri",
				"notitle": true,
				"prepend": "<span class=\"jsonform-title\">URI:</span>",
				"append": "<font color=red><i>required</i></font>",
				"description": "A URI uniquely identifying this "+entity+". If left empty it will be the same with Id with 'asclepios:' NS prefix. <button id=\"btn-gen-uri\" onclick=\"generateUri(); return false;\" class=\"btn btn-warning\">Make URI</button>",
				"fieldHtmlClass": "input-xxlarge",
			},
			{	"key": "type",
				"notitle": true,
				"prepend": "<span class=\"jsonform-title\">Type:</span>",
				"append": "<font color=red><i>required</i></font>",
				"description": "The type of this "+entity+'. Allowed values:  ABAC-POLICY, ABAC-RULE',  /*, ABAC-POLICY-SET*/
				/*"description": "The type of this "+entity+'. Allowed values: '+
								'<button id="btn-set-type-concept"  onclick="setNodeType(0); return false;" class="btn btn-info">ABAC-POLICY</button> '+
								'<button id="btn-set-type-property" onclick="setNodeType(1); return false;" class="btn btn-info">ABAC-RULE</button> '+
								'<button id="btn-set-type-instance" onclick="setNodeType(2); return false;" class="btn btn-info">ABAC-POLICY-SET</button> '+
								//'<button id="btn-set-type-propinst" onclick="setNodeType(3); return false;" class="btn btn-info">PROPERTY-INSTANCE</button> '+
								'',*/
				"fieldHtmlClass": "input-xxlarge",
			},
			{	"key": "name", 
				"notitle": true,
				"prepend": "<span class=\"jsonform-title\">Name:</span>",
				"append": "<font color=red><i>required</i></font>",
				"description": "The display name of the "+entity,
				"fieldHtmlClass": "input-xxlarge",
			},
			{	"key": "description",
			    "type": "textarea",
			    "width": "60%",
			    "height": "100px",
				"notitle": true,
				"prepend": "<span class=\"jsonform-title\">Description:</span>",
				"description": "An explanatory description of the "+entity+"'s purpose",
				"fieldHtmlClass": "input-xxlarge",
			},
/*			{	"key": "createTimestamp", 
				//"type": "datetime",
				"notitle": true,
				"prepend": "<span class=\"jsonform-title\">Creation Date/Time:</span>",
			},
			{	"key": "lastUpdateTimestamp",
				//"type": "datetime",
				"notitle": true,
				"prepend": "<span class=\"jsonform-title\">Last Update Date/Time:</span>",
			},*/
			{	"key": "policyCombiningAlgorithm",
				"notitle": true,
				"prepend": "<span class=\"jsonform-title\">Comb. Algorithm:</span>",
				"append": "<font color=red><i>required</i></font>",
				"description": "The supported ABAC policy combining algorithms: <br/>"+
								'<button class="btn btn-info btn-mini" onclick="setCombiningAlgorithm(\'urn:oasis:names:tc:xacml:1.0:rule-combining-algorithm:first-applicable\'); return false;">First Applicable</button> '+
								'&nbsp;' +
								'<button class="btn btn-warning btn-mini" onclick="setCombiningAlgorithm(\'urn:oasis:names:tc:xacml:3.0:rule-combining-algorithm:deny-overrides\'); return false;">Deny Overrides</button> '+
								'<button class="btn btn-warning btn-mini" onclick="setCombiningAlgorithm(\'urn:oasis:names:tc:xacml:3.0:rule-combining-algorithm:permit-overrides\'); return false;">Permit Overrides</button> '+
								'&nbsp;' +
								'<button class="btn btn-primary btn-mini"  onclick="setCombiningAlgorithm(\'urn:oasis:names:tc:xacml:3.0:rule-combining-algorithm:ordered-deny-overrides\'); return false;">Ordered Deny Overrides</button> '+
								'<button class="btn btn-primary btn-mini"  onclick="setCombiningAlgorithm(\'urn:oasis:names:tc:xacml:3.0:rule-combining-algorithm:ordered-permit-overrides\'); return false;">Ordered Permit Overrides</button> '+
								'&nbsp;' +
								'<button class="btn btn-success btn-mini"  onclick="setCombiningAlgorithm(\'urn:oasis:names:tc:xacml:3.0:rule-combining-algorithm:deny-unless-permit\'); return false;">Deny unless Permit</button> '+
								'<button class="btn btn-success btn-mini"  onclick="setCombiningAlgorithm(\'urn:oasis:names:tc:xacml:3.0:rule-combining-algorithm:permit-unless-deny\'); return false;">Permit unless Deny</button> '+
								'',
				"fieldHtmlClass": "input-xxlarge",
			},
			{	"key": "ruleOutcome",
				"notitle": true,
				"prepend": "<span class=\"jsonform-title\">Outcome:</span>",
				"append": " "+
								'<button id="btn-set-rule-outcome-permit"  onclick="setRuleOutcome(\'PERMIT\'); return false;" class="btn btn-info btn-mini">PERMIT</button> &nbsp;'+
								'<button id="btn-set-rule-outcome-deny"  onclick="setRuleOutcome(\'DENY\'); return false;" class="btn btn-info btn-mini">&nbsp;DENY&nbsp;</button> '+
								'',
				"description": "Specifies the rule outcome, if condition evaluates to <i>True</i>",
				"fieldHtmlClass": "input-xxlarge",
			},
			/*{	"key": "ruleCondition",
				"notitle": true,
				"prepend": "<span class=\"jsonform-title\">Condition:</span>",
				"description": "The rule condition. Empty means always <i>True</i>",
				"fieldHtmlClass": "input-xxlarge",
			},*/
		]
	};
}
function getBaseButtonsFormOptions(entityName) {
	var entity;
	if (entityName) entity = entityName; else entity = 'Node';
	return {
		schema: {
		},
		"form": [
			{	"title": "Save Changes",
				"type": "button",
				"onClick": function (evt) {
					alert('NOT IMPLEMENTED');
				}
			},
			{	"title": "Delete "+entity,
				"type": "button",
				"onClick": function (evt) {
					alert('NOT IMPLEMENTED');
				}
			},
			{	"title": "Create "+entity,
				"type": "button",
				"onClick": function (evt) {
					alert('NOT IMPLEMENTED');
				}
			},
			/*{	"title": "Clear form",
				"type": "button",
				"onClick": function (evt) {
					alert('NOT IMPLEMENTED');
				}
			},*/
		],
	}
}

// Form and form field functions
function setField(name, value) {
	$('input[name='+name+']').val(value);
	$('select[name='+name+']').val(value);
	$('textarea[name='+name+']').val(value);
}
function getField(name) {
	var v = $('input[name='+name+']').val();
	if (!v || v==null || v=='')
	    v = $('textarea[name='+name+']').val();
	return v;
}
function getFormValues() {
	var values = {};
	for (ii in formFields) {
		var fld = formFields[ii];
		values[fld] = getField(fld);
		//console.log('getFormValues: '+fld+' --> '+values[fld]);
	}
	return values;
}
function setFormValues(values) {
	attrNode = values;
	for (ii in formFields) {
		var fld = formFields[ii];
		var val = values[fld];
		//console.log('setFormValues: '+fld+' --> '+val);
		if (fld==='rulePolicy' && val && val.id) {
			attrParent = val;
			val = attrParent.id;
			//console.log('setFormValues: "rulePolicy" field value has been set to: '+val);
		}
		setField(fld, val);
	}
}
function setFormData(data) {
	setFormValues(data);
}
function submitForm(frmSel) {
	if (frmSel && frmSel.trim()!=='')  $(frmSel).submit();
	else $(formSelector).submit();
}
function clearForm(frmSel) {
	setFormValues(emptyData);
	for (f in formFields) $('input[name='+f+']').val('');
	$('input[name=id]').prop('readonly',true);
	setEnabledIdAndUriButtons(false);
}

// Data exchange functions
function retrieveData(methodStr, urlStr, values, mesg) {
	var valStr = '';

	// Prepare values for submit
	if (values) {
	    if (values['type'] && values['type']==='ABAC-RULE') {

	        // Replace 'rulePolicy' id with the actual object
	        if (values['rulePolicy'] && values['rulePolicy']!=null && values['rulePolicy']!=='') {
    	        var parId = values['rulePolicy'];
    	        var parNode = tree.jstree(true).get_node(parId);
    	        //console.log('retrieveData: parent-node to set in rulePolicy: '+JSON.stringify(parNode));

    	        var ruleId = values['id'];
    	        var ruleNode = tree.jstree(true).get_node(ruleId);
    	        if (ruleNode) {
                    var ruleData = ruleNode.data;
                    var ruleDataPolicy = ruleData.rulePolicy;

                    if (ruleDataPolicy && ruleDataPolicy!=null) {
                        values['rulePolicy'] = ruleDataPolicy;
                    } else {
                        alert('>>> INTERNAL ERROR: ruleDataPolicy not set: '+ruleId);
                        throw '>>> INTERNAL ERROR: ruleDataPolicy not set: '+ruleId;
                    }
    	        } else {
                    if (parNode.data && parNode.data!=null) {
                        values['rulePolicy'] = parNode.data;
                    } else {
                        alert('>>> INTERNAL ERROR: node.data not set: '+parId);
                        throw '>>> INTERNAL ERROR: node.data not set: '+parId;
                    }
    	        }
	        } else {
                alert('>>> INTERNAL ERROR: attrParent not set');
                throw '>>> INTERNAL ERROR: attrParent not set';
	        }

	        // Set 'ruleCondition' field with querybuilder data
	        if (isExpressionValid()) {
                var exprData = getExpressionBuilderData();
                //console.log('RULE EXPRESSION DATA:  '+JSON.stringify(exprData));

                values['ruleCondition'] = exprData;
	        } else {
	            alert('Rule condition is invalid');
	            throw 'Rule condition is invalid';
	        }

	    } else {
	        delete values['rulePolicy'];
	    }
	}

    // Submit data
	if (values) valStr = JSON.stringify(values);
	statusContacting(methodStr+' '+urlStr, valStr);
	//clearForm();
	setStatus('<p>Waiting for reply... <img src="../ajax-loader.gif" /></p>');
	if (mesg) showIndicator(mesg); else showIndicator("Contacting server...");
	callStartTm = new Date().getTime();
	$.ajax({
		async: false,
		type: methodStr,
		url: urlStr,
		data: valStr,
		contentType: 'application/json',
		dataType: 'json',
		beforeSend: setCsrfHeaders,
		success: function(data, textStatus, jqXHR) {
					hideIndicator();
					var node = tree.jstree(true).get_node(data.id);
					renderData(data, textStatus);
					//console.log('retrieveData: DATA RECEIVED: '+JSON.stringify(data));
					attrParent = data;
				},
		error: function(jqXHR, textStatus, errorThrown) {
					showErrorMessage(textStatus, errorThrown);
				},
		//complete: function() { alert('ALWAYS'); }
	});
}

// Rendering functions
function renderData(data, textStatus) {
	var callEndTm = new Date().getTime();
	var dataStr = JSON.stringify(data);
	//console.log('renderData: '+dataStr);
	if (prepareForm) prepareForm(data);
	prepareValuesForRender(data);
	clearForm();
	setFormData(data);
	setFormTitle(data.type, data.name, false);
	var endTm = new Date().getTime();
	setStatus('<p>Status: '+textStatus+'</p>'+
					'<p>Duration: '+(endTm-startTm)+'ms   contacting server: '+(callEndTm-callStartTm)+'ms</p>'+
					'<p>JSON: '+dataStr+'</p>');
}
function renderDate(tm) {
	if (tm) {
		try {
			if ($.isNumeric(tm)) { return new Date(tm).toISOString(); }
		} catch (e) { alert(tm+'\n'+e); }
	}
	return tm;
}
function prepareValuesForSubmit(data) {
    //console.log('prepareValuesForSubmit: data: '+JSON.stringify(data));
    /*if (data.type!=='ABAC-RULE' && data.rulePolicy && data.rulePolicy!==='') {
        data.rulePolicy='';
    }*/
}
function prepareValuesForRender(data) {
	data.lastUpdateTimestamp = renderDate(data.lastUpdateTimestamp);
	data.createTimestamp = renderDate(data.createTimestamp);
}

// Define custom date/time picker input field
JSONForm.fieldTypes['datetime'] = {
	'template': '<input type="text" ' +
	  '<%= (fieldHtmlClass ? "class=\'" + fieldHtmlClass + "\' " : "") %>' +
	  'name="<%= node.name %>" value="<%= escape(value) %>" id="<%= id %>"' +
	  '<%= (node.disabled? " disabled" : "")%>' +
	  '<%= (node.readOnly ? " readonly=\'readonly\'" : "") %>' +
	  '<%= (node.schemaElement && node.schemaElement.maxLength ? " maxlength=\'" + node.schemaElement.maxLength + "\'" : "") %>' +
	  '<%= (node.schemaElement && node.schemaElement.required && (node.schemaElement.type !== "boolean") ? " required=\'required\'" : "") %>' +
	  ' />\n' +
	  ' <script type="text\/javascript">$("#<%= id %>").datetimepicker({format:"d/m/Y H:i:s",mask:"9999-19-39T29:59:59.999Z"});<\/script>\n',
	'fieldtemplate': true,
	'inputfield': true
}

// Tree Node Selection functions
var selectTreeNode_active = false;
var selectTreeNode_end_timestamp = 0;
var selectTreeNode_State = '';
var selectTreeNode_selectedNodeIdBefore = '';
var selectTreeNode_acceptedType = '';
var selectTreeNode_selectorId = '';
var selectTreeNode_inputId = '';
var selectTreeNode_displayId = '';
var selectTreeNode_callback = '';

function startSelectTreeNode(selectorId, inputId, displayId, type, callback = '') {
	console.log('>> startSelectTreeNode: BEGIN: '+selectorId);
	if (selectTreeNode_active) return;
	console.log('>> startSelectTreeNode: CHECK DELAY');
	if (Date.now()-selectTreeNode_end_timestamp < 200) return;
	console.log('>> startSelectTreeNode: ACTIVATED');
	selectTreeNode_active = true;
	
	// get selected node and cache it for restore
	selectTreeNode_selectedNodeIdBefore = tree.jstree("get_selected");
	selectTreeNode_selectorId = selectorId;
	selectTreeNode_inputId = inputId;
	selectTreeNode_displayId = displayId;
	selectTreeNode_acceptedType = type;
	selectTreeNode_callback = callback;
	
	// start property selection
	selectTreeNode_State = 'SELECT_NODE';
	html = $("#"+selectorId).html();
	anyType = (type==='*' || type==='ANY');
	parentType = (type==='PARENT');
	innerHtml = 'Click on a '+(anyType||parentType ? 'node' : type)+' in Tree view or click <a style="cursor: pointer;" onclick="endSelectTreeNode(); return false;">Cancel</a> to abort';
	$("#"+selectorId).html( innerHtml );
}

function endSelectTreeNode() {
	console.log('>> endSelectTreeNode: BEGIN: '+selectTreeNode_selectorId);
	if (!selectTreeNode_active) return;
	console.log('>> endSelectTreeNode: DONE');
	selectTreeNode_active = false;
	
	// end property selection
	html = $("#"+selectTreeNode_selectorId).html();
	params = "'"+selectTreeNode_selectorId+"', '"+selectTreeNode_inputId+"', '"+selectTreeNode_displayId+"', '"+selectTreeNode_acceptedType+"'";
	if (selectTreeNode_callback!=='') params += ", '"+selectTreeNode_callback+"'";
	innerHtml = '<a style="cursor: pointer;" onclick="startSelectTreeNode('+params+'); return false;">Change</a>';
	$("#"+selectTreeNode_selectorId).html( innerHtml );
	
	// restore previous selection
	selectTreeNode_State = 'IGNORE';
	deselectTreeNode();
	//data.instance.deselect_node(data.node);
	selectTreeNode(tree, selectTreeNode_selectedNodeIdBefore);
	selectTreeNode_selectedNodeIdBefore = '';
	selectTreeNode_State = '';
	selectTreeNode_callback = null;
	
	selectTreeNode_end_timestamp = Date.now();
}

function setFormFieldToNode(id, text) {
	var callback = selectTreeNode_callback;
	endSelectTreeNode();
	
	$("input[name='"+selectTreeNode_inputId+"']").val(id);
	if (selectTreeNode_displayId!=='') $("input[name='"+selectTreeNode_displayId+"']").val(text);
	
	if (callback!=='') {
		//console.log('setFormFieldToNode: BEFORE calling Callback: '+callback);
		eval( callback+"('"+id+"')" );
		//console.log('setFormFieldToNode: AFTER calling callback: '+callback);
	}
	
	selectTreeNode_selectorId = '';
	selectTreeNode_inputId = '';
	selectTreeNode_displayId = '';
	selectTreeNode_callback = '';
}

function setCombiningAlgorithm(combiningAlgorithm) {
	$("input[name='policyCombiningAlgorithm']").val(combiningAlgorithm);
}

function setRuleOutcome(outcome) {
	$("input[name='ruleOutcome']").val(outcome);
}

//function changeParent(id) {
function changeRulePolicy(id) {
	urlStr = '/gui/abac/get-policy/'+id;
	setStatus('<p>Waiting for reply... <img src="../ajax-loader.gif" /></p>');
	showIndicator('Fetching parent policy information...');
	callStartTm = new Date().getTime();
	$.ajax({
		async: false,
		type: 'get',
		url: urlStr,
		data: '',
		contentType: 'application/json',
		dataType: 'json',
		beforeSend: setCsrfHeaders,
		success: function(data, textStatus, jqXHR) {
					hideIndicator();
                    tree.jstree(true).get_node(data.id).data = data;
					attrParent = data;
				},
		error: function(jqXHR, textStatus, errorThrown) {
					showErrorMessage(textStatus, errorThrown);
				},
		//complete: function() { alert('ALWAYS'); }
	});	
}