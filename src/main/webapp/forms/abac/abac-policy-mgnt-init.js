/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *	jsTree and hotkeys initialization
 */
		/*
		 * Initialize jstree and load initial data
		 */
		// Policies jsTree
		var tree;

		$( document ).ready(function() {
			// Initialize json forms
			initEditForm();
			initButtonsForm();
			initButtonsForm2();
			
			// Initialize tree
			//$.jstree.defaults.core.data = true;
			tree = $('#policyList').jstree({
			    "core" : {
				    "animation" : 500,
				    "multiple": false,
				    "check_callback" :
                        function (operation, node, node_parent, node_position, more) {
                            // operation can be 'create_node', 'rename_node', 'delete_node', 'move_node', 'copy_node' or 'edit'
                            // in case of 'rename_node' node_position is filled with the new node name

                            if (operation==="move_node" && more && more.pos && more.ref && more.ref.original && more.ref.original.type)
                                return more.pos!=='i' && more.ref.original.type==='ABAC-RULE'
                                    || more.pos==='i' && more.ref.original.type==='ABAC-POLICY';
                            return true;
                        },
				    "themes" : { "stripes" : true },
				    'data' : {
					    'url': function (node) {
								var url = node.id === '#' ?
										baseUrl+'/gui/abac/get-toplevel-policies' :
										baseUrl+'/gui/abac/get-policy-rules/'+node.id;
								//alert('URL: '+url);
								return url;
							},
					    'data': function (node) { /*alert('DATA: '+JSON.stringify(node));*/ return ''; }
				    }
			    },
			    "dnd" : {
			        'copy' : false,
			        'inside_pos' : 'last',
			        'is_draggable' : function (nodeArray) {
			                //console.log('is_draggable:  ', nodeArray);
			                if (nodeArray.length==0) alert('No node selected for drag');
			                if (nodeArray.length>1) alert('Many nodes selected for drag');
			                if (nodeArray[0] && nodeArray[0].data && nodeArray[0].data.type)
			                    return nodeArray[0].data.type==='ABAC-RULE';
			                return false;
			            },
			    },
			    "contextmenu" : {
                    "items": function($node) {
                        var tree = $(LEFT_LIST_DOM_ID).jstree(true);
                        var sel = tree.get_selected(true);
                        var type = sel[0].original.type;

                        var createItemsArr;
                        if (type==='ABAC-POLICY') {
                            createItemsArr = {
                                "create_policy": {
                                    "separator_before": false,
                                    "separator_after": false,
                                    "label": "New Policy",
                                    "action": function (obj) {
                                        deselectTreeNode();
                                        createSibling('ABAC-POLICY');
                                    },
                                },
                                "create_rule": {
                                    "separator_before": false,
                                    "separator_after": false,
                                    "label": "New Rule",
                                    "action": function (obj) {
                                        deselectTreeNode();
                                        createChild('ABAC-RULE');
                                    },
                                },
                            };
                        } else
                        /*if (type==='CONCEPT-INSTANCE') {
                            createItemsArr = {
                                "create_child_prop_instance": {
                                    "separator_before": false,
                                    "separator_after": true,
                                    "label": "New Property Instance",
                                    "action": function (obj) {
                                        createChild('PROPERTY-INSTANCE');
                                    },
                                },
                            };
                        } else*/
                        if (type==='ABAC-RULE') {
                            createItemsArr = {
                                "create_rule": {
                                    "separator_before": false,
                                    "separator_after": false,
                                    "label": "New Rule",
                                    "action": function (obj) {
                                        deselectTreeNode();
                                        createSibling('ABAC-RULE');
                                    },
                                },
                            };
                        } else
                        /*if (type==='PROPERTY-INSTANCE') {
                            createItemsArr = { };
                        } else */
                        {
                            createItemsArr = { };
                        }

                        var commonItemsArr = {
                            "expand_all": {
                                "separator_before": true,
                                "separator_after": false,
                                "label": "Expand all",
                                "action": function (obj) {
                                    expand_all();
                                }
                            },
                            "collapse_all": {
                                "separator_before": false,
                                "separator_after": false,
                                "label": "Collapse all",
                                "action": function (obj) {
                                    collapse_all();
                                }
                            },
                            "clear": {
                                "separator_before": true,
                                "separator_after": false,
                                "label": "Clear form",
                                "action": function (obj) {
                                    clearForm();
                                    deselectTreeNode();
                                }
                            },
                            "remove": {
                                "separator_before": true,
                                "separator_after": false,
                                "label": "Delete",
                                "action": function (obj) {
                                    deleteNode();
                                    deselectTreeNode();
                                }
                            },
                            "revoke-from-pap": {
                                "separator_before": true,
                                "separator_after": false,
                                "label": "Revoke Policy",
                                "action": function (obj) {
                                    if (confirm('Do you want to revoke this ABAC policy?')) {
                                        revokePolicy();
                                    }
                                }
                            },
                        };

                        var ctxMenu = $.extend(createItemsArr, commonItemsArr);
                        return ctxMenu;
                    }
			    },
			    "types" : {
				    "#" : {
				        "max_children" : 1,
				        "valid_children" : ["root"]
				    },
				    "root" : {
				        "icon" : "/static/3.0.0/assets/images/tree_icon.png",
				        "valid_children" : ["default"]
				    },
				    "default" : {
				        "valid_children" : ["default","file"]
			        },
				    "file" : {
				        "icon" : "glyphicon glyphicon-file",
				        "valid_children" : []
				    }
			    },
			    "plugins" : [
				    "contextmenu", "dnd", //"search",
				    "state", "types", "wholerow", "hotkeys"//,
			        //"sort"
			    ]
			})
			.on('changed.jstree', function (e, data) {
				// select a property for PropertyIsA field
				if (selectTreeNode_State==='SELECT_NODE') {
					if (data && data.selected && data.selected.length && data.node && data.node.id) {
						// accept 'acceptedType' nodes and ignore the rest
						if (data.node.original.type===selectTreeNode_acceptedType || selectTreeNode_acceptedType==='*' || selectTreeNode_acceptedType==='ANY') {
							// update json form
							setFormFieldToNode(data.node['id'], data.node['text']);
						} else
						// if 'acceptedType'=='PARENT' accept CONCEPT nodes for concept, property and concept instance nodes, and CONCEPT-INSTANCE for property instance nodes
						if (selectTreeNode_acceptedType==='PARENT') {
							// get node type
							var type = $('input[name="type"]').val();
							if (/*type==='PROPERTY-INSTANCE' && data.node.original.type==='CONCEPT-INSTANCE' ||
								type!=='PROPERTY-INSTANCE' && */data.node.original.type==='ABAC-POLICY')
							{
								// update json form
								setFormFieldToNode(data.node['id'], data.node['text']);
							}
						} else {
							console.log('Not a property: '+data.node.id);
						}
					} else {
						console.log('ERROR: "data" or "data.selected" or "data.node" or "data.node.original" not defined');
					}
					return;
				}
				// ending node selection
				if (selectTreeNode_State==='IGNORE') {
					//tree.jstree("select_node", selectedNodeIdBefore);
					return;
				}
				
				// Update json form with selected node information
				// An AJAX call will be started
				$('input[name=id]').prop('readonly', true);
				if(data && data.selected && data.selected.length) {
					startTm = new Date().getTime();
					var url = '';
					switch (data.node.original.type) {
					    case 'ABAC-POLICY' : url = baseUrl+'/gui/abac/get-policy/'+data.selected; break;
					    case 'ABAC-RULE' : url = baseUrl+'/gui/abac/get-rule/'+data.selected; break;
					    default:
                                alert('TYPE NOT YET SUPPORTED [3]');
                                return;
					}
					statusContacting(url, 'n/a');
					clearForm();
					callStartTm = new Date().getTime();
					$.get(url, function (d) {
					    //alert('changed.jstree: received data: '+JSON.stringify(d));
						if(d) {
						    var node = tree.jstree(true).get_node(d.id);
						    node.data = d;
						    if (d.type==='ABAC-RULE') {
						        node.parent.data = d.rulePolicy;
						    }
							renderData(d, 'obviously OK');
							//console.log('NODE_CHANGED: server data: '+JSON.stringify(d));
                            //console.log('NODE_CHANGED: tree node after update: '+JSON.stringify(node));
							attrParent = d;

							// update expression builder
						    if (d.type==='ABAC-RULE') {
							    setExpressionBuilderData(d.ruleCondition);
							}
						}
					});
				}
				else {
					$('#data .content').hide();
					$('#data .default').html('---').show();
				}
			})
			.on('loaded.jstree', function() {
				// check if tree view is empty
				/*if (tree.jstree(true).get_children_dom('#').length==0) {
					createSibling('ABAC-POLICY');
					//prepareCreateForm('ABAC-POLICY');    // makes 'id' field writable (it seems some code makes field readonly after 'createSibling')
				}*/
			})
			.on('load_node.jstree', function(e, data) {
				// enable/disable type buttons
				setFormTypeButtonState(data.node);
			})
			.on('ready.jstree', function(e, data) {
				//console.log('CALL ready.jstree');
				showOrHideEmptyTreeMessage();
				fixTreeContainerHeight();
			})
			.on('refresh.jstree', function(e, data) {
				//console.log('CALL refresh.jstree');
				showOrHideEmptyTreeMessage();
				fixTreeContainerHeight();
			})
			.on('move_node.jstree', function(e, data) {
                //console.log('MOVE_NODE: node: ', data.node.id, data.node.text);
  				if (data.parent!==data.old_parent) {
				    //console.log('MOVE_NODE: from-policy="'+data.old_parent+'", to-policy="'+data.parent+'"');
				    setField('rulePolicy', data.parent);
				    changeRulePolicy(data.parent);

                    var parentNode = $(LEFT_LIST_DOM_ID).jstree('get_node', data.parent);
                    var originalOrder = parentNode.children;
				    if (confirm('Move rule "'+data.node.text+'" to policy "'+parentNode.text+'"?')) {
				        // update rule node data
				        var ruleNode = tree.jstree(true).get_node(data.node.id);
				        ruleNode.data.rulePolicy = parentNode.data;

				        // save rule changes
				        saveNode();
				        saveChildrenOrder(data.parent, originalOrder);
				    } else
				        reloadTree();
				} else {
				    console.log('MOVE_NODE: reordering under same policy');
				    saveChildrenOrder(data.parent);
				}
			})
            ;

            // add slim scrollbar to jsTree
            fixTreeContainerHeight();

            // make readonly fields... readonly
            prepareEditForm();
			
            initTreeHotkeys();
		});
		
		function showOrHideEmptyTreeMessage() {
			// check if tree contains nodes
			var rootNode = tree.jstree(true).get_node('#');
			if (rootNode && rootNode.children) {
				if (rootNode.children.length==0) {
				    $('#empty_tree_mesg').show();
				    createPolicy();
				} else {
				    $('#empty_tree_mesg').hide();
				    var selArr = tree.jstree(true).get_selected();
				    if (selArr && selArr.length==0) {
				        var first = rootNode.children[0];
				        tree.jstree(true).select_node(first);
				    }
				}
			} else
				//console.log('ready.jstree: Could not get root node')
				;
		}

        function fixTreeContainerHeight() {
            // size jsTree container
            var diff = $('#treeContainer').offset().top;
            diff += $('#formButtons2').height();
            diff += $('#treeInstructions').height();
            diff += 50;
            $('#treeContainer').height(document.documentElement.clientHeight - diff);
		}
		
		/*
		 *	Hotkeys initialization
		 */
		function initTreeHotkeys() {
			var tree = $(LEFT_LIST_DOM_ID);	//jQuery(document);
			tree.bind('keydown', 'insert', function (evt) {
                    var tree = $(LEFT_LIST_DOM_ID).jstree();
                    var sel = selectedTreeNode(tree);
                    if (sel && sel!=null) {
                        var id = getField('id');
                        var type = tree.get_node(id).original.type;

                        if (type==='ABAC-POLICY') { deselectTreeNode(); createSibling('ABAC-POLICY'); }
                        if (type==='ABAC-RULE') { deselectTreeNode(); createSibling('ABAC-RULE'); }
                    }
                    return false;
            });
			tree.bind('keydown', 'Shift+insert', function (evt) {
                    var tree = $(LEFT_LIST_DOM_ID).jstree();
                    var sel = selectedTreeNode(tree);
                    if (sel && sel!=null) {
                        var id = getField('id');
                        var type = getField('type');

                        if (type==='ABAC-POLICY') { deselectTreeNode(); createChild('ABAC-RULE'); }
                        if (type==='ABAC-RULE') { deselectTreeNode(); createSibling('ABAC-RULE'); }
                    }
                    return false;
			});
			tree.bind('keydown', 'del',function (evt){ deleteNode(); return false; });
		}
