<!DOCTYPE html>
<!--
  ~
  ~ Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~      http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  ~
  -->
<%@ include file="../../includes/prelude.html" %>
<html>
  <head>
<% pageTitle = "ASCLEPIOS Policy Validation Management"; %>
<%@ include file="../../includes/head.html" %>
	<link rel="stylesheet" href="../slick/common-grid-styles.css" type="text/css"/>
    <link rel="stylesheet" style="text/css" href="/forms/admin/common.css" />
    <link rel="stylesheet" style="text/css" href="/forms/deps/opt/bootstrap.css" />

    <link href="/forms/deps/bootstrap2-toggle.min.css" rel="stylesheet">
    <style>
        .toggle-padding {
            padding-top: 0px !important;
            padding-bottom: 0px !important;
            border-radius: 5px !important;
        }
    </style>
    <script src="/forms/deps/bootstrap2-toggle.min.js"></script>

    <script src="validation-results.js"></script>

<%@ include file="../../includes/expression-builder-assets.html" %>

  </head>
  <body>
	
<%@ include file="../../includes/header.html" %>
	
				<div class="row-fluid" style="margin-top: 20px;">
					<div id="leftContainer" class="span3">
						<div >
							<!-- Legent line -->
							<p align="center"><font size="-1"><i>
							    Policy Inspection: &nbsp;&nbsp;
								<img src="/images/validation/validation-group-inspection-32x32.png" width="16" height="16"/>&nbsp;Group&nbsp;
								<img src="/images/validation/validation-set-inspection-32x32.png" width="16" height="16"/>&nbsp;Set&nbsp;
								<img src="/images/validation/validation-rule-inspection-32x32.png" width="16" height="16"/>&nbsp;Rule&nbsp;
								<br/>
								Security Awareness: &nbsp;&nbsp;
								<img src="/images/validation/validation-group-awareness-32x32.png" width="16" height="16"/>&nbsp;Group&nbsp;
								<img src="/images/validation/validation-set-awareness-32x32.png" width="16" height="16"/>&nbsp;Category&nbsp;
								<img src="/images/validation/validation-rule-awareness-32x32.png" width="16" height="16"/>&nbsp;Check&nbsp;
							</i></font></p>
						</div>
						<!-- Policies tree view -->
						<div id="treeContainer" style="overflow-y: scroll">
							<!-- jstree -->
							<div id="validationList"></div>
						</div>
                        <!-- Extra buttons -->
                        <!--<div id="formButtons2" class="span12" style="padding: 0px; float: center;"></div>-->
                        <div style="height: 5px;"> </div>
                        <div id="formButtons2" class="span11 container-fluid" style="padding: 0px; float: center;">
                            <div class="row-fluid">
                                <button id="btn-import-validation" class="btn btn-transition btn-danger span6"><img src="/images/upload-white-32x32.png" width="18" height="18"/> Import Validation </button>
                                <button id="btn-export-validation-as-ttl" class="btn btn-transition btn-info span6"><img src="/images/download-white-32x32.png" width="18" height="18"/> Export Validation </button>
                            </div>
                            <div style="height: 5px;"> </div>
                            <div class="row-fluid">
                                <button id="btn-validate-policies" class="btn btn-transition btn-warning span12"><img src="/images/validation/validation-rule-white-32x32.png" width="18" height="18"/> Validate Policies </button>
                            </div>
                            <div style="height: 5px;"> </div>
                        </div>
                        <!-- Instructions -->
                        <div id="treeInstructions" class="span12">
                            <div style="font:italic 10pt arial,sans-serif; border-top:black 1pt solid; margin-top:10px; padding-top:15px;">
                                &rtrif; Use <strong>Del</strong>, <strong>Ins</strong> and <strong>Shift+Ins</strong> hotkeys to respectively delete selected, create sibling and create child node.<br/>
                                &rtrif; Import policy validation rules from an RDF/TTL file by clicking on <b>Import Validation</b> button
                            </div>
                            <!--<div id="empty_tree_mesg" class="alert" style="color:black; font-size:10pt; font-style:italic;"><blockquote></blockquote></div>-->
                        </div>
					</div>
					<div id="formContainer" class="span9">
						<!--<div class="alert"></div>-->
						<div style="font-size: 12pt; font-weight: bold; font-style: italic; background: #eeeeee; padding: 2px; padding: 10px 10px 10px 10px;">
						    <span id="form-title-group"><img src="/images/validation/validation-group-inspection-32x32.png"/>/<img src="/images/validation/validation-group-awareness-32x32.png"/>&nbsp;Group details</span>
						    <span id="form-title-set"><img src="/images/validation/validation-set-inspection-32x32.png"/>/<img src="/images/validation/validation-set-awareness-32x32.png"/>&nbsp;Set details</span>
						    <span id="form-title-rule"><img src="/images/validation/validation-rule-inspection-32x32.png"/>/ <img src="/images/validation/validation-rule-awareness-32x32.png"/>&nbsp;Rule details</span>
						    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						    <span id="form-title-breadcrumb" style="font-weight: normal; font-style: normal; font-size: 150%"></span>
						</div>
						<p style="margin: 0px;"><hr /></p>
						<!--<p id="update_notice" class="alert" style="display: none"></p>-->
						<form id="formEdit"></form>
						<p><hr /></p>
						<div id="formButtons"></div>
						<p><br/></p>
						<!--<div id="res" class="alert">
							...<br/>
						</div>-->
						<!--<div id="res" class="alert"></div>-->

						<div id="validationExpressionContainer" class="span9" style="margin-left: 0px;">
						    <div id="validationRulePart">
						        <select id="rule-outcome" style="width:100px">
						            <option value="ERROR">Error</option>
						            <option value="WARNING">Warning</option>
						            <option value="INFO">Info</option>
						            <option value="DEBUG">Debug</option>
						        </select>
						        when
						        <select id="rule-target" style="width:150px">
						            <option value="ABAC-RULE">ABAC Rule</option>
						            <option value="ABE-POLICY">ABE Policy</option>
						            <option value="ANY">Any</option>
						        </select>
						        <select id="rule-operation">
						            <option value="EXISTS">Exists / Contains</option>
						            <option value="NOT-EXISTS">Not Exists / Not Contains</option>
						        </select>
						        with
						        <select id="rule-expression-type">
						            <option value="ATTRIBUTE">Attribute</option>
						            <option value="PROPERTY">Attribute and Property</option>
						            <option value="OPERATOR">Attribute, Property and Operator</option>
						            <option value="EXPRESSION">Expression</option>
						        </select>
						        :
						    </div>
						    <div id="validationExpressionJQB"></div>
						    <!--<button class="btn btn-success" id="btn-validate">Validate</button>
						    <button class="btn btn-primary" id="btn-get-data">Get Data</button>-->
						</div>
					</div>
				</div>
				
<%@ include file="../../includes/js-libs.html" %>
    <script type="text/javascript" src="/forms/validation/form-validation.js"></script>

				<script type="text/javascript">
					// Override form-validation vars to meet this form's fields
					var formFields = ['id', 'parent', /*'owner',*/ 'uri', 'type', 'name', 'description', 'enabled', /*'createTimestamp', 'lastUpdateTimestamp',*/ 'policyValidationRuleDefinition'];
					var emptyData = {'id':'', 'parent':'', /*'owner':'',*/ 'uri':'', 'type':'', 'name':'', 'description':'', 'enabled':true, /*'createTimestamp':'', 'lastUpdateTimestamp':'',*/ 'policyValidationRuleDefinition':'ANY EXISTS WITH EXPRESSION' };
					formSelector = "#formEdit";
					statusSelector = '#res';

					const LEFT_LIST_DOM_ID = "#validationList";

				</script>
				
				<!-- Main Form initialization -->
				<script type="text/javascript" src="validation-mgnt-jsonform.js"></script>
				
				<!-- Operation functions -->
				<script type="text/javascript" src="validation-mgnt-node-ops.js"></script>
				
				<!-- jsTree and hotkeys initialization -->
				<script type="text/javascript" src="validation-mgnt-init.js"></script>

				<script><!--
				    $(function() {
				        correctFieldDimensions();
                    });
				//--></script>

				<!-- QueryBuilder initialization for Rule Conditions -->
                <script type="text/javascript" src="/forms/ExpressionBuilder.js"></script>

                <script>
                    QUERY_BUILDER_CONTAINER = '#validationExpressionJQB';
                    QUERY_BUILDER_SELECT2_SEARCH_FOR_ATTRIBUTES_URL = '/gui/attributes/search/attributes';
                    QUERY_BUILDER_SELECT2_SEARCH_FOR_PROPERTIES_URL = '/gui/attributes/search/properties';

                    // create an empty ExpressionBuilder instance
                    createExpressionBuilder(null);

                    // initialize buttons if exists
                    $('#btn-validate').on('click', function (e) {
                        if (isExpressionValid())
                            alert('Expression is valid');
                        else
                            alert('Expression is not valid');
                    });
                    $('#btn-get-data').on('click', function (e) {
                        var data = getExpressionBuilderData();
                        console.log(data);
                    });

                </script>

<%@ include file="../../includes/footer.html" %>
  </body>
</html>
<%@ include file="../../includes/debug.html" %>
<%@ include file="../../includes/trail.html" %>