/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *	Validation results initialization
 */
	
var validationResultsOriginalData = null;
var validationResultsJson = null;
var validationResultsText = null;
var validationResultsGrouping = null;
var validationResultsPromiseCallbacks = null;
var validationResultsIdToItemMap = [];

$( document ).ready(function() {
    // Add validation results DIV in DOM
    $(`
        <!-- validation Results Modal -->
        <div id="validationResultsModal" title="Validation Results" style="overflow:hidden;">
            <div id="validationResultsModalBody" style="overflow:auto; height:87%;">
                <!-- jstree -->
                <div id="validationResultsList" ></div>
            </div>
            <div id="validationResultsButtons" class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"
             style="width:100%; height:10%; min-height:30px; position:absolute; bottom:0px; right:0px;">
                <div class="pull-left" style="padding-left:30px;">
                    <button id="btn-toggle-grouping-mode" class="btn btn-secondary">Toggle Grouping</button>
                </div>
                <span>&nbsp;</span>
                <div id="validation-message-counts" class="pull-left">
                    <a style="cursor:pointer;" onClick="setResultsFilter('ERROR');"><code>Errors: <span id="ERROR-validation-message-counts" /></code></a>
                    <a style="cursor:pointer;" onClick="setResultsFilter('WARNING');"><code>Warns: <span id="WARNING-validation-message-counts" /></code></a>
                    <a style="cursor:pointer;" onClick="setResultsFilter('INFO');"><code>Info: <span id="INFO-validation-message-counts" /></code></a>
                    <a style="cursor:pointer;" onClick="setResultsFilter('DEBUG');"><code>Debug: <span id="DEBUG-validation-message-counts" /></code></a>
                    <a style="cursor:pointer;" onClick="clearResultsFilter();"><code>Show all</code></a>
                </div>
                <span>&nbsp;</span>
                <div class="pull-right">
                    <button id="btn-copy-text" class="btn btn-info">Copy as Text</button>
                    <button id="btn-copy-json" class="btn btn-info">Copy as JSON</button>
                    <span>&nbsp;</span>
                    <button id="btn-save-text" class="btn btn-success">Save as Text</button>
                    <button id="btn-save-json" class="btn btn-success">Save as JSON</button>
                    <span>&nbsp;</span>
                    <button id="btn-dialog-close" class="btn btn-danger">&nbsp;&nbsp;&nbsp; Close &nbsp;&nbsp;&nbsp;</button>
                    <span>&nbsp;</span>
                    <button id="btn-continue" class="btn btn-warning">Continue</button>
                </div>
            </div>
        </div>
    `)
    .appendTo(document.body);

    // Hide "Continue" dialog button
    $("#btn-continue").hide();

    // Initialize toggle results grouping action
    $('#btn-toggle-grouping-mode').on('click', function() {
        toggleValidationResultsGrouping();
    })

    // Initialize clipboard actions
    initClipboardAction('#btn-copy-text', function(trigger) { return validationResultsText; }, 'validationResultsModal', 'Validation results copied to clipboard as text')
    initClipboardAction('#btn-copy-json', function(trigger) { return JSON.stringify(validationResultsJson, null, 2); }, 'validationResultsModal', 'Validation results copied to clipboard as JSON')

    // Initialize save button actions
    $('#btn-save-text').on('click', function() {
        downloadToFile(validationResultsText, 'validation-results-'+Date.now()+'.txt', 'text/plain');
    })
    $('#btn-save-json').on('click', function() {
        downloadToFile(JSON.stringify(validationResultsJson, null, 2), 'validation-results-'+Date.now()+'.json', 'application/json');
    })

    // Initialize close and continue button actions
    $('#btn-dialog-close').on('click', function() {
        $("#validationResultsModal").dialog("close");
        $('body').css('overflow', 'scroll');
        if (validationResultsPromiseCallbacks!=null)
            validationResultsPromiseCallbacks.reject({ type: 'warning', message: 'Action cancelled by the user', title: 'Cancelled' });
    })
    $('#btn-continue').on('click', function() {
        $("#validationResultsModal").dialog("close");
        $('body').css('overflow', 'scroll');
        validationResultsPromiseCallbacks.resolve('Warnings exist');
    })

    // Initialize validation results dialog
    $('#validationResultsModal').dialog({
        autoOpen: false,
        modal: true,
        resizable: true,
        position: { my: "center", at: "center", of: window },
        closeOnEscape: false,
        close: function(event, ui) { $('body').css('overflow', 'scroll'); },
        draggable: true,
    });

    // ------------------------------------------------------------------------

    // Add messages DIV in DOM
    $(`
        <!-- message Modal -->
        <div id="messageModal" title="" style="overflow:hidden;">
            <div id="messageModalBody" style="overflow:auto; height:87%;">
                <!-- message -->
            </div>
            <div id="messageModalButtons" class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"
             style="width:100%; height:10%; min-height:30px; position:absolute; bottom:0px; right:0px;">
                <div class="pull-right">
                    <button id="btn-dialog-close-2" class="btn btn-danger">&nbsp;&nbsp;&nbsp; Close &nbsp;&nbsp;&nbsp;</button>
                </div>
            </div>
        </div>
    `)
    .appendTo(document.body);
    $('#messageModal').hide();

    // Initialize close button action for message dialog
    $('#btn-dialog-close-2').on('click', function() {
        $("#messageModal").dialog("close");
        $("#messageModal").dialog("destroy");
        $('#messageModal').hide();
    })
});

function validatePolicy(policyId, baseUrl) {
    if (!baseUrl) baseUrl = '/gui/validation/validate-policy/';
    //console.log('Validate policy: id='+policyId+' : Contacting '+baseUrl + policyId);

    return new Promise((resolve, reject) => {
        validationResultsPromiseCallbacks = {
            resolve: resolve,
            reject: reject
        };
        showIndicator('<p><img src="/images/validation/validation-rule-inspection-24x24.png" /> Validating policy...</p>');
        $("#btn-continue").hide();
        $.ajax({
            url: baseUrl + policyId,
            beforeSend: setCsrfHeaders,
            success: function (data) {
                //console.log('Validation results: received data: ', data);
                hideIndicator();

                if (data) {
                    var messageCounts = {};
                    var treeData = groupByValidationRuleThenByPolicy(data, messageCounts);
                    //console.log('Validation results: jstree data: ', treeData);

                    // check for errors or warnings
                    var hasErrors = (messageCounts.hasOwnProperty('ERROR') && messageCounts['ERROR']>0);
                    var hasWarnings = (messageCounts.hasOwnProperty('WARNING') && messageCounts['WARNING']>0);
                    if (hasErrors) {
                        validationResultsPromiseCallbacks = null;
                        reject('Validation errors exist');
                    } else
                    if (hasWarnings) {
                        // Make visible the "Continue" dialog button
                        $("#btn-continue").show();
                    }

                    // check if messages exist
                    var hasMessages = false;
                    for (k in  messageCounts)
                        if (messageCounts[k]>0) { hasMessages = true; break;}

                    if (hasMessages) {
                        // Show validation results in a modal dialog
                        showResultsDialog(treeData, messageCounts);
                    } else {
                        // No messages
                        validationResultsPromiseCallbacks = null;
                        resolve();
                    }
                } else {
                    validationResultsPromiseCallbacks = null;
                    reject('Validation error: No results returned');
                    showErrorMessage('Validation error: No results returned');
                }
            },
        })
        .fail(function(results, textStatus, jqXHR) {
            hideIndicator();
            console.log('Validation error: connection failed: results: ', results);
            console.log('Validation error: connection failed: textStatus: ', textStatus);

            validationResultsPromiseCallbacks = null;
            reject('Validation error: connection failed: '+textStatus);
            showErrorMessage('Validation error: connection failed: \n'+textStatus);
        });
    });
}

function validatePolicies(baseUrl) {
    if (!baseUrl) baseUrl = '/gui/validation/validate-policies-with/';

    var id = getField('id');
    var type = getField('type');
    //var parentId = getField('parent');
    var node = $(LEFT_LIST_DOM_ID).jstree(true).get_node(id);
    var groupId = node.parents[node.parents.length-2];
    if (type==='POLICY-VALIDATION-GROUP') groupId = id;
    //console.log('Validate policies using selected node and its children: id='+id+', type='+type+', groupId='+groupId);

    if (!type || type==null || type.trim()=='') return;
    if (!groupId || groupId==null || groupId.trim()=='') return;
    if (!id || id==null || id=='') return;

    var url = baseUrl + id;
    //console.log('Validate policies: Contacting '+url);

    showIndicator('<p><img src="/images/validation/validation-rule-inspection-24x24.png" /> Validating policies...</p>');
    $.get(url, function (data) {
        //console.log('Validation results: received data: ', data);
        hideIndicator();

        if (data) {
            var messageCounts = {};
            //var treeData = groupByValidationRuleThenByPolicy(data, messageCounts);
            var treeData = groupByPolicyThenByValidationRule(data, messageCounts);
            //console.log('Validation results: jstree data: ', treeData);

            // Show validation results in a modal dialog
            showResultsDialog(treeData, messageCounts);

        } else {
            showErrorMessage('No results returned', 'Validation Error');
        }
    });
}

// ----------------------------------------------------------------------------

function showResultsDialog(treeData, messageCounts) {
    // Update validation results jstree
    updateResultsTree(treeData);

    // Update validation message counts
    $('#validation-message-counts').children('a').each(function(e) { $(this).hide(); });
    for (const k in messageCounts) {
        var e = $(`#${k}-validation-message-counts`);
        e.html(messageCounts[k]);
        e.closest('a').show();
    }
    $('#validation-message-counts').children('a').last().show();
    //$("#validation-message-counts").html( JSON.stringify(messageCounts) );

    // Clear any open toasts
    hideToasts();

    // Show validation results in a modal dialog
    $("#validationResultsModal").dialog('option', {
        width: window.innerWidth*0.8,
        height: window.innerHeight*0.8,
    });
    $("#validationResultsModal").dialog('open');
    $('body').css('overflow', 'hidden');
}

function setResultsFilter(messageType) {
    $('#validationResultsList').jstree(true).search(messageType);
}
function clearResultsFilter() {
    $('#validationResultsList').jstree(true).clear_search();
}

function toggleValidationResultsGrouping() {
    var messageCounts = {};
    var treeData;
    if (validationResultsGrouping==='GROUP-BY-VALIDATION-RULE-THEN-BY-POLICY') {
        treeData = groupByPolicyThenByValidationRule(validationResultsOriginalData, messageCounts);
    } else {
        treeData = groupByValidationRuleThenByPolicy(validationResultsOriginalData, messageCounts);
    }

    // Update validation results jstree
    updateResultsTree(treeData);
}

function updateResultsTree(treeData) {
    // Update json and text to copy/save
    validationResultsJson = simplifyJson( JSON.parse(JSON.stringify(treeData)) );
    validationResultsText = convertResultsToText(validationResultsJson);

    // Update validation results jstree
    var treeExists = $('#validationResultsList').jstree(true);
    if (treeExists) $("#validationResultsList").jstree('destroy');
    $("#validationResultsList").jstree({
        "core" : {
            "animation" : 500,
            "multiple": false,
            "themes" : { "stripes" : true },
            "data" : treeData
        },
        "search": {
            "case_insensitive": true,
            "show_only_matches": true,
            "search_leaves_only": true,
            "search_callback": function(str, node) {
                //console.log('jstree.search_callback: str=', str);
                //console.log('jstree.search_callback: node=', node);
                if (node.original && node.original.object && node.original.object.messageType) {
                    if (node.original.object.messageType===str)
                        return true;
                }
                return false;
            }
        },
        "plugins" : [ "search" ],
    });
}

function groupByValidationRuleThenByPolicy(data, messageCounts) {
    validationResultsGrouping = 'GROUP-BY-VALIDATION-RULE-THEN-BY-POLICY';
    validationResultsOriginalData = data;
    return _doResultsGrouping(data, messageCounts, 'validationObject', 'objectChecked');
}

function groupByPolicyThenByValidationRule(data, messageCounts) {
    validationResultsGrouping = 'GROUP-BY-POLICY-THEN-BY-VALIDATION-RULE';
    validationResultsOriginalData = data;
    var treeData = _doResultsGrouping(data, messageCounts, 'objectChecked', 'validationObject');

    if (treeData.length>0) {
        var abacItems = [];
        var abeItems = [];
        for (const item of treeData) {
            if (item.object.type==='ABAC-POLICY') abacItems.push(item);
            if (item.object.type==='ABE-POLICY') abeItems.push(item);
        }
        treeData = [];
        if (abacItems.length>0)
            treeData.push({
                text: '<b>ABAC Policies</b>',
                icon: '/images/abac/abac-policy-16x16-4.png',
                children: abacItems,
                state : { 'opened' : true, 'selected' : false },
            });
        if (abeItems.length>0)
            treeData.push({
                text: '<b>ABE Policies</b>',
                icon: '/images/abe/abe-policy-16x16.png',
                children: abeItems,
                state : { 'opened' : true, 'selected' : false },
            });
    }
    return treeData;
}

function _doResultsGrouping(data, messageCounts, key1, key2) {
    validationResultsIdToItemMap = [];
    var treeData = [];
    for (const item of data) {
        if (item.message && item.message!=='') {
            // create jsTree data
            var itemNode = getOrCreateParentPathFor(item[key1], treeData);
            var leafNode = getOrCreateParentPathFor(item[key2], itemNode.children);
            // create message node
            var mesgNode = {
                text: getMessageText(item),
                icon: getMessageIcon(item),
                object: item,
            };
            leafNode.children.push(mesgNode);

            // count errors and warnings
            var messageType = item.messageType;
            if (!messageCounts[messageType]) messageCounts[messageType] = 0;
            messageCounts[messageType]++;
        }
    }

    if (treeData.length==0) {
        treeData = {
            text: '<i>No messages</i>',
            icon: false,
        };
    }
    return treeData;
}

function getOrCreateParentPathFor(item, treeData) {
    if (item==null) return;

    // update 'validationResultsIdToItemMap'
    if (item.id && item.id!=='') {
        if (!validationResultsIdToItemMap[item.id]) {
            validationResultsIdToItemMap[item.id] = item;
        }
    }

    var itemNode = null;
    var arrayToSearch = null;
    if (item.parent && item.parent!=null) {
        var parentNode = getOrCreateParentPathFor(item.parent, treeData);
        arrayToSearch = parentNode.children;
    } else
    if (item.rulePolicy && item.rulePolicy!=null) {
        var parentNode = getOrCreateParentPathFor(item.rulePolicy, treeData);
        arrayToSearch = parentNode.children;
    } else {
        arrayToSearch = treeData;
    }

    for (const childItem of arrayToSearch) {
        if (childItem.object.id===item.id) {
            itemNode = childItem;
            break;
        }
    }
    if (itemNode==null) {
        itemNode = {
            text: getNodeNameWithLink(item),
            icon: getNodeIcon(item),
            object: item,
            children: [],
            state : { 'opened' : true, 'selected' : false },
        };
        arrayToSearch.push(itemNode);
    }

    return itemNode;
}

function getNodeNameWithLink(item) {
    /*switch (item.type) {
        case 'ABAC-POLICY': return `<a href="/${item.id}" target="_NEW">${item.name}</a>`;
        case 'ABAC-RULE': return `<a href="/${item.id}" target="_NEW">${item.name}</a>`;
        case 'ABE-POLICY': return `<a href="/${item.id}" target="_NEW">${item.name}</a>`;
    }*/
    return `<a onClick="showDescription('${item.id}');">${item.name}</a>`;
}

function showDescription(id) {
    var data = validationResultsIdToItemMap[id];
    if (data.description && data.description!=null) {
        var message = data.description;
        //console.log('showDescription: description=', message);
        if (message.trim()!=='') {
            var title = id;
            if (data.name && data.name!=null && data.name.trim()!=='') {
                title = data.name.trim();
            }
            //console.log('showDescription: title=', title, 'message=', message);
            showMessageDialog(message, title);
            return;
        }
    }
    showToast('warning', '<b>No description</b>');
}

function getNodeIcon(item) {
    var n = item;
    while (n.parent && n.parent!=null) n = n.parent;
    var groupId = n.id;
    switch (item.type) {
        case 'POLICY-VALIDATION-GROUP':
            if (groupId==='POLICY-INSPECTION') return '/images/validation/validation-group-inspection.png';
            else if (groupId==='SECURITY-AWARENESS') return '/images/validation/validation-group-awareness.png';
            else return null;
        case 'POLICY-VALIDATION-SET':
            if (groupId==='POLICY-INSPECTION') return '/images/validation/validation-set-inspection.png';
            else if (groupId==='SECURITY-AWARENESS') return '/images/validation/validation-set-awareness.png';
            else return null;
        case 'POLICY-VALIDATION-RULE':
            if (groupId==='POLICY-INSPECTION') return '/images/validation/validation-rule-inspection.png';
            else if (groupId==='SECURITY-AWARENESS') return '/images/validation/validation-rule-awareness.png';
            else return null;
        case 'ABAC-POLICY': return '/images/abac/abac-policy-16x16-4.png';
        case 'ABAC-RULE':   return '/images/abac/abac-rule-16x16.png';
        case 'ABE-POLICY':  return '/images/abe/abe-policy-16x16.png';
    }
    return false;
}

function getMessageText(item) {
    return item.message;
}

function getMessageIcon(item) {
    switch (item.messageType) {
        case 'DEBUG': return '/images/error-red-exclamation-16x16.png';
        case 'INFO': return '/images/info-2-small.png';
        case 'WARNING': return '/images/info-1-small.png';
        case 'ERROR': return '/images/error-red-x-16x16.png';
        default: return '';
    }
}

// ----------------------------------------------------------------------------

function showMessageDialog(message, titleStr) {
    // Update message and title
    $('#messageModalBody').html(message);
    $('#messageModal').show();

    // Clear any open toasts
    hideToasts();

    // Initialize message dialog
    $('#messageModal').dialog({
        autoOpen: false,
        modal: true,
        resizable: true,
        position: { my: "center", at: "center", of: window },
        closeOnEscape: true,
        draggable: true,
        title: titleStr,
        width: window.innerWidth*0.4,
        height: window.innerHeight*0.4,
    });

    // Show message modal dialog
    $("#messageModal").dialog('open');
}

// ----------------------------------------------------------------------------

function simplifyJson(data) {
    return Array.isArray(data)
            ? simplifyJsonArray( data )
            : simplifyJsonObject( data );
}
function simplifyJsonArray(array) {
    var result = [];
    for (const elem of array)
        result.push( simplifyJsonObject(elem) );
    return result;
}
function simplifyJsonObject(data) {
    var result = { };
    for (const key in data) {
        if (key==='id') result[key] = data[key];
        if (key==='text') result[key] = $("<div>").html(data[key]).text();
    }
    if (data.children && data.children.length>0) {
        result['children'] = simplifyJsonArray(data.children);
    }
    return result;
}

function convertResultsToText(data, indent) {
    if (!indent) indent = '';
    var result = '';

    if (Array.isArray(data)) {
        for (const elem of data) {
            result += convertResultsToText(elem, indent);
        }
    }
    else
    {
        if (data.text && data.text!=null && (typeof data.text)==='string' && data.text.trim()!=='') {
            result = indent + '- ' + data.text.trim()+'\n';
            indent += '  ';
        }
        if (data.children) {
            for (const child of data.children) {
                result += convertResultsToText(child, indent);
            }
        }
        if (data.messages) {
            for (const child of data.messages) {
                result += convertResultsToText(child, indent);
            }
        }
    }
    return result;
}

// Source: https://stackoverflow.com/questions/13405129/javascript-create-and-save-file [2020-05-03]
// Function to download data to a file
function downloadToFile(data, filename, type) {
    var file = new Blob([data], {type: type});
    if (window.navigator.msSaveOrOpenBlob) // IE10+
        window.navigator.msSaveOrOpenBlob(file, filename);
    else { // Others
        var a = document.createElement("a"),
                url = URL.createObjectURL(file);
        a.href = url;
        a.download = filename;
        document.body.appendChild(a);
        a.click();
        setTimeout(function() {
            document.body.removeChild(a);
            window.URL.revokeObjectURL(url);
        }, 0);
    }
}

//EOF