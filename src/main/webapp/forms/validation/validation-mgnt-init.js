/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *	jsTree and hotkeys initialization
 */
		/*
		 * Initialize jstree and load initial data
		 */
		// Policies jsTree
		var tree;

		$( document ).ready(function() {
			// Initialize json forms
			initEditForm();
			initButtonsForm();
			initButtonsForm2();

			// Validation results dialog is initialized in 'validation-results.js'
			
            // Initialize form switches
			$('.toggle-control').bootstrapToggle({
			    on: 'Yes',
			    off: 'No',
			    onstyle: 'success',
			    offstyle: 'danger',
			    style: 'toggle-padding',
			    width: 10,
			    height: 20,
			})
			.on('change', function(e) {
			    $(e.target).closest('.add-on').prev().val(e.target.checked);
                e.stopImmediatePropagation();
			})
			;
			$('.toggle-value').hide();

            // Add change listener to rule definition controls
            $('#rule-outcome').on('change', ruleListener);
            $('#rule-target').on('change', ruleListener);
            $('#rule-operation').on('change', ruleListener);
            $('#rule-expression-type').on('change', ruleListener);

			// Initialize tree
			//$.jstree.defaults.core.data = true;
			tree = $(LEFT_LIST_DOM_ID).jstree({
			    "core" : {
				    "animation" : 500,
				    "multiple": false,
				    "check_callback" :
                        function (operation, node, node_parent, node_position, more) {
                            // operation can be 'create_node', 'rename_node', 'delete_node', 'move_node', 'copy_node' or 'edit'
                            // in case of 'rename_node' node_position is filled with the new node name

                            if (operation==="move_node" && more && more.pos && more.ref && more.ref.original && more.ref.original.type)
                                return more.pos!=='i' && more.ref.original.type==='POLICY-VALIDATION-RULE'
                                    || more.pos==='i' && more.ref.original.type==='POLICY-VALIDATION-SET';
                            return true;
                        },
				    "themes" : { "stripes" : true },
				    'data' : {
					    'url': function (node) {
								var url = null;
								if (node.id === '#') url = baseUrl+'/gui/validation/';
								else url = baseUrl+'/gui/validation/get-children/' + node.id;
								//alert('URL: '+url);
								return url;
							},
					    'data': function (node) { /*alert('DATA: '+JSON.stringify(node));*/ return ''; }
				    }
			    },
			    "dnd" : {
			        'copy' : false,
			        'inside_pos' : 'last',
			        'is_draggable' : function (nodeArray) {
			                //console.log('is_draggable:  ', nodeArray);
			                if (nodeArray.length==0) alert('No node selected for drag');
			                if (nodeArray.length>1) alert('Many nodes selected for drag');
			                if (nodeArray[0] && nodeArray[0].data && nodeArray[0].data.type)
			                    return nodeArray[0].data.type==='POLICY-VALIDATION-RULE';
			                return false;
			            },
			    },
			    "contextmenu" : {
                    "items": function($node) {
                        var tree = $(LEFT_LIST_DOM_ID).jstree(true);
                        var sel = tree.get_selected(true);
                        var type = sel[0].original.type;

                        var createItemsArr;
                        if (type==='POLICY-VALIDATION-GROUP') {
                            createItemsArr = {
                                "create_set": {
                                    "separator_before": false,
                                    "separator_after": false,
                                    "label": "New Validation Set",
                                    "action": function (obj) {
                                        deselectTreeNode();
                                        createChild('POLICY-VALIDATION-SET');
                                    },
                                },
                            };
                        } else
                        if (type==='POLICY-VALIDATION-SET') {
                            createItemsArr = {
                                "create_set": {
                                    "separator_before": false,
                                    "separator_after": false,
                                    "label": "New Validation Set",
                                    "action": function (obj) {
                                        deselectTreeNode();
                                        createSibling('POLICY-VALIDATION-SET');
                                    },
                                },
                                "create_rule": {
                                    "separator_before": false,
                                    "separator_after": false,
                                    "label": "New Validation Rule",
                                    "action": function (obj) {
                                        deselectTreeNode();
                                        createChild('POLICY-VALIDATION-RULE');
                                    },
                                },
                            };
                        } else
                        if (type==='POLICY-VALIDATION-RULE') {
                            createItemsArr = {
                                "create_rule": {
                                    "separator_before": false,
                                    "separator_after": false,
                                    "label": "New Validation Rule",
                                    "action": function (obj) {
                                        deselectTreeNode();
                                        createSibling('POLICY-VALIDATION-RULE');
                                    },
                                },
                            };
                        } else
                        {
                            createItemsArr = { };
                        }

                        var commonItemsArr = {
                            "expand_all": {
                                "separator_before": true,
                                "separator_after": false,
                                "label": "Expand all",
                                "action": function (obj) {
                                    expand_all();
                                }
                            },
                            "collapse_all": {
                                "separator_before": false,
                                "separator_after": false,
                                "label": "Collapse all",
                                "action": function (obj) {
                                    collapse_all();
                                }
                            },
                            "clear": {
                                "separator_before": true,
                                "separator_after": false,
                                "label": "Clear form",
                                "action": function (obj) {
                                    clearForm();
                                    deselectTreeNode();
                                }
                            },
                            "remove": {
                                "separator_before": true,
                                "separator_after": false,
                                "label": "Delete",
                                "_disabled": function (o) {
                                    if (o.reference.length) {
                                        var selNodeId = o.reference[0].parentNode.id;
                                        var selNode = $(LEFT_LIST_DOM_ID).jstree(true).get_node(selNodeId);
                                        if (selNode.original && selNode.original.readonly)
                                            return true;
                                    }
                                    return false;
                                },
                                "action": function (obj) {
                                    deleteNode();
                                    deselectTreeNode();
                                }
                            },
                        };

                        var ctxMenu = $.extend(createItemsArr, commonItemsArr);
                        return ctxMenu;
                    }
			    },
			    "types" : {
				    "#" : {
				        "max_children" : 1,
				        "valid_children" : ["root"]
				    },
				    "root" : {
				        "icon" : "/static/3.0.0/assets/images/tree_icon.png",
				        "valid_children" : ["default"]
				    },
				    "default" : {
				        "valid_children" : ["default","file"]
			        },
				    "file" : {
				        "icon" : "glyphicon glyphicon-file",
				        "valid_children" : []
				    }
			    },
			    "plugins" : [
				    "contextmenu", "dnd", //"search",
				    "state", "types", "wholerow", "hotkeys"//,
			        //"sort"
			    ]
			})
			.on('changed.jstree', function (e, data) {
				// select a property for PropertyIsA field
				if (selectTreeNode_State==='SELECT_NODE') {
					if (data && data.selected && data.selected.length && data.node && data.node.id) {
						// accept 'acceptedType' nodes and ignore the rest
						if (data.node.original.type===selectTreeNode_acceptedType || selectTreeNode_acceptedType==='*' || selectTreeNode_acceptedType==='ANY') {
							// update json form
							setFormFieldToNode(data.node['id'], data.node['text']);
						} else
						// if 'acceptedType'=='PARENT' accept CONCEPT nodes for concept, property and concept instance nodes, and CONCEPT-INSTANCE for property instance nodes
						if (selectTreeNode_acceptedType==='PARENT') {
							// get node type
							var type = $('input[name="type"]').val();
							if (/*type==='PROPERTY-INSTANCE' && data.node.original.type==='CONCEPT-INSTANCE' ||
								type!=='PROPERTY-INSTANCE' && */data.node.original.type==='ABAC-POLICY')
							{
								// update json form
								setFormFieldToNode(data.node['id'], data.node['text']);
							}
						} else {
							console.log('Not a property: '+data.node.id);
						}
					} else {
						console.log('ERROR: "data" or "data.selected" or "data.node" or "data.node.original" not defined');
					}
					return;
				}
				// ending node selection
				if (selectTreeNode_State==='IGNORE') {
					//tree.jstree("select_node", selectedNodeIdBefore);
					return;
				}

				// Update json form with selected node information
				// An AJAX call will be started
				$('input[name=id]').prop('readonly', true);
				if(data && data.selected && data.selected.length) {
					startTm = new Date().getTime();
					var url = baseUrl+'/gui/validation/'+data.selected;
					statusContacting(url, 'n/a');
					clearForm();
					callStartTm = new Date().getTime();
					$.get(url, function (d) {
					    //alert('changed.jstree: received data: '+JSON.stringify(d));
						if(d) {
						    var node = tree.jstree(true).get_node(d.id);
						    node.data = d;
						    if (d.type==='POLICY-VALIDATION-RULE') {
						        node.parent.data = d.parent;
						    }
							renderData(d, 'obviously OK');
							//console.log('NODE_CHANGED: server data: '+JSON.stringify(d));
                            //console.log('NODE_CHANGED: tree node after update: '+JSON.stringify(node));
							attrParent = d;

							// update expression builder
						    if (d.type==='POLICY-VALIDATION-RULE') {
							    setExpressionBuilderData(d.policyValidationExpression);
							}
						}
					});
				}
				else {
					$('#data .content').hide();
					$('#data .default').html('---').show();
				}
			})
			.on('loaded.jstree', function() {
				// check if tree view is empty
				/*if (tree.jstree(true).get_children_dom('#').length==0) {
					createValidationSet();
					//prepareCreateForm('POLICY-VALIDATION-SET');    // makes 'id' field writable (it seems some code makes field readonly after 'createSibling')
				}*/
			})
			.on('load_node.jstree', function(e, data) {
				// enable/disable type buttons
				setFormTypeButtonState(data.node);
			})
			.on('ready.jstree', function(e, data) {
				//console.log('CALL ready.jstree');
				showOrHideEmptyTreeMessage();
				fixTreeContainerHeight();
			})
			.on('refresh.jstree', function(e, data) {
				//console.log('CALL refresh.jstree');
				showOrHideEmptyTreeMessage();
				fixTreeContainerHeight();
			})
			.on('move_node.jstree', function(e, data) {
                //console.log('MOVE_NODE: node: ', data.node.id, data.node.text);
  				if (data.parent!==data.old_parent) {
				    //console.log('MOVE_NODE: from-policy="'+data.old_parent+'", to-policy="'+data.parent+'"');
				    setField('parent', data.parent);
				    changeParent(data.parent);

                    var parentNode = $(LEFT_LIST_DOM_ID).jstree('get_node', data.parent);
                    var originalOrder = parentNode.children;
				    if (confirm('Move "'+data.node.text+'" to "'+parentNode.text+'"?')) {
				        // update rule node data
				        var objectNode = tree.jstree(true).get_node(data.node.id);
				        objectNode.data.parent = parentNode.data;

				        // save rule changes
				        saveNode();
				        saveChildrenOrder(data.parent, originalOrder);
				    } else
				        reloadTree();
				} else {
				    //console.log('MOVE_NODE: reordering under same validation');
				    saveChildrenOrder(data.parent);
				}
			})
            ;

            // add slim scrollbar to jsTree
            fixTreeContainerHeight();

            // make readonly fields... readonly
            prepareEditForm();
			
            initTreeHotkeys();
		});
		
		function showOrHideEmptyTreeMessage() {
			// check if tree contains nodes
			var rootNode = tree.jstree(true).get_node('#');
			if (rootNode && rootNode.children) {
				if (rootNode.children.length==0) {
				    $('#empty_tree_mesg').show();
				    createPolicy();
				} else {
				    $('#empty_tree_mesg').hide();
				    var selArr = tree.jstree(true).get_selected();
				    if (selArr && selArr.length==0) {
				        var first = rootNode.children[0];
				        tree.jstree(true).select_node(first);
				    }
				}
			} else
				//console.log('ready.jstree: Could not get root node')
				;
		}

        function fixTreeContainerHeight() {
            // size jsTree container
            var diff = $('#treeContainer').offset().top;
            diff += $('#formButtons2').height();
            diff += $('#treeInstructions').height();
            diff += 50;
            $('#treeContainer').height(document.documentElement.clientHeight - diff);
		}
		
		/*
		 *	Hotkeys initialization
		 */
		function initTreeHotkeys() {
			var tree = $(LEFT_LIST_DOM_ID);	//jQuery(document);
			tree.bind('keydown', 'insert', function (evt) {
                    var tree = $(LEFT_LIST_DOM_ID).jstree();
                    var sel = selectedTreeNode(tree);
                    if (sel && sel!=null) {
                        var id = getField('id');
                        var type = tree.get_node(id).original.type;

                        if (type==='POLICY-VALIDATION-GROUP') { deselectTreeNode(); createChild('POLICY-VALIDATION-SET'); }
                        if (type==='POLICY-VALIDATION-SET') { deselectTreeNode(); createSibling('POLICY-VALIDATION-SET'); }
                        if (type==='POLICY-VALIDATION-RULE') { deselectTreeNode(); createSibling('POLICY-VALIDATION-RULE'); }
                    }
                    return false;
            });
			tree.bind('keydown', 'Shift+insert', function (evt) {
                    var tree = $(LEFT_LIST_DOM_ID).jstree();
                    var sel = selectedTreeNode(tree);
                    if (sel && sel!=null) {
                        var id = getField('id');
                        var type = getField('type');

                        if (type==='POLICY-VALIDATION-GROUP') { deselectTreeNode(); createChild('POLICY-VALIDATION-SET'); }
                        if (type==='POLICY-VALIDATION-SET') { deselectTreeNode(); createChild('POLICY-VALIDATION-SET'); }
                        if (type==='POLICY-VALIDATION-RULE') { deselectTreeNode(); createSibling('POLICY-VALIDATION-RULE'); }
                    }
                    return false;
			});
			tree.bind('keydown', 'del',function (evt){ deleteNode(); return false; });
		}
