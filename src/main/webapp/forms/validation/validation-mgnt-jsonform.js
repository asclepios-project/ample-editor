/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *	JSONform initialization
 */
	
/*
 * Main Form specification
 */
function initEditForm() {
	var options = getBaseEditFormOptions('node');
	options['onSubmitValid'] = function (values) {
								doSaveNode();
							};
	$('#formEdit').jsonForm(options);
	
	// add placeholders
	$('input[name="parent_display"').attr('placeholder', 'To select a Parent node, click here or on "Change" and select a node from the tree on the left');
/*
	$('input[name="parent_display"').attr('placeholder', 'To select a Parent node, click here or on "Change" and select a node from the tree on the left');
	$('input[name="range_display"').attr('placeholder', 'To select a Range, click here or on "Change" and select a concept from the tree on the left');
	$('input[name="propertyIsA_display"').attr('placeholder', 'To select a Property, click here or on "Change" and select a property from the tree on the left');
*/
}

/*
 * Buttons Form specification
 */
function initButtonsForm() {
	var options = {
		schema: {
		},
		"form": [
			{	"title": '<img src="/images/save-white-32x32.png" width="18" height="18"/> Save Changes &nbsp;&nbsp;',
				"type": "button",
				"htmlClass": "btn btn-transition btn-success",
				"onClick": function (evt) { saveNode(); }
			},
			{	"title": '<img src="/images/validation/validation-set-white-32x32.png" width="20" height="20"/> Create Validation Set &nbsp;&nbsp;',
				"type": "button",
				"htmlClass": "btn btn-transition btn-primary",
				"onClick": function (evt) { deselectTreeNode(); createValidationSet(); }
			},
			{	"title": '<img src="/images/validation/validation-rule-white-32x32.png" width="18" height="18"/> Create Validation Rule &nbsp;&nbsp;',
				"type": "button",
				"htmlClass": "btn btn-transition btn-primary",
				"onClick": function (evt) { deselectTreeNode(); createValidationRule(); }
			},
			/*{	"title": '<img src="/images/clear-18x18.png" width="20" height="20"/> Clear form &nbsp;&nbsp;',
				"type": "button",
				"htmlClass": "btn btn-transition btn-warning",
				"onClick": function (evt) { clearForm(); deselectTreeNode(); }
			},*/
			{	"title": '<img src="/images/delete-white-18x18.png" width="18" height="18"/> Delete Node &nbsp;&nbsp;',
				"type": "button",
				"htmlClass": "btn btn-transition btn-danger",
				"onClick": function (evt) { deleteNode(); deselectTreeNode(); }
			},
		],
	};
	
	$('#formButtons').jsonForm(options);
}

function initButtonsForm2() {
	$("#btn-export-validation-as-ttl").on('click', function(evt) {
        exportValidationObject('/gui/validation/export-validation-set-as-rdf/');
	});
	$("#btn-import-validation").on('click', function(evt) {
        if (confirm('Import page will be loaded. Any unsaved work will be lost.\nContinue?')) {
            document.location = '/forms/admin/import.jsp?mode=APPEND';
        }
	});
	$("#btn-validate-policies").on('click', function(evt) {
        if (confirm('Validate all policies using selected validation node and its children?')) {
            validatePolicies();
        }
	});
}

function exportValidationObject(baseUrl) {
    var id = getField('id');
    var type = getField('type');
    var parentId = getField('parent');
    //console.log('Export validation as RDF: id='+id+', type='+type+', parentId='+parentId);

    if (!type || type==null || type.trim()=='') return;
    if (type==='POLICY-VALIDATION-RULE') id = policyId
    if (!id || id==null || id=='') return;

    var url = baseUrl + id;
    //console.log('Export policy as RDF: Contacting '+url);
    document.location = url;
}

/*
 * Form preparation (before assigning data)
 */
function prepareForm(data) {
	setFormByNodeType(data.type, data.propertyType, data.range_display, data.propertyIsA_display);

	// enable/disable form buttons
	var type = data.type;
	var readonly = (data && data.readonly);
	if (type==='POLICY-VALIDATION-GROUP') {
		setFormButtonStates(true, false, true, false, !readonly, true, !readonly);
	} else
	if (type==='POLICY-VALIDATION-SET') {
		setFormButtonStates(true, true, true, false, !readonly, true, !readonly);
	} else
	if (type==='POLICY-VALIDATION-RULE') {
		setFormButtonStates(false, true, false, false, !readonly, true, !readonly);
	}
	
	// enable/disable type buttons
	setFormTypeButtonState(data);
}

function setFormTitle(type, breadcrumb, isCreate) {
    // display the right form type
    $('#form-title-group').hide();
    $('#form-title-set').hide();
    $('#form-title-rule').hide();
    if (type==='POLICY-VALIDATION-GROUP') $('#form-title-group').show();
    if (type==='POLICY-VALIDATION-SET') $('#form-title-set').show();
    if (type==='POLICY-VALIDATION-RULE')   $('#form-title-rule').show();

    // display the breadcrumb
    if (breadcrumb!=='') {
        $('#form-title-breadcrumb').text(breadcrumb);
    } else {
        if (isCreate) {
            if (type==='POLICY-VALIDATION-GROUP') $('#form-title-breadcrumb').html('<i>&lt;&lt; New Validation Group &gt;&gt;</i>');
            if (type==='POLICY-VALIDATION-SET') $('#form-title-breadcrumb').html('<i>&lt;&lt; New Validation Set &gt;&gt;</i>');
            if (type==='POLICY-VALIDATION-RULE') $('#form-title-breadcrumb').html('<i>&lt;&lt; New Validation Rule &gt;&gt;</i>');
        } else {
            $('#form-title-breadcrumb').html($("#name").val());
        }
    }
}

function correctFieldDimensions() {
    // resize 'description' textarea to align to other fields
    var nameFieldWidth =
            $("input[name=name]").outerWidth()
            + $("input[name=name]").parent().children().last().outerWidth();
    $("textarea[name=description]").outerWidth(nameFieldWidth);

    // resize 'policyValidationOutcome' input to align end to other fields
    var outcomeInputWidth = $("input[name=policyValidationOutcome]").outerWidth();
    var outcomeFieldWidth =
            outcomeInputWidth
            + $("input[name=policyValidationOutcome]").parent().children().last().outerWidth();
    var diff = outcomeFieldWidth - nameFieldWidth;
    $("input[name=policyValidationOutcome]").outerWidth(outcomeInputWidth - diff);

    // resize '.toggle-control' toggles to fit in their lines
    $('.toggle-control').each(function () {
        var h = $(this).closest('.add-on').outerWidth(nameFieldWidth);
    })
}

/*$(document).ready(function() {    // Not working...
    correctFieldDimensions();
});*/
$(window).on('resize', function() {
    correctFieldDimensions();
});


function setFormButtonStates(vset, vrul, exp, xxx, save, clear, del) {
	$('#formButtons').find('button:contains("Create Validation Set")').prop( "disabled", !vset ).addClass( vset?'':'grayscale' ).removeClass( vset?'grayscale':'' );
	$('#formButtons').find('button:contains("Create Validation Rule")').prop( "disabled", !vrul ).addClass( vrul?'':'grayscale' ).removeClass( vrul?'grayscale':'' );
	//
	$('#formButtons2').find('button:contains("Export ")').prop( "disabled", !exp ).addClass( exp?'':'grayscale' ).removeClass( exp?'grayscale':'' );
	//$('#formButtons2').find('button:contains("Apply ")').prop( "disabled", !exp ).addClass( exp?'':'grayscale' ).removeClass( exp?'grayscale':'' );
	//
	$('#formButtons').find('button:contains("Save")').prop( "disabled", !save ).addClass( save?'':'grayscale' ).removeClass( save?'grayscale':'' );
	$('#formButtons').find('button:contains("Clear")').prop( "disabled", !clear ).addClass( clear?'':'grayscale' ).removeClass( clear?'grayscale':'' );
	$('#formButtons').find('button:contains("Delete")').prop( "disabled", !del ).addClass( del?'':'grayscale' ).removeClass( del?'grayscale':'' );
}

function setFormTypeButtonState(data) {
	var is_loaded = $(LEFT_LIST_DOM_ID).jstree(true).is_loaded(data.id);
	var is_leaf = $(LEFT_LIST_DOM_ID).jstree(true).is_leaf(data.id);
	var enable = (is_loaded && is_leaf);
	
	enableFormTypeButtons(enable, enable, enable/*, false*/);
}
	
function enableFormTypeButtons(cpol,crul,dummy1,dummy2) {
	$('#btn-set-type-policy-validation-set') .prop( "disabled", !cpol ).addClass( cpol?'':'grayscale' ).removeClass( cpol?'grayscale':'' );
	$('#btn-set-type-policy-validation-rule').prop( "disabled", !crul ).addClass( crul?'':'grayscale' ).removeClass( crul?'grayscale':'' );
	//$('#btn-set-type-instance').prop( "disabled", !dummy1 ).addClass( dummy1?'':'grayscale' ).removeClass( dummy1?'grayscale':'' );
	//$('#btn-set-type-propinst').prop( "disabled", !dummy2 ).addClass( dummy2?'':'grayscale' ).removeClass( dummy2?'grayscale':'' );
}
