/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *	Utility functions and variables common to all pages
 */

// ================================================================================================
// Various utility functions and variables (loading indicator and row renumbering)

var loadingIndicator = null;

function showIndicator(mesg, elem) {
	if (!loadingIndicator) {
		loadingIndicator = $("<span class='loading-indicator'><label>"+mesg+"</label></span>").appendTo(document.body);
		if (!elem) elem = document.body;
		var $g = $(elem);
		loadingIndicator
			.css("position", "absolute")
			.css("top", $g.position().top + $g.height() / 2 - loadingIndicator.height() / 2)
			.css("left", $g.position().left + $g.width() / 2 - loadingIndicator.width() / 2);
	} else {
		$('.loading-indicator').html('<label>'+mesg+'</label>');
	}
	loadingIndicator.show();
}

function hideIndicator() {
    if (!loadingIndicator) return;
    if (loadingIndicator==null) return;
	loadingIndicator.fadeOut();
	loadingIndicator.hide();
}

var statusSelector = '#res';

// Status functions
function setStatus(mesg) {
    window.status = mesg;
}

function statusContacting(url, valStr) {
}

// Clipboard actions
function initClipboardAction(selector, textFunction, containerId, toastSuccessMesg) {
    var opts = { text: textFunction };
    if (containerId && typeof containerId==='string' && containerId.trim()!=='')
        opts["container"] = document.getElementById(containerId.trim());
    var clipboard = new ClipboardJS(selector, opts);
    if (toastSuccessMesg) {
        clipboard.on('success', function(e) {
            //console.info('Action:', e.action);
            //console.info('Text:', e.text);
            //console.info('Trigger:', e.trigger);

            if (toastr) {
                //if (!toastSuccessMesg) toastSuccessMesg = 'Validation results copied to clipboard';
                toastr.info(toastSuccessMesg, null, {
                    closeButton: true,
                    positionClass: "toast-bottom-center",
                    timeOut: 5000,
                    progressBar: true,
                    preventDuplicates: true,
                });
            } else
                alert(toastSuccessMesg);

            e.clearSelection();
        });
    }
    clipboard.on('error', function(e) {
        //console.error('Action:', e.action);
        //console.error('Trigger:', e.trigger);

        if (toastr) {
            toastr.error('Failed to copy validation results to clipboard', null, {
                closeButton: true,
                positionClass: "toast-bottom-center",
                timeOut: 0,
                //extendedTimeOut: 5000,
            });
        } else
            alert('Failed to copy validation results to clipboard');
    });

    return clipboard;
}

// Toasts
function showToast(type, message, title, timeout) {
    if (toastr) {
        if (!title) title = null;
        else title = `<div style="font-weight: bold; margin-bottom: 10px; border-bottom: 1px solid white;">${title}</div>`;
        if (!timeout) timeout = 5000;
        var options = {
                          closeButton: true,
                          positionClass: "toast-bottom-center",
                          timeOut: timeout,
                          progressBar: true,
                      };
        switch (type.toLowerCase()) {
            case 'success':
                toastr.success(message, title, options);
                break;
            case 'warning':
                if (timeout<3000) options.timeOut = 3000;
                options.positionClass = "toast-top-center";
                toastr.warning(message, title, options);
                break;
            case 'error':
                options.timeOut = 0;
                options.positionClass = "toast-bottom-full-width";
                toastr.error(message, title, options);
                break;
            case 'info':
            default:
                toastr.info(message, title, options);
        }
    } else
        alert(message);
}

function hideToasts() {
    if (toastr) {
        $('.toast').remove();
    }
}

function showErrorMessage(errorText, errorObject) {
    hideIndicator();

    var message = `<b>Status:</b> ${errorText}<br/>`+
                   (errorObject ? '<b>Object:</b> '+JSON.stringify(errorObject) : '');
    setStatus('Status: '+errorText);
    //alert(message);
    showToast('error', message, 'Error');
}

/*function escapeHtml(html){
    var text = document.createTextNode(html);
    var p = document.createElement('p');
    p.appendChild(text);
    return p.innerHTML;
}*/

//EOF