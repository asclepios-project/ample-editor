<!DOCTYPE html>
<!--
  ~
  ~ Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~      http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  ~
  -->
<%@ include file="../../includes/prelude.html" %>
<html>
  <head>
<% pageTitle = "ASCLEPIOS Context-Aware Security Model"; %>
<%@ include file="../../includes/head.html" %>
	<link rel="stylesheet" href="../slick/common-grid-styles.css" type="text/css"/>
	<script src="dropzone.js"></script>
	<link rel="stylesheet" href="dropzone.css">

	<script>
		Dropzone.options.ttlUploadDropzone = {
			paramName: "file", // The name that will be used to transfer the file
			//params: { "import_mode": "append" },
			//maxFilesize: 2, // MB
			//acceptedFiles: "text/turtle,.ttl",
			accept: function(file, done) {
				if (!file.name.toLowerCase().endsWith(".ttl")) {
					if (confirm("File "+file.name+" doesn't look like a TTL file.\nContinue with upload?")) { done(); }
					else { done("User cancelled upload"); this.removeFile(file); }
				}
				else { done(); }
			},
			uploadMultiple: false,
			addRemoveLinks: true,
			clickable: true,
			dictCancelUpload: "cancel",
			//dictCancelUploadConfirmation: "Are you sure?",
			dictRemoveFile: "remove",
			/*sending: function(file, xhr, formData){
                    formData.append('import_mode', 'append');
                }
            }*/
		};
	</script>
	
  </head>
  <body>
<%@ include file="../../includes/header.html" %>
<%@ include file="../../includes/js-libs.html" %>
	
	<div style="width:80%; margin-left:auto; margin-right:auto;">
		<form action="/gui/admin/importRdf" method="POST" enctype="multipart/form-data" accept-charset="UTF-8" class="dropzone" id="ttl-upload-dropzone">
    	    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
			<p class="dz-message" style="color:green; background:rgb(250,250,250); padding:5px; font-size:larger; font-style:italic;">Import ASCLEPIOS Policies and Context-Aware Security Model</p>
			<p class="dz-message" style="margin-top: 35px; margin-bottom: 40px;">
			    Indicate whether the file to upload will replace any existing contents or append to them
			    <br/>
<%
    String appendChecked = "";
    String replaceChecked = "";
    String mode = request.getParameter("mode");
    if (mode!=null && !(mode=mode.trim()).isEmpty()) {
        if ("append".equalsIgnoreCase(mode)) appendChecked = "checked=\"checked\" ";
        if ("replace".equalsIgnoreCase(mode)) replaceChecked = "checked=\"checked\" ";
        if (!appendChecked.isEmpty() || !replaceChecked.isEmpty()) {
            appendChecked += "disabled ";
            replaceChecked += "disabled ";
        } else {
            appendChecked = "checked=\"checked\" ";
        }
    } else {
        appendChecked = "checked=\"checked\" ";
    }
%>
		        <input type="radio" name="import_mode" value="append" <%=appendChecked%>/>&nbsp;Append
		        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		        <input type="radio" name="import_mode" value="replace" <%=replaceChecked%>/>&nbsp;Replace
		    </p>
			<p class="dz-message" style="color:blue; margin-top: 35px; margin-bottom: 40px;">Drag and drop an RDF/TTL file in this area...</p>
			<div class="fallback">
			    <p>Indicate whether the file to upload will replace any existing contents or append to them
		        <input type="text" name="import_mode" value="append" /> Valid values: append, replace
				<br/>
				<p><input name="file" type="file" /></p>
				<br/>
				<p><input type="submit" value="Upload file" /></p>
			</div>
		</form>
	</div>

	<%@ include file="../../includes/footer.html" %>
  </body>
</html>
<%@ include file="../../includes/debug.html" %>
<%@ include file="../../includes/trail.html" %>