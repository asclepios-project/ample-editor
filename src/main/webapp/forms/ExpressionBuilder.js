/*
 *
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

var QUERY_BUILDER_CONTAINER;
var QUERY_BUILDER_NOT_GROUP_ON = true;
var QUERY_BUILDER_ABE_OPERATORS = false;
var QUERY_BUILDER_SELECT2_SEARCH_FOR_ATTRIBUTES_URL = '/gui/attributes/search/attributes';
var QUERY_BUILDER_SELECT2_SEARCH_FOR_PROPERTIES_URL = '/gui/attributes/search/properties';

function get_attributes_select2_options() {
    return {
        language: { searching: function () { return 'Searching...'; } },
        placeholder: 'Type an attribute name',
        width: 200,
        allowClear: true,
        minimumInputLength: 3,
        ajax: {
            url: QUERY_BUILDER_SELECT2_SEARCH_FOR_ATTRIBUTES_URL,
            dataType: 'json',
            // Additional AJAX parameters go here
            delay: 250,
        },
    };
}

function get_properties_select2_options() {
    return {
        language: { searching: function () { return 'Searching...'; } },
        placeholder: 'Type a property name',
        width: 200,
        allowClear: true,
        disabled: true,
        //minimumInputLength: 0,
        ajax: {
            url: QUERY_BUILDER_SELECT2_SEARCH_FOR_PROPERTIES_URL,
            dataType: 'json',
            // Additional AJAX parameters go here
            delay: 250,
            data: function(params) {
                // get selected attribute id
                var propId = $(this).attr("id");
                var attrSel = $(this).parent().prev().children().first().next();
                var attrId = attrSel.val();
                var attrText = attrSel.text();

                // modify query string
                var query = {
                    attributeId: attrId
                }
                return query;
            },
            processResults: function (data) {
                var resultsNew = [];
                var groupText = '';
                var groupItems = [];
                data.results.forEach(item => {
                    //console.log('select2: processResults: item: '+JSON.stringify(item));
                    if (item.parentAttribute && item.parentAttribute!==groupText) {
                        if (groupText!=="" && groupItems.length>0) {
                            resultsNew.push({
                                text: groupText,
                                children: groupItems
                            });
                        }
                        groupText = item.parentAttribute;
                        groupItems = [];
                    }
                    groupItems.push(item);
                });
                if (groupItems.length>0) {
                    resultsNew.push({
                        text: groupText,
                        children: groupItems
                    });
                }
                //console.log('select2: processResults: ', resultsNew);
                return {
                    results: resultsNew
                };
            }
        }
    };
}

function get_data_type_from_xsd_type(dataType) {
    var xsdType = dataType;
    var p1 = xsdType.lastIndexOf(':');
    var p2 = xsdType.lastIndexOf('#');
    var p = Math.max(p1, p2);
    if (p>=0) xsdType = xsdType.substring(p+1);
    return xsdType.toLowerCase();
}

function get_operators_for_data_type(dataType) {
    if (QUERY_BUILDER_ABE_OPERATORS)
        return get_abe_operators_for_data_type(dataType);

    switch (dataType) {
        case 'string':
                return {'EQUALS':'=', 'NOT_EQUALS':'&ne;',
                        'LESS_THAN':'&lt;', 'LESS_OR_EQUAL':'&le;',
                        'GREATER_THAN':'&gt;', 'GREATER_OR_EQUAL':'&ge;',
                        'EQUALS_CI':'equals case insensitive', 'NOT_EQUALS_CI':'not equals case insensitive',
                        'STARTS_WITH':'starts with', 'ENDS_WITH':'ends with',
                        'STARTS_WITH_CI':'starts with case insensitive', 'ENDS_WITH_CI':'ends with case insensitive',
                        /*'IS_EMPTY':'is empty', 'IS_NOT_EMPTY':'is not empty',
                        'IS_BLANK':'is blank', 'IS_NOT_BLANK':'is not blank',*/
                        'CONTAINS':'contains', 'NOT_CONTAINS':'not contains',
                        'CONTAINS_CI':'contains case insensitive', 'NOT_CONTAINS_CI':'not contains case insensitive' };
        case 'byte': case 'short': case 'int': case 'integer': case 'long':
        case 'positiveinteger': case 'nonnegativeinteger': case 'negativeinteger': case 'nonpositiveinteger':
        case 'decimal': case 'float': case 'double':
        case 'unsignedbyte': case 'unsignedshort': case 'unsignedint': case 'unsignedlong':
        case 'date': case 'time': case 'datetime':
                return {'EQUALS':'=', 'NOT_EQUALS':'&ne;',
                        'LESS_THAN':'&lt;', 'LESS_OR_EQUAL':'&le;',
                        'GREATER_THAN':'&gt;', 'GREATER_OR_EQUAL':'&ge;' };
        case 'boolean':
                return { 'EQUALS':'=', 'NOT_EQUALS':'&ne;' };
        case 'instances':
                return { 'EQUALS':'=', 'NOT_EQUALS':'&ne;' };
        default:
                return { 'EQUALS':'=', 'NOT_EQUALS':'&ne;' };
    }
}

function get_abe_operators_for_data_type(dataType) {
    switch (dataType) {
        case 'string':
                return {'EQUALS':'=', 'NOT_EQUALS':'&ne;',
                        'LESS_THAN':'&lt;', 'LESS_OR_EQUAL':'&le;',
                        'GREATER_THAN':'&gt;', 'GREATER_OR_EQUAL':'&ge;' };
        case 'byte': case 'short': case 'int': case 'integer': case 'long':
        case 'positiveinteger': case 'nonnegativeinteger': case 'negativeinteger': case 'nonpositiveinteger':
        case 'decimal': case 'float': case 'double':
        case 'unsignedbyte': case 'unsignedshort': case 'unsignedint': case 'unsignedlong':
        case 'date': case 'time': case 'datetime':
                return {'EQUALS':'=', 'NOT_EQUALS':'&ne;',
                        'LESS_THAN':'&lt;', 'LESS_OR_EQUAL':'&le;',
                        'GREATER_THAN':'&gt;', 'GREATER_OR_EQUAL':'&ge;' };
        case 'boolean':
                return { 'EQUALS':'=', 'NOT_EQUALS':'&ne;' };
        case 'instances':
                return { 'EQUALS':'=', 'NOT_EQUALS':'&ne;' };
        default:
                return { 'EQUALS':'=', 'NOT_EQUALS':'&ne;' };
    }
}

function get_options_for_data_type(dataType) {
    html = '';
    for (let [val, text] of Object.entries(get_operators_for_data_type(dataType))) {
        //val = String(val).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
        html += `<option value="${val}">${text}</option>\n`;
    }
    return html;
}

function addExtraButtons(rule) {
    // Add 'duplicate rule' control near 'Delete rule' button
    var duplicateRuleControl = $('<button type="button" class="btn btn-xs btn-info"><i class="fa fa-clone"></i></button>');
    rule.$el.children('.rule-header').children().last().prepend(duplicateRuleControl);
    duplicateRuleControl.on('click', function (e) {
        cloneRule(rule);
    });
}

function cloneRule(rule) {
    // get parent group
    var parentGroup = rule.parent;
    var positionInParent = rule.getPos();

    // create and setup new rule
    var newRule = $(QUERY_BUILDER_CONTAINER).queryBuilder('addRule', parentGroup, ruleData);
    newRule.move(parentGroup, positionInParent+1);

    // get source rule data
    var ruleData = (rule.__.data) ? JSON.parse(JSON.stringify( rule.__.data )) : {};
    newRule.__.data = ruleData;

    // setup new rule attribute
    copyStateOfSelect2(rule, newRule, '.rule-filter-container .eb-attribute');

    // setup new rule property
    copyStateOfSelect2(rule, newRule, '.rule-operator-container .eb-property');

    // setup new rule operator
    var srcOp = rule.$el.find('.rule-operator-container .eb-property-op').first();
    var destOp = newRule.$el.find('.rule-operator-container .eb-property-op').first();
    destOp.html( srcOp.html() );
    destOp.width( srcOp.width() );
    copyStateOfSelect2(rule, newRule, '.rule-operator-container .eb-property-op');

    // setup new rule value
    //updateValueControl(rule.__.data, newRule.$el.find('.rule-value-container'));
    var isValueControlEmpty = rule.$el.find('.rule-value-container').html().trim()==='';
    if (isValueControlEmpty) {
        newRule.$el.find('.rule-value-container').html('');
    } else {
        setupRuleValue(newRule);

        var valueControl = newRule.$el.find('.rule-value-container .form-control').first();
        var valueControlType = valueControl.prop('type');
        switch (valueControlType) {
            case 'select-one':
                var selectedOption = rule.$el.find('.rule-value-container option:selected');
                var selectedValue = selectedOption.val();
                var selectedText = selectedOption.text();
                valueControl.val(selectedValue).trigger('change');
                break;
            case 'radio':
                var checkedRadio = rule.$el.find('.rule-value-container input[type="radio"]:checked');
                var checkedValue = checkedRadio.val();
                var newCheckedRadio = newRule.$el.find('.rule-value-container input[type="radio"]:checked');
                var newCheckedValue = newCheckedRadio.val();
                if (checkedValue!==newCheckedValue) {
                    newCheckedRadio.removeAttr('checked');
                    var newerCheckedRadio = newRule.$el.find(`.rule-value-container input[value="${checkedValue}"]`);
                    newerCheckedRadio.prop('checked', true);
                }
                break;
            default:
                var ruleValue = rule.$el.find('.rule-value-container .form-control').first().val();
                valueControl.val(ruleValue);
        }
        valueControl.width(valueControl.width());
    }
}

function copyStateOfSelect2(srcRule, destRule, controlClass) {
    // copy select options, values and texts
    var srcSel = srcRule.$el.find(controlClass).first();
    var srcOpt = srcSel.find('option:selected').first();
    var srcId = srcOpt.val();
    var srcText = srcOpt.text();

    var destSel = destRule.$el.find(controlClass).first();
    var destOpt = destSel.find('option[value="'+srcId+'"]').first();
    if (destOpt.length>0) {
        destSel.val(srcId);
    } else {
        var newOpt = new Option(srcText, srcId, true, true);
        destSel.append(newOpt);
    }

    // copy disabled/readonly/ and show/hidden states
    destSel.prop('disabled', srcSel.prop('disabled'));
    destSel.prop('readonly', srcSel.prop('readonly'));
    if (srcSel.is(":visible")) destSel.show(); else destSel.hide();

    // copy width
    destSel.width( srcSel.width() );

    return { 'src':srcSel, 'dest':destSel };
}

// Attributes_plugin definition
$.fn.queryBuilder.define('attributes_plugin', function(options) {
    /* "this" is the QueryBuilder instance */

    this.on('afterAddGroup', function (group) {
        // add extra buttons (i.e. clone) to new groups too (EXCEPT the top-level one)
    });

    this.on('afterCreateRuleFilters', function(event, rule) {
        // update rule buttons (if needed)
        addExtraButtons(rule);

        // setup attribute controls (select2)
        setupRuleAttribute(rule);
    });

    this.on('afterCreateRuleOperators', function(event, rule) {
        setupRulePropertyAndOperator(rule);
    });

    this.on('afterCreateRuleInput', function(e, rule) {
        setupRuleValue(rule);
    });
});

/*function updateRuleData(el, data) {
    if (arguments.length>2)
        console.log(arguments[2], data);
    if (! (el instanceof jQuery)) el = $(el);
    var ruleEl = el.closest('.rule-container');
    var ruleModel = $(QUERY_BUILDER_CONTAINER).queryBuilder('getModel', ruleEl);
    var ruleData = ruleModel.__.data;
    if (!ruleData) ruleData = {};
    for (let [key, val] of Object.entries(data)) {
        ruleData[key] = val;
    }
}*/

function setupRuleAttribute(rule) {
    var ruleFilterContainer = rule.$el.find('.rule-filter-container');
    //var filterSelect = rule.$el.find('.rule-filter-container [name*=_filter]');

    if (ruleFilterContainer.children().length < 3)
    {
        // create 'select2' control for 'attribute'
        var target = ruleFilterContainer.children().last();
        target.select2( get_attributes_select2_options() );

        // add event listeners
        target.on("select2:select", function(e) {
            // enable property 'select2'
            var attrSel = $(e.target).parent().next().children().first().next();
            attrSel.prop('disabled', false);
            attrSel.prop('readonly', false);

            // enable operators 'select' and set options
            setupRulePropertyAndOperator(rule);
        });
        target.on("select2:clear", function(e) {
            // disable property 'select2'
            var propertySelect2 = $(e.target).parent().next().children().first().next();
            propertySelect2.prop('disabled', true);
            propertySelect2.val(null).trigger('change');

            // disable operator 'select'
            var operatorSelect = $(e.target).parent().next().children().last();
            operatorSelect.html('');
            operatorSelect.prop('disabled', true);
            operatorSelect.val(null).trigger('change');

            // clear value controls
            var ruleValueContainer = $(e.target).parent().next().next();
            ruleValueContainer.html('');
        });
        /*target.on("change.select2", function(e) {
            // clear property 'select2'
            var operatorSelect2 = $(e.target).parent().next().children().first().next();
            operatorSelect2.val(null).trigger('change');

            // clear value controls
            var ruleValueContainer = $(e.target).parent().next().next();
            ruleValueContainer.html('');
        });*/

        // add preselected option
        if (rule.data && rule.data.attribute && rule.data.attribute!=null && rule.data.attribute!=='') {
            // add new option in attribute 'select2'
            var option = new Option(rule.data.attributeText, rule.data.attribute, true, true);
            target.append(option).trigger('change');
            target.trigger({
                type: 'select2:select',
                params: { data: rule.data }
            });
        }
    }
}

function setupRulePropertyAndOperator(rule) {
    var ruleOperatorContainer = rule.$el.find('.rule-operator-container');

    // create 'select' control for 'operator' and its set options
    var targetOp = updateOperatorControl(rule.data, ruleOperatorContainer);

    if (ruleOperatorContainer.children().length < 4)
    {
        // create 'select2' control for 'property'
        var target = ruleOperatorContainer.children().last().prev();
        target.select2( get_properties_select2_options() );

        // add event listeners
        target.on("select2:select", function(e) {
            // retrieve and save current input control values

            // change operator control based on property data
            if (e.params && e.params.data && typeof(e.params.data.dataProperty)!='undefined') {
                var ruleOperatorContainer = $(e.target).parent();
                var targetOp = updateOperatorControl(e.params.data, ruleOperatorContainer);
            } else
                ; //alert('EXPRESSION BUILDER: INTERNAL ERROR [0]');

            // change value input control based on property data
            var ruleValueContainer = $(e.target).parent().next();

            if (e.params && e.params.data && e.params.data.dataProperty!=undefined && ruleValueContainer) {
                updateValueControl(e.params.data, ruleValueContainer);
            } else
                ; //alert('EXPRESSION BUILDER: INTERNAL ERROR [1]');

            // restore previously saved values to new input controls
        });
        target.on("select2:clear", function(e) {
            // retrieve and save current input control values

            // clear operator control
            var ruleOperatorContainer = $(e.target).parent();
            updateOperatorControl(null, ruleOperatorContainer);

            // clear value input controls
            var ruleValueContainer = $(e.target).parent().next();
            ruleValueContainer.html('');
        });

        // add preselected option
        if (rule.data && rule.data.property && rule.data.property!=null && rule.data.property!=='') {
            var option = new Option(rule.data.propertyText, rule.data.property, true, true);
            target.append(option).trigger('change');
            target.trigger({
                type: 'select2:select',
                params: { data: rule.data }
            });
        }

        // enable if 'attribute' is present
        if (rule.data && rule.data.attribute && rule.data.attribute!=null && rule.data.attribute!=='') {
            target.prop('disabled', false);
            targetOp.prop('disabled', false);
        }
    }
}

function setupRuleValue(rule) {
    var ruleValueContainer = rule.$el.find('.rule-value-container');
    if (rule.__.data && ! $.isEmptyObject(rule.__.data)) {
        updateValueControl(rule.__.data, ruleValueContainer);
    } else {
        ruleValueContainer.html('');
    }
}

function updateOperatorControl(data, ruleOperatorContainer) {
    var options = '';
    if (data!=null) {
        var dataType = 'string';
        if (typeof data.dataProperty !== 'undefined' && data.dataProperty !== null) {
            if (data.dataProperty===true && data.rangeType && data.rangeType!=null && data.rangeType.trim()!=='') {
                dataType = get_data_type_from_xsd_type( data.rangeType.trim() );
            } else
            if (data.dataProperty===false) {
                if (data.instances && Array.isArray(data.instances) && data.instances.length>0) {
                    dataType = 'instances';
                }
            }
        }
        options = get_options_for_data_type(dataType);
    }

    var targetOp = ruleOperatorContainer.children().last();
    targetOp.html( options );

    if (data && data!=null && data.operator) {
        targetOp.val(data.operator);
    }
    updateOperatorControlWidth(targetOp);

    targetOp.on('change', function(e) {
        // adjust operator control width
        var targetOp = $(e.target);
        updateOperatorControlWidth(targetOp);

        // enable operator control
        targetOp.prop('disabled', false);
        targetOp.prop('readonly', false);
    });

    return targetOp;
}

function updateOperatorControlWidth(operatorElem) {
    // get selected option
    var optionSelected = operatorElem.children("option:selected");

    // estimate the required option width
    var tempId = 'TEMP-'+(new Date().getTime());
    var tempElem = $(`<span id="${tempId}" style="background: red; white-space: nowrap;" />`);
    tempElem.append(optionSelected.text());
    tempElem.appendTo(document.body);
    var width = tempElem.width() * 1.03 + 20;
    width += ( optionSelected.outerWidth(true)-optionSelected.width() );
    tempElem.remove();

    // adjust control width
    operatorElem.width(width);
}

function updateValueControl(data, ruleValueContainer) {
	if (!data) throw "updateValueControl: Invalid argument 'data'";
	if (!ruleValueContainer) throw "updateValueControl: Invalid argument 'ruleValueContainer'";
	if (typeof(data.dataProperty)=='undefined') throw "updateValueControl: 'data' argument does not contain 'dataProperty'";

	var inputs = ruleValueContainer.children().filter('[name*=_value_]');
	var inputName = inputs.prop('name');
	if (inputName==undefined) {
        var propertyName = ruleValueContainer.prev().children().first().prop('name');
        if (! propertyName.endsWith('_operator'))
            throw 'updateValueControl: INTERNAL ERROR: Invalid property select name: '+propertyName;
        inputName = propertyName.substring(0, propertyName.length - '_operator'.length) + '_value_0';
	}

	var newInpHtml = '';
	var useSelect2 = false;
	var isDate = false;
	var isTime = false;
	var isDateTime = false;
	//console.log(`datatProp: ${data.dataProperty}, range-type: ${data.rangeType}, -id: ${data.rangeId}, instances: ${data.instances}`);

	// Prepare the HTML code for the new value control, based on property type (Data/Object), range type (XSD), having instances
	if (data.dataProperty===true) {
		// Input for a Data Property
		var xsdType = get_data_type_from_xsd_type(data.rangeType);
		switch (xsdType) {
			case 'string':
					newInpHtml = `<input class="form-control" type="text" name="${inputName}">`;
					break;
			case 'byte': case 'short': case 'int': case 'integer': case 'long':
			case 'positiveinteger': case 'nonnegativeinteger': case 'negativeinteger': case 'nonpositiveinteger':
			case 'decimal': case 'float': case 'double':
			case 'unsignedbyte': case 'unsignedshort': case 'unsignedint': case 'unsignedlong':
					newInpHtml = `<input class="form-control" type="number" name="${inputName}">`;
					break;
			case 'date':
			        isDate = true;
					newInpHtml = `<input class="form-control" type="text" name="${inputName}">`;
					break;
			case 'time':
			        isTime = true;
					newInpHtml = `<input class="form-control" type="text" name="${inputName}">`;
					break;
			case 'datetime':
			        isDateTime = true;
					newInpHtml = `<input class="form-control" type="text" name="${inputName}">`;
					break;
			case 'boolean':
			        var checkedTrue = (data.value==='true' ? ' checked' : '');
			        var checkedFalse = (data.value==='false' ? ' checked' : '');
					newInpHtml =
							`<label><input id="${inputName}_0" class="form-control" type="radio" name="${inputName}__" value="true"${checkedTrue}> True</label> &nbsp; ` +
							`<label><input id="${inputName}_1" class="form-control" type="radio" name="${inputName}__" value="false"${checkedFalse}> False</label>`;
					break;
			default:
					throw `updateValueControl: XSD type not supported: ${data.rangeType} [${xsdType}]`;
		}
	} else {
		// Input for an Object Property
		if (data.instances && data.instances.length && data.instances.length>0) {
			// Select2 control for Object Type instances
			newInpHtml = `<select class="form-control" name="${inputName}">`;
			data.instances.forEach(function(val, index, arr) {
				newInpHtml += `<option value="${val.id}">${val.name}</option>`;
			});
			newInpHtml += '</select>';
			useSelect2 = true;
		} else
		{
			// URL input for instances of an Object Type without instances
			var inpVal = '';
			if (data.rangeType) inpVal = data.rangeType;
			newInpHtml = `<input class="form-control" type="url" name="${inputName}">`;
		}
	}
	//console.log(newInpHtml);

	if (newInpHtml=='') throw "updateValueControl: INTERNAL ERROR: No HTML code generated for new value control";

	// Replace existing value control with the new HTML code
	var newInp = $(newInpHtml);
	ruleValueContainer.html(newInp);
	if (useSelect2) {
		newInp.select2({
			placeholder: 'Type a value',
			width: 200,
			allowClear: true,
		});
	}
	if (isDate || isTime || isDateTime) {
	    var options = {
            datepicker: isDate || isDateTime,
            timepicker: isTime || isDateTime,
            step: 5,
            mask: false,
            todayButton: true,
            prevButton: true,
            nextButton: true,
            showApplyButton: false,
            onClose: function (current_time, $input) {
                current_time.setSeconds(0);
                current_time.setMilliseconds(0);
                if (isTime) $input.val(current_time.toLocaleTimeString('it-IT'));
                if (isDateTime) $input.val(""
                    + current_time.getFullYear()
                    + "-" + ("0"+(current_time.getMonth()+1)).slice(-2)
                    + "-" + ("0" + current_time.getDate()).slice(-2)
                    + "T" + ("0" + current_time.getHours()).slice(-2)
                    + ":" + ("0" + current_time.getMinutes()).slice(-2)
                    + ":" + ("0" + current_time.getSeconds()).slice(-2)
                    //+ "." + ("00" + current_time.getMilliseconds()).slice(-3)
                    //+ (-current_time.getTimezoneOffset()>=0 ? "+" : "-")
                    //+ ("0" + Math.abs(Math.floor(-current_time.getTimezoneOffset()/60))).slice(-2)
                    //+ ":"
                    //+ ("0" + (Math.abs(-current_time.getTimezoneOffset()) - 60*Math.abs(Math.floor(-current_time.getTimezoneOffset()/60)))).slice(-2)
                );
            }
        };
        if (isDateTime) options['format'] = 'Y-m-d\\TH:i:s';
        if (isDate) options['format'] = 'Y-m-d';
        if (isTime) options['format'] = 'H:i:s';
	    newInp.datetimepicker(options);
	}

	$(newInp).on('change', function(e) {
	    var newValue = this.value;
	    if (typeof(newValue)=='undefined')
	        newValue = $(e.target).val();
	    var ruleContainer = $(e.target).closest('.rule-container');
	    var ruleModel =  $(QUERY_BUILDER_CONTAINER).queryBuilder('getModel', ruleContainer);
	    ruleModel.__.value = newValue;
	});

	return newInp;
}


// ExpressionBuilder functions
function createExpressionBuilder(data) {
	// convert data to rules
	var dataRules = convertDataToRules(data);

	// destroy previous query builder instance
	var queryBuilderElement = $(QUERY_BUILDER_CONTAINER);
	queryBuilderElement.queryBuilder('destroy');

	// create query builder instance
	var queryBuilderOptions = {
		plugins: {
			'attributes_plugin': { },
            'sortable': { icon: 'fa fa-sort' },
            'bt-checkbox': { },
			'not-group': {
                icon_checked: "fa fa-check-square",
                icon_unchecked: "fa fa-square",
			},
		},

        display_empty_filter: false,
        allow_empty: true,
        lang: {
            add_rule: 'Add Simple Expr.',
            add_group: 'Add Composite Expr.',
        },

		filters: [
			{
				id: 'attribute',
				label: 'Attribute',
				type: 'string',
				input: 'text',
				operators: null,
			}
		],

		rules: dataRules,

		icons: {
            add_group: 'fa fa-plus-square',
            add_rule: 'fa fa-plus',
            remove_group: 'fa fa-minus-square',
            remove_rule: 'fa fa-trash',
            error: 'fa fa-exclamation-circle'
        },

		templates: {
			filterSelect:
				'<select class="form-control hide" name="{{= it.rule.id }}_filter"><option value="attribute">Attribute</option></select>'+
				'<select class="eb-attribute disable" id="{{= it.rule.id }}-eb-attribute-'+(new Date().getTime())+'"></select>'+
				'',
			operatorSelect:
				'<select class="form-control hide" name="{{= it.rule.id }}_operator"><option value="equal">equal</option></select>'+
				'<select class="eb-property disable" id="{{= it.rule.id }}-eb-property-'+(new Date().getTime())+'"></select>'+
				'<select class="eb-property-op disable" id="{{= it.rule.id }}-eb-property-op-'+(new Date().getTime())+'" style="width: 50px"></select>'+
				'',
		}
	};
	if (QUERY_BUILDER_NOT_GROUP_ON===false)
	    delete queryBuilderOptions.plugins['not-group'];
	queryBuilderElement.queryBuilder(queryBuilderOptions);
}

function clearExpressionBuilder() {
    createExpressionBuilder({} );
}

function convertDataToRules(data) {
    if (!data || data==null || typeof(data)!=='object' || $.isEmptyObject(data))
        return [];

	var id = data.id;
	var type = data['@type'];
	if (type=='CompositeExpression') {
		// It is a Group
		var group = {
			condition: data.operator,
			not: data.not,
			rules: []
		};
		for (var i=0; i<data.expressions.length; i++) {
			var childExpr = data.expressions[i];
			group.rules.push( convertDataToRules(childExpr) );
		}
		return group;
	} else {
		// It is a Rule
		return {
			id: 'attribute',
			operator: 'equal',
			value: data.value,
			data: {
			    attribute: data.attribute,
			    attributeText: data.attributeText,
			    property: data.property,
			    propertyText: data.propertyText,
			    dataProperty: data.dataProperty,
			    rangeType: data.rangeType,
			    rangeId: data.rangeId,
			    instances: data.instances,
			    operator: data.operator,
			    value: data.value
			}
		};
	}
}

function setExpressionBuilderData(data) {
    createExpressionBuilder(data);
}

function getExpressionBuilderData() {
    var model = $(QUERY_BUILDER_CONTAINER).queryBuilder('getModel');
    //console.log(model);
    return getExpressionBuilderDataFromModel(model);
}

function getExpressionBuilderDataFromModel(model) {
    // get model id and level
    var modelId = model.id;
    var modelData = model.__.data;
    var modelLevel = model.__.level;

    var data = null;

    // check if model is a group or rule
    if (model.rules) {
        // It is a group model

        // get group operator
        var groupOperator = model.__.condition;

        // get 'not' flag
        var notFlag = (model.__.not ? true : false);

        // check if group contains rules
        if (model.rules.length==0)
            return null;

        // process group rules
        var subModelDataArr = [];
        for (var i=0; i<model.rules.length; i++) {
            var subModelData = getExpressionBuilderDataFromModel(model.rules[i]);
            if (subModelData && subModelData!=null && Object.keys(subModelData).length>0) {
                subModelDataArr.push( subModelData );
            }
        }
        if (groupOperator && groupOperator!=null && groupOperator.trim()!=='' &&
            subModelDataArr.length>0)
        {
            data = {
                '@type': 'CompositeExpression',
                not: notFlag,
                operator: groupOperator,
                expressions: subModelDataArr
            };
        }
    } else {
        // It is a rule model
        var ruleContainer = model.$el;

        // get rule attribute
        var attributeElem = ruleContainer.children('.rule-filter-container').children('.eb-attribute');
        var attributeId = attributeElem.val();
        var attributeText = attributeElem.text();

        // get rule property
        var propertyElem = ruleContainer.children('.rule-operator-container').children('.eb-property');
        var propertyId = propertyElem.val();
        var propertyText = propertyElem.text();

        var propertyData = propertyElem.select2('data')[0];

        // get rule operator
        var operatorElem = ruleContainer.children('.rule-operator-container').children('.eb-property-op');
        var operatorId = operatorElem.val();
        var operatorText = operatorElem.text();

        // get rule value
        var valueElem = ruleContainer.children('.rule-value-container').children().first();
        var value = $(valueElem).val();
        if (value==undefined || value==='') {
            value = $("input:checked", ruleContainer.children('.rule-value-container')).val();
        }

        // set rule data
        if (attributeId && attributeId!=null && attributeId.trim()!=='' &&
            propertyId && propertyId!=null && propertyId.trim()!=='' &&
            value && value!=null && value.trim()!=='')
        {
            data = {
                '@type': 'SimpleExpression',
                dataProperty: propertyData.dataProperty,
                attribute: attributeId,
                attributeText: attributeText,
                property: propertyId,
                propertyText: propertyText,
                operator: operatorId,
                value: value
            };
        }
    }
    return data;
}

function isExpressionValid() {
    return $(QUERY_BUILDER_CONTAINER).queryBuilder('validate');
}
