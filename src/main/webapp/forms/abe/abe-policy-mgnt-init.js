/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *	jsTree and hotkeys initialization
 */
		/*
		 * Initialize jstree and load initial data
		 */
		// Policies jsTree
		var tree;

		$( document ).ready(function() {
			// Initialize json forms
			initEditForm();
			initButtonsForm();
			initButtonsForm2();
			
			// Initialize tree
			//$.jstree.defaults.core.data = true;
			tree = $('#policyList').jstree({
			    "core" : {
				    "animation" : 500,
				    "multiple": true,
				    "check_callback" : true,
				    "themes" : { "stripes" : true },
				    'data' : {
					    'url': function (node) {
								var url = //node.id === '#' ?
										baseUrl+'/gui/abe/get-toplevel-policies' ;	//:
										//baseUrl+'/gui/abe/get-policy/'+node.id;
								//alert('URL: '+url);
								return url;
							},
					    'data': function (node) { /*alert('DATA: '+JSON.stringify(node));*/ return ''; }
				    }
			    },
			    "contextmenu" : {
                    "items": function($node) {
                        var tree = $(LEFT_LIST_DOM_ID).jstree(true);
                        var sel = tree.get_selected(true);
                        var type = sel[0].original.type;

                        var createItemsArr;
                        if (type==='ABE-POLICY') {
                            createItemsArr = {
                                "create_policy": {
                                    "separator_before": false,
                                    "separator_after": false,
                                    "label": "New Policy",
                                    "action": function (obj) {
                                        deselectTreeNode();
                                        createSibling('ABE-POLICY');
                                    },
                                },
                            };
                        } else
                        {
                            createItemsArr = { };
                        }

                        var commonItemsArr = {
                            "expand_all": {
                                "separator_before": true,
                                "separator_after": false,
                                "label": "Expand all",
                                "action": function (obj) {
                                    expand_all();
                                }
                            },
                            "collapse_all": {
                                "separator_before": false,
                                "separator_after": false,
                                "label": "Collapse all",
                                "action": function (obj) {
                                    collapse_all();
                                }
                            },
                            "clear": {
                                "separator_before": true,
                                "separator_after": false,
                                "label": "Clear form",
                                "action": function (obj) {
                                    clearForm();
                                    deselectTreeNode();
                                }
                            },
                            "remove": {
                                "separator_before": true,
                                "separator_after": false,
                                "label": "Delete",
                                "action": function (obj) {
                                    deleteNode();
                                    deselectTreeNode();
                                }
                            },
                            "revoke-from-pap": {
                                "separator_before": true,
                                "separator_after": false,
                                "label": "Revoke Policy",
                                "action": function (obj) {
                                    if (confirm('Do you want to revoke this ABE policy?')) {
                                        revokePolicy();
                                    }
                                }
                            },
                        };

                        var ctxMenu = $.extend(createItemsArr, commonItemsArr);
                        return ctxMenu;
                    }
			    },
			    "types" : {
				    "#" : {
				        "max_children" : 1,
				        "valid_children" : ["root"]
				    },
				    "root" : {
				        "icon" : "/static/3.0.0/assets/images/tree_icon.png",
				        "valid_children" : ["default"]
				    },
				    "default" : {
				        "valid_children" : ["default","file"]
			        },
				    "file" : {
				        "icon" : "glyphicon glyphicon-file",
				        "valid_children" : []
				    }
			    },
			    "plugins" : [
				    "contextmenu", //"dnd", "search",
				    "state", "types", "wholerow", "hotkeys"//,
			        //"sort"
			    ]
			})
			.on('changed.jstree', function (e, data) {
				// select a property for PropertyIsA field
				/*if (selectTreeNode_State==='SELECT_NODE') {
					if (data && data.selected && data.selected.length && data.node && data.node.id) {
						// accept 'acceptedType' nodes and ignore the rest
						if (data.node.original.type===selectTreeNode_acceptedType || selectTreeNode_acceptedType==='*' || selectTreeNode_acceptedType==='ANY') {
							// update json form
							setFormFieldToNode(data.node['id'], data.node['text']);
						} else
						// if 'acceptedType'=='PARENT' accept CONCEPT nodes for concept, property and concept instance nodes, and CONCEPT-INSTANCE for property instance nodes
						if (selectTreeNode_acceptedType==='PARENT') {
							// get node type
							var type = $('input[name="type"]').val();
							if (data.node.original.type==='ABE-POLICY')
							{
								// update json form
								setFormFieldToNode(data.node['id'], data.node['text']);
							}
						} else {
							console.log('Not a property: '+data.node.id);
						}
					} else {
						console.log('ERROR: "data" or "data.selected" or "data.node" or "data.node.original" not defined');
					}
					return;
				}
				// ending node selection
				if (selectTreeNode_State==='IGNORE') {
					//tree.jstree("select_node", selectedNodeIdBefore);
					return;
				}*/
				
				// Update json form with selected node information
				// An AJAX call will be started
				$('input[name=id]').prop('readonly', true);
				if(data && data.selected && data.selected.length) {
					startTm = new Date().getTime();
					var url = '';
					switch (data.node.original.type) {
					    case 'ABE-POLICY' : url = baseUrl+'/gui/abe/get-policy/'+data.selected; break;
					    default:
                                alert('TYPE NOT YET SUPPORTED [3]');
                                return;
					}
					statusContacting(url, 'n/a');
					clearForm();
					callStartTm = new Date().getTime();
					$.get(url, function (d) {
					    //alert('changed.jstree: received data: '+JSON.stringify(d));
						if(d) {
						    var node = tree.jstree(true).get_node(d.id);
						    node.data = d;
							renderData(d, 'obviously OK');
							//console.log('NODE_CHANGED: server data: '+JSON.stringify(d));
                            //console.log('NODE_CHANGED: tree node after update: '+JSON.stringify(node));
							attrParent = d;

							// update expression builder
							setExpressionBuilderData(d.policyExpression);
						}
					});
				}
				else {
					$('#data .content').hide();
					$('#data .default').html('---').show();
				}
			})
			.on('loaded.jstree', function() {
			})
			.on('load_node.jstree', function(e, data) {
				// enable/disable type buttons
				setFormTypeButtonState(data.node);
			})
			.on('ready.jstree', function(e, data) {
				//console.log('CALL ready.jstree');
				showOrHideEmptyTreeMessage();
				fixTreeContainerHeight();
			})
			.on('refresh.jstree', function(e, data) {
				//console.log('CALL refresh.jstree');
				showOrHideEmptyTreeMessage();
				fixTreeContainerHeight();
			})
            ;

            // add slim scrollbar to jsTree
            fixTreeContainerHeight();

            // make readonly fields... readonly
            prepareEditForm();
			
            initTreeHotkeys();
		});
		
		function showOrHideEmptyTreeMessage() {
			// check if tree contains nodes
			var rootNode = tree.jstree(true).get_node('#');
			if (rootNode && rootNode.children) {
				if (rootNode.children.length==0) {
				    $('#empty_tree_mesg').show();
				    createPolicy();
				} else {
				    $('#empty_tree_mesg').hide();
				    var selArr = tree.jstree(true).get_selected();
				    if (selArr && selArr.length==0) {
				        var first = rootNode.children[0];
				        tree.jstree(true).select_node(first);
				    }
				}
			} else
				//console.log('ready.jstree: Could not get root node')
				;
		}

        function fixTreeContainerHeight() {
            // size jsTree container
            var diff = $('#treeContainer').offset().top;
            diff += $('#formButtons2').height();
            diff += $('#treeInstructions').height();
            diff += 50;
            $('#treeContainer').height(document.documentElement.clientHeight - diff);
		}
		
		/*
		 *	Hotkeys initialization
		 */
		function initTreeHotkeys() {
			var tree = $(LEFT_LIST_DOM_ID);	//jQuery(document);
			tree.bind('keydown', 'insert', function (evt) {
                    var tree = $(LEFT_LIST_DOM_ID).jstree();
                    var sel = selectedTreeNode(tree);
                    if (sel && sel!=null) {
                        var id = getField('id');
                        var type = tree.get_node(id).original.type;

                        if (type==='ABE-POLICY') { deselectTreeNode(); createPolicy(); }
                    }
                    return false;
            });
			tree.bind('keydown', 'Shift+insert', function (evt) {
                    var tree = $(LEFT_LIST_DOM_ID).jstree();
                    var sel = selectedTreeNode(tree);
                    if (sel && sel!=null) {
                        var id = getField('id');
                        var type = getField('type');

                        if (type==='ABE-POLICY') { deselectTreeNode(); createPolicy(); }
                    }
                    return false;
			});
			tree.bind('keydown', 'del',function (evt){ deleteNode(); return false; });
		}
