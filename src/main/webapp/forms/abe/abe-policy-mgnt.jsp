<!DOCTYPE html>
<!--
  ~
  ~ Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~      http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  ~
  -->
<%@ include file="../../includes/prelude.html" %>
<html>
  <head>
<% pageTitle = "ASCLEPIOS ABE Policy Management"; %>
<%@ include file="../../includes/head.html" %>
	<link rel="stylesheet" href="../slick/common-grid-styles.css" type="text/css"/>
    <link rel="stylesheet" style="text/css" href="/forms/admin/common.css" />
    <link rel="stylesheet" style="text/css" href="/forms/deps/opt/bootstrap.css" />

    <script src="/forms/validation/validation-results.js"></script>

<%@ include file="../../includes/expression-builder-assets.html" %>

  </head>
  <body>
	
<%@ include file="../../includes/header.html" %>
	
				<div class="row-fluid" style="margin-top: 20px;">
					<div id="leftContainer" class="span3">
						<div >
							<!-- Legent line -->
							<p align="center"><font size="-1"><i>
								<img src="/images/abe/abe-policy-32x32.png" width="16" height="16"/>&nbsp;ABE Policy &nbsp;
							</i></font></p>
						</div>
						<!-- Policies tree view -->
						<div id="treeContainer" style="overflow-y: scroll">
							<!-- jstree -->
							<div id="policyList"></div>
						</div>
                        <!-- Extra buttons -->
                        <!--<div id="formButtons2" class="span12" style="padding: 0px; float: center;"></div>-->
                        <div style="height: 5px;"> </div>
                        <div id="formButtons2" class="span11 container-fluid" style="padding: 0px; float: center;">
                            <div class="row-fluid">
                                <button id="btn-export-policy-as-ttl" class="btn btn-transition btn-info span6 my-extra-btn"><img src="/images/download-white-32x32.png" width="18" height="18"/> Export Policy </button>
                                <button id="btn-export-policy-as-text" class="btn btn-transition btn-info span6 my-extra-btn"><img src="/images/download-white-32x32.png" width="18" height="18"/> Export as Text </button>
                            </div>
                            <div style="height: 5px;"> </div>
                            <div class="row-fluid">
                                <button id="btn-import-policy" class="btn btn-transition btn-danger span6 my-extra-btn"><img src="/images/upload-white-32x32.png" width="18" height="18"/> Import Policies </button>
                                <button id="btn-apply-policy" class="btn btn-transition btn-warning span6 my-extra-btn"><img src="/images/abac/paper-airplane-white-32x24.png" width="18" height="18"/> Apply Policy </button>
                            </div>
                        </div>
                        <!-- Instructions -->
                        <div id="treeInstructions" class="span12">
                            <div style="font:italic 10pt arial,sans-serif; border-top:black 1pt solid; margin-top:10px; padding-top:15px;">
                                &rtrif; Use <strong>Del</strong>, <strong>Ins</strong> or <strong>Shift+Ins</strong> hotkeys to respectively delete selected policy or create a new one.<br/>
                                &rtrif; Import ABE policy from an RDF/TTL file by clicking on <b>Import Policies</b> button.
                            </div>
                            <!--<div id="empty_tree_mesg" class="alert" style="color:black; font-size:10pt; font-style:italic;"><blockquote></blockquote></div>-->
                        </div>
					</div>
					<div id="formContainer" class="span9">
						<!--<div class="alert"></div>-->
						<div style="font-size: 12pt; font-weight: bold; font-style: italic; background: #eeeeee; padding: 2px; padding: 10px 10px 10px 10px;">
						    <span id="form-title-policy"><img src="/images/abe/abe-policy-32x32.png"/>&nbsp;ABE Policy details</span>
						    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						    <span id="form-title-breadcrumb" style="font-weight: normal; font-style: normal; font-size: 150%"></span>
						</div>
						<p style="margin: 0px;"><hr /></p>
						<!--<p id="update_notice" class="alert" style="display: none"></p>-->
						<form id="formEdit"></form>
						<p><hr /></p>
						<div id="formButtons"></div>
						<p><br/></p>
						<!--<div id="res" class="alert">
							...<br/>
						</div>-->
						<!--<div id="res" class="alert"></div>-->

						<div id="policyExpressionContainer" class="span9" style="margin-left: 0px;">
						    <div id="policyExpressionJQB"></div>
						    <!--<button class="btn btn-success" id="btn-validate">Validate</button>
						    <button class="btn btn-primary" id="btn-get-data">Get Data</button>-->
						</div>
					</div>
				</div>
				
<%@ include file="../../includes/js-libs.html" %>
    <script type="text/javascript" src="/forms/abe/form-abe.js"></script>

				<script type="text/javascript">
					// Override form-abe vars to meet this form's fields
					var formFields = ['id', /*'owner',*/ 'uri', 'type', 'name', 'description', /*'createTimestamp', 'lastUpdateTimestamp',*/ /*'parent',*/ ];
					var emptyData = {'id':'', /*'owner':'',*/ 'uri':'', 'type':'', 'name':'', 'description':'', /*'createTimestamp':'', 'lastUpdateTimestamp':'',*/ /*'parent':'',*/ };
					formSelector = "#formEdit";
					statusSelector = '#res';

					const LEFT_LIST_DOM_ID = "#policyList";

				</script>
				
				<!-- Main Form initialization -->
				<script type="text/javascript" src="abe-policy-mgnt-jsonform.js"></script>
				
				<!-- Operation functions -->
				<script type="text/javascript" src="abe-policy-mgnt-node-ops.js"></script>
				
				<!-- jsTree and hotkeys initialization -->
				<script type="text/javascript" src="abe-policy-mgnt-init.js"></script>

				<!-- QueryBuilder initialization -->
                <script type="text/javascript" src="/forms/ExpressionBuilder.js"></script>

				<script><!--
				    $(function() {
				        correctFieldDimensions();
                    });
				//--></script>

				<!-- QueryBuilder initialization for Policy Expressions -->
                <script type="text/javascript" src="/forms/ExpressionBuilder.js"></script>

                <script>
                    QUERY_BUILDER_CONTAINER = '#policyExpressionJQB';
                    QUERY_BUILDER_NOT_GROUP_ON = false;
                    QUERY_BUILDER_ABE_OPERATORS = true;
                    QUERY_BUILDER_SELECT2_SEARCH_FOR_ATTRIBUTES_URL = '/gui/attributes/search-in-abe/attributes';
                    QUERY_BUILDER_SELECT2_SEARCH_FOR_PROPERTIES_URL = '/gui/attributes/search-in-abe/properties';

                    // create an empty ExpressionBuilder instance
                    createExpressionBuilder(null);

                    // initialize buttons, if exists
                    $('#btn-validate').on('click', function (e) {
                        if (isExpressionValid())
                            alert('Expression is valid');
                        else
                            alert('Expression is not valid');
                    });
                    $('#btn-get-data').on('click', function (e) {
                        var data = getExpressionBuilderData();
                        console.log(data);
                    });

               </script>

<%@ include file="../../includes/footer.html" %>
  </body>
</html>
<%@ include file="../../includes/debug.html" %>
<%@ include file="../../includes/trail.html" %>