/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  Operations
 */
		function reloadTree() {
			//console.log('CALL reloadTree');
			$(LEFT_LIST_DOM_ID).jstree("refresh");
		}
		function saveNode() {
			submitForm();
		}
		function doSaveNode() {
			var urlStr;
			if ($('input[name=id]').prop('readonly')===true) {  // save
			    //console.log('doSaveNode: save: type='+$('input[name=type]').val());
			    switch ($('input[name=type]').val()) {
			        case 'ABE-POLICY' : urlStr = baseUrl+"/gui/abe/save-policy"; break;
			        default:
			            urlStr = '';
			            alert('TYPE NOT YET SUPPORTED [1]');
			            return;
				}
			} else {  // create
				if ($('input[name=id]').val().trim()=='') {
					generateId();
				}
				if ($('input[name=uri]').val().trim()=='') {
					generateUri();
				}
				prepareEditForm();
			    //console.log('doSaveNode: create: type='+$('input[name=type]').val());
			    switch ($('input[name=type]').val()) {
			        case 'ABE-POLICY' : urlStr = baseUrl+"/gui/abe/create-policy"; break;
			        default:
			            urlStr = '';
			            alert('TYPE NOT YET SUPPORTED [2]');
			            return;
				}
			}
			var methodStr = "POST";
			var values = getFormValues();
//			if (values['propertyIsA_display']) {
//				delete values['propertyIsA_display'];
//			}
			prepareValuesForSubmit(values);
			retrieveData(methodStr, urlStr, values);
			reloadTree();
			$('#update_notice').show();
		}
		function deleteNode(cascade) {
			if ($('input[name=id]').prop('readonly')===true) {	// if 'id' field is readonly then it is delete of an existing node, not a new (unsaved) one
				var tree = $(LEFT_LIST_DOM_ID).jstree();
				var sel = selectedTreeNode(tree);
				if (sel && sel!=null) {
					/*if (tree.is_parent(sel)) {
						alert('Node cannot be deleted!\n\nThis node has child node. You must first delete them.');
						return;
					}*/
				} else {
					alert('Select a node to delete');
					return;
				}
				
				var id = getField('id');
				var name = tree.get_node(id).text;
				if (confirm('Delete ABE policy?\nName: '+name+'\nId: '+id)) {
					// check if cascade delete
					if (cascade && cascade===true) cascade = '/cascade'; else cascade = '';
					// prepare WS url
					var urlStr = baseUrl+"/gui/abe/delete-policy/"+id+cascade;

					clearForm();
					setStatus('<p>Waiting to DELETE... <img src="../ajax-loader.gif" /></p>');
					$.ajax({
						async: false,
						type: 'GET',
						url: urlStr,
						success: function(data, textStatus, jqXHR) {
									setStatus('<p>Node '+id+' DELETED</p><p>'+textStatus+'</p>');
									reloadTree();
									deselectTreeNode();
								},
						error: function(jqXHR, textStatus, errorThrown) {
                					showErrorMessage(textStatus, errorThrown);
								},
					});
					$('#update_notice').show();
				}
			} else {	// if 'id' field is NOT readonly then it is delete of a new and unsaved attribute. So we can simply clear the form (no actual delete occurs in attributes store)
				prepareEditForm();
				clearForm();
				deselectTreeNode();
			}
		}
		function createSibling(type) {
		    createPolicy();
		}
		function createChild(type) {
			alert('createChild CALLED !!! Please remove !!!');
		    createPolicy();
		}
		function createPolicy() {
			var tmp = attrParent;
			clearForm();
			clearExpressionBuilder();
			//setField('id', newPar+'-');
			var id = createNewId();
			setField('id', id);
			setField('uri', createUriById(id));
			prepareCreateForm('ABE-POLICY');
			attrParent = tmp;
		}
		function prepareCreateForm(type) {
			$('input[name=id]').prop('readonly', false);
			//$('input[name=uri]').prop('readonly', false);
			if (type!==undefined) {
				$('input[name=type]').prop('readonly', true);
				$('input[name=type]').val(type);
				setFormByNodeType(type, 'DataProperty');
			} else {
				$('input[name=type]').prop('readonly', false);
				$('input[name=type]').val('');
				setFormByNodeType('ABE-POLICY', 'DataProperty');
			}

            setFormTitle(type, '', true);
			setFormButtonStates(false, false, false, false, true, true, false);
			setEnabledIdAndUriButtons(true);
			var btnEnable = false; //(type!=='PROPERTY-INSTANCE');
			enableFormTypeButtons(btnEnable, btnEnable, btnEnable/*, false*/);
		}
		function prepareEditForm() {
			$('input[name=id]').prop('readonly', true);
			//$('input[name=uri]').prop('readonly', true);
			$('input[name=type]').prop('readonly', true);
			setFormButtonStates(false, false, false, false, true, true, true);
			setEnabledIdAndUriButtons(false);
		}
		function selectedTreeNode(tree) {
			if (!tree) tree = $(LEFT_LIST_DOM_ID).jstree();
			var sel = tree.get_selected(true);
			if (sel.length>0) {
				return sel[0];
			} else {
				return null;
			}
		}
		function selectTreeNode(tree, nodeId) {
			if (!tree) tree = $(LEFT_LIST_DOM_ID).jstree();
			//tree.jstree("select_node", '#'+nodeId);
			tree.jstree("select_node", nodeId);
		}
		function deselectTreeNode(tree) {
			if (!tree) tree = $(LEFT_LIST_DOM_ID).jstree();
			var sel1 = tree.get_selected(true);
			if (sel1.length>0) {
				tree.deselect_node(sel1[0]);
			}
		}
		function expand_all(tree) {
			if (!tree) tree = $(LEFT_LIST_DOM_ID).jstree();
			var node = selectedTreeNode(tree);
			if (node && node!=null) tree.open_all(node);
		}
		function collapse_all(tree) {
			if (!tree) tree = $(LEFT_LIST_DOM_ID).jstree();
			var node = selectedTreeNode(tree);
			if (node && node!=null) tree.close_all(node);
		}
		function setNodeType(typeId) {
			var type = '';
			if (typeId===0) type = 'ABE-POLICY';
			else {
				alert('Invalid Node Type Id: '+typeId);
				return;
			}
			//if ($('input[name=type]').prop('readonly')===false) {  // we are in create mode, so changing type is allowed
				$('input[name=type]').val(type);
				setFormByNodeType(type, 'DataProperty');
			//}
		}
		function setFormByNodeType(type, propType, range_display, propertyIsA_display) {
		}
