/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *	JSONform initialization
 */
	
/*
 * Main Form specification
 */
function initEditForm() {
	var options = getBaseEditFormOptions('node');
	options['onSubmitValid'] = function (values) {
								doSaveNode();
							};
	$('#formEdit').jsonForm(options);
}

/*
 * Buttons Form specification
 */
function initButtonsForm() {
	var options = {
		schema: {
		},
		"form": [
			{	"title": '<img src="/images/save-white-32x32.png" width="18" height="18"/> Save Changes &nbsp;&nbsp;',
				"type": "button",
				"htmlClass": "btn btn-transition btn-success",
				"onClick": function (evt) { saveNode(); }
			},
			{	"title": '<img src="/images/abe/abe-policy-white-32x32.png" width="20" height="20"/> Create Policy &nbsp;&nbsp;',
				"type": "button",
				"htmlClass": "btn btn-transition btn-primary",
				"onClick": function (evt) { deselectTreeNode(); createPolicy(); }
			},
			/*{	"title": '<img src="/images/clear-18x18.png" width="20" height="20"/> Clear form &nbsp;&nbsp;',
				"type": "button",
				"htmlClass": "btn btn-transition btn-warning",
				"onClick": function (evt) { clearForm(); deselectTreeNode(); }
			},*/
			{	"title": '<img src="/images/delete-white-18x18.png" width="18" height="18"/> Delete Policy &nbsp;&nbsp;',
				"type": "button",
				"htmlClass": "btn btn-transition btn-danger",
				"onClick": function (evt) { deleteNode(); deselectTreeNode(); }
			},
		],
	};
	
	$('#formButtons').jsonForm(options);
}

function initButtonsForm2() {

	$("#btn-export-policy-as-ttl").on('click', function(evt) {
        exportPolicy('/gui/abe/export-policy-as-rdf/');
	});
	$("#btn-export-policy-as-text").on('click', function(evt) {
        exportPolicy('/gui/abe/export-policy-as-text/');
	});
	$("#btn-import-policy").on('click', function(evt) {
        if (confirm('Import page will be loaded. Any unsaved work will be lost.\nContinue?')) {
                document.location = '/forms/admin/import.jsp?mode=APPEND';
        }
	});
	$("#btn-apply-policy").on('click', function(evt) {
        if (confirm('Do you want to apply this ABE policy?')) {
            var id = getField('id');
            if (!id || id==null || id=='') return;

            validatePolicy(id)
                .then(function(message) {
                    applyPolicy();
                })
                .catch(function (error) {
                    console.log('Validation error: ', error);
                    if (typeof error==='object' && error!=null)
                        showToast(error.type, error.message, error.title);
                    else
                        ;//showToast('error', error, 'Validation Error');
                });
        }
	});
}

function exportPolicy(baseUrl) {
    var id = getField('id');
    var type = getField('type');
    //console.log('Export policy as RDF: id='+id+', type='+type);

    if (!type || type==null || type.trim()=='') return;
    if (!id || id==null || id=='') return;

    var url = baseUrl + id;
    //console.log('Export policy as RDF: Contacting '+url);
    document.location = url;
}

function applyPolicy() {
    var id = getField('id');
    var type = getField('type');
    //console.log('Export policy as RDF: id='+id+', type='+type);

    if (!type || type==null || type.trim()=='') return;
    if (!id || id==null || id=='') return;

    var urlStr = '/gui/abe/apply-policy/' + id;
    //console.log('Applying policy: Contacting '+url);

    showIndicator('<p>Applying ABE policy...</p>');
    $.ajax({
        async: false,
        type: 'GET',
        url: urlStr,
        success: function(data, textStatus, jqXHR) {
                    hideIndicator();
                },
        error: function(jqXHR, textStatus, errorThrown) {
					showErrorMessage(textStatus, errorThrown);
                },
    });
    //$('#update_notice').show();
}

function revokePolicy() {
    var id = getField('id');
    var type = getField('type');
    //console.log('Export policy as RDF: id='+id+', type='+type);

    if (!type || type==null || type.trim()=='') return;
    if (!id || id==null || id=='') return;

    var urlStr = '/gui/abe/revoke-policy/' + id;
    //console.log('Applying policy: Contacting '+url);

    showIndicator('<p>Revoking ABE policy...</p>');
    $.ajax({
        async: false,
        type: 'GET',
        url: urlStr,
        success: function(data, textStatus, jqXHR) {
                    hideIndicator();
                },
        error: function(jqXHR, textStatus, errorThrown) {
					showErrorMessage(textStatus, errorThrown);
                },
    });
    //$('#update_notice').show();
}

/*
 * Form preparation (before assigning data)
 */
function prepareForm(data) {
	setFormByNodeType(data.type, data.propertyType, data.range_display, data.propertyIsA_display);
	
	// enable/disable form buttons
	var type = data.type;
	if (type==='ABE-POLICY') {
		setFormButtonStates(true, true, true, false, true, true, true);
	}
	
	// enable/disable type buttons
	setFormTypeButtonState(data);
}

function setFormTitle(type, breadcrumb, isCreate) {
    // display the right form type
    //$('#form-title-policy').hide();
    //if (type==='ABE-POLICY') $('#form-title-policy').show();

    // display the breadcrumb
    if (breadcrumb!=='') {
        $('#form-title-breadcrumb').text(breadcrumb);
    } else {
        if (isCreate) {
            if (type==='ABE-POLICY') $('#form-title-breadcrumb').html('<i>&lt;&lt; New ABE Policy &gt;&gt;</i>');
        } else {
            $('#form-title-breadcrumb').html($("#name").val());
        }
    }
}

function correctFieldDimensions() {
    // resize 'description' textarea to align to other fields
    var nameFieldWidth =
            $("input[name=name]").outerWidth()
            + $("input[name=name]").parent().children().last().outerWidth();
    $("textarea[name=description]").outerWidth(nameFieldWidth);
}

/*$(document).ready(function() {    // Not working...
    correctFieldDimensions();
});*/
$(window).on('resize', function() {
    correctFieldDimensions();
});


function setFormButtonStates(cpol, crul, exp, xxx, save, clear, del) {
	$('#formButtons').find('button:contains("Create Policy")').prop( "disabled", !cpol ).addClass( cpol?'':'grayscale' ).removeClass( cpol?'grayscale':'' );
	//
	$('#formButtons2').find('button:contains("Export ")').prop( "disabled", !exp ).addClass( exp?'':'grayscale' ).removeClass( exp?'grayscale':'' );
	$('#formButtons2').find('button:contains("Apply ")').prop( "disabled", !exp ).addClass( exp?'':'grayscale' ).removeClass( exp?'grayscale':'' );
	//
	$('#formButtons').find('button:contains("Save")').prop( "disabled", !save ).addClass( save?'':'grayscale' ).removeClass( save?'grayscale':'' );
	$('#formButtons').find('button:contains("Clear")').prop( "disabled", !clear ).addClass( clear?'':'grayscale' ).removeClass( clear?'grayscale':'' );
	$('#formButtons').find('button:contains("Delete")').prop( "disabled", !del ).addClass( del?'':'grayscale' ).removeClass( del?'grayscale':'' );
}

function setFormTypeButtonState(data) {
	var is_loaded = $(LEFT_LIST_DOM_ID).jstree(true).is_loaded(data.id);
	var is_leaf = $(LEFT_LIST_DOM_ID).jstree(true).is_leaf(data.id);
	var enable = (is_loaded && is_leaf);
	
	enableFormTypeButtons(enable, enable, enable/*, false*/);
}
	
function enableFormTypeButtons(cpol,crul,xxx,pi) {
	$('#btn-set-type-abe-policy') .prop( "disabled", !cpol ).addClass( cpol?'':'grayscale' ).removeClass( cpol?'grayscale':'' );
}
