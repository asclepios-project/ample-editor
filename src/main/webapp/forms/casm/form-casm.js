/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *	Utility Function common to all forms
 */
// User-provided variables
var baseUrl = '';
var formFields = [];
var emptyData = {};
var formSelector = "";
var attrParent;
var attrNode;

// Timer variables
var startTm = null;
var callStartTm = null;

/*
 * Common form functions
 */
// Initialization and auxiliary functions
function makeEmpty(fields) {
	if (!fields) fields = formFields;
	emptyData = {};
	for (f in fields) emptyData[f] = '';
}
function uuidv4() {
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
		var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
		return v.toString(16);
	});
}
function createNewId() {
	return uuidv4();
}
function generateId() {
	var idf = $('input[name=id]');
	if (idf.prop('readonly')===false) {
		var guid = uuidv4();
		idf.val( guid );
		return guid;
	}
	return false;
}
function createUriById(id) {
	return 'asclepios:'+id;
}
function generateUri() {
	var idf = $('input[name=id]');
	var urif = $('input[name=uri]');
	if (urif.prop('readonly')===false && idf.val().trim()!=='') {
		var uri = 'asclepios:'+idf.val().trim();
		urif.val( uri );
		return uri;
	}
	return false;
}
function setEnabledIdAndUriButtons(enable) {
	var idf = $('input[name=id]');
	var urif = $('input[name=uri]');
	if (idf.prop('readonly')===false) {
		$('#btn-gen-id').prop( "disabled", false ).removeClass( 'grayscale' );
	} else {
		$('#btn-gen-id').prop( "disabled", true ).addClass( 'grayscale' );
	}
	if (urif.prop('readonly')===false) {
		$('#btn-gen-uri').prop( "disabled", false ).removeClass( 'grayscale' );
	} else {
		$('#btn-gen-uri').prop( "disabled", true ).addClass( 'grayscale' );
	}
}
function getBaseEditFormOptions(entityName) {
	var entity;
	if (!entityName) entity = 'node'; else entity = entityName;
	return {
		schema: {
		    id: {
		        type: 'string',
		        title: 'Id',
		        readonly: true,
		        required: true
		    },
		    parent: {
		        type: 'string',
		        title: 'Parent',
		        readonly: true
		    },
/*		    owner: {
				type: 'string',
				title: 'Owner',
				readonly: true,
				required: false
		    },*/
		    uri: {
		        type: 'string',
		        title: 'URI',
		        readonly: false,
		        required: true
		    },
		    type: {
		        type: 'string',
		        title: 'Type',
		        //enum: ['CONCEPT', 'PROPERTY', 'CONCEPT-INSTANCE', 'PROPERTY-INSTANCE'],
		        readonly: true,
		        required: true
		    },
		    name: {
		        type: 'string',
		        title: 'Name',
		        required: true
		    },
		    description: {
		        type: 'string',
		        title: 'Description',
		    },
/*		    createTimestamp: {
				//type: 'date',
				type: 'string',
				title: 'Creation',
				readonly: true,
		    },
		    lastUpdateTimestamp: {
				//type: 'date',
				type: 'string',
				title: 'Last Update',
				readonly: true,
		    },*/
		    propertyType: {
		        type: 'string',
		        title: 'Property Type',
		        readonly: true,
		    },
		    range: {
		        type: 'string',
		        title: 'Range Concept',
		        readonly: true,
		    },
		    range_display: {
		        type: 'string',
		        title: 'Range Concept',
		        readonly: true,
		    },
		    rangeUri: {
		        type: 'string',
		        title: 'Range Data Type',
		    },
		    propertyIsA: {
		        type: 'string',
		        title: 'Property',
		        readonly: true,
		    },
		    propertyIsA_display: {
		        type: 'string',
		        title: 'Property',
		        readonly: true,
		    },
		    propertyValue: {
		        type: 'string',
		        title: 'Property Value',
		    },
		    inAbac: {
		        type: 'string',
		        title: 'In ABAC',
		        readonly: true,
		    },
		    inAbe: {
		        type: 'string',
		        title: 'In ABE',
		        readonly: true,
		    },
		},
		"form": [
			{	"key": "id",
				"notitle": true,
				"prepend": "<span class=\"jsonform-title\">Id:</span>",
				"append": "<font color=red><i>required</i></font>",
				"description": "A string uniquely identifying this "+entity+". If left empty a GUID will be generated for Id. <button id=\"btn-gen-id\" onclick=\"generateId(); return false;\" class=\"btn btn-warning\">Generate new Id as GUID</button>",
				"fieldHtmlClass": "input-xxlarge",
			},
			{	"key": "parent",
				"notitle": true,
				"prepend": "<span class=\"jsonform-title\">Parent:</span>",
				"append": '<span id="parent_Selector"><a style="cursor: pointer;" onclick="startSelectTreeNode(\'parent_Selector\', \'parent\', \'\', \'PARENT\', \'changeParent\'); return false;">Change</a></span>',
				"description": "A URI specifying the parent element. If left empty it is a top-level element",
				"fieldHtmlClass": "input-xxlarge",
				onClick: function (e) {
					e.preventDefault();
					startSelectTreeNode('parent_Selector', 'parent', '', 'PARENT', 'changeParent');
				}
			},
/*			{	"key": "owner",
				"notitle": true,
				"prepend": "class=\"jsonform-title\"Owner:</span>",
				"append": "<font color=red><i>required</i></font>",
				"description": "The owner of the "+entity+" (normally you)",
				"fieldHtmlClass": "input-xxlarge",
			},*/
			{	"key": "uri",
				"notitle": true,
				"prepend": "<span class=\"jsonform-title\">URI:</span>",
				"append": "<font color=red><i>required</i></font>",
				"description": "A URI uniquely identifying this "+entity+". If left empty it will be the same with Id with 'asclepios:' NS prefix. <button id=\"btn-gen-uri\" onclick=\"generateUri(); return false;\" class=\"btn btn-warning\">Make URI</button>",
				"fieldHtmlClass": "input-xxlarge",
			},
			{	"key": "type",
				"notitle": true,
				"prepend": "<span class=\"jsonform-title\">Type:</span>",
				"append": "<font color=red><i>required</i></font>",
				//"description": "The type of this "+entity+'. Allowed values:  CONCEPT, PROPERTY, CONCEPT-INSTANCE, PROPERTY-INSTANCE',
				"description": "The type of this "+entity+'. Allowed values: '+
								'<button id="btn-set-type-concept"  onclick="setNodeType(0); return false;" class="btn btn-info">CONCEPT</button> '+
								'<button id="btn-set-type-property" onclick="setNodeType(1); return false;" class="btn btn-info">PROPERTY</button> '+
								'<button id="btn-set-type-instance" onclick="setNodeType(2); return false;" class="btn btn-info">CONCEPT-INSTANCE</button> '+
								//'<button id="btn-set-type-propinst" onclick="setNodeType(3); return false;" class="btn btn-info">PROPERTY-INSTANCE</button> '+
								/*'<a id="btn-set-type-concept"  style="cursor: pointer;" onclick="setNodeType(0);">CONCEPT</a>, '+
								'<a id="btn-set-type-property" style="cursor: pointer;" onclick="setNodeType(1);">PROPERTY</a>, '+
								'<a id="btn-set-type-instance" style="cursor: pointer;" onclick="setNodeType(2);">CONCEPT-INSTANCE</a>, '+
								'<a id="btn-set-type-propinst" style="cursor: pointer;" onclick="setNodeType(3);">PROPERTY-INSTANCE</a>'+*/
								'',
				"fieldHtmlClass": "input-xxlarge",
			},
			{	"key": "name", 
				"notitle": true,
				"prepend": "<span class=\"jsonform-title\">Name:</span>",
				"append": "<font color=red><i>required</i></font>",
				"description": "The display name of the "+entity,
				"fieldHtmlClass": "input-xxlarge",
			},
			{	"key": "description",
			    "type": "textarea",
			    "width": "60%",
			    "height": "100px",
				"notitle": true,
				"prepend": "<span class=\"jsonform-title\">Description:</span>",
				"description": "An explanatory description of the "+entity+"'s purpose",
				"fieldHtmlClass": "input-xxlarge",
			},
/*			{	"key": "createTimestamp", 
				//"type": "datetime",
				"notitle": true,
				"prepend": "<span class=\"jsonform-title\">Creation Date/Time:</span>",
			},
			{	"key": "lastUpdateTimestamp",
				//"type": "datetime",
				"notitle": true,
				"prepend": "<span class=\"jsonform-title\">Last Update Date/Time:</span>",
			},*/
			{	"key": "propertyType",
				"notitle": true,
				"prepend": "<span class=\"jsonform-title\">Property Type:</span>",
				"append": '<a style="cursor: pointer;" onclick="setFormPropertyType(0); return false;">Data</a> | '+
							'<a style="cursor: pointer;" onclick="setFormPropertyType(1); return false;">Object</a>',
				"description": "0: Data Property ; 1: Object Property",
				"fieldHtmlClass": "input-xxlarge",
			},
			{	"key": "range",
				"notitle": true,
				"prepend": "<span class=\"jsonform-title\">Concept Id:</span>",
				"fieldHtmlClass": "input-xxlarge",
			},
			{	"key": "range_display",
				"notitle": true,
				"prepend": "<span class=\"jsonform-title\">Range Concept:</span>",
				"append": '<span id="range_Selector"><a style="cursor: pointer;" onclick="startSelectTreeNode(\'range_Selector\', \'range\', \'range_display\', \'CONCEPT\'); return false;">Change</a></span>',
				"description": "The property range (it is a Concept)",
				"fieldHtmlClass": "input-xxlarge",
				onClick: function (e) {
					e.preventDefault();
					startSelectTreeNode('range_Selector', 'range', 'range_display', 'CONCEPT');
				}
			},
			{	"key": "rangeUri",
				"notitle": true,
				"prepend": "<span class=\"jsonform-title\">Data Type:</span>",
				"description": "The property data type. Standard XSD data types: "+
								'<a style="cursor: pointer;" onclick="setNodeDataType(\'xsd:string\');">String</a>, '+
								'<a style="cursor: pointer;" onclick="setNodeDataType(\'xsd:integer\');">Integer</a>, '+
								'<a style="cursor: pointer;" onclick="setNodeDataType(\'xsd:double\');">Double</a>, '+
								'<a style="cursor: pointer;" onclick="setNodeDataType(\'xsd:boolean\');">Boolean</a>, '+
								'',
				"fieldHtmlClass": "input-xxlarge",
			},
			/*{	"key": "propertyIsA",
				"notitle": true,
				"prepend": "<span class=\"jsonform-title\">Property IsA:</span>",
				"fieldHtmlClass": "input-xxlarge",
			},
			{	"key": "propertyIsA_display",
				"notitle": true,
				"prepend": "<span class=\"jsonform-title\">Ref'd Property:</span>",
				"append": '<span id="propertyIsA_Selector"><a style="cursor: pointer;" onclick="startSelectTreeNode(\'propertyIsA_Selector\', \'propertyIsA\', \'propertyIsA_display\', \'PROPERTY\'); return false;">Change</a></span>',
				"description": "The referenced property",
				"fieldHtmlClass": "input-xxlarge",
				onClick: function (e) {
					e.preventDefault();
					startSelectTreeNode('propertyIsA_Selector', 'propertyIsA', 'propertyIsA_display', 'PROPERTY');
				}
			},
			{	"key": "propertyValue",
				"notitle": true,
				"prepend": "<span class=\"jsonform-title\">Property Value:</span>",
				"description": "The property value for the current concept instance",
				"fieldHtmlClass": "input-xxlarge",
			},*/
			{	"key": "inAbac",
				"notitle": true,
				"prepend": "<img src=\"/images/abac/abac-policy-16x16-4.png\" /><span class=\"jsonform-title\">Use in ABAC?</span>",
				"append": '<div style="text-align: left; padding-left: 10px;"> <input type="checkbox" id="inAbac-toggle" class="toggle-control" data-toggle="toggle" />' +
				            ' &nbsp; Controls if this element will be available in ABAC policy rule conditions</div>',
				"fieldHtmlClass": "toggle-value",
				onChange: function (e) {
				    var value = $(e.target).val();
				    var toggleState = value==='true' ? 'on' : 'off';
				    $('#inAbac-toggle').bootstrapToggle(toggleState);
					e.preventDefault();
				}
			},
			{	"key": "inAbe",
				"notitle": true,
				"prepend": "<img src=\"/images/abe/abe-policy-16x16.png\" /><span class=\"jsonform-title\">Use in ABE?</span>",
				"append": '<div style="text-align: left; padding-left: 10px;"> <input type="checkbox" id="inAbe-toggle" class="toggle-control" data-toggle="toggle" />' +
				            ' &nbsp; Controls if this element will be available in ABE policies</div>',
				"fieldHtmlClass": "toggle-value",
				onChange: function (e) {
				    var value = $(e.target).val();
				    var toggleState = value==='true' ? 'on' : 'off';
				    $('#inAbe-toggle').bootstrapToggle(toggleState);
					e.preventDefault();
				}
			},
		]
	};
}
function getBaseButtonsFormOptions(entityName) {
	var entity;
	if (entityName) entity = entityName; else entity = 'Node';
	return {
		schema: {
		},
		"form": [
			{	"title": "Save Changes",
				"type": "button",
				"onClick": function (evt) {
					alert('NOT IMPLEMENTED');
				}
			},
			{	"title": "Delete "+entity,
				"type": "button",
				"onClick": function (evt) {
					alert('NOT IMPLEMENTED');
				}
			},
			{	"title": "Create "+entity,
				"type": "button",
				"onClick": function (evt) {
					alert('NOT IMPLEMENTED');
				}
			},
			/*{	"title": "Clear form",
				"type": "button",
				"onClick": function (evt) {
					alert('NOT IMPLEMENTED');
				}
			},*/
		],
	}
}

// Form and form field functions
function setField(name, value) {
	var elem = $('input[name='+name+']');
	if (elem.length==0) elem = $('select[name='+name+']');
	if (elem.length==0) elem = $('textarea[name='+name+']');
	elem.val(value);
	elem.trigger('change');
}
function getField(name) {
	var v = $('input[name='+name+']').val();
	if (!v || v==null || v=='')
	    v = $('textarea[name='+name+']').val();
	return v;
}
function getFormValues() {
	var values = {};
	for (ii in formFields) {
		var fld = formFields[ii];
		values[fld] = getField(fld);
		//console.log('getFormValues: '+fld+' --> '+values[fld]);
	}
	return values;
}
function setFormValues(values) {
	attrNode = values;
	for (ii in formFields) {
		var fld = formFields[ii];
		var val = values[fld];
		//console.log('setFormValues: '+fld+' --> '+val);
		if (fld==='parent' && val && val.id) {
			attrParent = val;
			val = attrParent.id;
			//console.log('setFormValues: "parent" field value has been set to: '+val);
		}
		setField(fld, val);
	}
}
function setFormData(data) {
	setFormValues(data);
}
function submitForm(frmSel) {
	if (frmSel && frmSel.trim()!=='')  $(frmSel).submit();
	else $(formSelector).submit();
}
function clearForm(frmSel) {
	setFormValues(emptyData);
	for (f in formFields) $('input[name='+f+']').val('');
	$('input[name=id]').prop('readonly',true);
	setEnabledIdAndUriButtons(false);
}

// Data exchange functions
function retrieveData(methodStr, urlStr, values, mesg) {
	var valStr = '';
	//for (i in values) if (values[i]==='-') values[i]='';
	var _hasParent = ( values && values['parent'] && (typeof attrParent!=='undefined') && values['parent']===attrParent.id );
	if (_hasParent) values['parent'] = attrParent;
	else delete values['parent'];
	if (values) valStr = JSON.stringify(values);
	if (_hasParent) values['parent'] = attrParent.id;
	statusContacting(methodStr+' '+urlStr, valStr);
	//clearForm();
	setStatus('<p>Waiting for reply... <img src="../ajax-loader.gif" /></p>');
	if (mesg) showIndicator(mesg); else showIndicator("Contacting server...");
	callStartTm = new Date().getTime();
	$.ajax({
		async: false,
		type: methodStr,
		url: urlStr,
		data: valStr,
		contentType: 'application/json',
		dataType: 'json',
        beforeSend: setCsrfHeaders,
		success: function(data, textStatus, jqXHR) {
					hideIndicator();
					renderData(data, textStatus);
				},
		error: function(jqXHR, textStatus, errorThrown) {
					showErrorMessage(textStatus, errorThrown);
				},
		//complete: function() { alert('ALWAYS'); }
	});
}

// Rendering functions
function renderData(data, textStatus) {
	var callEndTm = new Date().getTime();
	var dataStr = JSON.stringify(data);
	//console.log('renderData: '+dataStr);
	if (prepareForm) prepareForm(data);
	prepareValuesForRender(data);
	clearForm();
	setFormData(data);
	setFormTitle(data.type, data.name, false);
	var endTm = new Date().getTime();
	setStatus('<p>Status: '+textStatus+'</p>'+
					'<p>Duration: '+(endTm-startTm)+'ms   contacting server: '+(callEndTm-callStartTm)+'ms</p>'+
					'<p>JSON: '+dataStr+'</p>');
}
function renderDate(tm) {
	if (tm) {
		try {
			if ($.isNumeric(tm)) { return new Date(tm).toISOString(); }
		} catch (e) { alert(tm+'\n'+e); }
	}
	return tm;
}
function prepareValuesForSubmit(data) {
}
function prepareValuesForRender(data) {
	data.lastUpdateTimestamp = renderDate(data.lastUpdateTimestamp);
	data.createTimestamp = renderDate(data.createTimestamp);
}

// Define custom date/time picker input field
JSONForm.fieldTypes['datetime'] = {
	'template': '<input type="text" ' +
	  '<%= (fieldHtmlClass ? "class=\'" + fieldHtmlClass + "\' " : "") %>' +
	  'name="<%= node.name %>" value="<%= escape(value) %>" id="<%= id %>"' +
	  '<%= (node.disabled? " disabled" : "")%>' +
	  '<%= (node.readOnly ? " readonly=\'readonly\'" : "") %>' +
	  '<%= (node.schemaElement && node.schemaElement.maxLength ? " maxlength=\'" + node.schemaElement.maxLength + "\'" : "") %>' +
	  '<%= (node.schemaElement && node.schemaElement.required && (node.schemaElement.type !== "boolean") ? " required=\'required\'" : "") %>' +
	  ' />\n' +
	  ' <script type="text\/javascript">$("#<%= id %>").datetimepicker({format:"d/m/Y H:i:s",mask:"9999-19-39T29:59:59.999Z"});<\/script>\n',
	'fieldtemplate': true,
	'inputfield': true
}

// Tree Node Selection functions
var selectTreeNode_active = false;
var selectTreeNode_end_timestamp = 0;
var selectTreeNode_State = '';
var selectTreeNode_selectedNodeIdBefore = '';
var selectTreeNode_acceptedType = '';
var selectTreeNode_selectorId = '';
var selectTreeNode_inputId = '';
var selectTreeNode_displayId = '';
var selectTreeNode_callback = '';

function startSelectTreeNode(selectorId, inputId, displayId, type, callback = '') {
	console.log('>> startSelectTreeNode: BEGIN: '+selectorId);
	if (selectTreeNode_active) return;
	console.log('>> startSelectTreeNode: CHECK DELAY');
	if (Date.now()-selectTreeNode_end_timestamp < 200) return;
	console.log('>> startSelectTreeNode: ACTIVATED');
	selectTreeNode_active = true;
	
	// get selected node and cache it for restore
	selectTreeNode_selectedNodeIdBefore = treeAttributes.jstree("get_selected");
	selectTreeNode_selectorId = selectorId;
	selectTreeNode_inputId = inputId;
	selectTreeNode_displayId = displayId;
	selectTreeNode_acceptedType = type;
	selectTreeNode_callback = callback;
	
	// start property selection
	selectTreeNode_State = 'SELECT_NODE';
	html = $("#"+selectorId).html();
	anyType = (type==='*' || type==='ANY');
	parentType = (type==='PARENT');
	innerHtml = 'Click on a '+(anyType||parentType ? 'node' : type)+' in Tree view or click <a style="cursor: pointer;" onclick="endSelectTreeNode(); return false;">Cancel</a> to abort';
	$("#"+selectorId).html( innerHtml );
}

function endSelectTreeNode() {
	console.log('>> endSelectTreeNode: BEGIN: '+selectTreeNode_selectorId);
	if (!selectTreeNode_active) return;
	console.log('>> endSelectTreeNode: DONE');
	selectTreeNode_active = false;
	
	// end property selection
	html = $("#"+selectTreeNode_selectorId).html();
	params = "'"+selectTreeNode_selectorId+"', '"+selectTreeNode_inputId+"', '"+selectTreeNode_displayId+"', '"+selectTreeNode_acceptedType+"'";
	if (selectTreeNode_callback!=='') params += ", '"+selectTreeNode_callback+"'";
	innerHtml = '<a style="cursor: pointer;" onclick="startSelectTreeNode('+params+'); return false;">Change</a>';
	$("#"+selectTreeNode_selectorId).html( innerHtml );
	
	// restore previous selection
	selectTreeNode_State = 'IGNORE';
	deselectTreeNode();
	//data.instance.deselect_node(data.node);
	selectTreeNode(treeAttributes, selectTreeNode_selectedNodeIdBefore);
	selectTreeNode_selectedNodeIdBefore = '';
	selectTreeNode_State = '';
	selectTreeNode_callback = null;
	
	selectTreeNode_end_timestamp = Date.now();
}

function setFormFieldToNode(id, text) {
	var callback = selectTreeNode_callback;
	endSelectTreeNode();
	
	$("input[name='"+selectTreeNode_inputId+"']").val(id);
	if (selectTreeNode_displayId!=='') $("input[name='"+selectTreeNode_displayId+"']").val(text);
	
	if (callback!=='') {
		//console.log('setFormFieldToNode: BEFORE calling Callback: '+callback);
		eval( callback+"('"+id+"')" );
		//console.log('setFormFieldToNode: AFTER calling callback: '+callback);
	}
	
	selectTreeNode_selectorId = '';
	selectTreeNode_inputId = '';
	selectTreeNode_displayId = '';
	selectTreeNode_callback = '';
}
function setNodeDataType(dataType) {
	$("input[name='rangeUri']").val(dataType);
}

function changeParent(id) {
	urlStr = '/gui/attributes/get-attribute/'+id;
	setStatus('<p>Waiting for reply... <img src="../ajax-loader.gif" /></p>');
	showIndicator('Fetching parent information...');
	callStartTm = new Date().getTime();
	$.ajax({
		async: false,
		type: 'get',
		url: urlStr,
		data: '',
		contentType: 'application/json',
		dataType: 'json',
		success: function(data, textStatus, jqXHR) {
					hideIndicator();
					attrParent = data;
				},
		error: function(jqXHR, textStatus, errorThrown) {
					showErrorMessage(textStatus, errorThrown);
				},
		//complete: function() { alert('ALWAYS'); }
	});	
}