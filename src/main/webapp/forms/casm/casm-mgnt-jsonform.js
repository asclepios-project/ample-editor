/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *	JSONform initialization
 */
	
/*
 * Main Form specification
 */
function initAttributeEditForm() {
	var options = getBaseEditFormOptions('node');
	options['onSubmitValid'] = function (values) {
								doSaveNode();
							};
	$('#formEdit').jsonForm(options);
	
	// add placeholders
	$('input[name="parent_display"').attr('placeholder', 'To select a Parent node, click here or on "Change" and select a node from the tree on the left');
	$('input[name="range_display"').attr('placeholder', 'To select a Range, click here or on "Change" and select a concept from the tree on the left');
	$('input[name="propertyIsA_display"').attr('placeholder', 'To select a Property, click here or on "Change" and select a property from the tree on the left');
}

/*
 * Buttons Form specification
 */
function initAttributeButtonsForm() {
	var options = {
		schema: {
		},
		"form": [
			{	"title": '<img src="/images/save-white-32x32.png" width="18" height="18"/> Save Changes &nbsp;&nbsp;',
				"type": "button",
				"htmlClass": "btn btn-transition btn-success",
				"onClick": function (evt) { saveNode(); }
			},
			{	"title": '<img src="/images/concept.png" width="20" height="20"/> Create Root Concept &nbsp;&nbsp;',
				"type": "button",
				"htmlClass": "btn btn-transition btn-primary",
				"onClick": function (evt) { deselectTreeNode(); createSibling('CONCEPT'); }
			},
			{	"title": '<img src="/images/concept.png" width="20" height="20"/> Create Concept &nbsp;&nbsp;',
				"type": "button",
				"htmlClass": "btn btn-transition btn-primary",
				"onClick": function (evt) { deselectTreeNode(); createChild('CONCEPT'); }
			},
			{	"title": '<img src="/images/property.png" width="18" height="18"/> Create Property &nbsp;&nbsp;',
				"type": "button",
				"htmlClass": "btn btn-transition btn-primary",
				"onClick": function (evt) { deselectTreeNode(); createChild('PROPERTY'); }
			},
			{	"title": '<img src="/images/concept-instance.png" width="14" height="14"/> Create Conc. Inst. &nbsp;&nbsp;',
				"type": "button",
				"htmlClass": "btn btn-transition btn-primary",
				"onClick": function (evt) { deselectTreeNode(); createChild('CONCEPT-INSTANCE'); }
			},
			/*{	"title": '<img src="/images/property-instance.png" width="18" height="18"/> Create Prop. Inst. &nbsp;&nbsp;',
				"type": "button",
				"htmlClass": "btn btn-transition btn-primary",
				"onClick": function (evt) { deselectTreeNode(); createChild('PROPERTY-INSTANCE'); }
			},*/
			/*{	"title": '<img src="/images/clear-18x18.png" width="20" height="20"/> Clear form &nbsp;&nbsp;',
				"type": "button",
				"htmlClass": "btn btn-transition btn-warning",
				"onClick": function (evt) { clearForm(); deselectTreeNode(); }
			},*/
			{	"title": '<img src="/images/delete-white-18x18.png" width="18" height="18"/> Delete Node &nbsp;&nbsp;',
				"type": "button",
				"htmlClass": "btn btn-transition btn-danger",
				"onClick": function (evt) { deleteNode(); deselectTreeNode(); }
			},
		],
	};
	
	$('#formButtons').jsonForm(options);
}

function initButtonsForm2() {
	$("#btn-import-casm").on('click', function(evt) {
        if (confirm('Import page will be loaded. Any unsaved work will be lost.\nContinue?')) {
                document.location = '/forms/admin/import.jsp?mode=APPEND';
        }
	});
	$("#btn-export-casm-as-ttl").on('click', function(evt) {
        exportPolicy('/gui/attributes/export-casm-as-rdf');
	});
}

function exportPolicy(url) {
    //console.log('Export CASM as RDF: Contacting '+url);
    document.location = url;
}

/*
 * Form preparation (before assigning data)
 */
function prepareForm(data) {
	setFormByNodeType(data.type, data.propertyType, data.range_display, data.propertyIsA_display);
	
	// enable/disable create buttons
	var type = data.type;
	if (type==='CONCEPT') {
		setFormButtonStates(true, true, true, false, true, true, true);
	} else
	if (type==='PROPERTY') {
		setFormButtonStates(false, false, false, false, true, true, true);
	} else
	if (type==='CONCEPT-INSTANCE') {
		setFormButtonStates(false, false, false, true, true, true, true);
	} else
	if (type==='PROPERTY-INSTANCE') {
		setFormButtonStates(false, false, false, false, true, true, true);
	}
	
	// enable/disable type buttons
	setFormTypeButtonState(data);
}

function setFormTitle(type, breadcrumb, isCreate) {
    // display the right form type
    $('#form-title-concept').hide();
    $('#form-title-property').hide();
    $('#form-title-concept-instance').hide();
    if (type==='CONCEPT') $('#form-title-concept').show();
    if (type==='PROPERTY')   $('#form-title-property').show();
    if (type==='CONCEPT-INSTANCE') $('#form-title-concept-instance').show();

    // display the breadcrumb
    if (breadcrumb!=='') {
        $('#form-title-breadcrumb').text(breadcrumb);
    } else {
        if (isCreate) {
            if (type==='CONCEPT') $('#form-title-breadcrumb').html('<i>&lt;&lt; New Concept &gt;&gt;</i>');
            if (type==='PROPERTY') $('#form-title-breadcrumb').html('<i>&lt;&lt; New Property &gt;&gt;</i>');
            if (type==='CONCEPT-INSTANCE') $('#form-title-breadcrumb').html('<i>&lt;&lt; New Concept Instance &gt;&gt;</i>');
        } else {
            $('#form-title-breadcrumb').html($("#name").val());
        }
    }
}

function setFormButtonStates(cc, cp, cci, cpi, save, clear, del) {
	$('#formButtons').find('button:contains("Create Concept")').prop( "disabled", !cc ).addClass( cc?'':'grayscale' ).removeClass( cc?'grayscale':'' );
	$('#formButtons').find('button:contains("Create Property")').prop( "disabled", !cp ).addClass( cp?'':'grayscale' ).removeClass( cp?'grayscale':'' );
	$('#formButtons').find('button:contains("Create Conc. Inst.")').prop( "disabled", !cci ).addClass( cci?'':'grayscale' ).removeClass( cci?'grayscale':'' );
	$('#formButtons').find('button:contains("Create Prop. Inst.")').prop( "disabled", !cpi ).addClass( cpi?'':'grayscale' ).removeClass( cpi?'grayscale':'' );
	//
	$('#formButtons').find('button:contains("Save")').prop( "disabled", !save ).addClass( save?'':'grayscale' ).removeClass( save?'grayscale':'' );
	$('#formButtons').find('button:contains("Clear")').prop( "disabled", !clear ).addClass( clear?'':'grayscale' ).removeClass( clear?'grayscale':'' );
	$('#formButtons').find('button:contains("Delete")').prop( "disabled", !del ).addClass( del?'':'grayscale' ).removeClass( del?'grayscale':'' );
}

function setFormTypeButtonState(data) {
	var is_loaded = $('#attrList').jstree(true).is_loaded(data.id);
	var is_leaf = $('#attrList').jstree(true).is_leaf(data.id);
	var enable = (is_loaded && is_leaf);
	
	enableFormTypeButtons(enable, enable, enable/*, false*/);
}
	
function enableFormTypeButtons(cc,pp,ci,pi) {
	$('#btn-set-type-concept') .prop( "disabled", !cc ).addClass( cc?'':'grayscale' ).removeClass( cc?'grayscale':'' );
	$('#btn-set-type-property').prop( "disabled", !pp ).addClass( pp?'':'grayscale' ).removeClass( pp?'grayscale':'' );
	$('#btn-set-type-instance').prop( "disabled", !ci ).addClass( ci?'':'grayscale' ).removeClass( ci?'grayscale':'' );
	//$('#btn-set-type-propinst').prop( "disabled", !pi ).addClass( pi?'':'grayscale' ).removeClass( pi?'grayscale':'' );
}

/*
 *  Form element's utility functions
 */
function setFormPropertyType(type) {
	if (type===0) {
		$("input[name='propertyType']").val('DataProperty');
		$("input[name='range']").parent().parent().parent().hide();
		$("input[name='range_display']").parent().parent().parent().hide();
		$("input[name='rangeUri']").parent().parent().parent().show();
	} else 
	if (type===1) {
		$("input[name='propertyType']").val('ObjectProperty');
		$("input[name='range']").parent().parent().parent().show();
		$("input[name='range_display']").parent().parent().parent().show();
		$("input[name='rangeUri']").parent().parent().parent().hide();
	} else {
		alert('Invalid Property Type: '+type);
	}
}

function correctFieldDimensions() {
    // resize 'description' textarea to align to other fields
    var nameFieldWidth =
            $("input[name=name]").outerWidth()
            + $("input[name=name]").parent().children().last().outerWidth();
    $("textarea[name=description]").outerWidth(nameFieldWidth);

    // resize 'propertyType' input to align end to other fields
    var outcomeInputWidth = $("input[name=propertyType]").outerWidth();
    var outcomeFieldWidth =
            outcomeInputWidth
            + $("input[name=propertyType]").parent().children().last().outerWidth();
    var diff = outcomeFieldWidth - nameFieldWidth;
    $("input[name=propertyType]").outerWidth(outcomeInputWidth - diff);

    // resize '.toggle-control' toggles to fit in their lines
    $('.toggle-control').each(function () {
        var h = $(this).closest('.add-on').outerWidth(nameFieldWidth);
    })
}

/*$(document).ready(function() {    // Not working...
    correctFieldDimensions();
});*/
$(window).on('resize', function() {
    correctFieldDimensions();
});
