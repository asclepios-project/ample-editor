/*
 * Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// 'policyimages' jsTree plugin: ABAC and ABE icons on node (outside of anchor)
(function ($, undefined) {
	"use strict";
	var html =
	    '<span class="policyimages" style="position:relative; float:right; padding-left:10px; padding-right:5px;">'
	    + '<img src="/images/abac/abac-policy-16x16-4.png" class="policyimages-abac" />&nbsp;'
	    + '<img src="/images/abe/abe-policy-16x16.png" class="policyimages-abe" />'
	    + '</span>';
	var policyimagesSpan = $(html);

	$.jstree.defaults.policyimages = $.noop;
	$.jstree.plugins.policyimages = function (options, parent) {
		/*this.bind = function () {
			parent.bind.call(this);
			this.element
				.on("click.jstree", ".policyimages", $.proxy(function (e) {
						e.stopImmediatePropagation();
						this.settings.policyimages.call(this, this.get_node(e.target));
					}, this));
		};*/
		this.teardown = function () {
			if(this.settings.policyimages) {
				this.element.find(".policyimages").remove();
			}
			parent.teardown.call(this);
		};
		this.redraw_node = function(obj, deep, callback, force_draw) {
		    var node = obj;
			obj = parent.redraw_node.call(this, obj, deep, callback, force_draw);
			if (obj && obj.childNodes && obj.childNodes.length && obj.childNodes.length>2) {
			    if ($.isPlainObject(node) && node.id) node = node.id;
			    node = treeAttributes.jstree().get_node(node);
				var piClone = policyimagesSpan.clone();
			    if (node && node.original) {
			        if (node.original.inAbac===false) piClone.find('.policyimages-abac').css('visibility', 'hidden');
			        if (node.original.inAbe===false) piClone.find('.policyimages-abe').css('visibility', 'hidden');
			    }
				piClone.insertAfter(obj.childNodes[2]);
			}
			return obj;
		};
	};
})(jQuery);

/*
 *	jsTree and hotkeys initialization
 */
		/*
		 * Initialize jstree and load initial data
		 */
		// Attributes jsTree
		var treeAttributes;

		$( document ).ready(function() {
			// Initialize json forms
			initAttributeEditForm();
			initAttributeButtonsForm();
			initButtonsForm2();

            // Initialize form switches (for inAbac, inAbe)
			$('.toggle-control').bootstrapToggle({
			    on: 'Yes',
			    off: 'No',
			    onstyle: 'success',
			    offstyle: 'danger',
			    style: 'toggle-padding',
			    width: 10,
			    height: 20,
			})
			.on('change', function(e) {
			    $(e.target).closest('.add-on').prev().val(e.target.checked);
                e.stopImmediatePropagation();
			})
			;
			$('.toggle-value').hide();
			
			// Initialize attribute tree
			//$.jstree.defaults.core.data = true;
			treeAttributes = $('#attrList').jstree({
			    "core" : {
			        "animation" : 500,
			        "multiple": false,
                    "check_callback" :
                        function (operation, node, node_parent, node_position, more) {
                            // operation can be 'create_node', 'rename_node', 'delete_node', 'move_node', 'copy_node' or 'edit'
                            // in case of 'rename_node' node_position is filled with the new node name

                            if (operation==="move_node" && more && more.pos && more.ref && more.ref.original && more.ref.original.type)
                                return more.pos!=='i'
                                    || more.pos==='i' && more.ref.original.type==='CONCEPT';
                            return true;
                        },
                    "themes" : { "stripes" : true },
                    'data' : {
                        'url': function (node) {
                                    var url = node.id === '#' ?
                                            baseUrl+'/gui/attributes/get-toplevel-attributes' :
                                            baseUrl+'/gui/attributes/get-attribute-children/'+node.id;
                                    //alert('URL: '+url);
                                    return url;
                                },
                        'data': function (node) { /*alert('DATA: '+JSON.stringify(node));*/ return ''; }
                    }
			    },
                "dnd" : {
                    'copy' : false,
                    'inside_pos' : 'last',
                    'is_draggable' : function (nodeArray) {
                            //console.log('is_draggable:  ', nodeArray);
                            if (nodeArray.length==0) alert('No node selected for drag');
                            if (nodeArray.length>1) alert('Many nodes selected for drag');
                            return true;
                        },
                },
                "contextmenu" : {
                    "items": function($node) {
                        var tree = $("#attrList").jstree(true);
                        var sel = tree.get_selected(true);
                        var type = sel[0].original.type;

                        var createItemsArr;
                        if (type==='CONCEPT') {
                            createItemsArr = {
                                "create_child_concept": {
                                    "separator_before": false,
                                    "separator_after": false,
                                    "label": "New Concept",
                                    "action": function (obj) {
                                        createChild('CONCEPT');
                                    },
                                },
                                "create_child_instance": {
                                    "separator_before": false,
                                    "separator_after": false,
                                    "label": "New Concept Instance",
                                    "action": function (obj) {
                                        createChild('CONCEPT-INSTANCE');
                                    },
                                },
                                "create_child_property": {
                                    "separator_before": false,
                                    "separator_after": false,
                                    "label": "New Property",
                                    "action": function (obj) {
                                        createChild('PROPERTY');
                                    },
                                },
                            };
                        } else
                        /*if (type==='CONCEPT-INSTANCE') {
                            createItemsArr = {
                                "create_child_prop_instance": {
                                    "separator_before": false,
                                    "separator_after": true,
                                    "label": "New Property Instance",
                                    "action": function (obj) {
                                        createChild('PROPERTY-INSTANCE');
                                    },
                                },
                            };
                        } else*/
                        if (type==='PROPERTY') {
                            createItemsArr = { };
                        } else
                        /*if (type==='PROPERTY-INSTANCE') {
                            createItemsArr = { };
                        } else */
                        {
                            createItemsArr = { };
                        }

                        var commonItemsArr = {
                            "expand_all": {
                                "separator_before": true,
                                "separator_after": false,
                                "label": "Expand all",
                                "action": function (obj) {
                                    expand_all();
                                }
                            },
                            "collapse_all": {
                                "separator_before": false,
                                "separator_after": false,
                                "label": "Collapse all",
                                "action": function (obj) {
                                    collapse_all();
                                }
                            },
                            "clear": {
                                "separator_before": true,
                                "separator_after": false,
                                "label": "Clear form",
                                "action": function (obj) {
                                    clearForm();
                                    deselectTreeNode();
                                }
                            },
                            // set/clear in-ABAC and in-ABE flags
                            "useInPolicies": {
                                "separator_before": true,
                                "separator_after": true,
                                "label": "Use in policies...",
                                "submenu": {
                                    "setInAbac": {
                                        "separator_before": false,
                                        "separator_after": false,
                                        "label": "Use in ABAC policies",
                                        "action": function (obj) {
                                            setUsePolicies('set', 'abac');
                                        }
                                    },
                                    "clearInAbac": {
                                        "separator_before": false,
                                        "separator_after": true,
                                        "label": "Don't use in ABAC policies",
                                        "action": function (obj) {
                                            setUsePolicies('clear', 'abac');
                                        }
                                    },
                                    "setInAbe": {
                                        "separator_before": true,
                                        "separator_after": false,
                                        "label": "Use in ABE policies",
                                        "action": function (obj) {
                                            setUsePolicies('set', 'abe');
                                        }
                                    },
                                    "clearInAbe": {
                                        "separator_before": false,
                                        "separator_after": false,
                                        "label": "Don't use in ABE policies",
                                        "action": function (obj) {
                                            setUsePolicies('clear', 'abe');
                                        }
                                    },
                                }
                            },
                            // Delete option
                            "remove": {
                                "separator_before": true,
                                "separator_after": false,
                                "label": "Delete",
                                "action": function (obj) {
                                    deleteNode();
                                    deselectTreeNode();
                                }
                            },
                        };

                        var ctxMenu = $.extend(createItemsArr, commonItemsArr);
                        return ctxMenu;
                    }
                },
                "types" : {
                    "#" : {
                      "max_children" : 1,
                      "valid_children" : ["root"]
                    },
                    "root" : {
                      "icon" : "/static/3.0.0/assets/images/tree_icon.png",
                      "valid_children" : ["default"]
                    },
                    "default" : {
                      "valid_children" : ["default","file"]
                    },
                    "file" : {
                      "icon" : "glyphicon glyphicon-file",
                      "valid_children" : []
                    }
                },
                "plugins" : [
                    "contextmenu", "dnd", //"search",
                    "state", "types", "wholerow", "hotkeys",
                    //"sort"
                    "policyimages"
                ]
			})
			.on('changed.jstree', function (e, data) {
				// select a property for PropertyIsA field
				if (selectTreeNode_State==='SELECT_NODE') {
					if (data && data.selected && data.selected.length && data.node && data.node.id) {
						// accept 'acceptedType' nodes and ignore the rest
						if (data.node.original.type===selectTreeNode_acceptedType || selectTreeNode_acceptedType==='*' || selectTreeNode_acceptedType==='ANY') {
							// update json form
							setFormFieldToNode(data.node['id'], data.node['text']);
						} else
						// if 'acceptedType'=='PARENT' accept CONCEPT nodes for concept, property and concept instance nodes, and CONCEPT-INSTANCE for property instance nodes
						if (selectTreeNode_acceptedType==='PARENT') {
							// get node type
							var type = $('input[name="type"]').val();
							if (/*type==='PROPERTY-INSTANCE' && data.node.original.type==='CONCEPT-INSTANCE' ||
								type!=='PROPERTY-INSTANCE' && */data.node.original.type==='CONCEPT')
							{
								// update json form
								setFormFieldToNode(data.node['id'], data.node['text']);
							}
						} else {
							console.log('Not a property: '+data.node.id);
						}
					} else {
						console.log('ERROR: "data" or "data.selected" or "data.node" or "data.node.original" not defined');
					}
					return;
				}
				// ending node selection
				if (selectTreeNode_State==='IGNORE') {
					//treeAttributes.jstree("select_node", selectedNodeIdBefore);
					return;
				}
				
				// Update json form with selected node information
				// An AJAX call will be started
				$('input[name=id]').prop('readonly', true);
				if(data && data.selected && data.selected.length) {
					startTm = new Date().getTime();
					var url = baseUrl+'/gui/attributes/get-attribute/'+data.selected;
					statusContacting(url, 'n/a');
					clearForm();
					callStartTm = new Date().getTime();
					$.get(url, function (d) {
						if(d) {
							renderData(d, 'obviously OK');
						}
					});
				}
				else {
					$('#data .content').hide();
					$('#data .default').html('---').show();
				}
			})
			.on('loaded.jstree', function() {
				// check if tree view is empty
				if (treeAttributes.jstree(true)._model.data['#'].children.length==0) {
					$('#formButtons').find('button:contains("Create Root")').show();
				} else {
					$('#formButtons').find('button:contains("Create Root")').hide();
				}
			})
			.on('load_node.jstree', function(e, data) {
				// enable/disable type buttons
				setFormTypeButtonState(data.node);
			})
			.on('ready.jstree', function(e, data) {
				//console.log('CALL ready.jstree');
				showOrHideEmptyTreeMessage();
				fixTreeContainerHeight();
			})
			.on('refresh.jstree', function(e, data) {
				//console.log('CALL refresh.jstree');
				showOrHideEmptyTreeMessage();
				fixTreeContainerHeight();
			})
			.on('move_node.jstree', function(e, data) {
                //console.log('MOVE_NODE: node: ', data.node.id, data.node.text);
  				if (data.parent!==data.old_parent) {
				    //console.log('MOVE_NODE: from-policy="'+data.old_parent+'", to-policy="'+data.parent+'"');
				    setField('parent', data.parent);
				    changeParent(data.parent);

                    var parentNode = $('#attrList').jstree('get_node', data.parent);
                    var originalOrder = parentNode.children;
				    if (confirm('Move "'+data.node.text+'" to concept "'+parentNode.text+'"?')) {
				        // update node data
				        var node = treeAttributes.jstree(true).get_node(data.node.id);
				        if (node.data && node.data!=null) node.data.parent = parentNode.data;

				        // save node changes
				        saveNode();
				        saveChildrenOrder(data.parent, originalOrder);
				    } else
				        reloadTree();
				} else {
				    console.log('MOVE_NODE: reordering under same parent');
				    saveChildrenOrder(data.parent);
				}
			})
			;

            // add slim scrollbar to jsTree
            fixTreeContainerHeight();

			// make readonly fields... readonly
			prepareEditForm();
			
			//initTreeHotkeys();
		});
		
		function showOrHideEmptyTreeMessage() {
			// check if tree contains nodes
			var rootNode = treeAttributes.jstree(true).get_node('#');
			if (rootNode && rootNode.children) {
				if (rootNode.children.length==0) $('#empty_tree_mesg').show();
				else $('#empty_tree_mesg').hide();
			} else
				//console.log('ready.jstree: Could not get root node')
				;
		}

        function fixTreeContainerHeight() {
            // size jsTree container
            var diff = $('#treeContainer').offset().top;
            diff += $('#formButtons2').height();
            diff += $('#treeInstructions').height();
            diff += 20;
            $('#treeContainer').height(document.documentElement.clientHeight - diff);
		}

		/*
		 *	Hotkeys initialization
		 */
		function initTreeHotkeys() {
			var tree = $("#attrList");	//jQuery(document);
			tree.bind('keydown', 'insert',function (evt){ createSibling(); return false; });
			tree.bind('keydown', 'Shift+insert',function (evt){ createChild(); return false; });
			tree.bind('keydown', 'del',function (evt){ deleteNode(); return false; });
		}
