<!DOCTYPE html>
<!--
  ~
  ~ Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~      http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  ~
  -->
<%@ include file="../../includes/prelude.html" %>
<html>
  <head>
<% pageTitle = "ASCLEPIOS Context-Aware Security Model"; %>
<%@ include file="../../includes/head.html" %>
	<link rel="stylesheet" href="../slick/common-grid-styles.css" type="text/css"/>
    <link rel="stylesheet" style="text/css" href="/forms/admin/common.css" />
    <link rel="stylesheet" style="text/css" href="/forms/deps/opt/bootstrap.css" />

    <link href="/forms/deps/bootstrap2-toggle.min.css" rel="stylesheet">
    <style>
        .toggle-padding {
            padding-top: 0px !important;
            padding-bottom: 0px !important;
            border-radius: 5px !important;
        }
    </style>
    <script src="/forms/deps/bootstrap2-toggle.min.js"></script>

  </head>
  <body>
	
<%@ include file="../../includes/header.html" %>
	
				<div class="row-fluid" style="margin-top: 20px;">
					<div id="leftContainer" class="span3">
						<div >
							<!-- Legent line -->
							<p align="center"><font size="-1"><i>
								<img src="/images/concept.png" width="18" height="18"/> Concept &nbsp;&nbsp;
								<img src="/images/property.png" width="12" height="12"/> Property &nbsp;&nbsp;
								<img src="/images/concept-instance.png" width="12" height="12"/> Concept Instance
								<!--<img src="/images/property-instance.png" width="12" height="12"/>Property Inst.-->
								<br/>
								Usable in: &nbsp;&nbsp;
								<img src="/images/abac/abac-policy-16x16-4.png" /> ABAC policies &nbsp;&nbsp;
								<img src="/images/abe/abe-policy-16x16.png" /> ABE policies
							</i></font></p>
						</div>
						<!-- Schema tree view -->
						<div class="span12" id="treeContainer" style="overflow-y: scroll">
							<!-- jstree -->
							<div id="attrList"></div>
						</div>
                        <!-- Extra buttons -->
                        <!--<div id="formButtons2" class="span12" style="padding: 0px; float: center;"></div>-->
                        <div id="formButtons2" class="span12 container-fluid" style="padding: 0px; float: center;">
                            <div class="row-fluid">
                                <button id="btn-import-casm" class="btn btn-transition btn-danger span6"><img src="/images/upload-white-32x32.png" width="18" height="18"/> Import CASM </button>
                                <button id="btn-export-casm-as-ttl" class="btn btn-transition btn-info span6"><img src="/images/download-white-32x32.png" width="18" height="18"/> Export CASM </button>
                            </div>
                        </div>
                        <!-- Instructions -->
                        <div class="span12" id="treeInstructions">
                            <div style="font:italic 10pt arial,sans-serif; border-top:black 1pt solid; margin-top:10px; padding-top:15px;">Use <strong>Del</strong>, <strong>Ins</strong> and <strong>Shift+Ins</strong> hotkeys to respectively delete selected, create sibling and create child node.</div>
                            <div id="empty_tree_mesg" class="alert" style="color:black; font-size:10pt; font-style:italic;"><blockquote>
                                &rtrif; Create a new ASCLEPIOS Context-Aware Security Model by clicking <b>Create Root Concept</b> button or<br/>
                                &rtrif; Import ASCLEPIOS Context-Aware Security Model from an RDF/TTL file by clicking on <b>Import Model</b> menu item
                            </blockquote></div>
                        </div>
					</div>
					<div id="formContainer" class="span9">
						<!--<div class="alert"></div>-->
						<div style="font-size: 12pt; font-weight: bold; font-style: italic; background: #eeeeee; padding: 2px; padding: 10px 10px 10px 10px;">
						    <span id="form-title-concept"><img src="/images/concept.png"/>&nbsp;Concept details</span>
						    <span id="form-title-property"><img src="/images/property.png"/>&nbsp;Property details</span>
						    <span id="form-title-concept-instance"><img src="/images/concept-instance.png"/>&nbsp;Concept Instance details</span>
						    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						    <span id="form-title-breadcrumb" style="font-weight: normal; font-style: normal; font-size: 150%"></span>
						</div>
						<p style="margin: 0px;"><hr /></p>
						<!--<p id="update_notice" class="alert" style="display: none"></p>-->
						<form id="formEdit"></form>
						<p><hr /></p>
						<div id="formButtons"></div>
						<p><br/></p>
						<!--<div id="res" class="alert">
							...<br/>
						</div>-->
						<!--<div id="res" class="alert"></div>-->
					</div>
				</div>
				
<%@ include file="../../includes/js-libs.html" %>
    <script type="text/javascript" src="/forms/casm/form-casm.js"></script>

				<script type="text/javascript">
					// Override form-casm vars to meet this form's fields
					var formFields = ['id', /*'owner',*/ 'uri', 'type', 'name', 'description', /*'createTimestamp', 'lastUpdateTimestamp',*/ 'parent' /*, 'source'*/, 'propertyType', 'range', 'range_display', 'rangeUri', 'propertyIsA', 'propertyIsA_display', 'propertyValue', 'inAbac', 'inAbe'];
					var emptyData = {'id':'', /*'owner':'',*/ 'uri':'', 'type':'', 'name':'', 'description':'', /*'createTimestamp':'', 'lastUpdateTimestamp':'',*/ 'parent':'' /*, 'source':''*/, 'propertyType':'', 'range':'', 'range_display':'', 'rangeUri':'', 'propertyIsA':'', 'propertyIsA_display':'', 'propertyValue':'', 'inAbac':true, 'inAbe':true};
					formSelector = "#formEdit";
					statusSelector = '#res';
				</script>
				
				<!-- Main Form initialization -->
				<script type="text/javascript" src="casm-mgnt-jsonform.js"></script>
				
				<!-- Schema operation functions -->
				<script type="text/javascript" src="casm-mgnt-node-ops.js"></script>
				
				<!-- jsTree and hotkeys initialization -->
				<script type="text/javascript" src="casm-mgnt-init.js"></script>
	
				<script><!--
				    $(function() {
				        correctFieldDimensions();
                    });
				//--></script>

<%@ include file="../../includes/footer.html" %>
  </body>
</html>
<%@ include file="../../includes/debug.html" %>
<%@ include file="../../includes/trail.html" %>