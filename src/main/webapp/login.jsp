<!DOCTYPE html>
<!--
  ~
  ~ Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~      http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  ~
  -->
<%@ include file="includes/prelude.html" %>
<%
boolean loginFailed = request.getParameter("loginFailed")!=null;
%>
<html>
<head>
	<title><%= editorFullName %></title>
	<meta charset="utf-8" />
	<link rel="stylesheet" style="text/css" href="/forms/deps/opt/bootstrap.css" />
	<link rel="stylesheet" href="/forms/slick/common-grid-styles.css" type="text/css"/>
	<style>
		input[type="text"], input[type="password"]
		{
			height: 40px;
		}
	</style>
</head>
<body style="padding:10px;">
	<h1 align="center"><%= editorFullName %></h1>
	<c:out value="${lalala}" ></c:out>
	<p><br/></p>
	<% if (loginFailed) { %>
	<p align="center">
		<font size="+2" color="red">Authentication failed</font>
	</p>
	<% } %>
	<p><br/></p>
	<form action="/login" method="POST" xxxenctype="application/x-www-form-urlencoded" xxxaccept-charset="UTF-8">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		<div align=center>
			<table border="0" cellpadding="2">
				<tr>
					<td valign="middle"><em><label for="password">Username:</label></em>&nbsp;</td>
					<td valign="middle"><input type="text" id="username" name="username" size="12" value="" placeholder="Username" required autofocus></td>
				</tr>
				<tr>
					<td valign="middle"><em><label for="password">Password:</label></em>&nbsp;</td>
					<td valign="middle"><input type="password" id="password" name="password" size="12" value="" placeholder="Password" required></td>
				</tr>
				<tr>
					<td colspan="2"><p align="center" style="text-align:center">
						<input class="btn btn-info" id="btn_submit" type="submit" value="Login" name="submit">
					</p></td>
				</tr>
			</table>
		</div>
	</form>
	<div><div><div>
	<% boolean isStandalone = true;
	%><%@ include file="includes/footer.html" %>
</body>
</html>
<%@ include file="includes/debug.html" %>