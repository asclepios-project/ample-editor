<!--
  ~
  ~ Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~      http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  ~
  -->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>2.2.1.RELEASE</version>
	</parent>

	<groupId>eu.asclepios</groupId>
	<artifactId>ample-editor</artifactId>
	<version>1.5.0</version>

	<name>ASCLEPIOS Models and Policy Editor (AMPLE)</name>
	<description>ASCLEPIOS editor for Metadata Schema Models, ABAC &amp;amp; ABE policies, and policy validation</description>
	<!--<url>http://imu.ntua.gr/software/ample</url>-->

	<organization>
		<name>Information Management Unit, Institute of Communication and Computer Systems, National Technical University of Athens</name>
		<url>http://imu.iccs.gr/</url>
	</organization>
	<licenses>
		<license>
			<name>Apache License, Version 2.0</name>
			<url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
			<distribution>repo</distribution>
			<comments>A business-friendly OSS license</comments>
		</license>
	</licenses>
	
	<properties>
		<!-- dependency properties -->
		<jasypt.starter.version>3.0.3</jasypt.starter.version>
		<lombok.version>1.18.10</lombok.version>
		<fuseki.version>3.14.0</fuseki.version>
		<swagger.version>2.9.2</swagger.version>

		<!-- maven plugin properties -->
		<docker-maven-plugin.version>0.33.0</docker-maven-plugin.version>
		<maven-buildnumber-plugin.version>1.4</maven-buildnumber-plugin.version>
		<maven-javadoc-plugin.version>3.2.0</maven-javadoc-plugin.version>
	</properties>
	
	<dependencies>
		<!-- Spring-boot dependencies -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-security</artifactId>
		</dependency>
		<!--<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-configuration-processor</artifactId>
		</dependency>-->
		<dependency>
			<groupId>com.github.ulisesbocchio</groupId>
			<artifactId>jasypt-spring-boot-starter</artifactId>
			<version>${jasypt.starter.version}</version>
		</dependency>

		<!-- JSP dependencies -->
		<!-- JSTL for JSP -->
		<dependency>
			<groupId>javax.servlet</groupId>
			<artifactId>jstl</artifactId>
		</dependency>
		<!-- Need this to compile JSP -->
		<dependency>
			<groupId>org.apache.tomcat.embed</groupId>
			<artifactId>tomcat-embed-jasper</artifactId>
			<!--<scope>provided</scope>-->
		</dependency>

		<!-- Lombok project -->
		<dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok</artifactId>
			<version>${lombok.version}</version>
			<scope>provided</scope>
		</dependency>

		<!-- Apache Jena support -->
		<dependency>
			<groupId>org.apache.jena</groupId>
			<artifactId>apache-jena</artifactId>
			<type>pom</type>
			<version>${fuseki.version}</version>
			<exclusions>
				<exclusion>
					<groupId>org.slf4j</groupId>
					<artifactId>slf4j-api</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.slf4j</groupId>
					<artifactId>slf4j-log4j12</artifactId>
				</exclusion>
				<exclusion>
					<groupId>log4j</groupId>
					<artifactId>log4j</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.apache.jena</groupId>
			<artifactId>jena-fuseki-main</artifactId>
			<version>${fuseki.version}</version>
		</dependency>

		<!-- Swagger -->
		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-swagger2</artifactId>
			<version>${swagger.version}</version>
		</dependency>
		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-swagger-ui</artifactId>
			<version>${swagger.version}</version>
		</dependency>

	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
			</plugin>
			<plugin>
				<artifactId>maven-compiler-plugin</artifactId>
				<configuration>
					<source>1.8</source>
					<target>1.8</target>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>buildnumber-maven-plugin</artifactId>
				<version>${maven-buildnumber-plugin.version}</version>
			</plugin>
		</plugins>
	</build>

	<profiles>
		<profile>
			<id>with-docker</id>
			<build>
				<plugins>

					<plugin>
						<artifactId>maven-resources-plugin</artifactId>
						<version>3.2.0</version>
						<executions>
							<execution>
								<id>copy-resources-to-docker-context</id>
								<phase>package</phase>
								<goals>
									<goal>copy-resources</goal>
								</goals>
								<configuration>
									<outputDirectory>${project.build.directory}/docker-context</outputDirectory>
									<resources>
										<resource>
											<directory>${project.basedir}/src/main/docker</directory>
										</resource>
										<resource>
											<directory>${project.basedir}/</directory>
											<includes>
												<include>src/main/webapp/**</include>
											</includes>
										</resource>
										<resource>
											<directory>${project.basedir}</directory>
											<includes>
												<include>bin/*.sh</include>
												<include>config/**</include>
												<include>LICENSE*</include>
												<include>README*</include>
											</includes>
										</resource>
									</resources>
								</configuration>
							</execution>
						</executions>
					</plugin>

					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-dependency-plugin</artifactId>
						<executions>
							<execution>
								<id>copy-jar</id>
								<phase>package</phase>
								<goals>
									<goal>copy</goal>
								</goals>
								<configuration>
									<artifactItems>
										<artifactItem>
											<groupId>${project.groupId}</groupId>
											<artifactId>${project.artifactId}</artifactId>
											<version>${project.version}</version>
											<!--<classifier>exec</classifier>-->
											<type>jar</type>
											<outputDirectory>${project.build.directory}/docker-context</outputDirectory>
											<destFileName>${project.artifactId}.jar</destFileName>
										</artifactItem>
									</artifactItems>
								</configuration>
							</execution>
						</executions>
					</plugin>

					<plugin>
						<groupId>io.fabric8</groupId>
						<artifactId>docker-maven-plugin</artifactId>
						<version>${docker-maven-plugin.version}</version>
						<configuration>
							<!--<dockerHost>https://localhost:2376</dockerHost>-->
							<verbose>true</verbose>
							<useColor>true</useColor>
							<images>
								<image>
									<name>%g/%a:%l</name>
									<build>
										<dockerFileDir>${project.build.directory}/docker-context</dockerFileDir>
									</build>
									<run>
										<ports>
											<port>9090:9090</port>
										</ports>
									</run>
								</image>
							</images>
						</configuration>
						<executions>
							<execution>
								<id>docker-image-build</id>
								<phase>install</phase>
								<goals>
									<goal>build</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>

	<reporting>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<version>${maven-javadoc-plugin.version}</version>
				<configuration>
					<reportOutputDirectory>/</reportOutputDirectory>
					<destDir>ample-api-docs</destDir>
				</configuration>
			</plugin>
		</plugins>
	</reporting>
</project>