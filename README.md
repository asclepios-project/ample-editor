<!--
  ~
  ~ Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~      http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  ~
  -->
# ASCLEPIOS Models and Policies Editor (AMPLE)

*ASCLEPIOS Models and PoLicies Editor*, or *AMPLE*, is a graphical web-based environment that provides the necessary design-time tools for creating, maintaining and verifying access control policies of ASCLEPIOS platform. AMPLE provides an editor for maintaining and extending ASCLEPIOS Context-Aware Security Model (CASM), two policy editors for developing ABAC and ABE policies, and a Policy Validation module for checking policy correctness, completeness and security awareness.

Source and binary distributions of AMPLE can be downloaded from [Releases page](https://gitlab.com/asclepios-project/ample-editor/-/releases). Docker images are also [available](https://gitlab.com/asclepios-project/abac-authorization/container_registry).



**Contents:**

- [Install and run AMPLE](#install-and-run-ample) [for Testing or Home usage]
- [Build from source](#build-from-source) [for Developers]
- [Run AMPLE with Docker](#run-ample-with-docker) [for Testing or Home usage]
- [Run AMPLE with Docker Compose](#run-ample-with-docker-compose) [for Testing or Home usage]
- [Deploy AMPLE service in Docker Swarm](#deploy-ample-service-in-docker-swarm) [Secure solution]
- [Deploy AMPLE stack in Docker Swarm](#deploy-ample-stack-in-docker-swarm) [Secure solution]
- [Swagger documentation](#swagger-documentation)



------

### Install and Run AMPLE

##### *For Testing or Home usage*

AMPLE installation involves downloading and extracting an AMPLE binary distribution, and initializing its secrets.

**Note:** This method does not provide a secure solution and therefore should not be used in production environments. It is offered for testing, development or home usage.

##### Download

1. Download one of AMPLE distributions from Releases page: https://gitlab.com/asclepios-project/ample-editor/-/releases. For example:

   ````sh
   wget https://gitlab.com/api/v4/projects/18795478/packages/generic/ample-editor/1.5.0/ample-editor-1.5.0.tar.gz
   ````

2. Extract distribution file into an empty folder. For example:

   ```sh
   tar xzvf ample-editor-1.5.0.tar.gz
   ```



##### Secrets initialization

Secrets are the key stores  holding AMPLE's private key (for HTTPS), the corresponding certificate, as well as various passwords and API Keys for PDP and PAP services. Private Key is stored in a key store, typically named `ample-editor-ks.p12`. AMPLE certificate and other services certificates are stored in a key store, typically named `ample-editor-ts.p12`. Passwords, API Keys and the location of key stores are saved in a secrets property file, typically named `ample-editor-secret.properties`. AMPLE provides a utility to easily initialize or generate the secrets.

**Note 1:** Secrets must be kept in a safe location, which might be different than AMPLE configuration directory.

**Note 2:** Passwords in `ample-editor-secret.properties` file can be encrypted with a **master password**, referred as Jasypt password. This is an extra measure of protection. If set, master password must be entered every time AMPLE starts, or stored in `JASYPT_PASSWORD` environment variable.

Script `bin/init-secrets.sh` or `bin\init-secrets.bat` can be used to initialize secrets. The script will ask user to enter key store passwords, entry name, API Keys, Admin password, Jasypt (master) password as well as usernames/passwords of additional users. For each question it will suggest a random value as default, therefore just pressing enter in every question will eventually initialize secrets and store them in AMPLE's configuration directory.

The script usage is `init-secrets.sh [--auto-init] [OUTPUT_DIR]`.
The `OUTPUT_DIR` defaults to `config` directory of AMPLE.

*Linux:*

```
cd ample-editor
./bin/init-secrets.sh
```

*Windows:*

```
cd ample-editor
bin\init-secrets.bat
```

*Output:*

```
Keystore password [4r_fTfo$bly]:
Truststore password [4r_fTfo$bly]:
Entry alias [ample-editor]:
Keystore files prefix [ample-editor]:
PAP endpoint API Key []:
ABE endpoint API Key []:
Admin user password [TEQO[e+IK3=v&=]: admin
Keystore created
Certificate created (DER)
Certificate created (PEM)
Truststore created

Set a Jasypt password to enable encryption of secrets. Leave password empty to skip encryption
Jasypt password:
Jasypt encryption disabled
Add more users? (y/n) [n]:
Secrets properties saved at: C:\ample-editor\config\ample-editor-secret.properties
```



##### Auto-generate secrets

In order to generate secrets quietly (without asking) the `--auto-init` flag can be used.

**Note:** `--auto-init` flag does not set Jasypt password, therefore secrets (except admin password) are stored in clear text.

*Linux:*

```
cd ample-editor
./bin/init-secrets.sh --auto-init /my/secure/location 
```

*Windows:*

```
cd ample-editor
bin\init-secrets.bat --auto-init C:\my\secure\location 
```

*Output:*

```
Keystore created
Certificate created (DER)
Certificate created (PEM)
Truststore created
Secrets properties saved at: /my/secure/location/ample-editor-secret.properties
Admin password: ~`u-WAST
```



**Note:** You can add certificates of other services (like ABAC PDP/PAP server) into AMPLE trust store. For example:

```sh
keytool -import -noprompt -keystore /path/to/ample-editor-ts.p12 -file /path/to/pdp-server.crt -alias pdp-server
```



##### Run AMPLE Editor

*Linux:*

```
cd ample-editor
./bin/run.sh
```

*Windows:*

```
cd ample-editor
bin\run.bat
```

**Note:** You must provide Jasypt password at the console or set it in `JASYPT_PASSWORD` environment variable. If no Jasypt password has been set then add `-njp` flag to prevent AMPLE from asking it.

```sh
./bin/run.sh -njp
```



In order to suppress console output add a single dash `-` after `run.bat` or `run.sh` in above commands. For example

*Linux*

```sh
./bin/run.sh - -njp
```

*Windows*

```
bin\run.bat - -njp
```



##### Access and Use AMPLE

Using a web browser go to address: https://localhost:9090.

Since HTTPS private key is self-signed browser will give a warning. Just select to continue.



------

### Build from source

##### *For Developers*

AMPLE source code is available at Gitlab at https://gitlab.com/asclepios-project/ample-editor.

##### *Prerequisites*

Building AMPLE requires having Git, Apache Maven and Java 8 already installed in your system. In order to build Docker image requires having Docker system installed as well.

##### *Building AMPLE*

```sh
git clone https://gitlab.com/asclepios-project/ample-editor.git
cd ample-editor
maven clean install
```

In order to build a Docker image the last command must change to:

```sh
mvn clean install -P with-docker
```

The Docker image produced will be named: `asclepios/ample-editor:1.5.0`



------

### Run AMPLE with Docker

##### *For Testing or Home usage*

An alternative to downloading and installing an AMPLE distribution is to use an already built Docker image. Again secrets initialization is required.

**Note:** This method does not provide a secure solution and therefore should not be used in production environments. It is offered for testing, development or home usage.

##### Quick Start

```sh
docker run -it -p 9090:9090 registry.gitlab.com/asclepios-project/ample-editor:1.5.0 --auto-init
```

**Note 1:** `--auto-init` flag initializes secrets with random values. **CAUTION:** It will overwrite any previous secret files.
**Note 2:** `--auto-init` flag does not set Jasypt password, therefore secrets (except admin password) are stored in clear text. Use `init-secrets` or `init-secrets-and-run` options to set Jasypt password. For example:

```sh
docker run -it -p 9090:9090 registry.gitlab.com/asclepios-project/ample-editor:1.5.0 init-secrets-and-run
```



##### Initialize secrets

The Docker container will be used to initialize secrets. Secrets will be stored inside Docker container and hence they will be lost when container exists. In order to preserve them you must specify a volume or mount a directory of host system to a container path. **Caution:** Volumes is not a safe place to store secrets.

Using a volume:

```sh
docker run -it -v secrets_vol:/ample/secrets registry.gitlab.com/asclepios-project/ample-editor:1.5.0 init-secrets /ample/secrets
```

Mounting a host system directory:

*Linux:*

```sh
docker run -it -v /path/to/secure/location:/ample/secrets registry.gitlab.com/asclepios-project/ample-editor:1.5.0 init-secrets /ample/secrets
```

*Windows:*

```
docker run --rm -it -v "C:\path\to\secure\location":/ample/secrets registry.gitlab.com/asclepios-project/ample-editor:1.5.0 init-secrets /ample/secrets
```



**Note:** You can add certificates of other services (like ABAC PDP/PAP server) into AMPLE trust store. For example:

```sh
keytool -import -noprompt -keystore /path/to/secure/location/ample-editor-ts.p12 -file /path/to/pdp-server.crt -alias pdp-server
```



##### Start AMPLE Docker container

Using a volume:

```sh
docker run -it -v secrets_vol:/ample/secrets -p 9090:9090 registry.gitlab.com/asclepios-project/ample-editor:1.5.0 /ample/secrets
```

Mounting a host system directory:

*Linux:*

```sh
docker run -it -v /path/to/secure/location:/ample/secrets -p 9090:9090 registry.gitlab.com/asclepios-project/ample-editor:1.5.0 /ample/secrets
```

*Windows:*

```
docker run -it -v "C:\path\to\secure\location":/ample/secrets -p 9090:9090 registry.gitlab.com/asclepios-project/ample-editor:1.5.0 /ample/secrets
```



**Note:** If Jasypt password has not been set add `-njp` flag to prevent AMPLE from asking it. For example:

```sh
docker run -it -v secrets_vol:/ample/secrets -p 9090:9090 registry.gitlab.com/asclepios-project/ample-editor:1.5.0 /ample/secrets -njp
```



##### Cleanup secrets

If not needed anymore secrets should be removed.

Remove secrets volume:

```sh
docker volume rm secrets_vol
```

Removed from host directory. **Caution:** Take care to not accidentally remove files not belonging to AMPLE that might also be stored in the same directory.

*Linux:*

```sh
rm -rf /path/to/secure/location/*
```

*Windows:*

```
del /q C:\path\to\secure\location\*
```



------

### Run AMPLE with Docker Compose

##### *For Testing or Home usage*

An improved option to deploying an individual AMPLE Docker container is to use Docker Compose. The `docker-compose.yml` file included with [source code](https://gitlab.com/asclepios-project/ample-editor) is provided as an example.

##### Initialize secrets

The first time docker-compose is deployed, secrets initialization must be performed. The sample `docker-compose.yml` file stores secrets in `ample_conf` volume mapped onto `/ample/config` directory in container.

```sh
docker-compose run -d -e SECRETS_AUTO_INIT=true ample-editor
```

**Note:** Use `docker-compose down` to stop container (without `-v` switch, in order to prevent removing volumes). If the volume is accidentally removed the initialization must be repeated.



An alternative to setting `-e SECRETS_AUTO_INIT=true` is to edit *docker-compose.yml* file and uncomment line `SECRETS_AUTO_INIT: "true"`. Then deploy docker-compose normally:

```sh
docker-compose up -d
```

After the first run (and the secrets initialization) you must revert the change to avoid overwriting secrets with new values. I.e. comment out the line `#SECRETS_AUTO_INIT: "true"`.



##### Deploy AMPLE docker-compose.yml

The next time, AMPLE docker-compose can be normally deployed without setting any additional parameters.

```sh
docker-compose up -d
```



------

### Deploy AMPLE service in Docker Swarm

##### *Secure solution*

Docker Swarm provides a more secure solution to using AMPLE with Docker or Docker Compose or using it without Docker. Swarm provides Docker Secrets service, which can be used for secure storage of secrets. A container must specifically be given access to a secret. In this case, a secret can be accessed as a file located in `/run/secrets` directory. This is an in-memory location and is not stored along with container's data.

AMPLE secrets need to be initialized and imported into Docker Secrets service. **Caution:** When a swarm is terminated all stored secrets are discarded.



##### Initialize secrets

The Docker container will be used to initialize secrets as shown in the [Run AMPLE with Docker](#run-ample-with-docker) section. Secrets will be temporarily stored at a location mapped onto a host system directory.

*Linux:*

```sh
docker run --rm -it -v path/to/temp/directory:/run/secrets registry.gitlab.com/asclepios-project/ample-editor:1.5.0 init-secrets /run/secrets
```

*Windows:*

```
docker run --rm -it -v "C:\path\to\temp\diretory":/run/secrets registry.gitlab.com/asclepios-project/ample-editor:1.5.0 init-secrets /run/secrets
```



**Note:** You can add certificates of other services (like ABAC PDP/PAP server) into AMPLE trust store. For example:

```sh
keytool -import -noprompt -keystore /path/to/temp/directory/ample-editor-ts.p12 -file /path/to/pdp-server.crt -alias pdp-server
```



##### Import secrets into Docker Secrets

In order to use Docker Secrets a swarm must be created or join to an already existing one. For example:

```sh
docker swarm init
```



Next, the AMPLE secrets files can be imported.

*Linux:*

```sh
docker secret create ample-editor-secret.properties path/to/temp/directory/ample-editor-secret.properties
docker secret create ample-editor-ks.p12 path/to/temp/directory/ample-editor-ks.p12
docker secret create ample-editor-ts.p12 path/to/temp/directory/ample-editor-ts.p12
```

*Windows:*

```
docker secret create ample-editor-secret.properties path\to\temp\directory\ample-editor-secret.properties
docker secret create ample-editor-ks.p12 path\to\temp\directory\ample-editor-ks.p12
docker secret create ample-editor-ts.p12 path\to\temp\directory\ample-editor-ts.p12
```



Secrets must be removed from temporary (host) directory after import. **Caution:** Take care to not accidentally remove files not belonging to AMPLE, which might also be stored in the same directory.

*Linux:*

```sh
rm -rf /path/to/temp/directory/*
```

*Windows:*

```
del /q C:\path\to\temp\directory\*
```



##### Deploy AMPLE service

Deploying AMPLE as swarm service will launch an instance of AMPLE server. Additional instances can be started at any time. The needed secrets from Docker Secrets service must be specified in command line. For example:

```sh
docker service create --name ample --secret ample-editor-secret.properties --secret ample-editor-ks.p12 --secret ample-editor-ts.p12 -p 9090:9090 registry.gitlab.com/asclepios-project/ample-editor:1.5.0 -- -njp
```



In order to view the logs of the AMPLE service the following commands can be used:

```sh
docker ps
docker logs -f <CONTAINER-ID>
```

or (in Linux only)

```sh
docker logs -f '$(docker ps -lq)'
```



Stopping AMPLE service:

```sh
docker service rm ample
```



**Caution:** Terminating swarm will cause all stored secrets to be discarded.



------

### Deploy AMPLE stack in Docker Swarm

##### *Secure solution*

An improved option to deploying an individual AMPLE service in Docker swarm, is to use a Docker Compose file to deploy an AMPLE stack in swarm. The `docker-compose-swarm.yml` file included with [source code](https://gitlab.com/asclepios-project/ample-editor) is provided as an example.

##### Initialize secrets

Secrets initialization and import into Docker Secrets service process is identical to the one presented in [Deploy AMPLE service in Docker Swarm](#deploy-ample-service-in-docker-swarm) section.



##### Deploy AMPLE stack

An AMPLE service instance will be started with the stack deployment. The needed secrets from Docker Secrets service are specified in the `docker-compose-swarm.yml` file.

```sh
docker stack deploy -c docker-compose-swarm.yml ample
```



In order to view the logs of the started AMPLE service the next commands can be used:

```sh
docker ps
docker logs -f <CONTAINER-ID>
```

or (in Linux only)

```sh
docker logs -f '$(docker ps -lq)'
```



Stopping AMPLE stack (and service):

```sh
docker stack rm ample
```



**Caution:** Terminating swarm will cause all stored secrets to be discarded.



------

### Swagger Documentation

Run AMPLE and check the following links with a browser:

```
https://<AMPLE_IP_ADDRESS>:9090/v2/api-docs
https://<AMPLE_IP_ADDRESS>:9090/swagger-ui.html
```



------
