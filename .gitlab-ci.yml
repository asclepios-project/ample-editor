#
# Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

stages:
  - build
  - deploy
  - release

services:
  - docker:dind

variables:
  DOCKER_HOST: tcp://docker:2375
  DOCKER_DRIVER: overlay
  SPRING_PROFILES_ACTIVE: gitlab-ci
  # This will suppress any download for dependencies and plugins or upload messages which would clutter the console log.
  # `showDateTime` will show the passed time in milliseconds. You need to specify `--batch-mode` to make this work.
  MAVEN_OPTS: "-Dmaven.repo.local=.m2/repository -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true"
  # As of Maven 3.3.0 instead of this you may define these options in `.mvn/maven.config` so the same config is used
  # when running from the command line.
  # `installAtEnd` and `deployAtEnd`are only effective with recent version of the corresponding plugins.
  MAVEN_CLI_OPTS: "--batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true"
  #
  RELEASE_BINARY: ample-editor-${CI_COMMIT_TAG}.tar.gz
  PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/ample-editor/${CI_COMMIT_TAG}"
  DOCKER_REGISTRY_URL: "${CI_PROJECT_URL}/container_registry/"

# Cache downloaded dependencies and plugins between builds.
cache:
  paths:
    - target/ample-*.jar
    - target/docker-context/**
    - src/main/webapp/**
    - config/application.yml
    - LICENSE*
    - README*
    - bin/*

.default:
  rules:
    - if: $CI_COMMIT_TAG

build-jar:
  extends: .default
  stage: build
  image: maven:3.6-jdk-8
  script:
    - 'mvn $MAVEN_CLI_OPTS package -Dskiptests -P with-docker'

build-docker-image:
  extends: .default
  stage: deploy
  image: docker:latest
  script:
    - cd target/docker-context
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build -t $CI_REGISTRY/$CI_PROJECT_PATH:latest -t $CI_REGISTRY/$CI_PROJECT_PATH:$CI_COMMIT_TAG .
    - docker push $CI_REGISTRY/$CI_PROJECT_PATH:latest
    - docker push $CI_REGISTRY/$CI_PROJECT_PATH:$CI_COMMIT_TAG

build-and-upload-binary:
  extends: .default
  stage: deploy
  image: curlimages/curl:latest
  script:
    - 'echo CI_COMMIT_TAG: ${CI_COMMIT_TAG}'
    - 'echo PACKAGE_REGISTRY_URL: ${PACKAGE_REGISTRY_URL}'
    - 'tar -czf ${RELEASE_BINARY} target/ample-*.jar src/main/webapp/** config/application.yml LICENSE* README* bin/*'
    - 'ls -l ample*'
    - 'echo URL: ${PACKAGE_REGISTRY_URL}/${RELEASE_BINARY}'
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file ${RELEASE_BINARY} ${PACKAGE_REGISTRY_URL}/${RELEASE_BINARY}

create-release:
  extends: .default
  needs:
    - build-and-upload-binary
  # Caution, as of 2021-02-02 these assets links require a login, see:
  # https://gitlab.com/gitlab-org/gitlab/-/issues/299384
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  script:
    - |
      release-cli create --name "Release $CI_COMMIT_TAG" --tag-name $CI_COMMIT_TAG \
        --description "Release Docker image: ${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}" \
        --assets-link "{\"name\":\"Docker images\",\"url\":\"${DOCKER_REGISTRY_URL}\"}" \
        --assets-link "{\"name\":\"${RELEASE_BINARY}\",\"url\":\"${PACKAGE_REGISTRY_URL}/${RELEASE_BINARY}\"}"
