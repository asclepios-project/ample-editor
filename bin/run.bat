::
:: Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
::
:: Licensed under the Apache License, Version 2.0 (the "License");
:: you may not use this file except in compliance with the License.
:: You may obtain a copy of the License at
::
::      http://www.apache.org/licenses/LICENSE-2.0
::
:: Unless required by applicable law or agreed to in writing, software
:: distributed under the License is distributed on an "AS IS" BASIS,
:: WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
:: See the License for the specific language governing permissions and
:: limitations under the License.
::
@echo off
setlocal
set olddir=%cd%
cd %~dp0..

set OUT=CON
IF "%1" == "-" set OUT=NUL
set NJP=
IF "%1" == "-" set NJP=-njp

::set AMPLE_KEYSTORE_FILE=config/ample.p12
::set AMPLE_TRUSTSTORE_FILE=config/ample.p12

IF "%SECRETS_FILE%x" == "x" set SECRETS_FILE=config/ample-editor-secret.properties
set SB_CONFIG=--spring.config.location=file:%SECRETS_FILE%,file:config/

IF "%AMPLE_JAR%x" == "x" set AMPLE_JAR=%~dp0../target/ample-editor-1.5.0.jar

::set /p "JASYPT_ENCRYPTOR_PASSWORD=Jasypt password: " || set "JASYPT_ENCRYPTOR_PASSWORD=ample"
java -jar %AMPLE_JAR% %SB_CONFIG% %NJP% %* >%OUT% 2>%OUT%

cd %olddir%
endlocal