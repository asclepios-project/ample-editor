#!/bin/sh
#
# Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
olddir=`pwd`
cd $(cd "$(dirname "$0")"/.. && pwd)

#AMPLE_KEYSTORE_FILE=config/ample.p12
#AMPLE_TRUSTSTORE_FILE=config/ample.p12
#export AMPLE_KEYSTORE_FILE AMPLE_TRUSTSTORE_FILE

[ "$SECRETS_FILE" == "" ] && SECRETS_FILE=/run/secrets/ample-editor-secret.properties
[ ! -f "$SECRETS_FILE" ] && SECRETS_FILE=config/ample-editor-secret.properties
SB_CONFIG=--spring.config.location=file:$SECRETS_FILE,file:config/

EGD=-Djava.security.egd=file:/dev/./urandom
[ "$AMPLE_JAR" == "" ] && AMPLE_JAR=./target/ample-editor-1.5.0.jar
if [ "$1" != "-" ]; then
  java $EGD -jar ${AMPLE_JAR} ${SB_CONFIG} $*
else
  java $EGD -jar ${AMPLE_JAR} ${SB_CONFIG} -njp $* &> /dev/null &
fi
cd $olddir