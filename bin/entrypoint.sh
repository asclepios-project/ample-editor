#!/bin/sh
#
# Copyright 2019-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
olddir=`pwd`
#cd $( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )
cd $(cd "$(dirname "$0")"/.. && pwd)

export AMPLE_JAR=`pwd`/ample-editor.jar

if [ "$1" == "init-secrets" ]; then
  shift
  ./bin/init-secrets.sh $*
elif [ "$1" == "auto-init" ] || [ "$1" == "--auto-init" ]; then
  shift
  ./bin/run.sh $* --auto-init
elif [ "$1" == "init-secrets-and-run" ]; then
  shift
  [ "$1" != "" ] && [ "$1" != "--" ] && SECRETS_FILE=$1/ample-editor-secret.properties && shift 1
  [ "$SECRETS_FILE" == "" ] && SECRETS_FILE=/run/secrets/ample-editor-secret.properties
  [ "$1" == "--" ] && shift 1
  SECRETS_DIR=`dirname $SECRETS_FILE`
  mkdir -p "$SECRETS_DIR"
  export SECRETS_FILE
  ./bin/init-secrets.sh $SECRETS_DIR
  ./bin/run.sh $*
else
  [ "$SECRETS_AUTO_INIT" == "true" ] && ARG_AI=--auto-init
  [ "$1" != "" ] && [ "$1" != "--" ] && SECRETS_FILE=$1/ample-editor-secret.properties && shift 1
  [ "$1" == "--" ] && shift 1
  [ "$SECRETS_FILE" == "" ] && SECRETS_FILE=/run/secrets/ample-editor-secret.properties
  SECRETS_DIR=`dirname $SECRETS_FILE`
  mkdir -p "$SECRETS_DIR"
  export SECRETS_FILE
  ./bin/run.sh $* $ARG_AI
fi

cd $olddir